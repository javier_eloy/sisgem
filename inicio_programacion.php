<?php 
//RUTINA PARA FILTRAR LOS EQUIPOS SEG�N EL DEPARTAMENTO Y LOCALIDAD AL QUE PERTENECE EL USUARIO
    
// CALCULA EL NUMERO DE LA SEMANA DEL A�O SEG�N LA FECHA
$anno          = date('Y');
$semana_actual = strftime("%W", mktime(0,0,0,date('m'),date('d'),date('Y')));

if($semana_actual == '00')
{
    $semana_actual = 53;
    //$anno = $anno - 1;
    $anno = $anno--;
}

$semana_actual_anno = $semana_actual."-".$anno;
$fecha_actual       = date('d').'-'.date('m').'-'.date('Y');
$fecha_actual_c     = strtotime($fecha_actual);
$FechaActualBD=date('Y').'-'.date('m').'-'.date('d');


//----- MODULO PARA ESTABLECER ACCESOS A LOS REEMPLAZOS ------

// INCORPORACION DE LOS REEMPLAZOS A LOS MODULOS DE ACCESO RESPECTIVOS
mysql_query("INSERT INTO J034T_MENU_USUARIO (co_bloque,
                                            co_usuario,
                                            nu_pos_menu_user)
                                            SELECT b.co_bloque,
                                                   s.co_usuario_suplente,
                                                   '1'
                                            FROM I042T_SUPLENCIA s
                                            INNER JOIN I004T_USUARIO u ON (s.co_usuario_suplente = u.co_usuario AND u.co_zona=".$UsuariConect[13]."  AND u.co_gerencia=".$UsuariConect[8].")
                                            INNER JOIN J034T_MENU_USUARIO b ON (s.co_usuario_ausente = b.co_usuario AND b.nu_pos_menu_user = '0')
                                            INNER JOIN I004T_USUARIO u2 ON (s.co_usuario_ausente = u2.co_usuario AND u2.in_disponible = '0')
                                            WHERE  s.fe_inicio <= '".$fecha_actual_c."' AND s.fe_fin >='".$fecha_actual_c."'",$link);

// EDITO LA DISPONIBILIDAD DE USUARIO QUE SE AUSENTA:
mysql_query("UPDATE I004T_USUARIO u 
            INNER JOIN I042T_SUPLENCIA s2 
            ON (u.co_usuario = s2.co_usuario_ausente AND (s2.fe_inicio <= '".$fecha_actual_c."' AND s2.fe_fin >= '".$fecha_actual_c."')) 
            SET u.in_disponible = '1'",$link);



//----- MODULO PARA RESTABLECER A LOS USUARIOS DESPUES DE LAS AUSENCIAS ------

// SE EDITA EL VALOR DISPONIBLE EN EL USUARIO AL TERMINARSE LA AUSENCIA
mysql_query("UPDATE I004T_USUARIO u 
            INNER JOIN I042T_SUPLENCIA s2 
            ON (u.co_usuario = s2.co_usuario_ausente AND (s2.fe_inicio >'".$fecha_actual_c."' OR s2.fe_fin < '".$fecha_actual_c."')) 
            SET u.in_disponible = '0' 
            WHERE u.in_disponible = '1'",$link);

// SE BORRAN LOS REGISTRO DE SUPLENCIA Y LOS BLOQUE PROVISIONALES DE ACCESO ASOCIADOS AL REEMPLAZO CUANDO PASA LA AUSENCIA
mysql_query("DELETE FROM b
            USING I042T_SUPLENCIA s
            INNER JOIN J034T_MENU_USUARIO b ON (s.co_usuario_suplente = b.co_usuario AND b.nu_pos_menu_user'1') 
            WHERE s.fecha_inicio > '".$fecha_actual_c."' OR s.fecha_final < '".$fecha_actual_c."'",$link);

// SE BUSCA SI EL USUARIO ESTA REALIZANDO ALGUNA SUPLENCIA
$SuplenUsuariConect=mysql_query("SELECT u2.co_gerencia 	 	AS ing_CodigoGerencia, 
                                        u2.co_superintendencia  AS ing_CodigoSuperi, 
                                        u2.co_departamento      AS ing_CodigoDepart,
                                        u2.co_unidad 	 	AS ing_CodigoUnidad, 
                                        u2.co_cargo 	  	AS ing_CodigoCargo, 
                                        u2.co_zona 		AS ing_CodigoZona,
                                        c.co_tipo_cargo
                                FROM I042T_SUPLENCIA s
                                INNER JOIN I004T_USUARIO u2 ON (s.co_usuario_ausente = u.co_usuario)
                                INNER JOIN I005T_CARGO c ON (u.co_cargo = c.co_usuario)
                                WHERE s.fe_inicio <= '".$fecha_actual_c."' AND s.fe_final >= '".$fecha_actual_c."' AND s.co_usuario_suplente = '".$UsuariConect[0]."'", $link);   
$VectorSuplenUsuariConect=mysql_fetch_array($SuplenUsuariConect);

//if($VectorSuplenUsuariConect['CodigoZona']!="")
if($VectorSuplenUsuariConect['ing_CodigoZona'] != "")    
{
    // SE DEFINE LAS CONDICIONES PARA LA ORGANIZACI�N DE LA SUPLENCIA:
    $condicion[0] = "cod_gerencia='".$VectorSuplenUsuariConect['ing_CodigoGerencia']."'";
    $condicion[1] = "cod_super='".$VectorSuplenUsuariConect['ing_CodigoSuperi']."'";
    $condicion[2] = "cod_departamento='".$VectorSuplenUsuariConect['ing_CodigoDepart']."'";
    $condicion[3] = "cod_unidad='".$VectorSuplenUsuariConect['ing_CodigoUnidad']."'";
    $condicion[4] = "cod_zona='".$VectorSuplenUsuariConect['ing_CodigoZona']."'";
    
    // CONDICION DE COMPARACION DE LOS query PARA INCORPORAR LA ORGANIZACION DEL QUE SE REEMPLAZARON 
    $OrganiSuplenTotal = 'OR ('.implode(" AND ",$condicion).')';
}
  
// SE VERIFICA LOS PLANES QUE ESTAN PENDIENTES:
$planes= mysql_query("SELECT co_plan,
                             co_estatus 
                      FROM I037T_PLAN 
                      WHERE fe_proxima_plan < '".$fecha_actual_c."' 
                      AND co_estatus != '2'", $link);

while($rplanes = mysql_fetch_array($planes))
{
    // SI LA ACTIVIDAD YA FUE ASIGNADA SE VERIFICA EL ESTATUS EN LA TABLA DE ACTIVIDAD ASIGNADA
    if($rplanes['CO_ESTATUS'] == '1')
    {
        $actividades= mysql_query("SELECT co_actividad 
                                   FROM I022T_ACTIVIDAD 
                                   WHERE co_plan = '".$rplanes['CO_PLAN']."' 
                                   AND nu_porcentaje_avance = '0' 
                                   AND co_estatus == '1'", $link);

        while($ractividades=mysql_fetch_array($actividades))
        {
            mysql_query("UPDATE I022T_ACTIVIDAD SET co_estatus ='2' 
                        WHERE co_actividad ='".$ractividades['CO_ACTIVIDAD']."'",$link);
        }
    }
    elseif($rplanes['CO_ESTATUS']=='0')
    {
        // CAMBIO EL ESTATUS DE LA ACTIVIDAD SI LA FECHA DEL PLAN PAS� LA FECHA ACTUAL
        mysql_query("UPDATE I037T_PLAN SET co_estatus ='2' 
                     WHERE co_plan ='".$rplanes['CO_PLAN']."'",$link);
    }
}    
    


// FUNCI�N QUE PERMITE VERIFICAR SI LA SEMANA DE PLANIFICACI�N YA PAS� CON RESPECTO A LA SEMANA ACTUAL
/*function validaSemana($semana_actual_anno,$sem_proximo_ano)
{
    list($sem,$ano) = explode("-",$sem_proximo_ano);
    list($sem2,$ano2) = explode("-",$semana_actual_anno);
    if ($ano<$ano2){
        return true;
    }elseif($ano==$ano2 AND ((int) $sem)<((int) $sem2)){
        return true;
    }
	else return false;
}*/

// VERIFICAR SI LA ACTIVIDAD PASA A EST�TUS DE PENDIENTE

    //  VERIFICO QUE TIPO DE ACTIVIDAD ES PARA VER EN QUE TABLA SE REALIZA LA CONSULTA:

?>