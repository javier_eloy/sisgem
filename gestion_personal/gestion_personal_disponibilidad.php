<?php
$msg = "";
$co_cargo_plan=$RecordUsuario["usuario_co_cargo_completo_plan"];

$objGestionPersonal = new gestion_personal();
if (isset($_REQUEST['accion']))
    switch ($_REQUEST['accion']) {
        case "Eliminar" :
            $deleted = $objGestionPersonal->EliminarPersonalDisponibilidad($_GET['id'], $fecha_actual_c);
            if ($deleted)
                $msg = MSG_DATOS_ELIMINADOS;
            else
                $message = MSG_ERROR_TRANSACCION;
            break;
    }
?>

<!-- script para mascaras -->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="aplicacion/cliente/html_tablas_gestionpersonal.js"></script>
<script type="text/javascript" src="publico/js/prototype.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript"> 
    var jq=jQuery.noConflict();

    Shadowbox.init({
        language: "es",
        modal:true,
        players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv', 'php'],
        overlayColor: "#000",
        overlayOpacity: "0.5"}); 

    jq(document).ready(function() {
        var buscardor = jq('#busqueda');
        var texto_informativo = jq('#texto_informativo');
        var span = jq('<font face="Arial" size="1" color="red"><span id="informacion"></span></font>').insertAfter(texto_informativo);
        var informar = "Puede buscar al trabajador que se va ausentar por indicador o cedula (Ej: 15669464):";
       
        // FUNCI�N QUE PERMITE INFORMAR ACERCA DE LO QUE SE PUEDE ENCONTRAR EN LA BUSQUEDA:
        buscardor.focus(function(){
            span.text(informar);
        });        
        buscardor.blur(function(){
            span.text("");
        });
    });
    
   function OpenShadow(sPath,pHeight,pWidth) {
        if(typeof(pHeight)==='undefined') pHeight=501;
        if(typeof(pWidth)==='undefined') pWidth= 801;
        
	Shadowbox.open({
	        content:    sPath,
	        player:     "iframe",
	        height:     pHeight,
		width:      pWidth
	});
    }
    
    function initBindLocal() {
       
    }
</script>    
<!-- script PARA BUSCADOR -->
<table>
    <tr>
        <td><a href='cpanel.php?sistema=23' class="titulomenu">Grafico</a></td>
        <td><a href='cpanel.php?sistema=24' class="titulomenu">Personal Disponible</a></td>
        <td><a href='cpanel.php?sistema=25' class="titulomodulo">Gesti&oacute;n del Personal</a></td>
    </tr>
</table>
<div class="titulomodulo">Gestion del Personal (ausencia del personal)</div>

<table class="tablaformulario">
    <tbody>
        <tr id="texto_informativo">
            <td>
                <label class="etiqueta" for="id_busqueda">Buscar: </label>
                <input name="busqueda" class="texto" type="text" id="busqueda" value="" > 
                <input class="boton"  type="button" name="accion" value="Consultar" onclick="return SelecUsuario('resultado_busqueda','loading',jQuery('#busqueda').val(),'<?php echo $co_cargo_plan;?>');">
            </td>				  
        </tr>
        <tr>
            <td>
                <div id="loading" ></div>
                <div id="resultado_busqueda" ></div>
            </td>
        </tr>
    </tbody>
</table>
<div class="titulodivision">Registro Cargados de Trabajadores ausentes en la fecha actual:</div>	
<table id="listaSaldos" class="tabla">
    <thead>
        <tr>
            <th scope="col">Ausente</th>
            <th scope="col">Periodo de Ausencia</th>
            <th scope="col">Detalle</th>
            <th scope="col">Clase</th>
            <th scope="col">Reemplazo</th>
            <th scope="col">Opci&oacute;n</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $rsPersonal = $objGestionPersonal->SelecPersonal($co_cargo_plan);
        while (!$rsPersonal->EOF) {
            echo "<tr>
             <td>", $rsPersonal->fields["NB_INDICADOR_AUSENTE"], "</td>
             <td>Del <strong>", date_format(new DateTime($rsPersonal->fields['FE_INICIO']), "d-m-Y"), "</strong> al <strong>", date_format(new DateTime($rsPersonal->fields['FE_FIN']), "d-m-Y"), "</strong></td>
             <td>", $rsPersonal->fields["TX_DESCRIPCION"], "</td>                    
             <td align=\"center\">", $rsPersonal->fields["TX_SIGLAS_CLASE"], "</td>                    
             <td>", $rsPersonal->fields["NB_INDICADOR_SUPLENTE"];

            echo "</td>
             <td>
             <a href='cpanel.php?sistema=25&update=true&id=" . $rsPersonal->fields["CO_SUPLENCIA"] . "'>Editar</a> /
             <a href='cpanel.php?sistema=25&delete=true&id=" . $rsPersonal->fields["CO_SUPLENCIA"] . "'>Eliminar</a>
             </td>
             </tr>";
            $rsPersonal->MoveNext();
        }
        ?>   
    </tbody>
</table>