<?php
include ("../aplicacion/configuracion/aut_lib.inc.php");
header('Content-Type: text/html; charset=UTF-8');
$objGestionPersonal = new gestion_personal();

$co_suplencia = "";
$co_usuario = "";
$co_cargo = "";

if (isset($_GET['co_suplencia'])) { //* Edita
   $co_suplencia = $_GET['co_suplencia'];
   $rsSuplencia = $objGestionPersonal->SelecSuplencia($co_suplencia);
} else { //* Inserta
   $co_usuario = $_GET['co_usuario'];
   $rsSuplencia = $objGestionPersonal->SelecDatosPersonal($co_usuario);
}

if (isset($_GET['co_cargo']))
   $co_cargo = $_GET['co_cargo'];

if (isset($_REQUEST['accion']))
   switch ($_REQUEST['accion']) {
      case "Guardar":
         $inserted = $objGestionPersonal->InsertarPersonalDisponibilidad($_POST['cod_ausente'], $_POST['descripcion'], $_POST['cod_suplente'], $_POST['cod_clases'], $fechaInicio_c, $fechaFinal_c);
         if ($inserted)
            $$msg = MSG_DATOS_REGISTRADOS;
         else
            $msg = MSG_ERROR_TRANSACCION;
         break;
      case "Editar":
         $updated = $objGestionPersonal->UpdatePersonalDisponibilidad($_POST['id'], $_POST['cod_suplente'], $fechaInicio_c, $fecha_actual_c, $fechaFinal_c, $_POST['cod_ausente'], $_POST['descripcion'], $_POST['cod_clases']);
         if ($updated)
            $msg = MSG_DATOS_EDITADOS;
         else
            $msg = MSG_ERROR_TRANSACCION;
         break;
   }
?>
<html>
   <head>
      <link type="text/css" rel="stylesheet" href="../publico/js/Archivos/tools.css">
      <link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">
      <link type="text/css" rel="stylesheet" href="../publico/estilos/jquery-ui-1.10.3.custom.min.css" >
      <script type="text/javascript" src="../publico/js/jquery-1.7.2.js"></script>
      <script type="text/javascript" src="../publico/js/jquery-ui.js"></script>
      <script type="text/javascript" src="../publico/js/jquery.ui-1.10.3.datepicker-es.js"></script>
      <script type="text/javascript" src="../aplicacion/cliente/html_tablas_gestionpersonal.js"></script>
      <script type="text/javascript" src="../publico/js/prototype.js"></script>
      <script type="text/javascript">
         var jq = jQuery.noConflict();
         jq(document).ready(function() {
            jq("#_fe_inicio").datepicker({
               showButtonPanel: true
            });
            jq("#_fe_fin").datepicker({
               showButtonPanel: true
            });
        
            jq("#_fe_inicio").change(function()
            {
               jq("#_fe_fin").datepicker("option","minDate",jq("#_fe_inicio").datepicker('getDate'));
            });
            jq("#_fe_fin").change(function()
            {
               jq("#_fe_inicio").datepicker("option","maxDate",jq("#_fe_fin").datepicker('getDate'));
            });
         });
         function validar(){
        
         }
    
         function setIndicadorBuscado(){
            jq("#usuario_reemplazo").val(jq("#indicador_usuario_reemplazo").val());
         }

         function setCheckedValue(newValue) {
            radioObj=jq('input[name=reemplazo]');
            if(!radioObj)
               return;
            var radioLength = radioObj.length;
            if(radioLength == undefined) {
               radioObj.checked = (radioObj.value == newValue.toString());
               return;
            }
            for(var i = 0; i < radioLength; i++) {
               radioObj[i].checked = false;
               if(radioObj[i].value == newValue.toString()) {
                  radioObj[i].checked = true;
               }
            }
         }

         function BuscarReemplazo() {
            SelecUsuarioReemplazo('nombre_usuario_reemplazo',jq('#usuario_reemplazo').val(),'<?php echo $co_cargo; ?>');
         }

         function LimpiarCampos(){
            jq("#nombre_usuario_reemplazo").html("&nbsp;");
            jq("#co_usuario_reemplazo").val("");
            jq("#indicador_usuario_reemplazo").val("");
            jq("#usuario_reemplazo").val("");
            jq("#disp_usuario_reemplazo").hide();


         }
         function ActualizarCampos(resultado) {
            jq("#nombre_usuario_reemplazo").html(resultado['NB_NOMBRE']+' ('+resultado['NB_CARGO']+')');
            jq("#co_usuario_reemplazo").val(resultado['CO_USUARIO']);
            jq("#indicador_usuario_reemplazo").val(resultado['NB_INDICADOR'])
            jq("#disp_usuario_reemplazo").html(resultado['NO_DISPONIBLE']);
            jq("#disp_usuario_reemplazo").show();
         }

      </script>
   </head>
   <body bgcolor="#ffffff">
      <form id="formularioLogin" method="post" action="ausencia.php?co_cargo=<?php echo $co_cargo ?>" >
         <input id="_co_suplencia" name="_co_suplencia" value="<?php echo $co_suplencia ?>" type="hidden">
         <input id="_co_usuario" name="_co_usuario" value="<?php echo $co_usuario ?>" type="hidden">
         <input id="co_usuario_reemplazo" type="hidden" value="">
         <input id="indicador_usuario_reemplazo" type="hidden" value="">

         <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="10" style="align:center;width:680;height:401;">
            <tbody>
               <tr>
                  <td height="401" valign="top" width="680">
                     <div class="titulomodulo">Registro de Ausencia:</div>
                     <table class="tablaformulario">
                        <tbody>
                           <tr>
                              <td colspan="3">
                                 <span class="etiqueta">Trabajador: <b><?php echo $rsSuplencia->fields['NB_NOMBRE_AUSENTE'] ?> (<?php echo $rsSuplencia->fields['NB_INDICADOR_AUSENTE'] ?>) </b></span><br>
                                 <span class="etiqueta">Cargo:<b><?php echo $rsSuplencia->fields['NB_CARGO'] ?></b> </span><br>
                                 <hr>
                              </td>

                           </tr>
                           <tr>
                              <td><span class="etiqueta">Dia de Inicio:</span></td>
                              <td><span class="etiqueta">Dia de Finalizacion:</span></td>
                              <td><span class="etiqueta">Tipo de Ausencia:</span></td>
                           </tr>
                           <tr>
                              <td><input id="_fe_inicio" name="_fe_inicio" class=":required  textofecha" type="text" readonly /></td>
                              <td><input id="_fe_fin" name="_fe_fin" class=":required  textofecha" type="text" readonly/></td>
                              <td>
                                 <?php
                                 $rsClase = $objGestionPersonal->SelecClase();
                                 $sHTML = "<select name='_co_ausencia' id='_co_ausencia' class=':required texto' style='width:300px'>";
                                 $sHTML.="<option value=''>Seleccione una opci&oacute;n...</option>";
                                 while (!$rsClase->EOF) {
                                    $sHTML.="<option value='" . $rsClase->fields['CO_CLASE'] . "'>" . $rsClase->fields['NB_CLASE'] . " (" . $rsClase->fields['TX_DESCRIPCION'] . ") </option>";
                                    $rsClase->MoveNext();
                                 }
                                 $sHTML.="</select>";
                                 echo $sHTML;
                                 ?>
                              </td>
                           </tr>
                           <tr>
                              <td><span class="etiqueta">Detalle de la Ausencia:</span></td>
                           </tr>
                           <tr>
                              <td colspan="3">
                                 <?php if ($rsSuplencia->fields['IN_REQUERIDO'] == 1) { ?>
                                    <div class="titulomodulo"> El trabajador ejerce un cargo requerido para reemplazo. Debe seleccionar su reemplazo.</div>
                                 <?php } else { ?>
                                    <input style='width:650px;' maxlength="200" name='tx_descripcion' class=":required texto" type="text" />
<?php } ?>
                              </td>
                           </tr>
                           <tr>
                              <td colspan="3"><br><input type="radio" name="reemplazo" id="reemplazo1" value="0"><span class="etiqueta">SIN REEMPLAZO </span></td>
                           </tr>
                           <tr>
                              <td colspan="2" nowrap>
                                 <input type="radio" name="reemplazo" id="reemplazo2" value="1"><span class="etiqueta">REEMPLAZAR POR:</span>
                                 <input id='usuario_reemplazo' class='texto_min' type='text' value='' size="20" maxlength="17" title="Escriba el Indicador o C.I. (Ej: 15669464) del reemplazo" onChange="javascript:this.value=this.value.toUpperCase();" onfocus="return setCheckedValue('1');" >
                                 <input id='boton_reemplazo' class='boton_min' type='button' value='Buscar' onclick="return BuscarReemplazo();">
                              </td>
                              <td><div id="nombre_usuario_reemplazo" class="titulomodulo">&nbsp;</div></td>
                           </tr>
                           <tr>
                              <td colspan="3"><div id="disp_usuario_reemplazo" class="resultado_ausencia" style="display:none"></div></td>
                           </tr>
                        </tbody>
                     </table>
                     <div class="grupobotones">
                        <input class="submit boton" name="accion" value="Guardar" type="button" onfocus="return setIndicadorBuscado();" />
                        <input class="boton" name="Volver" value="Cerrar" type="button" onclick="window.parent.Shadowbox.close();"/>
                     </div>
                  </td>
               </tr>
            </tbody>
         </table>
      </form>
   </body>
</html>