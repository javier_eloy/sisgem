<?php
$objGraficoPersonal = new gestion_personal();
$rsGrafico = $objGraficoPersonal->SelecGrafPersonal();
?>
<table>
    <tr>
        <td><a href='cpanel.php?sistema=23' class="titulomodulo">Grafico</a></td>
        <td><a href='cpanel.php?sistema=24' class="titulomenu">Personal Disponible</a></td>
        <td><a href='cpanel.php?sistema=25' class="titulomenu">Gesti&oacute;n del Personal</a></td>
    </tr>
</table>
<div class="titulomodulo">Gr&aacute;fico de disponibilidad de los Trabajadores</div>
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="excanvas.js"></script><![endif]-->
<link rel="stylesheet" type="text/css" media="all" href="publico/js/grafico/jquery.jqplot.css" >
<link rel="stylesheet" type="text/css" media="all" href="publico/js/grafico/examples/examples.css">
<script language="javascript" type="text/javascript" src="publico/js/grafico/jquery.js"></script>
<script type="text/javascript" src="publico/js/prototype.js"></script>
<script language="javascript" type="text/javascript" src="publico/js/grafico/jquery.jqplot.js"></script>
<script language="javascript" type="text/javascript" src="publico/js/grafico/plugins/jqplot.pieRenderer.js"></script>
<script class="include" language="javascript" type="text/javascript" src="publico/js/grafico/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="publico/js/grafico/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="publico/js/grafico/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="publico/js/grafico/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="publico/js/grafico/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="publico/js/grafico/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="publico/js/grafico/plugins/jqplot.pointLabels.min.js"></script>

<script type="text/javascript" src="aplicacion/cliente/html_tablas_gestionpersonal.js"></script>
<!-- END: load jqplot -->
<script type="text/javascript">
    var jq=jQuery.noConflict();
    jq(document).ready(function(){
        var data=[
            ['NO DISPONIBLE - <?php echo $rsGrafico->fields['NoDisp']; ?>',<?php echo $rsGrafico->fields['NoDisp']; ?>],
            ['DISPONIBLE - <?php echo $rsGrafico->fields['Disp']; ?>',<?php echo $rsGrafico->fields['Disp']; ?>]
        ];

        jQuery.jqplot.config.enablePlugins = true;
        plot7 = jQuery.jqplot('chart7', [data],
        {
            title: 'Relación Disponibilidad de Trabajadores',
            cursor: {
                show: true,
                tooltipLocation:'sw',
                zoom:true
            },
            seriesDefaults: {shadow: true,
                renderer: jQuery.jqplot.PieRenderer,
                rendererOptions: { showDataLabels: true } },
            legend: { show:true }
        }
    );    
    SelecUsuarioVaca();
});


</script>
<style type="text/css">
#chart_vacacion .jqplot-point-label {
  font-weight: bold;
  font-size:11px;
  color:white;
}
</style>

<div class="jqplot-target" id="chart7" style="display:block;width: 750px; height: 300px;"></div>
<div class="jqplot-target" id="chart_vacacion" style="displa:block;width: 750px; height: 300px;"></div>

