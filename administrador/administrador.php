<!-- REGISTRO DE ADMINISTRADOR -->
<?php

if($RecordUsuario["usuario_administrador"] != 1) 
{
    echo "<font style=\"color:red;font-size:13px;font-weight:bold;\">",MSG_ACCESO_ADMINISTRADOR,"</font>";
    exit;
}
$objAdmin= new administrador();
$msg="";
if(isset($_REQUEST['accion']))
switch ($_REQUEST['accion']) {
    case "Guardar":
       if($_POST['_tx_clave'] == $_POST['_tx_clave_valida']) {
        if ($objAdmin->InsertAdministrador($_POST['_nb_indicador'], $_POST['_tx_clave'], $_POST['_nb_nombre'], $_POST['_co_ubic_tecnica']))
            $msg = MSG_DATOS_REGISTRADOS;
        else
            $msg = MSG_ERROR_TRANSACCION;
       } else
           $msg="La clave y la reescritura de la clave son distintas, no se puede crear el administrador";
        break;
    case "Eliminar":
        if ($objAdmin->EliminarAdministrador($_REQUEST['co_usuario']))
            $msg = MSG_DATOS_ELIMINADOS;
        else
            $msg = MSG_ERROR_TRANSACCION;
        break;
}

$objInventario=new inventarioSAP();
$arrayUbicNivel1=$objInventario->SelecUbicacionTecnica("__");
$arraUbicNivel2=$objInventario->SelecUbicacionTecnica("____");
$arraUbicNivel3=$objInventario->SelecUbicacionTecnica("______");

$arrayUbic=array_merge($arrayUbicNivel1,$arraUbicNivel2,$arraUbicNivel3);

/* Ordena el Arreglo */
foreach ($arrayUbic as $key => $row) {
    $volume[$key]  = $row[$key]['TPLNR'];
}
array_multisort($volume,$arrayUbic);
/*fin - Ordenar */
?>
<!-- Librerias JSON -->        
<script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="publico/js/prototype.js"></script>
<!--Funcionalidades específicas en Ajax --->
<div class="titulomodulo">&nbsp;</div>	
<form name="formulario" id="formulario" method="POST"  action="cpanel.php?sistema=5" onsubmit="return validar(this);">
    <table class="tablaformulario">
        <tr>
            <td><span class="etiqueta">Indicador del Administrador</span></td>
            <td valign="bottom"><span class="etiqueta">Descripci&oacute;n del Administrador</span></td>
        </tr>
        <tr>
            <td><input name="_nb_indicador" id="_nb_indicador" class=":required texto_min" onChange="javascript:this.value=this.value.toUpperCase();" type="text" size="40" maxlength="20"></td>
            <td><input name="_nb_nombre" id="_nb_nombre" class=":required texto" onChange="javascript:this.value=this.value.toUpperCase();" type="text" size="40" maxlength="50"></td>
        </tr>
        <tr>
            <td><span class="etiqueta">Clave del Usuario Administrador</span></td>
            <td valign="bottom"><span class="etiqueta">Repita la Clave del Usuario Administrador</span></td>
        </tr>
        <tr>
            <td><input name="_tx_clave" id="_tx_clave" class=":required texto" type="password" size="40" maxlength="25"></td>            
            <td><input name="_tx_clave_valida" id="_tx_clave_valida" class=":required texto" type="password" size="40" maxlength="25"></td>            
        </tr>
        <tr>
            <td colspan="2"><span class="etiqueta">Acceso a Ubicación Tecnica:</span></td>
        </tr>
        <tr>
            <td colspan="2" >
                <select id="_co_ubic_tecnica" name="_co_ubic_tecnica" style="width:400px">
                    <?php
                    $countUbic=count($arrayUbic);
                     for($i=1;$i<$countUbic;$i++) {
                         echo "<option value='",$arrayUbic[$i]['TPLNR'],"'> (",$arrayUbic[$i]['TPLNR'],") ",utf8_encode($arrayUbic[$i]['PLTXU']) ,"</option>";
                     }
                    ?>
                </select>
            </td>
        </tr>

    </table>
    <div id="grupobtn" name="grupobtn" class="grupobotones">
        <input class="boton" id="accion" name="accion"  value="Guardar" type="submit">	
        <input class="boton"  value="Limpiar" type="reset" >
    </div>
    <?php if (strlen($msg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$msg**</span>"; ?>        
</form>

<div class="titulodivision">Registro Cargados:</div>	
<table id="listaSaldos" class="tabla">
    <thead>
        <tr>
            <th scope="col" style="width:100px">Indicador</th>
            <th scope="col" style="width:200px" >Descripci&oacute;n del Administrador</th>
            <th scope="col">Ubicaci&oacute;n T&eacute;cnica</th>
            <th scope="col" style="width:90px">Opci&oacute;n</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $rsAdmin = $objAdmin->SelecAdministrador($RecordUsuario["usuario_id"]);
        while (!$rsAdmin->EOF) {
            ?>		
            <tr>
                <td><?php echo $rsAdmin->fields["NB_INDICADOR"]; ?></td>
                <td><?php echo $rsAdmin->fields["NB_NOMBRE"]; ?></td>
                <td><?php echo $rsAdmin->fields["CO_UBIC_TECNICA_ADMIN"]; ?></td>
                <td>
                    <a <?php echo "href='cpanel.php?sistema=5&accion=Eliminar&co_usuario=",$rsAdmin->fields["CO_USUARIO"],"'"; ?> onclick="return confirm('Esta seguro de eliminar este usuario');">Eliminar</a>
                </td>
            </tr>
            <?php
            $rsAdmin->MoveNext();
        }
        ?>
    </tbody>
</table>
<script type="text/javascript">
    function validar(frm)
    {   
        if(frm._nb_nombre.value.length < 5) 
        {   
            alert("La descripción del administrador debe contener al menos 5 caracteres");
            return false;           
        } else if(frm._nb_indicador.value.length < 5) {
            alert("El INDICADOR del administrador debe contener al menos 5 caracteres");
            return false;
        } else if(frm._tx_clave.value.length < 6) {
            alert("La clave debe ser mayor o igual a 6 caracteres alfanumericos");
            return false;
        }else if(frm._tx_clave.value != frm._tx_clave_valida.value) {
            alert("Las claves y la validación no coinciden, reescriba las claves");
            return false;
        }
        return true;
    }
   
</script>
