<?php 

if(!empty($_POST)){
    if ($_POST['accion'] == "Guardar") //si action tiene como valor UPLOAD haga algo (el value de este hidden es es UPLOAD iniciado desde el value
        {   

            $archivo = $_FILES['rutArchivo']['name']; //captura el nombre del archivo
            $tipo = $_FILES['rutArchivo']['type']; //captura el tipo de archivo 
            $prefijo = substr(md5(uniqid(rand())),0,6);
            $destino = FUENTES_IMPORT."/".$prefijo.$archivo; //lugar donde se copiara el archivo

            if (move_uploaded_file($_FILES['rutArchivo']['tmp_name'],$destino)) //si dese copiar la variable rutArchivo (archivo).nombreTemporal a destino (bak_.archivo) (si se ha dejado copiar)
                {
                    $archivo = FUENTES_IMPORT."/".$prefijo.$_FILES['rutArchivo']['name']; 
                    $msg = "Archivo Cargado Con Exito \n";
                }
            else
                {
                    $msg = "Error Al Cargar el Archivo ";
                }
        }

    if ($_POST['accion'] == "Procesar") //si action tiene como valor UPLOAD haga algo (el value de este hidden es es UPLOAD iniciado desde el value
        {   

            $archivo = $_FILES['rutArchivo']['name']; //captura el nombre del archivo
            $tipo = $_FILES['rutArchivo']['type']; //captura el tipo de archivo 
            $prefijo = substr(md5(uniqid(rand())),0,6);
            $destino = FUENTES_IMPORT."/".$prefijo.$archivo; //lugar donde se copiara el archivo

            if (move_uploaded_file($_FILES['rutArchivo']['tmp_name'],$destino)) //si dese copiar la variable rutArchivo (archivo).nombreTemporal a destino (bak_.archivo) (si se ha dejado copiar)
                {
                    $archivo = FUENTES_IMPORT."/".$prefijo.$_FILES['rutArchivo']['name']; 
                    $msg = "Archivo Cargado Con Exito \n";

                    $carga_masiva = new CargaMasiva(); //i created a new object
                  
                    // procesar archivo cargado
                    $inputFileName = $archivo;               
                    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

                    //$Inventario = new inventarioSAP();

                    
                    $VA_TIPO_PLAN = $_POST['seleccion'];

                    for ($i=1;$i<=count($sheetData);$i++)
                    {

                        $VA_DETALLE = $sheetData[$i]['A'];
                        $CO_HOJARUTA = $sheetData[$i]['B'];
                        $NU_HOJARUTA = $sheetData[$i]['C'];
                        $FE_PROXIMA_PLAN = $sheetData[$i]['D'];
                        list($year, $month, $day ) = explode('-', $FE_PROXIMA_PLAN);

                        $IN_CDC = $sheetData[$i]['E'];
                        $IN_CONTRATO_SERV = $sheetData[$i]['F'];
                        $IN_ESFUERZO_PROP = $sheetData[$i]['G'];

                        if($IN_CDC=='X'){ $IN_CDC=1; }else{ $IN_CDC=0; }
                        if($IN_CONTRATO_SERV=='X'){ $IN_CONTRATO_SERV=1; }else{ $IN_CONTRATO_SERV=0; }
                        if($IN_ESFUERZO_PROP=='X'){ $IN_ESFUERZO_PROP=1; }else{ $IN_ESFUERZO_PROP=0; }
                        

                        if ($VA_TIPO_PLAN=='E')
                        {

                            //$SAProw = $Inventario->SelecDetalleEquipoSAP(($co_equipo);
                            if (count($SAProw)>0){
                                $CO_VALIDACION = 0;
                            }else{
                                $CO_VALIDACION = 1;
                            }

                            if(checkdate( $day, $month, $year)===false)
                            {
                                // fecha incorrecta formato 0000-00-00
                                $CO_VALIDACION = 2;
                                $FE_PROXIMA_PLAN = '0000-00-00';
                            }

                            ///$SAProw = $Inventario->SelectHojaRutaEquipo($coEquipo);
                            if (count($SAProw)<=0){
                                $CO_VALIDACION = 3;
                            }

                            //$SAProw = $Inventario->SelectHojaRutaEquipo( $operacion);
                            if (count($SAProw)<=0){
                                $CO_VALIDACION = 4;
                            }

                        }

                        if ($VA_TIPO_PLAN=='I')
                        {

                            //$SAProw = $Inventario->SelecUbicacionTecnica(($co_ubicacion_tecnica);
                            if (count($SAProw)>0){
                                $CO_VALIDACION = 0;
                            }else{
                                $CO_VALIDACION = 1;
                            }

                            if(datecheck($FE_PROXIMA_PLAN)===false)
                            {
                                // fecha incorrecta formato 0000-00-00
                                $CO_VALIDACION = 2;
                                $FE_PROXIMA_PLAN = '0000-00-00';
                            }

                            //$SAProw = $Inventario->SelectHojaRutaInstalacion($coInstalacion);
                            if (count($SAProw)<=0){
                                $CO_VALIDACION = 3;
                            }

                            //$SAProw = $Inventario->SelectHojaRutaInstalacion($operacion);
                            if (count($SAProw)<=0){
                                $CO_VALIDACION = 4;
                            }

                        }

                        $sql = "INSERT INTO D037T_TMP_PLAN VALUES(NULL,'".$VA_DETALLE."','".$CO_HOJARUTA."','".$NU_HOJARUTA."','".$FE_PROXIMA_PLAN."','".$VA_TIPO_PLAN."','".$IN_CDC."','".$IN_CONTRATO_SERV."','".$IN_ESFUERZO_PROP."','".$CO_VALIDACION."')";                        
                        $carga_masiva->query($sql);


                    }
                    $msg .= "Archivo Procesado Con Exito \n";                      

                    //$connection->closeConnection();
                }
            else
                {
                    $msg = "Error Al Cargar el Archivo ";
                }
        }
 
}

?>



<form  action="cpanel.php?sistema=3000" method="POST" enctype="multipart/form-data" name="formulario" id="formulario">
    <table class="tablaformulario"  style="table-layout: fixed;">
        <col style="width:420px">
        <col>
        <tbody>
        <tr>
            <td width="200"><input type="file" name="rutArchivo" id="rutArchivo" /></td>
            <td width="110" align="left">
                <input type="radio" name="seleccion" value="E" checked>Equipo</td>
            <td width="110" align="left">
               <input type="radio" name="seleccion" value="I">Instalaci&oacute;n</td>
         
        </tr>
              
        </tbody>
    </table>
<input type="hidden" value="upload" name="action" />
    <div id="grupobtn" name="grupobtn" class="grupobotones">
        <!--input class="boton" id="accion" name="accion" value="Guardar" type="submit" -->	
        <input class="boton" id="accion" name="accion" value="Procesar" type="submit" >
    </div>
    <?php if (strlen($msg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$msg**</span>"; ?>        
</form>

