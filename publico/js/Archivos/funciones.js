document.oncontextmenu = function(){return false}

var strSeperator = '/';
var shift=false;
var crtl=false;
var alt=false;

function identificarNavegador() {
	var userAgent = navigator.userAgent.toLowerCase();
	jQuery.browser = {
		version: (userAgent.match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],
		msie: /msie/.test( userAgent ),
		mozilla: /mozilla/.test( userAgent ) 
	};
	if (!jQuery.browser.msie){
		document.getElementById("formRegistrar:ccedula").removeAttribute("onkeydown");
		document.getElementById("formRegistrar:ccedula").setAttribute("onkeypress", "return CedulaFormat(this,'Cédula de Indentidad Invalida',-1,true,event)"); 
	}
}

function CedulaFormat(vCedulaName,mensaje,postab,escribo,evento) {
	tecla=getkey(evento);
	vCedulaName.value=vCedulaName.value.toUpperCase();
	vCedulaValue=vCedulaName.value;
	valor=vCedulaValue.substring(2,12);
	tam=vCedulaValue.length;
	var numeros='0123456789/';
	var digit;
	var noerror=true;
	tam=vCedulaValue.length;
	
	if (shift && tam>1) {
	return false;
	}
	for (var s=0;s<valor.length;s++){
	digit=valor.substr(s,1);
	if (numeros.indexOf(digit)<0) {
	noerror=false;
	break;
	}
	}
	if (escribo) {
	if ( tecla==8 || tecla==37) {
	if (tam>2)
	vCedulaName.value=vCedulaValue.substr(0,tam-1);
	else
	vCedulaName.value='';
	return false;
	}
	if (tam==0 && tecla==86) {
	vCedulaName.value='V-';
	return false;
	}
	else if ((tam==0 && ! (tecla<14 || tecla==69 || tecla==86 || tecla==46)))
	return false;
	else if ((tam>1) && !(tecla<14 || tecla==16 || tecla==46 || tecla==8 || (tecla >= 48 && tecla <= 57) || (tecla>=96 && tecla<=105)))
	return false;
	}
}

function getkey(e){
	if (window.event) {
	shift= event.shiftKey;
	ctrl= event.ctrlKey;
	alt=event.altKey;
	return window.event.keyCode;
	}
	else if (e) {
	var valor=e.which;
	if (valor>96 && valor<123) {
	valor=valor-32;
	}
	return valor;
	}
	else
	return null;
}
//
//function mostrarcal(ano, mes, dia){ 
//	jQuery( '#formRegistrar\\:facta' ).datepicker({
//		dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
//		monthNamesShort:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
//		monthNames:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Augosto','Septiembre','Octubre','Noviembre','Diciembre'], 
//		minDate: new Date(ano-17, mes, dia),
//		maxDate: new Date(ano, mes, dia),
//		rangeSelect: true,
//		changeMonth: true,
//		changeYear: true,
//		dateFormat:'dd/mm/yy',
//		yearRange: '-17:+17'	
//	});
//	
//	jQuery( '#formRegistrar\\:fnacimiento' ).datepicker({
//		dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
//		monthNamesShort:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
//		monthNames:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Augosto','Septiembre','Octubre','Noviembre','Diciembre'], 
//		minDate: new Date(ano-18, mes, dia),
//		maxDate: new Date(ano, mes, dia),
//		rangeSelect: true,
//		changeMonth: true,
//		changeYear: true,
//		dateFormat:'dd/mm/yy',
//		yearRange: '-18:+18'	
//	});
//
//}

function validarCampo(campo) {
	if(!campo.value){
		campo.className="dijit dijitReset dijitInlineTable dijitLeft dijitTextBox dijitTextBoxError dijitError";
	}else{
		campo.className="dijit dijitReset dijitInlineTable dijitLeft dijitTextBox";
	}
}


function mensajesUsuario(codigo){
	switch(codigo){
		case 1:
			mensaje ="Debe indicar el número de cédula";
			 break;
		case 3:
			mensaje ="Debe indicar el número de pasaporte";
			 break;
		case 4:
			mensaje ="Debe indicar el número de folio";
			  break;
		case 5:
			mensaje ="Debe indicar el número de acta";
			  break;
		case 6:
			mensaje ="Debe indicar la fecha del acta";
			  break;
		case 7:
			mensaje ="El primer nombre debe ser válido";
			  break;
		case 8:
			mensaje ="Debe indicar el primer nombre ";  
			break;
		case 9:
			mensaje ="El primer apellido debe ser válido";
			  break;
		case 10:
			mensaje ="Debe indicar el primer apellido ";
			  break;
		case 11:
			mensaje ="Debe indicar la fecha de nacimiento para poder calcular la edad";
			  break;
		case 12:
			mensaje ="Debe indicar la fecha de nacimiento bajo el siguiente formato 'dd/mm/yyy'";
			  break;
		case 13:
			mensaje ="Debe indicar la fecha del acta bajo el siguiente formato 'dd/mm/yyy'";
			  break;
		case 14:
			mensaje ="Debe indicar el número de cédula ";
			  break;
		case 15:
			mensaje ="El formato de la cédula es incorrecto";
			  break;
			
	}
	return mensaje;
}

function validarFormulario(boton)
{	
	var cpasaporte = document.getElementById( 'formRegistrar:cpasaporte');
	var facta = document.getElementById( 'formRegistrar:facta');
	var nedad = document.getElementById( 'formRegistrar:nedad');
	var nfolio = document.getElementById( 'formRegistrar:nfolio');
	var nacta = document.getElementById( 'formRegistrar:nacta');
	var dsnombre = document.getElementById( 'formRegistrar:dsnombre');
	var dpnombre = document.getElementById( 'formRegistrar:dpnombre');
	var dsapellido = document.getElementById( 'formRegistrar:dsapellido');
	var dpapellido = document.getElementById( 'formRegistrar:dpapellido');
	var fnacimiento= document.getElementById( 'formRegistrar:fnacimiento');
	var patronFecha=/^(((0[1-9]|[12][0-9]|3[01])([\/])(0[13578]|10|12)([\/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([\/])(0[469]|11)([\/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([\/])(02)([\/])(\d{4}))|((29)(\.|-|\/)(02)([\/])([02468][048]00))|((29)([\/])(02)([\/])([13579][26]00))|((29)([\/])(02)([\/])([0-9][0-9][0][48]))|((29)([\/])(02)([\/])([0-9][0-9][2468][048]))|((29)([\/])(02)([\/])([0-9][0-9][13579][26])))$/;
	var patronNombreApellido =/^[a-zA-Z áÁéÉíÍóÓúÚñÑ]{1,15}$/;
	var patronFolio=/^[a-zA-Z0-9]{1,10}$/;
	var patronPasaporte =/^[a-zA-Z0-9]{1,15}$/;
	var patronCedula =/^(V|){1,1}-[0-9]{1,8}$/;
	var patronEdad =/^[0-9]*$/;
	sw = 0;
		
		
	if(!fnacimiento.value){
		sw = 1;
		mensaje = mensajesUsuario(11);
		
	}else if(!patronFecha.test(fnacimiento.value))	{
		sw = 1;
		mensaje = mensajesUsuario(12);
		
	}
	
	validarCampo(fnacimiento);
	
	if(!cpasaporte.value){
		sw = 1;
		mensaje = mensajesUsuario(3);
		cpasaporte.focus();
	}else if(!patronPasaporte.test(cpasaporte.value)){
		sw = 1;
		mensaje = mensajesUsuario(3);
		cpasaporte.focus();
	}
	
	if(!facta.value){
		sw = 1;
		mensaje = mensajesUsuario(6);
	}else if(!patronFecha.test(facta.value))	{
		sw = 1;
		mensaje = mensajesUsuario(13);
		
	}	
	validarCampo(facta);
	
	
	if(!nfolio.value){
		sw = 1;
		mensaje = mensajesUsuario(4);
		nfolio.focus();
	}else if(!patronFolio.test(nfolio.value)){
		sw = 1;
		mensaje = mensajesUsuario(4);
		nfolio.focus();
	}
	
	if(!nacta.value){
		sw = 1;
		mensaje = mensajesUsuario(5);
		nacta.focus();
	}else if(nacta.value==0){
		sw = 1;
		mensaje = mensajesUsuario(5);
		nacta.focus();
	}

	validarCampo(dpnombre);	
	if(!dpnombre.value){
		sw = 1;
		mensaje = mensajesUsuario(8);
		dpnombre.focus();
	}else if(!patronNombreApellido.test(dpnombre.value)){
		sw = 1;
		mensaje = mensajesUsuario(7);
		dpnombre.className="dijit dijitReset dijitInlineTable dijitLeft dijitTextBox dijitTextBoxError dijitError";
		dpnombre.focus();
	}

	validarCampo(dpapellido);	
	if(!dpapellido.value){
		sw = 1;
		mensaje = mensajesUsuario(10);
		dpapellido.focus();
	}else if(!patronNombreApellido.test(dpapellido.value)){
		sw = 1;
		mensaje = mensajesUsuario(9);
		dpapellido.className="dijit dijitReset dijitInlineTable dijitLeft dijitTextBox dijitTextBoxError dijitError";
		dpapellido.focus();
	}

	if(sw==1){
		alert(mensaje);
		boton.blur();
	}
	
	
}

function CalcularEdad(boton)
{	
	var fnacimiento = document.getElementById( 'formRegistrar:fnacimiento');
	alert(fnacimiento);
	if((fnacimiento.value)&&(patronFecha.test(fnacimiento.value))){
		alert(fnacimiento);
		
	}
		
}

function selectEdad(edad){
	var ccedula = document.getElementById('formRegistrar:ccedula');
	if(edad > 8){
		jQuery( '#etiqueta_ccedula').show();
		jQuery( '#div_ccedula').show();
		jQuery( '#format').show();
		if(ccedula.value=""){
				alert("Debe ingresar el número de cédula dado que es mayor a 9 años.");
		}
	}else {
		jQuery( '#etiqueta_ccedula').hide();
		jQuery( '#div_ccedula').hide();
		jQuery( '#format').hide();
		ccedula.value="";
	}
	
}

function selectCedula(edad){
	
	if(edad > 8){
		jQuery( '#etiqueta_ccedula').show();
		jQuery( '#div_ccedula').show();
		
	}else {
	
		jQuery( '#etiqueta_ccedula').hide();
		jQuery( '#div_ccedula').hide();
		var ccedula = document.getElementById('formRegistrar:ccedula');
		ccedula.value="";
	}
		
	
}

function validarCedula(edad,boton){
	var patronCedula =/^(V){1,1}-[0-9]{1,8}$/;
	var ccedula = document.getElementById('formRegistrar:ccedula');
	var edadform = document.getElementById('formRegistrar:nedad');
	edadform.value=edad;
	
	var sw=0;
	if(edad > 8){
		if(!ccedula.value){
			sw = 1;
			mensaje = mensajesUsuario(14);
			ccedula.focus();
		}else if(!patronCedula.test(ccedula.value))	{
			sw = 1;
			mensaje = mensajesUsuario(15);
			ccedula.focus();
		}	
	}

	if(sw==1){
		alert(mensaje);
		boton.blur();
	}
	
}