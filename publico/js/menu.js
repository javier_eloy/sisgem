var lastmenu;

document.oncontextmenu = function(){
    return false;
}

function ajustarPaneles(){
  
        jQuery(".izquierdaplantilla").height(jQuery(".centroplantilla").height());
}

function mostrarMenu() {
    ajustarPaneles();
    jQuery(document).ready(function()
    {
        jQuery(".centroplantilla").resize(function() {
            ajustarPaneles();
        });
        
        jQuery("#firstpane p.botonrol").click(function()            
        {        
            jQuery(this).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
            if(lastmenu) lastmenu.className="botonrol";
            this.className="botonrol_selected";
            lastmenu=this;
        });
        jQuery("#secondpane p.botonrol").mouseover(function()
        {
            jQuery(this).next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
        });

    });	
	
}

function encode_utf8( s ) {
  return decodeURIComponent( encodeURIComponent( s ) );
}

function decode_utf8( s ) {
  return decodeURIComponent( escape( s ) );
}
