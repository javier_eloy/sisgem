<?php 


include ("../../validacion_central.php");
require_once('../config/lang/eng.php');
require_once('../tcpdf.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Informacion del Documento
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('');
$pdf->SetTitle('');
$pdf->SetSubject('');
$pdf->SetKeywords('');

$pdf->setLanguageArray($l);

$pdf->AddPage();


$pdf->SetFont('helvetica', 'B', 20);
$style = array(
	'position' => 'R',
	'align' => 'C',
	'stretch' => false,
	'fitwidth' => true,
	'cellfitalign' => '',
	//'border' => true,
	'padding' => 'auto',
	'fgcolor' => array(0,0,0),
	'bgcolor' => false, //array(255,255,255),
	'text' => true,
	'font' => 'helvetica',
	'fontsize' => 8,
	//'stretchtext' => 4
);

$cedula=$_SESSION['usuario_cedula'];
$cmb=pg_Exec($con,"select  * from public.historico,public.historico_detalle where public.historico.hist_codigo=public.historico_detalle.hist_codigo and public.historico_detalle.histdet_cedula='$cedula'");
$_POST['cmb'];
if($_POST['cmb']!=""){
$historico=pg_Exec($con,"select * from public.historico where  hist_codigo='".$_POST['cmb']."'");
$v_historico = pg_fetch_array($historico);

$datos=pg_Exec($con,"select * from public.historico_detalle where  histdet_cedula='".$cedula."' and hist_codigo='".$_POST['cmb']."'");
$v_datos = pg_fetch_array($datos);





//crear

$currentdate = date("jnyhis",time());
list($usecs,$secs) = microtime();
  $llave="{$currentdate}{$usecs}";
  $fecha=date("d-m-Y");
$sql="INSERT INTO  public.validacion (cedula,codigo,fecha) VALUES ('$cedula','$llave','$fecha')";
pg_Exec($con,$sql);

$html1 = '

<table width="556" border="0">
  <tr>
    <td width="350">     GOBERNACION DEL ESTADO BARINAS<BR>
    SECRETARIA EJECUTIVA DE RECURSOS HUMANOS<BR>
    SISTEMA INTEGRAL DE RECURSOS HUMANOS V1.0<BR></td>
    <td width="171"><center>CODIGO:&nbsp; '.$llave.'</center></td>
    <td width="278" align="right"></td>
  </tr>
</table>


<table width="455" border="0">
  <tr>
    <td align="center">NOMINA PERSONAL EMPLEADO CONTRATADO</td>
  </tr>
  <tr>
    <td align="center">'.$v_historico['hist_titulo'].'</td>
  </tr>
  <tr>
    <td align="right">FECHA PAGO '.$v_historico['hist_hasta'].'</td>
  </tr>
</table>
<table width="553" border="1">
  <tr>
    <td width="329">DEPENDENCIA: '.$v_datos['histdet_dependencia'].'</td>
    <td width="228">&nbsp;</td>
  </tr>
  <tr>
    <td>NOMBRE :'.$v_datos['histdet_apellidos'].' '.$v_datos['histdet_nombres'].'</td>
    <td>CEDULA: '.$v_datos['histdet_cedula'].'</td>
  </tr>
  <tr>
    <td>CARGO : '.$v_datos['histdet_cargo'].'</td>
    <td>FEHA INGRESO:  '.$v_datos['histdet_fechab'].'</td>
  </tr>
</table>
';
$datos3=pg_Exec($con,"select * from public.historico_movimiento where  histdet_cedula='".$cedula."' and hist_codigo='".$_POST['cmb']."'");
$v_datos3 = pg_fetch_array($datos3);
$mov=pg_Exec($con,"select * from public.historico_movimiento where  histdet_cedula='".$cedula."' and hist_codigo='".$_POST['cmb']."'");



 if($v_datos3['histmov_codigo']!=40){

 $html4='
<table  width="451" border="0" >
    <tr>
    <td width="68">CODIGO</td>
    <td width="170">DESCRIPCION</td>
    <td width="110">ASIGNACIONES</td>
    <td width="100">DEDUCCIONES</td>
    <td width="100">NETO A COBRAR</td>
  </tr>
 </table>';

  while($rmov=pg_fetch_array($mov)){
$i++;
  $a="";
   $b="";
if($rmov['histmov_operacion']==1){$a=$rmov['histmov_monto'];}
if($rmov['histmov_operacion']!=1){$b=$rmov['histmov_monto'];}

$html5[]='
<table  width="451" border="0" >
  <tr>
    <td width="68">'.$rmov['histmov_codigo'].'</td>
    <td width="170">'.$rmov['histmov_nombre'].'</td>
	<td width="110">'.$a.'</td>
    <td width="100">'.$b.'</td>
	<td width="100">&nbsp;</td>
	</tr> 
</table>
 '; }}
 
 
 

 if($v_datos3['histmov_codigo']==40){
$MONTO=$v_datos3['histmov_monto']/$v_datos3['histmov_diaslaborados'];


$html2='
<table width="451" border="0" >
 <tr>
    <td width="68">CODIGO</td>
    <td width="170">DESCRIPCION</td>
    <td width="110">DIAS TRABAJADOS </td>
    <td width="100">VALOR FACIAL</td>
    <td width="100">NETO A COBRAR</td>
  </tr>
</table>
';
while($rmov=pg_fetch_array($mov)){

$html3=' 
<table  width="451" border="0" >
<tr>
	<td width="68">'.$rmov['histmov_codigo'].'</td>
	<td width="170">'.$rmov['histmov_nombre'].'</td>
	<td width="110">'.$rmov['histmov_diaslaborados'].'</td>
	<td width="100">'.$MONTO.'</td>
    <td width="100">'.$v_datos['histdet_valorc'].'</td>
</tr>
</table> 
<table>
<tr>
<td>
  <br>
  Art: 5 LPAT
  Este beneficio no sera considerado como salario de conformidad con<br>
  lo establecido en el paragrafo tercero del Articulo 133 de la LOT 
 </td></tr> 
 </table>

 '; }}
 
 
  $c="";
  $d="";
 
if($v_datos3['histmov_codigo']!=40){$c=$v_datos['histdet_tasig'];}
if($v_datos3['histmov_codigo']!=40){$d=$v_datos['histdet_tdeduc'];}

 $html7='
 <table width="451" border="1">


  <tr>
    <td width="232">TOTALES </td>
    <td width="110">'.$c.'</td>
    <td width="100">'.$d.'</td>
    <td width="100">'.$v_datos['histdet_valorc'].'</td>
  </tr>
</table>
 
 ';
  }



// Estilo 
$pdf->SetFont('helvetica', '', 10);


// Imprimir contenido
$pdf->writeHTML($html1, true, 0, true, true);
$pdf->Ln(); 
if($v_datos3['histmov_codigo']==40){
$pdf->writeHTML($html2, true, 0, true, true);
$pdf->Ln();
$pdf->writeHTML($html3, true, 0, true, true);
$pdf->Ln();
}else{
$pdf->writeHTML($html4, true, 0, true, true);
$pdf->Ln();
for($x=0;$x<=$i;$x++){
$pdf->writeHTML($html5[$x], true, 0, true, true);
$pdf->Ln();
}
}
$pdf->writeHTML($html7, true, 0, true, true);
$pdf->Ln();

$pdf->write1DBarcode(''.$cedula.'', 'C39E+', '', '', '', 18, 0.4, $style, 'N');
$pdf->Ln();


$pdf->Output('example_039.pdf', 'I');
?>