/**
 * CONSTRUCTOR
 * @param sUrl string que contiene la URL donde se realizará la petición
 * @param fRetorno función que manejará el retorno de la petición
 * @return XMLHttpRequest
 */
function Ajax(sUrl,fRetorno){
    // propiedad petición: utiliza el método crearAJAX() para instanciar un objeto XHR
    this.peticion = this.crearAJAX();
    // inicializa la petición XHR con el método GET, la URL recibida, y de forma asíncrona
    this.peticion.open("GET", sUrl, true);
               
    // declara una variable temporal para utilizar la propiedad "peticion" 
    // dentro de la función clausurada manejadorInterno()
    var tempPeticion = this.peticion;
    /**
     * FUNCIÓN manejadorInterno
     * 1. Verifica que los datos solicitados han sido plenamente recibidos
     * 2. Verifica que el servidor haya respondido OK
     * 3. 
     *   a. Si se cumplen el punto 1 y 2, se pasa la respuesta de la peticion a la función recibida
     *   b. Sino se informa
     */
    function manejadorInterno(){
        if (tempPeticion.readyState == 4) {
            if (tempPeticion.status == 200) {
                fRetorno(tempPeticion.responseText);
            }else{
                alert("El servidor respondio " + tempPeticion.status + " =(");
            }
        }
    }
    // Se asigna el método manejadorInterno a la propiedad onreadystatechange
    this.peticion.onreadystatechange = manejadorInterno;
}
            
/**
 * MÉTODO crearAJAX
 * (Documentación disponible en ejemplo anterior)
 */ 
Ajax.prototype.crearAJAX = function (){
    var i;
    if (window.XMLHttpRequest) {
        var oAJAX = new XMLHttpRequest();
        return oAJAX;
    }else if(window.ActiveXObject){
        var versiones = [
        "MSXML2.XmlHttp.6.0",
        "MSXML2.XmlHttp.3.0"
        ];
                    
        for (i = 0; i < versiones.length; i++) {
            try {
                var oAJAX = new ActiveXObject(versiones[i]);
                return oAJAX;
            }catch (error) {
            // no hacer nada
            }
        }
    }
    return null;
}

/**
 * MÉTODO enviar
 * Utiliza la función "send" de la propiedad "peticion" que contiene el objeto XHR
 */
Ajax.prototype.enviar = function (){
    this.peticion.send(null);
}