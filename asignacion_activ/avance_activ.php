<?php
include ("../aplicacion/configuracion/aut_lib.inc.php");

$co_orden = $_GET['co_orden'];
$sMsg = "";
$script = "";

$objComun = new comun();
$objInventario = new inventarioSAP();
$objAsignacionAct = new asignacion_activ();
$rsAsigOrden = $objAsignacionAct->SelecDatosOrden($co_orden);
?>
<html>
   <head>
      <link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">
      <script type="text/javascript" src="../publico/js/jquery-1.7.2.js"></script>
   </head>
   <body bgcolor="#FFFFFF">
      <div class="titulomodulo">Orden de Trabajo</div>
      <div class="tabla">
         <?php
         echo "<b>No de Orden:</b>", $rsAsigOrden->fields["NU_ORDEN"], "<br/>";
         echo "<b>Fecha de Creaci&oacute;n de Orden de Trabajo:</b>", date_format(new DateTime($rsAsigOrden->fields['FE_ASIGNACION']), "d/m/Y"), "<br/>";
         echo "<br/>";
         ?>
      </div>
      <hr>
      <div>
         <?php
         $cur_plan = 0;
         while(!$rsAsigOrden->EOF) {
            
            if($cur_plan != $rsAsigOrden->fields["CO_PLAN"]) {
               $objComun->SelectUbicDesc($objInventario, $rsAsigOrden->fields['VA_HOJARUTA'], $rsAsigOrden->fields['VA_DETALLE'], $rsAsigOrden->fields['VA_TIPO_PLAN'], $Desc, $sUbic);
               if ($rsAsigOrden->fields["ACT_STATUS"] == STATUS_ACTIVIDAD_CERRADA) $sStatusAct="<span style=\"color:red\">Cerrada</span>";
                                                                           else $sStatusAct="<span style=\"color:green\">Abierta</span>";

               $sDetalle=($rsAsigOrden->fields['VA_TIPO_PLAN'] == "E") ? ltrim($rsAsigOrden->fields["VA_DETALLE"],"0") : $rsAsigOrden->fields["VA_DETALLE"];
               echo "<div class='divCabecera'>";
               echo "<b>Codigo/Caracteristica:</b> ",$sDetalle," ",utf8_encode($Desc),"<br/>";
               echo "<b>Actividad:</b> ", $rsAsigOrden->fields["TX_HOJARUTA"],"<br/>";
               echo "<b>Porcentaje Avance(%):</b> ",$rsAsigOrden->fields["PC_AVANCE"],"% <br/>";
               echo "<b>Estado:</b> ", $sStatusAct;
               echo "</div>";
               echo "<br/><b>Procedimientos:</b><br>";
              $cur_plan = $rsAsigOrden->fields["CO_PLAN"];
          }
            echo "<br>&nbsp;&nbsp;&nbsp;(",$rsAsigOrden->fields["VA_PROCEDIMIENTO"],") ",$rsAsigOrden->fields["NB_TEXTO"];

            $rsAsigOrden->MoveNext();
         }
         ?>
      </div>
      <hr>
      <div class="grupobotones">
         <input class="boton" name="Volver" value="Cerrar" type="button" onclick="window.parent.Shadowbox.close();"/>
      </div>

   </body>
</html>