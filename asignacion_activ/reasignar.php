<?php
include ("../aplicacion/configuracion/aut_lib.inc.php");

$co_actividad = $_GET['co_actividad'];
$co_cargo = $_GET['co_cargo'];
$fila=$_GET['fila'];
$sMsg="";
$script="";

$objAsignacionAct = new asignacion_activ();

if(isset($_POST['accion']))
switch($_POST['accion']){
    case "Guardar":
        $co_usuario_nuevo=$_POST['co_usuario_nuevo'];
        $nb_usuario_nuevo=$_POST['nb_usuario'];
        $hoy_reasigna=date_format(new DateTime(),"d/m/Y");
        if($objAsignacionAct->ActUsuarioActividad($co_actividad,$co_usuario_nuevo,$hoy_reasigna)) {
            $script = "window.parent.AsignarValorCelda('$fila','$nb_usuario_nuevo'); window.parent.Shadowbox.close();";
        }
        else
            $sMsg=MSG_ERROR_TRANSACCION;
        break;
    
}
    

$rsAsignacion = $objAsignacionAct->SelecDatosActividad($co_actividad);
$rsUsuariosDisp = $objAsignacionAct->SelecUsuariosDisponibleOrden($rsAsignacion->fields['CO_ORDEN'], $co_cargo,"'{$rsAsignacion->fields['CO_EJEC_PLAN']}'");
?>
<script type="text/javascript">
<?php echo $script ?>
</script>
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">   
<script type="text/javascript" src="../publico/js/jquery-1.7.2.js"></script>
<form action='<?php echo "reasignar.php?fila=",$fila,"&co_actividad=",$co_actividad,"&co_cargo=",$co_cargo; ?>' method='POST' >
<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="10" width="592" height="292">
    <tbody>
        <tr>
            <td valign="top" width="756">
                <div class="titulomodulo">Orden de Trabajo.</div>
                <div class="tabla">
                    <?php
                    if (!$rsAsignacion->EOF) {
                        $fechaProgram = new DateTime($rsAsignacion->fields['FE_ASIG_ORDEN']);
                        echo "<b>Fecha de Creaci&oacute;n de Orden de Trabajo:</b> ", utf8_encode(ucfirst(strftime("%A, %d de %B", $fechaProgram->getTimestamp())));
                        echo "<br>";
                        echo "<b>No de Orden: </b>", $rsAsignacion->fields['NU_ORDEN'];
                        echo "<br>";
                        echo "<b>Fecha de Asignacion a Orden de Trabajo: </b>", date_format(new DateTime($rsAsignacion->fields['FE_ASIGNACION']), "d/m/Y");
                    }
                    ?>
                </div>
                <table class="tabla">
                    <tr>
                        <td>
                            Asignado Actualmente:  <?php echo $rsAsignacion->fields['NB_NOMBRE']; ?> <br>
                            Porcentaje de Avance:   <?php echo $rsAsignacion->fields['NU_PORCENTAJE_AVANCE']; ?>%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" id="nb_usuario" name="nb_usuario" value="<?php echo $rsUsuariosDisp->fields['NB_NOMBRE'] ?>">
                            Reemplazar por: <select id="co_usuario_nuevo" name="co_usuario_nuevo" onchange="return actualiza_post_nombre('co_usuario_nuevo');">
                                <?php
                                while (!$rsUsuariosDisp->EOF) {
                                    echo "<option value=", $rsUsuariosDisp->fields['CO_USUARIO'], ">",
                                    $rsUsuariosDisp->fields['NB_NOMBRE'],
                                    "</option>";
                                    $rsUsuariosDisp->MoveNext();
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
                 <?php if (strlen($sMsg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$sMsg**</span>"; ?>      
                <div class="grupobotones">
                            <input class="submit boton" name="accion" value="Guardar" type="submit" />
                            <input class="boton" name="Volver" value="Cerrar" type="button" onclick="window.parent.Shadowbox.close();"/>
                 </div>
            </td>                
        </tr>       
    </tbody>
</table>
</form>
<script>
    function actualiza_post_nombre(name) {
        var jq=jQuery.noConflict();
        var selected=jq("#"+name+" option:selected").html();
        jq('#nb_usuario').val(selected);
    }
</script>