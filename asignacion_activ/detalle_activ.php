<?php
$query = $_GET['p'];
$param = decryptLink($query);
parse_str($param);

$objAsignacion = new asignacion_activ;
$rsPersonalAsignacion = $objAsignacion->SelectPersonalActividadesProgramadas($nu_orden, $va_hojaruta);

$fe_asig = new DateTime($rsPersonalAsignacion->fields['FE_ASIGNACION']);
$co_cargo = $rsPersonalAsignacion->fields['CO_CARGO'];
?>
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css"/>
<!-- script JQUERY -->
<script type="text/javascript" src="aplicacion/cliente/html_tablas_asignacion.js"></script>
<!-- script PARA REALIZAR TEXTO FLOTANTE -->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript">
   Shadowbox.init({
      language: "es",
      modal:true,
      players:  ['img', 'html', 'iframe', 'php'],
      overlayColor: "#000",
      overlayOpacity: "0.5"});
</script>

<!-- style PARA MOSTRAR UN EXTO FLOTANTE SOBRE COMPONENTES -->
<style type="text/css">
   /* Estilo que muestra la capa flotante */
   #flotante
   {
      position: absolute;
      display:none;
      font-family:Arial;
      font-size:0.7em;
      width:280px;
      border:0.1px solid #808080;
      background-color:#FFFFD0;
      padding:5px;
   }
   .text {}
</style>

<div class="f10bold"><strong>C&oacute;digo / Caracter&iacute;sticas: </strong> <?php echo $n_local; ?></div>
<div class="f10bold"><strong>No de Orden: </strong> <?php echo $nu_orden ?> </div>
<div class="f10bold"><strong>Fecha de Orden de Trabajo: </strong> <?php echo date_format($fe_asig, 'd/m/Y') ?></div>
<div class="f10bold"><strong>Actividad: </strong><?php echo $n_hojaruta ?></div>
<br>
<table id="listaSaldos" class="tabla">
   <thead>
      <tr>
         <th scope="col">Personal</th>
         <th scope="col">Cargo</th>
         <th scope="col">Fecha de Asig.</th>
         <th scope="col" style="align:center">% de Avance</th>
         <th scope="col" style="width:100px;"></th>
      </tr>
   </thead>
   <?php
   $conteo = 0;
   while (!$rsPersonalAsignacion->EOF) {
      //* Opcion
      $conteo++;
      $sLink = "<td><div id=\"entorno_$conteo\">";
      $sLink.= "<input id=\"reasignar_$conteo\" type=\"button\" class=\"boton\" onclick=\"OpenShadow('asignacion_activ/reasignar.php?co_actividad={$rsPersonalAsignacion->fields['CO_ACTIVIDAD']}&co_cargo={$co_cargo}&fila={$conteo}',300,600);\" value=\"Reasignar\">";
      $sLink.="</div></td>";

      echo "<tr>",
      "<td id='usuario_{$conteo}'>", utf8_decode($rsPersonalAsignacion->fields['NB_NOMBRE']), "</td>",
      "<td>",$rsPersonalAsignacion->fields['NB_CARGO'], "</td>",
      "<td>",date_format(new DateTime($rsPersonalAsignacion->fields['FE_ASIGNACION']),"d/m/Y"), "</td>",
      "<td>", $rsPersonalAsignacion->fields['NU_PORCENTAJE_AVANCE'], "</td>",
      $sLink,
      "</tr>";
      $rsPersonalAsignacion->MoveNext();
   }
   ?>
</table>
<!-- BOTON DE VOLVER -->
<form method="POST" <?php echo "action=\"cpanel.php?sistema=30&v=3\"";?> >
    <div class="grupobotones">
        <input class="submit boton"  value="Volver" type="submit">
    </div>
</form>

<script type="text/javascript">
   jq=jQuery.noConflict();

   function OpenShadow(sPath,pHeight,pWidth) {
      if(typeof(pHeight)==='undefined') pHeight=501;
      if(typeof(pWidth)==='undefined') pWidth= 801;

      Shadowbox.open({
         content:    sPath,
         player:     "iframe",
         height:     pHeight,
         width:      pWidth
      });
   }

   function AsignarValorCelda(filacelda,value){
      jq("#usuario_"+filacelda).html(value);
   }
</script>