<?php
$Date = new DateTime();
$semana_actual = $Date->format("W-Y");
$Date->add(new DateInterval('P8D'));
$semana_proxima = $Date->format("W-Y");

$vista_get=0;
if(isset($_GET["v"])) $vista_get = $_GET["v"];
?>
<!-- script JQUERY -->
<script type="text/javascript" src="aplicacion/cliente/html_tablas_asignacion.js"></script>
<!-- style PARA MOSTRAR UN EXTO FLOTANTE SOBRE COMPONENTES -->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript">
   Shadowbox.init({
      language: "es",
      modal:true,
      players:  ['img', 'html', 'php'],
      overlayColor: "#000",
      overlayOpacity: "0.5"});
</script>

<!-- style PARA MOSTRAR UN EXTO FLOTANTE SOBRE COMPONENTES -->
<style type="text/css">
    /* Estilo que muestra la capa flotante */
    #flotante
    {
        position: absolute;
        display:none;
        font-family:Arial;
        font-size:0.7em;
        width:280px;
        border:0.1px solid #808080;
        background-color:#FFFFD0;
        padding:4px;
    }
    .text {}
</style>

<script type="text/javascript" src="aplicacion/cliente/texto_flotante_td.js"></script>
<div class="titulosubmodulo">Asignaci&oacute;n de Recursos a las Ordenes de Mantenimiento</div>
<div>
    <SELECT id="filtroasignacion" name="filtroasignacion" onchange="return CargarAsignacion(this.value,'<?php echo $RecordUsuario["usuario_ubic_tecnica"]?>','<?php echo $RecordUsuario["usuario_grp_plan"] ?>');">
        <option value="0">Seleccione Vista...</option>
        <option value="1">Asignaci&oacute;n de Actividades</option>
        <option value="2">Pendientes por Asignar</option>
        <option value="3">Actividades Asignadas</option>
    </SELECT>
    <div class="etiqueta" id="Cargando" ></div>
</div>
<div id="tituloasignacion" class="titulomodulo" >Semanas a Programar y asignar Usuarios: Actual (N&deg; <?php echo $semana_actual; ?>) y la pr&oacute;xima semana (N&deg; <?php echo $semana_proxima; ?>):</div>
<div id="shadow_hidden"></div>
<div id="flotante"></div>

<div id="resultado_asignacion"  class="resultado_asignacion"></div>
<!-- script PARA CAMBIAR EL BOT�N O EL LINK DEPENDIENDO DE LA SELECCI�N DEL SELECT-->
<script type="text/javascript">
    var jq=jQuery.noConflict();
    var vista=<?php echo $vista_get; ?>;
       
    jq(document).ready(function(){      
        initBindLocal();
        jq("#tituloasignacion").hide();
        if(vista > 0 ) {
           jq("#filtroasignacion").val(vista);
           CargarAsignacionModificada();
        }
    });
    
    function initBindLocal(){
        jq("tbody#detalle").hide();
        jq("tbody#orden .ClassMas").click(function(event){
            var desplegable = jq(this).parents('#orden').get(0).next('#detalle')/*jq(this).next()*/;
            desplegable.toggle();
            event.preventDefault();
        });
    }

    function CargarAsignacionModificada() {
       CargarAsignacion(jq('#filtroasignacion').val());
       return true;
    }

    function CargarAsignacion(value){
        jq("#resultado_asignacion").html("");
        switch(value){
            case "1":
                SelecAsignacion("resultado_asignacion","Cargando",
                     '<?php echo $RecordUsuario["usuario_ubic_tecnica"]?>',
                     '<?php echo $RecordUsuario["usuario_grp_plan"] ?>',
                     '<?php echo $RecordUsuario["usuario_pto_plan"] ?>',
                     '<?php echo $RecordUsuario["usuario_co_cargo_completo"]?>');
                 jq("#tituloasignacion").show();
                break;
            case "2": 
                SelecAsignacionPendiente("resultado_asignacion","Cargando",
                     '<?php echo $RecordUsuario["usuario_ubic_tecnica"]?>',
                     '<?php echo $RecordUsuario["usuario_grp_plan"] ?>',
                     '<?php echo $RecordUsuario["usuario_pto_plan"] ?>',
                     '<?php echo $RecordUsuario["usuario_co_cargo_completo"]?>');
                 jq("#tituloasignacion").hide();
                break;
            case "3":
                SelecActividadAsignada("resultado_asignacion","Cargando",
                      '<?php echo $RecordUsuario["usuario_ubic_tecnica"]?>',
                      '<?php echo $RecordUsuario["usuario_grp_plan"] ?>',
                      '<?php echo $RecordUsuario["usuario_pto_plan"]?>',
                      '<?php echo $RecordUsuario["usuario_co_cargo_completo"]?>');
                jq("#tituloasignacion").hide();
                break;
            default:
                jq("#tituloasignacion").hide();
        }   

      return true;
    }
    function ActualizarEstado(obj,idplan,idnomenclatura) {
        var lastvalue=obj.value;
        obj.value="Ejecutando...";
        if(ActualizarAplazada(idplan,idnomenclatura)) { 
            CargarProgramacion(jq("#filtroprograma").val());
            return true;
        } else {
            obj.value=lastvalue;
            return false;
        }
    }
    
    function cargaEspera(idSelectOrigen)
    {
        // Obtener el ID del elemento
        var id = idSelectOrigen;
        var objcont=null;
        // DESCONCATENO PARA OBTENER EL NUMERO DE PROCEDIMIENTO
        id = id.split('_');
        id = id[1];

        // Obtengo el select nivel, que fueron modificado anteriormente
        var IdSelectMotivo=document.getElementById("idmotivo_"+id);

        // Obtengo la opci�n que el usuario selecciono en el select nivel
        var ValorSelectMotivo = IdSelectMotivo.options[IdSelectMotivo.selectedIndex].value;
        objasig=document.getElementById('asignar_'+id);
        objcont=document.getElementById('continuar_'+id);
        
        if(ValorSelectMotivo!=0)
        {
            // Oculto el link y muestro el boton con style="display: none;"
            if(objasig) objasig.setAttribute('style','display: none;');
            if(objcont) objcont.setAttribute('style','display: none;');
            document.getElementById('detener_'+id).setAttribute('style','');

        }
        if(ValorSelectMotivo==0)
        {
            // Oculto el bot�n y muestro el link con style="display: none;"
            document.getElementById('detener_'+id).setAttribute('style','display: none;');        
            if(objasig) objasig.setAttribute('style','');
            if(objcont) objcont.setAttribute('style','');
        }
        return true;
    }
    
   function OpenShadow(sPath,pHeight,pWidth) {
      if(typeof(pHeight)==='undefined') pHeight=511;
      if(typeof(pWidth)==='undefined') pWidth= 801;

      Shadowbox.open({
         content:    sPath,
         player:     "iframe",
         height:     pHeight,
         width:      pWidth
      });
   }

</script>
