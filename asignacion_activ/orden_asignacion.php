<?php
//error_reporting(E_ERROR | E_PARSE);
include ("../aplicacion/configuracion/aut_lib.inc.php");

$objAsignacionAct = new asignacion_activ();
$objInventario = new inventarioSAP();
$co_orden = $_GET['co_orden'];
$pto_plan = $_GET['pto_plan'];
$co_cargo = $_GET['co_cargo'];

$sMsg = "";
$script = "";
$sfechaMinima = "";
$hoy = new DateTime("NOW");
if (isset($_GET['accion']))
   switch ($_GET['accion']) {
      case 'Guardar' :
         if (isset($_POST['usuarios'])) {

            $usuariosList = $_POST['usuarios'];
            $fe_asignacion = date("d/m/Y");
            if ($objAsignacionAct->InsUsuariosOrden($fe_asignacion, $usuariosList, $co_orden)) {
               $script = "window.parent.CargarAsignacionModificada(); window.parent.Shadowbox.close();";
            } else
               $sMsg = "Error Interno de Base de Datos: No se puede asignar los usuarios";
         }
         break;
   }
$rsAsignacion = $objAsignacionAct->SelecPlanesAsignar($co_orden);
?>
<script type="text/javascript">
<?php echo $script ?>
</script>
<!--Cuerpo-->
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/tools.css">
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">                    
<script type="text/javascript" src="../publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="../publico/js/jquery-ui.js"></script>	
<script type="text/javascript" src="../aplicacion/cliente/texto_flotante_td.js"></script>
<!-- style PARA MOSTRAR UN EXTO FLOTANTE SOBRE COMPONENTES --> 
<style type="text/css">
   /* Estilo que muestra la capa flotante */
   #flotante
   {
      position: absolute;
      display:none;
      font-family:Arial;
      font-size:0.7em;
      width:280px;
      border:0.1px solid #808080;
      background-color:#FFFFD0;
      padding:4px;
   }
   .text {}
</style>
<link href="../publico/estilos/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css">
<body  bgcolor="#ffffff" >
   <form method="POST" name="formulario" id="formulario" action="orden_asignacion.php">
      <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="10" width="760" height="401">
         <tbody>
            <tr>
               <td valign="top" width="756">
                  <div class="titulomodulo">Las siguiente orden est&aacute; pendiente por asignar usuarios.</div>
                  <div class="tabla">
                     <?php
                     if (!$rsAsignacion->EOF) {
                        $fechaProgram = new DateTime($rsAsignacion->fields['FE_ASIG_ORDEN']);
                        echo "<b>Fecha de Programaci&oacute;n:</b> ", htmlspecialchars(ucfirst(strftime("%A, %d de %B", $fechaProgram->getTimestamp())));
                        echo "<br>";
                        echo "<b>No de Orden: </b>", $rsAsignacion->fields['NU_ORDEN'];
                     }
                     ?>
                  </div>
                  <table id="listaSaldos" class="tabla">
                     <thead>
                        <tr>
                           <th scope="col">C&oacute;digo / Caracter&iacute;sticas</th>
                           <th scope="col">Actividad<br /><font face="Arial" size="0.2em">(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></th>
                           <th scope="col">Fecha de Plan</th>
                        </tr>
                     </thead>
                     <!-- En este div se muestra la capa emergente -->
                     <div id="flotante"></div>
                     <tbody>
                        <?php
                        $conteo = 0;
                        $cantPersonasMin = 0;
                        $cantPersonasTotal = 0;
                        while (!$rsAsignacion->EOF) {
                           $conteo++;
                           list($grupohr, $operacionhr) = explode("-", $rsAsignacion->fields['VA_HOJARUTA']);

                           if ($rsAsignacion->fields['VA_TIPO_PLAN'] == "E") {
//                                            $arrayProcedimiento = $objInventario->SelectHojaRutaEquipo($rsAsignacion->fields['VA_DETALLE'],$operacionhr);
                              $arrayDatos = $objInventario->SelecDetalleEqpSAP($rsAsignacion->fields['VA_DETALLE']);
                              $Desc = $arrayDatos[1]['EQKTU'];
                              $sCodigo = $arrayDatos[1]['TPLNR'];
                           } else {
//                                            $arrayProcedimiento = $objInventario->SelectHojaRutaInstalacion($rsAsignacion->fields['VA_DETALLE'],$operacionhr);
                              $arrayDatos = $objInventario->SelecUbicacionTecnica($rsAsignacion->fields['VA_DETALLE']);
                              $Desc = $arrayDatos[1]['PLTXU'];
                           }

                           echo "<tr>";
                           //* Codigo de Equipo o Estacion y descripcion
                           echo "<td nowrap>(" . ltrim($rsAsignacion->fields['VA_DETALLE'], "0") . ")<br>" . $Desc . "</td>";

                           //* Actividad
                           $objComun = new comun();
                           $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsAsignacion->fields['CO_PLAN']);
                           if (!$rsProcedimiento->EOF) {
                              $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
                              $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
                              $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
                              $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong><ul>";
                              while (!$rsProcedimiento->EOF) {
                                 //* Busca las capacidades
                                 $cantPersonas = (int) $rsProcedimiento->fields['CAPACIDAD'];
                                 if ($cantPersonas > $cantPersonasMin)
                                    $cantPersonasMin = $cantPersonas;
                                 $cantPersonasTotal+=$cantPersonas;

                                 $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
                                 $rsProcedimiento->MoveNext();
                              }
                              $sProc.="</ul> \"></div>";
                              $sProc.= substr($textoActividad, 4) . "</td>";
                           }
                           echo $sProc;

                           //* Fecha Plan
                           $FechaPlan = new DateTime($rsAsignacion->fields['FE_PROXIMA_PLAN']);
                           if ($FechaPlan < $hoy)
                              echo "<td title=\"Actividad pendiente\" style=\"color:red\">" . date_format($FechaPlan, "d/m/Y") . "</td>";
                           else
                              echo "<td>" . date_format($FechaPlan, "d/m/Y") . "</td>";
                           echo "</tr>";
                           $rsAsignacion->MoveNext();
                        }
                        ?>
                     </tbody>
                  </table>
               </td>
            </tr>
            <tr>
               <td>
                  <hr>
                  <div class="tabla"> Usuarios Disponibles</div>
                  <div class="checklistdiv">

                     <?php
                     //---- Muestra los usurios disponibles
                     $rsUsuarioAsig = $objAsignacionAct->SelecUsuarioPto($pto_plan, $co_cargo);
                     while (!$rsUsuarioAsig->EOF) {
                        echo "<div  class='checklist' >",
                        "<input type='checkbox' onclick='return calcularTotal();' id='usuarioslist' name='usuarios[]' value=", $rsUsuarioAsig->fields['CO_USUARIO'], ">", $rsUsuarioAsig->fields['NB_NOMBRE'],
                        "(", $rsUsuarioAsig->fields['NB_INDICADOR'], ") - ", strtoupper($rsUsuarioAsig->fields['NB_CARGO']), "</div>";
                        $rsUsuarioAsig->MoveNext();
                     }
                     ?>
                  </div>
                  <div class="tabla">Cantidad de Personas a Seleccionar: Minima <?php echo $cantPersonasMin ?> / Maximo <?php echo $cantPersonasTotal ?>
                     <input type="hidden" id="cantidadMinima" name="cantidadMinima" value="<?php echo $cantPersonasMin ?>">
                     <input type="hidden" id="cantidadTotal" name="cantidadTotal" value="<?php echo $cantPersonasTotal ?>">
                  </div>
                  <div id="totalEmpleados" class="tabla">Total de Empleados Seleccionados: 0</div>
                  <br>
                  <?php if ($sMsg != "") echo " <span style=\"color:#f00;font-size:x-small;\">**$sMsg**</span>"; ?>
                  <div class="grupobotones">
                     <input class="submit boton" name="Guardar" value="Guardar" type="button" onclick="Enviar();"/>
                     <input class="boton" name="Volver" value="Cerrar" type="button" onclick="window.parent.Shadowbox.close();"/>
                  </div>
               </td>
            </tr>
         </tbody>
      </table>
   </form>
</body>

<!--Fin de Cuerpo-->
<script type="text/javascript">
   var jq = jQuery.noConflict();

   function Enviar() {
      var chk = jq('#formulario input[name="usuarios[]"]');
      var total = 0;
      var cantidadMinima = jq("#cantidadMinima")[0].value;
      var cantidadTotal = jq("#cantidadTotal")[0].value;

      for (var i = 0; i < chk.length; i++) {
         if (chk[i].checked)
            total++;
      }
      if (total < cantidadMinima)
      {
         alert("La cantidad mínima de usuarios es " + cantidadMinima + ", revise la Asignación");
         return false;
      } else if (cantidadTotal < total) {
         alert("La cantidad maxima de usuarios es " + cantidadTotal + ", revise la Asignación");
         return false;
      }


      jq('#formulario').attr('action', "orden_asignacion.php?accion=Guardar&co_orden=<?php echo $_GET['co_orden'] ?>" +
              "&pto_plan=<?php echo utf8_decode($_GET['pto_plan']) ?>&co_cargo=<?php $_GET['co_cargo'] ?>");
      jq('#formulario').submit();
      return true;
   }

   function calcularTotal() {

      var chk = jq('#formulario input[name="usuarios[]"]');
      var divTotal = jq('#totalEmpleados');
      var minValor = jq('#cantidadMinima')[0].value;

      var total = 0;
      for (var i = 0; i < chk.length; i++) {
         if (chk[i].checked)
            total++;
      }
      if (minValor > total)
         divTotal[0].innerHTML = 'Total de Empleados Seleccionados: <span style="color:red">' + total + '</span>';
      else
         divTotal[0].innerHTML = 'Total de Empleados Seleccionados: <span>' + total + '</span>';
   }
</script> 