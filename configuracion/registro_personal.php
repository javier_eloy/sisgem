<?php
/* * *************** OBJETO DE CLASE CONTROLADORA ************************** */
$rsUsuario = new registro_personal();

$action = "cpanel.php?sistema=12";
$etiqBoton = "Guardar";
$msg = "";
$nb_indicador = "";
$nu_username = "";
$nu_cedula = "";
$nb_nombre = "";
$nu_telefono = "";
$tx_direccion = "";
$co_cargo = "";
$in_planificador = 0;
$co_cargo_planificador = "";

if (isset($_REQUEST['accion'])) {
   $accion=$_REQUEST['accion'];
   // Valida que se ingresó un usuario valido
   $username = $_POST['indicador'];
   if (isset($username)) {
      $objComun = new seguridad();
      $arrayResult = $objComun->selDatosLDAP($username);
      if(!isset($arrayResult)) {
         $accion="NoExiste";
      }
   }

   switch ($accion) {
      //-****************** Guarda un Usuario *******************
      case "Guardar":

         if ($rsUsuario->usuarioExiste($username) == 0) {
            if ($rsUsuario->insertUsuario($username, filter_input(INPUT_POST,'nombre_apellido'), $_POST['cedula'], $_POST['direccion'], $_POST['telefono'], $_POST['cargo'], $_POST['gestion'], ($_POST['gestion'] == 1) ? $_POST['cargo_controlgestion'] : 0))
               $msg = MSG_DATOS_REGISTRADOS;
            else
               $msg = "<B> ERROR</B>. Error Registrando Datos";
         } else
            $msg = "<B> ERROR</B>. Usuario ya Registrado";

         break;

      case "Editar":
         //-***************** Edita un Usuario ********************
         if ($rsUsuario->editUsuario($_POST['nu_username'], $username, $_POST['nombre_apellido'], $_POST['cedula'], $_POST['direccion'], $_POST['telefono'], $_POST['cargo'], $_POST['gestion'], ($_POST['gestion'] == 1) ? $_POST['cargo_controlgestion'] : 0))
            $msg = MSG_DATOS_EDITADOS;
         else
            $msg = "<B> ERROR</B>. Error Editando Datos";
         break;

      case "Eliminar":
         //-***************** Edita un Usuario ********************
         if ($rsUsuario->deleteUsuario($_GET['id']))
            $msg = MSG_DATOS_ELIMINADOS;
         else
            $msg = "<B> ERROR</B>. Error Eliminando Datos";
         break;

      //-***************** Muestra para Editar un usuario********************
      case "Mostrar":
         $rsUsuarioEditar = $rsUsuario->selectUsuario($_GET['id']);
         $nb_indicador = $rsUsuarioEditar->fields['NB_INDICADOR'];
         $nu_cedula = $rsUsuarioEditar->fields['NU_CEDULA'];
         $nb_nombre = $rsUsuarioEditar->fields['NB_NOMBRE'];
         $nu_telefono = $rsUsuarioEditar->fields['NU_TELEFONO'];
         $tx_direccion = $rsUsuarioEditar->fields['TX_DIRECCION'];
         $co_cargo = $rsUsuarioEditar->fields['NU_CARGO'];
         $in_planificador = $rsUsuarioEditar->fields['IN_PLANIFICADOR'];
         $co_cargo_planificador = $rsUsuarioEditar->fields['NU_CARGO_PLANIFICADOR'];
         $nu_username = $_GET['id'];
         $action = "cpanel.php?sistema=12";
         $etiqBoton = "Editar";
         break;
      
      case "NoExiste":
         $msg="<B>ERROR<B/> Intento de ingreso invalido";
         break;
   }
}
if ($RecordUsuario["usuario_administrador"] == 1)
   $ubic_tecnica = $RecordUsuario["usuario_ubic_administrador"];
else
   $ubic_tecnica = $RecordUsuario["usuario_ubic_tecnica"];
//**** Precarga los cargos ****
$rscargos = $rsUsuario->selectCargos($ubic_tecnica);
$sHTMLCargos = "<option value='0'>Seleccione una Opci&oacute;n</option>";
while (!$rscargos->EOF) {
   $sHTMLCargos.="<option value={$rscargos->fields['NU_CARGO']}>{$rscargos->fields['NB_CARGO']}</option>";
   $rscargos->MoveNext();
}
//**** fin *** precarga *******
?>

<!-- Los siguientes 2 script (librer�a y funcion) son para determinar si el indicador esta registrado -->
<script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="publico/js/jquery-ui.js"></script> 
<script type="text/javascript" src="publico/js/jquery.domsearch.js"></script>
<script type="text/javascript" src="publico/js/liquidmetal.js"></script>
<link rel="stylesheet" type="text/css" href="publico/estilos/jquery-ui-1.10.3.custom.min.css" >
<script>jQuery.noConflict();</script>
<!----- Para el shadow box ---->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript">
   Shadowbox.init({
      language: "es",
      modal: true,
      players: ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv', 'php'],
      overlayColor: "#000",
      overlayOpacity: "0.5"});
</script>

<?php include("registro_personal_datos_basicos.inc.php"); ?>
<div id="datos_existentes">
   <div class="titulodivision">Registros cargados:</div>
   <!-- buscador -->
   <form id="frmsearch" action="#">
      <label class="etiqueta" for="search">Buscar: </label>
      <input type="text" class="texto" name="search" value="" id="search"  />
   </form>
   <div style="overflow:auto; height:400px;">
      <table id="listaSaldos" class="tabla">
         <thead>
            <tr>
               <th scope="col">Nombre</th>
               <th scope="col">Indicador</th>
               <th scope="col">C.I.</th>
               <th scope="col">Cargo</th>
               <th scope="col">Tel&eacute;fono</th>
               <th scope="col" style="width:100px;">Opcion</th>
            </tr>
         </thead>
         <tbody>
<?php
$rsUsuarios = $rsUsuario->listaUsuarios($ubic_tecnica);
while (!$rsUsuarios->EOF) {
   ?>
               <tr>
                  <td class="nombre">     <?php echo utf8_decode($rsUsuarios->fields["NB_NOMBRE"]); ?></td>
                  <td class="indicador">  <?php echo $rsUsuarios->fields["NB_INDICADOR"]; ?></td>
                  <td class="cedula">     <?php echo $rsUsuarios->fields["NU_CEDULA"]; ?></td>
                  <td class="cargo">      <?php echo $rsUsuarios->fields["NB_CARGO"]; ?></td>
                  <td class="telefono">   <?php echo $rsUsuarios->fields["NU_TELEFONO"]; ?></td>
                  <td>
   <?php
   echo "<a href='cpanel.php?sistema=12&accion=Mostrar&id=", $rsUsuarios->fields["CO_USUARIO"], "'>Editar</a>";
   if ($rsUsuarios->fields['TOT_MENU'] == 0 && $rsUsuarios->fields['TOT_ACTIVIDAD'] <= 0) {
      echo " / <a href='cpanel.php?sistema=12&accion=Eliminar&id=", $rsUsuarios->fields["CO_USUARIO"], "'  onclick=\"return confirm('Esta seguro de eliminar ésta persona');\">Eliminar</a>";
   }
   echo "<br>";
   if ($rsUsuarios->fields['TOT_MENU'] == 0)
      $titulo = "Asignar";
   else
      $titulo = "Editar";
   /* echo "<a href='cpanel.php?sistema=121&id=", $rsUsuarios->fields["CO_USUARIO"], "'>Asignar Acceso</a>"; */
   echo "<a  href='#' onclick=\"OpenShadow('configuracion/registro_personal_roles.php?id={$rsUsuarios->fields["CO_USUARIO"]}');\">$titulo Acceso</a>";
   ?>
                  </td>
               </tr>
   <?php
   $rsUsuarios->MoveNext();
}
?>
         </tbody>

      </table>
   </div>
</div>
<script type="text/javascript">
   var jq = jQuery.noConflict();
   var x = {resultado: ""};

   /*funciones de precarga*/
   jq(document).ready(function() {
      // Para validar si el usuario existe
      jq('#search').domsearch('#listaSaldos tbody', {criteria: ['td.nombre', 'td.indicador', 'td.cedula', 'td.cargo', 'td.telefono']});
      jq('input[type="submit"]').removeAttr('disabled');
      jq('#username').change(function() {
         if (esUsuarioRegistrado(x)) {
            jq('#Info').hide();
            jq('#Info').html(x.resultado).fadeIn(1000);
         } else {
            jq('#Info').hide();
         }
         ;
      });

      jq("#progress_ldap").progressbar({value: false});

      jq('#username').bind('keyup blur', function() {
         jq(this).val(jq(this).val().replace(/[^a-zA-Z]/g, '').toUpperCase());
      });

      jq("#id_gestion").change();

      jq("#frmsearch").bind("keypress", function(e) {
         if (e.keyCode == 13) {
            return false;
         }
      });

      refrescaFoto();
   });

   /*Limpia los datos del formulario */
   function reiniciarFormulario() {
      jq('#username').val("");
      jq('#nombre_apellido').val("");
      jq('#cedula').val("");
      jq('#direccion').val("");
      jq('#telefono').val("");
      jq('#id_gestion').val(0);
      jq('#cargo').val(0);
      jq('#cargos_control_gestion').hide();
      jq('#accion').val("Guardar");
      jq('#fotopersonal').attr('src', '');
      jq('#Info').hide();
   }

   /*Validar antes de enviar*/
   function validar(frm)
   {
      var tst;
      if (frm.cargo.value == 0) {
         alert("Debe seleccionar un cargo.");
         return false;
      } else if (frm.username.value == '') {
         alert("Debe colocar un indicador");
         return false;
      } else {
         tst = esUsuarioLDAP(true);
         switch (tst) {
            case -1 :
               alert("El indicador no pertenece al dominio PDVSA o el campo est\u00e1 vac\u00edo");
               return false;
            case -2 :
               alert("La cedula no pertenece al indicador ingresado seg\u00fan los registros del dominio PDVSA,\nUtilice el icono para buscar datos en el dominio.");
               return false;
         }
      }
      if (jq('#accion').val() == "Guardar" && esUsuarioRegistrado(x)) {
         alert(x.resultado);
         return false;
      }
      return true;
   }

   /*Activa los cargos cuando es por gestion*/
   function ActivarCargoGestion(valor) {
      switch (valor) {
         case '1':
            jq('#cargos_control_gestion').show();
            jq('#titulo_cargos_control_gestion').show();
            break;
         default:
            jq('#cargos_control_gestion').hide();
            jq('#titulo_cargos_control_gestion').hide();
      }
      return true;
   }
   /*Refresca la foto del usuario*/
   function refrescaFoto() {
      jq("#fotopersonal").attr("src", jq("#url_foto").val() + '0' + jq("#cedula").val() + '.jpg');
   }

   function asignaNoDisp() {
      jq("#fotopersonal").attr("src", jq("#url_foto_nodisp").val());
      return true;
   }
   /*
    * Valida si es un usuario LDAP, si el parametro "validar=true" quiere decir que
    * revisará al usuario pero no reemplazara su información
    */
   function esUsuarioLDAP(validar) {

      if (typeof validar == 'undefined')
         validar = false;
      jq('#cargando-ldap').show();
      var pars = 'fnc=UsuarioLDAP&username=' + jq("#username").val();
      var jax = new Ajax.Request("aplicacion/servidor/_ajax/controlador.comun.jquery.php",
              {
                 method: 'post',
                 parameters: pars,
                 asynchronous: false,
                 onLoading: function(request) {
                 },
                 onSuccess: function(request) {
                    var JsonRec = eval('(' + request.responseText + ')');
                    if (!validar) {
                       if (JsonRec) {
                          jq("#nombre_apellido").val(JsonRec['givenName'] + ' ' + JsonRec['middleName'] + ' ' + JsonRec['sn']);
                          jq("#cedula").val(JsonRec['pdvsacom-ad-Cedula']);
                          jq("#direccion").val(JsonRec['l'] + ' ' + JsonRec['pdvsacom-AD-organization']);
                          refrescaFoto();
                       } else {
                          alert('No existe el usuario');
                          jq("#nombre_apellido").val('');
                          jq("#cedula").val('');
                          jq("#direccion").val('');
                          jq("#fotopersonal").attr('src', '');
                       }
                    }
                 },
                 onComplete: function(request) {
                    jq('#cargando-ldap').hide();
                 }
              }
      );
      if (validar) {
         if (jax.transport.responseText == 'null')
            return -1;
         else {
            var JsonRec = eval('(' + jax.transport.responseText + ')');
            if (JsonRec['pdvsacom-ad-Cedula'] != jq("#cedula").val())
               return -2
            else
               return 0;
         }
      }
   }

   /*
    *  Verifica en BD si existe
    */
   function esUsuarioRegistrado(msg) {

      var pars = 'fnc=UsuarioRegistrado&indicador=' + jq("#username").val();
      var jax = new Ajax.Request("aplicacion/servidor/_ajax/controlador.comun.jquery.php",
              {
                 method: 'post',
                 parameters: pars,
                 asynchronous: false,
                 onSuccess: function(request) {
                 }
              });
      msg.resultado = jax.transport.responseText;
      if (msg.resultado === '')
         return false;
      else
         return true;
   }

   function OpenShadow(sPath, pHeight, pWidth) {
      if (typeof (pHeight) === 'undefined')
         pHeight = 601;
      if (typeof (pWidth) === 'undefined')
         pWidth = 951;

      Shadowbox.open({
         content: sPath,
         player: "iframe",
         height: pHeight,
         width: pWidth
      });
   }
</script>
