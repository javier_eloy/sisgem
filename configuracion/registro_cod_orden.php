<?php
//require("aplicacion/configuracion/aut_lib.inc.php");
/////////////INSERTAR
// echo $_POST['id_parrouia'];
$objConfigura = new configuracion();
$msg="";
if(isset($_REQUEST['accion']))
switch ($_REQUEST['accion']) {
    case "Guardar":
        if ($objConfigura->InsRegistroNomenclatura($_POST['codigo'], $_POST['nombre'], $_POST['descripcion'], 0))
            $msg = MSG_DATOS_REGISTRADOS;
        else
            $msg = MSG_ERROR_TRANSACCION;
        break;
    case "Eliminar":
        if ($objConfigura->DelRegistroNomenclatura($_REQUEST['id']))
            $msg = MSG_DATOS_ELIMINADOS;
        else
            $msg = MSG_ERROR_TRANSACCION;
        break;
    case "Actualizar":
        if ($objConfigura->UpdRegistroNomenclatura($_POST['co_estado_orden'], $_POST['codigo'], $_POST['nombre'], $_POST['descripcion'], 0))
            $msg = MSG_DATOS_EDITADOS;
        else
            $msg = MSG_ERROR_TRANSACCION;
        break;
}
?>
<!-- Librerias JSON -->        
<script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="publico/js/prototype.js"></script>


<!--Funcionalidades específicas en Ajax --->
<script type="text/javascript" src="aplicacion/cliente/html_combos.js"></script>
<script type="text/javascript" src="aplicacion/cliente/html_tablas.js"></script>

<p align="right">
<table>
    <tr>
        <td><a href='cpanel.php?sistema=127' class="titulomodulo2">Estado de Actividades</a></td>
        <td><a href='cpanel.php?sistema=129' class="titulomenu">Motivos Cierre de Actividades</a></td>
        <td><a href='cpanel.php?sistema=125' class="titulomenu">Gesti&oacute;n de Personal</a></td>               
    </tr>
</table>
</p>
<div class="titulomodulo">&nbsp;</div>	
<form name="formulario" id="formulario" method="POST"  action="cpanel.php?sistema=127" onsubmit="return validar(this);">
    <table class="tablaformulario"  style="table-layout: fixed;">
        <col style="width:110px">
        <col>
        <tbody>
        <tr>
            <td><span class="etiqueta"><center>Siglas de c&oacute;digo<br>para Aplazamiento de Actividades<br>(3 caracteres)</center></span></td>
            <td valign="bottom"><span class="etiqueta">Motivo</span></td>
        </tr>
        <tr>
            <td ><input name="codigo" id="codigo" class=":required texto_min" onChange="javascript:this.value=this.value.toUpperCase();" type="text" size="12" maxlength="3"></td>
            <td><input name="nombre" id="nombre" class=":required texto" onChange="javascript:this.value=this.value.toUpperCase();" style="width:585px" type="text" maxlength="50"></td>
        </tr>
        <tr>
            <td><span class="etiqueta">Descripci&oacute;n</span></td>            
        </tr>
        <tr>
            <td colspan="2">
                <input name="descripcion" id="descripcion" class=":required texto"  style="width:700px" type="text" maxlength="50">
            </td>
        </tr>
        </tbody>
    </table>
    <input type="hidden" name="co_estado_orden" id="co_estado_orden">
    <div id="grupobtn" name="grupobtn" class="grupobotones">
        <input class="boton" id="accion" name="accion"  value="Guardar" type="submit">	
        <input class="boton"  value="Limpiar" type="reset" >
    </div>
    <?php if (strlen($msg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$msg**</span>"; ?>        
</form>

<div class="titulodivision">Registro Cargados:</div>
<div class="marcoContenido">
<table id="listaSaldos" class="tabla">
    <thead>
        <tr>
            <th scope="col" style="width:30px">C&oacute;digo</th>
            <th scope="col" style="width:300px" >Motivo</th>
            <th scope="col">Descripci&oacute;n</th>
            <th scope="col" style="width:90px">Opci&oacute;n</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $rsNomen = $objConfigura->SelecListaNomenclatura();
        while (!$rsNomen->EOF) {
            ?>		
            <tr>
                <td><?php echo $rsNomen->fields["VA_ESTATUS_ORDEN"]; ?></td>
                <td><?php echo utf8_decode($rsNomen->fields["NB_CO_ESTATUS_ORDEN"]); ?></td>
                <td title="<?php echo utf8_decode($rsNomen->fields["TX_DESCRIPCION"]); ?>"><?php echo utf8_decode($rsNomen->fields["TX_DESCRIPCION"]); ?></td>
                <td><a href='#' onclick="CargarNomen('<?php echo $rsNomen->fields["CO_ESTATUS_ORDEN"] ?>',$('formulario'));">Editar</a>
                    <?php if ($rsNomen->fields["esAsig"] <= 0) { ?>
                        / <a href='cpanel.php?sistema=127&accion=Eliminar&id=<?php echo $rsNomen->fields["CO_ESTATUS_ORDEN"]; ?>' onclick="return confirm('Esta seguro de eliminar este codigo');">Eliminar</a>
                    <?php } ?>
                </td>
            </tr>
            <?php
            $rsNomen->MoveNext();
        }
        ?>
    </tbody>
</table>
</div>
<script type="text/javascript">
    function validar(frm)
    {   var  l_co_estado_orden;
                
        if(frm.co_estado_orden.value) l_co_estado_orden=frm.co_estado_orden.value
        else l_co_estado_orden=0;

        if(EsCodigoRepetidoNomen(frm.codigo.value, l_co_estado_orden)) {
            alert("El codigo para el motivo de aplazamiento "+frm.codigo.value+" se encuentra en el sistema");
            return false;
        } else if(frm.codigo.value.length !=3) 
        {   
            alert("El código para el motivo de aplazamiento debe contener exactamente tres(3) caracteres alfanumericos");
            return false;           
        } else if(frm.nombre.value.length < 5) {
            alert("Falta el nombre del motivo de aplazamiento, debe contener al menos cinco(5) caracteres");
            return false;
        } else if(frm.descripcion.value.length < 5) {
            alert("Falta la descripción del motivo de aplazamiento, debe contener al menos cinco(5) caracteres");
            return false;
        }   
        return true;
    }
   
</script>
