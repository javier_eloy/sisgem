<?php
//require("aplicacion/configuracion/aut_lib.inc.php");
$objConfigura = new configuracion();

$msg = "";
if (isset($_REQUEST['accion']))
    switch ($_REQUEST['accion']) {
        case "Actualizar":
            $arrayGrpPto = $_REQUEST['chkgrp'];
            if ($objConfigura->InsCargoGrupoPuesto($_POST['nu_cargo'], $arrayGrpPto)) 
                $msg = MSG_DATOS_REGISTRADOS;
            else
                $msg = MSG_ERROR_TRANSACCION;
            
            break;
    }
    
  if ($RecordUsuario["usuario_administrador"]==1) $ubic_tecnica=$RecordUsuario["usuario_ubic_administrador"];
                                             else $ubic_tecnica=$RecordUsuario["usuario_ubic_tecnica"];    
?>

<!-- Librerias JSON -->        
<script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="publico/js/prototype.js"></script>
<script type="text/javascript" src="publico/js/jquery.popupWindow.js"></script>

<!--Funcionalidades específicas en Ajax --->
<script type="text/javascript" src="aplicacion/cliente/html_combos.js"></script>
<script type="text/javascript" src="aplicacion/cliente/html_tablas.js"></script>

<p align="right">
<table>
    <tr>
        <td><a href='cpanel.php?sistema=6' class="titulomenu">Registro de Cargos</a></td>
        <td><a href='cpanel.php?sistema=66' class="titulomodulo">Asignaci&oacute;n con Grupos (SGE)</a></td>
    </tr>
</table>
</p>

<div class="titulomodulo" >&nbsp;</div>	
<div id="panelContenido" >
    <form name="formulario" id="formulario" method="POST" action="cpanel.php?sistema=66" >
        <table class="tablaformulario">
            <tr>
                <td></td>
            </tr>            
            <tr>
                <td>
                    <div id="listagrp" >
                    </div>
                </td>            
            </tr>    
        </table>
        <div id="grupobtn" name="grupobtn" class="grupobotones" style="display:none;">
            <input class="boton"  id="accion" name="accion" value="Actualizar" type="submit">		
        </div>
        <?php if (strlen($msg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$msg**</span>"; ?>        
    </form>
</div>
<!-- Este span con su id="datos_existentes" es la referencia para mostrar las tablas
 de los registros ya cargados seg�n la selecci�n de la Regi�n o Divisi�n -->
<span id="datos_existentes">    
    <div class="titulodivision">Lista de Cargos:</div>	
    <table id="listaSaldos" class="tabla">
        <thead>
            <tr>
                <th scope="col" width="100px">Codificaci&oacute;n</th>
                <th scope="col">Nombre del Cargo</th>
                <th scope="col">Ubicaci&oacute;n T&eacute;cnica</th>
                <th scope="col" width="90px">Opci&oacute;n</th>
            </tr>
        </thead>
        <tbody>

            <?php
            $rsConfigura = $objConfigura->SelecListaCargoEditable($ubic_tecnica);
            while (!$rsConfigura->EOF) {
                echo "<tr>";
                echo "<td>" . $rsConfigura->fields["Codigo"] . "</td>";
                echo "<td title=\"" . $rsConfigura->fields["NB_CARGO"] . "\">" . $rsConfigura->fields["NB_CARGO"] . "</td>";
                echo "<td>" . $rsConfigura->fields["CO_UBIC_TECNICA"] . "</td>";
                echo "<td><a href='#' onclick=\"return SelecGrupoPto('grupobtn','listagrp','1A00',".$rsConfigura->fields["NU_CARGO"] . ");\">Editar</a> ";
                echo "/ <a href='#' onclick=\"return MostarGrupoPto('1A00','".$rsConfigura->fields["NU_CARGO"]."')\"  title=\"Muestra los grupos asignados\">Mostrar</a> ";
                echo "</tr>";
                $rsConfigura->MoveNext();
            }
            ?>

        </tbody>
    </table>
</span>         
