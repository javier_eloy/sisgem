<?php
$objConfigura = new configuracion();

$msg = "";
if (isset($_REQUEST['accion']))
    switch ($_REQUEST['accion']) {
        case "Guardar":
            if ($objConfigura->InsRegistroCargo($_POST["_nb_cargo"], $_POST["_tx_descripcion"], $_POST["_co_cargo"],
                                                $_POST["_co_cargo_padre"], $_POST["_co_ubic_tecnica"], 
                                                (isset($_POST["_in_requerido"]) ? $_POST["_in_requerido"] : "0")))
                $msg = MSG_DATOS_REGISTRADOS;
            else
                $msg = MSG_ERROR_TRANSACCION;

            break;
        case "Eliminar":
            if ($objConfigura->DelRegistroCargo($_REQUEST['id']))
                $msg = MSG_DATOS_ELIMINADOS;
            else
                $msg = MSG_ERROR_TRANSACCION;
            break;
        case "Actualizar":
            if ($objConfigura->UpdRegistroCargo($_POST["_nu_cargo"], $_POST["_nb_cargo"], $_POST["_tx_descripcion"], 
                                                $_POST["_co_cargo"], 
                                                $_POST["_co_cargo_padre"], 
                                                $_POST["_codigo_original"],
                                                $_POST["_co_ubic_tecnica"], 
                                                $_POST['_co_ubic_tecnica_original'],
                                                (isset($_POST["_in_requerido"]) ? $_POST["_in_requerido"] : "0")))
                $msg = MSG_DATOS_EDITADOS;
            else
                $msg = MSG_ERROR_TRANSACCION;
            break;
    }
  if ($RecordUsuario["usuario_administrador"]==1) $ubic_tecnica=$RecordUsuario["usuario_ubic_administrador"];
                                             else $ubic_tecnica=$RecordUsuario["usuario_ubic_tecnica"];

    $sMenuLink=CodificarGet("sistema=6");
    $sMenuLink2=CodificarGet("sistema=66");
?>

<!-- Librerias JSON -->        
<script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="publico/js/prototype.js"></script>


<!--Funcionalidades específicas en Ajax --->
<script type="text/javascript" src="aplicacion/cliente/html_combos.js"></script>
<script type="text/javascript" src="aplicacion/cliente/html_tablas.js"></script>

<p align="right">
    <table>
        <tr>
            <td><a href='cpanel.php?k=<?php echo $sMenuLink;?>' class="titulomodulo">Registro de Cargos</a></td>
            <td><a href='cpanel.php?k=<?php echo $sMenuLink2;?>' class="titulomenu">Asignaci&oacute;n con Grupos (SGE)</a></td>
        </tr>
    </table>
</p>

<div class="titulomodulo" onclick="var pnl=document.getElementById('panelContenido'); if(pnl.style.display=='none') pnl.style.display='block'; else pnl.style.display='none';">&nbsp;</div>	
<div id="panelContenido">
    <form name="formulario" id="formulario" method="POST" action="cpanel.php?k=<?php echo $sMenuLink;?>" onsubmit="return validar(this);">
        <input type="hidden" id="_codigo_original"  name="_codigo_original" value="">
        <input type="hidden" id="_co_ubic_tecnica_original"  name="_co_ubic_tecnica_original" value="">
        <table class="tablaformulario" style="table-layout: fixed;">
            <col style="width: 130px;"/>
            <col style="width:220px"/>
            <col />
            <col />
            <tbody>
            <tr>
                <td colspan="2"><span class="etiqueta">Cargo Superior</span></td>
                <td colspan="2"><span class="etiqueta">Ubicaci&oacute;n T&eacute;cnica (SGE)</span></td>
            </tr>
            <tr>       
                <td colspan="2">
                    <input type="hidden" id="_co_cargo_padre"  name="_co_cargo_padre" value="">
                    <div id="divcargopadre" name="divcargopadre"> Cargando...<!---<select id="_nu_cargo_padre"> ---></select></div>
                </td>
                <td colspan="2">
                    <div id="div_ubic_tecnica" name="div_ubic_tecnica">Cargando...<!---<select id="_co_ubic_tecnica">---> </select> </div>
                </td>
            </tr>
            </tbody>
            <tbody>
            <tr>
                <td colspan="1"><span class="etiqueta">C&oacute;digo del Cargo</span></td>
                <td colspan="3"><span class="etiqueta">Nombre del cargo</span></td>
            </tr>
            <tr>
                <td colspan="1">
                    <input type="hidden" name="_nu_cargo" id="_nu_cargo">
                    <input  name="_co_cargo" class=":required texto_min" onChange="javascript:this.value=this.value.toUpperCase();" type="text" size="15" maxlength="3">
                </td>
                <td colspan="3">
                    <input  name="_nb_cargo" class=":required texto" onChange="javascript:this.value=this.value.toUpperCase();" type="text" size="65">
                </td>
            </tr>            
            <tr>
                <td colspan="4"><span class="etiqueta">Descripci&oacute;n del Cargo</span></td>
            </tr>
            <tr>
                <td colspan="4"><input  name="_tx_descripcion" class=":required texto" onChange="javascript:this.value=this.value.toUpperCase();" type="text" size="92" maxlength="50"></td>
            </tr>
            <tr>
                <td colspan="4">
                    <input  name="_in_requerido" id="_in_requerido" type="checkbox" value="1"><span class="etiqueta">Requiere Reemplazo por Ausencia</span><br>
                </td>
            </tr>
            </tbody>
        </table>
        <div id="grupobtn" name="grupobtn" class="grupobotones">
            <input class="boton"  id="accion" name="accion" value="Guardar" type="submit">		
            <input class="boton"  value="Limpiar" type="reset" >
        </div>
        <?php if (strlen($msg)>0) echo " <span style=\"color:#f00;font-size:x-small;\">**$msg**</span>"; ?>        
    </form>
</div>
<!-- Este span con su id="datos_existentes" es la referencia para mostrar las tablas
 de los registros ya cargados seg�n la selecci�n de la Regi�n o Divisi�n -->
<div id="datos_existentes">
    <div class="titulodivision">Lista de Cargos:</div>
    <div class="marcoContenido">
    <table id="listaSaldos" class="tabla">
        <thead>
            <tr>
                <th scope="col" width="100px">Codificaci&oacute;n</th>
                <th scope="col">Nombre del Cargo</th>
                <th scope="col">Ubicaci&oacute;n T&eacute;cnica</th>
                <th scope="col" width="90px">Opci&oacute;n</th>
            </tr>
        </thead>
        <tbody>
       
        <?php
        $rsConfigura = $objConfigura->SelecListaCargoEditable($ubic_tecnica);
        $shtml="";
        while (!$rsConfigura->EOF) {
            $shtml.= "<tr>";
            $shtml.="<td>" . $rsConfigura->fields["Codigo"] . "</td>";
            $shtml.="<td title=\"".$rsConfigura->fields["NB_CARGO"]."\">" . $rsConfigura->fields["NB_CARGO"] . "</td>";
            $shtml.="<td>" . $rsConfigura->fields["CO_UBIC_TECNICA"] . "</td>";
            $shtml.="<td><a href='#' onclick=\"CargarCargo(" . $rsConfigura->fields["NU_CARGO"] . ",$('formulario'));\">Editar</a>";
            if ($rsConfigura->fields['esSup'] == 0 && $rsConfigura->fields['esAsg'] == 0)
                $shtml.=" / <a href='cpanel.php?sistema=6&accion=Eliminar&id=" . $rsConfigura->fields["NU_CARGO"] . "' onclick=\"return confirm('Esta seguro de eliminar el Cargo');\">Eliminar</a>";
            $shtml.= "</td>";
            $shtml.="</tr>";
            $rsConfigura->MoveNext();
        }
        echo $shtml;
        ?>

        </tbody>
    </table>
       </div >
</div>
 <script type="text/javascript">
            var ubic_tecnica='<?php echo $ubic_tecnica; ?>';
            
            SelectCargos('divcargopadre','_nu_cargo_padre',0,ubic_tecnica,'return SelectUbicacionesTecnicasCargo("div_ubic_tecnica","_co_ubic_tecnica",_co_ubic_tecnica.value,this);');
            SelectUbicacionesTecnicas('div_ubic_tecnica','_co_ubic_tecnica','',ubic_tecnica);
   
            function validar(frm)
            {   var l_nu_cargo, lnu_cargo_estructura;
       
                if(!frm._nu_cargo.value) l_nu_cargo=0  
                else  l_nu_cargo=frm._nu_cargo.value;
                
                if(frm._nu_cargo_padre.value!=0)  
                    lnu_cargo_estructura0=frm._nu_cargo_padre.options[frm._nu_cargo_padre.selectedIndex].text.split("-")[0];
                else lnu_cargo_estructura0 = "";

                lnu_cargo_estructura=lnu_cargo_estructura0.trim()+frm._co_cargo.value;
                if(EsCodigoRepetidoCargo(lnu_cargo_estructura, l_nu_cargo)) {
                    alert("El código del cargo "+frm._co_cargo.value+" se encuentra en el sistema");
                    return false;
                } else if(frm._co_cargo.value.length !=3) 
                {   
                    alert("El código del cargo debe ser de 3 caracteres");
                    return false;           
                } else if(frm._tx_descripcion.value.length < 5) {
                    alert("Falta la descripción del cargo, debe contener al menos cinco caracteres");
                    return false;
                } else if(frm._co_ubic_tecnica.selectedIndex <= 0) {
                    alert("Falta la ubicación Técnica (SAP) que permita acceder a los equipos e instalaciones");
                    return false;
                }else if(frm._nb_cargo.value.length <= 5) {
                    alert("Falta el nombre del cargo, debe contener al menos cinco caracteres");
                    return false;          
                }
                if (l_nu_cargo == lnu_cargo_estructura0 && lnu_cargo_estructura0 > 0)
                {
                    alert("El cargo actual no puede tener como padre él mismo");
                    return false;
                }    
                
                frm._co_cargo_padre.value=lnu_cargo_estructura0.trim();
                return true;
            }
   
</script>
