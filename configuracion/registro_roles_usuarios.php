<?php
require_once('../aplicacion/configuracion/aut_lib.inc.php');
$objConfigura = new configuracion();
$co_rol = $_REQUEST['co_rol'];
$nb_rol = $_REQUEST['nb_rol'];
$rsUsuario = $objConfigura->MostrarRolUsuario($co_rol);
?>
<html>
   <head>
      <link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">
   </head>
   <body  style="background-color:white;">
      <h1 class="titulomodulo">Lista de Usuarios para el Rol:<br> <?php echo urldecode($nb_rol);?></h1>
      <hr>
      <?php
      while (!$rsUsuario->EOF) {
         echo "<div>(" , $rsUsuario->fields["NB_INDICADOR"] , ") " , utf8_decode($rsUsuario->fields["NB_NOMBRE"] ), "</div>";
         $rsUsuario->MoveNext();
      }
       $rsUsuario->Close();
      ?>
   </body>
</html>