<!-- REGISTRO DE clases ES REGISTRO DE REGI�N Y/O DIVISI�N -->
<?php
//require("aplicacion/configuracion/aut_lib.inc.php");
/////////////INSERTAR
// echo $_POST['id_parrouia'];
$objConfigura = new configuracion();
$msg="";
$activo = false;
if($_POST['activo']=='on')
    $activo = true;
    
if(isset($_REQUEST['accion']))
switch ($_REQUEST['accion']) {
    case "Guardar":
        if ($objConfigura->InsRegistroMotivoCierre($_POST['codigo'], $_POST['descripcion'], $_POST['motivoCierre'], true))
            $msg = MSG_DATOS_REGISTRADOS;
        else
            $msg = MSG_ERROR_TRANSACCION;
        break;
    case "Eliminar":
        if ($objConfigura->DelRegistroMotivoCierre($_POST['codigo']))
            $msg = MSG_DATOS_ELIMINADOS;
        else
            $msg = MSG_ERROR_TRANSACCION;
        break;
    case "Actualizar":
        if ($objConfigura->UpdRegistroMotivoCierre($_POST['codigo'], $_POST['descripcion'], $_POST['motivoCierre'], $activo))
            $msg = MSG_DATOS_EDITADOS;
        else
            $msg = MSG_ERROR_TRANSACCION;
        break;
}
?>
<!-- Librerias JSON -->        
<script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="publico/js/prototype.js"></script>


<!--Funcionalidades específicas en Ajax --->
<script type="text/javascript" src="aplicacion/cliente/html_combos.js"></script>
<script type="text/javascript" src="aplicacion/cliente/html_tablas.js"></script>

<p align="right">
<table>
    <tr>
        <td><a href='cpanel.php?sistema=127' class="titulomenu">Estado de Actividades</a></td>
        <td><a href='cpanel.php?sistema=129' class="titulomodulo2">Motivos Cierre de Actividades</a></td>
        <td><a href='cpanel.php?sistema=125' class="titulomenu">Gesti&oacute;n de Personal</a></td>               
    </tr>
</table>
</p>
<div class="titulomodulo">&nbsp;</div>	
<form name="formulario" id="formulario" method="POST"  action="cpanel.php?sistema=129" onsubmit="return validar(this);">
    <table class="tablaformulario"  style="table-layout: fixed;">
        <col style="width:110px">
        <col>
        <tbody>
        <tr>
            <td style="horizontal-align:center">
               <span class="etiqueta">Siglas de c&oacute;digo<br>para cierre de actividades<br>(3 caracteres)</span>
            </td>
            <td valign="bottom" style="width:430px"><span class="etiqueta">Descripci&oacute;n</span></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
               <input name="codigo" id="codigo" class=":required texto_min" onChange="javascript:this.value=this.value.toUpperCase();" type="text" size="10" maxlength="3">
            </td>
            <td>
               <input name="descripcion" id="descripcion" class=":required texto" style="width:400px" onChange="javascript:this.value=this.value.toUpperCase();" type="text" maxlength="60">
            </td>
            <td >
                <input name="activo" id="activo" type="checkBox">
                <span class="etiqueta">Activo</span>
            </td>
        </tr>
        <tr>
            <td><span class="etiqueta">Tipo</span></td>
           
        </tr>
        <tr>            
            <td colspan="2">
                <select name="motivoCierre" class=":required texto" id="motivoCierre" >
                    <option value = "-1">Seleccione una opci&oacute;n...</option>
                    <option value =  "0">Cierre</option>
                    <option value =  "1">Omisi&oacute;n</option>                                                
                </select>
            </td>
           
        </tr>
        </tbody>
    </table>
    <input type="hidden" name="nu_motivo_cierre" id="co_estado_orden">
    <div id="grupobtn" name="grupobtn" class="grupobotones">
        <input class="boton" id="accion" name="accion"  value="Guardar" type="submit">	
        <input class="boton"  value="Limpiar" type="reset" >
    </div>
    <?php if (strlen($msg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$msg**</span>"; ?>        
</form>

<div class="titulodivision">Registro Cargados:</div>
<div class="marcoContenido">
<table id="listaSaldos" class="tabla">
    <thead>
        <tr>
            <th scope="col" style="width:30px">C&oacute;digo</th>
            <th scope="col">Descripci&oacute;n</th>
            <th scope="col">Tipo</th>
            <th scope="col">Estado</th>
            <th scope="col" style="width:90px">Opci&oacute;n</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $rsMotivoCierre = $objConfigura->SelecListaMotivoCierre();
        while (!$rsMotivoCierre->EOF) {
            ?>		
            <tr>
                <td><?php echo $rsMotivoCierre->fields["CO_MOTIVO_CIERRE"]; ?></td>
                <td><?php echo $rsMotivoCierre->fields["TX_DESCRIPCION"]; ?></td>
                <td title="<?php echo $rsMotivoCierre->fields["CO_TIPO"]; ?>"> 
                    <?php 
                        switch ($rsMotivoCierre->fields["CO_TIPO"]){
                            case 0:
                                echo '<span class="etiqueta">Cierre</span>';
                                break;
                            case 1:
                                echo '<span class="etiqueta">Omisi&oacute;n</span>';
                                break;                            
                        }
                    ?>
                </td>
                <td title="<?php echo $rsMotivoCierre->fields["IN_ACTIVO"]; ?>"><?php if($rsMotivoCierre->fields["IN_ACTIVO"]) echo '<span class="etiqueta">Activo</span>'; else echo '<span class="etiqueta">Inactivo</span>'; ?></td>
                <td><a href='#' onclick="CargarMotivo('<?php echo $rsMotivoCierre->fields["NU_MOTIVO_CIERRE"] ?>',$('formulario'));">Editar</a></td>
            </tr>
            <?php
            $rsMotivoCierre->MoveNext();
        }
        ?>
    </tbody>
</table>
</div>
<script type="text/javascript">
    function validar(frm)
    {   var  l_co_estado_orden;
                
        if(frm.co_estado_orden.value) l_co_estado_orden=frm.co_estado_orden.value
        else l_co_estado_orden=0;

        if(EsCodigoRepetidoMotivoCierre(frm.codigo.value, l_co_estado_orden)) {
            alert("El codigo para el motivo de cierre de actividad "+frm.codigo.value+" se encuentra en el sistema");
            return false;
        } else if(frm.codigo.value.length !=3) 
        {   
            alert("El código para el motivo de cierre de actividad debe contener exactamente tres(3) caracteres alfanumericos");
            return false;           
        } else if(frm.descripcion.value.length < 5) {
            alert("La descripcion de cierre de actividad debe contener al menos cinco(5) caracteres");
            return false;
        } else if(frm.motivoCierre.value < 0) {
            alert("Debe definir el tipo de cierre de actividad");
            return false;
        }   
        return true;
    }
   
</script>
