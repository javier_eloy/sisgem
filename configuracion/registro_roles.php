<?php
$objConfigura = new configuracion();
$msg = "";

if (isset($_REQUEST['accion'])) {
   $admin = (isset($_POST['admin'])) ? 1 : 0;
   $coubictecadmin = (isset($_POST['co_ubic_tecnica'])) ? $_POST['co_ubic_tecnica'] : 0;
   if ($admin != 1)
      $coubictecadmin = ""; /* Valida que si no es administrador que no guarde la ubic tec */

   switch ($_REQUEST['accion']) {
      case "Guardar":
         if ($objConfigura->InsertRole($_POST['nombre'], $_POST['descripcion'], $coubictecadmin, $admin, $_POST['lstPermiso']))
            $msg = MSG_DATOS_REGISTRADOS;
         else
            $msg = MSG_ERROR_TRANSACCION;
         break;
      case "Actualizar":
         if ($objConfigura->UpdateRole($_POST['co_rol'], $_POST['nombre'], $_POST['descripcion'], $coubictecadmin, $admin, $_POST['lstPermiso']))
            $msg = MSG_DATOS_EDITADOS;
         else
            $msg = MSG_ERROR_TRANSACCION;
         break;
   }
}

if ($RecordUsuario["usuario_administrador"] === "1") {
   $objInventario = new inventarioSAP();
   $arrayUbicNivel1 = $objInventario->SelecUbicacionTecnica("__");
   $arraUbicNivel2 = $objInventario->SelecUbicacionTecnica("____");
   $arraUbicNivel3 = $objInventario->SelecUbicacionTecnica("______");
   $arrayUbic = array_merge($arrayUbicNivel1, $arraUbicNivel2, $arraUbicNivel3);
   /* Ordena el Arreglo */
   foreach ($arrayUbic as $key => $row) {
      $volume[$key] = $row[$key]['TPLNR'];
   }
   array_multisort($volume, $arrayUbic);
   /* fin - Ordenar */

   $sMenuLink = CodificarGet("sistema=123");
}
?>
<!-- Librerias JSON -->
<script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="publico/js/prototype.js"></script>


<!--Funcionalidades específicas en Ajax --->
<script type="text/javascript" src="aplicacion/cliente/html_combos.js"></script>
<script type="text/javascript" src="aplicacion/cliente/html_tablas.js"></script>

<!-- style PARA MOSTRAR UN EXTO FLOTANTE SOBRE COMPONENTES -->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript">
   Shadowbox.init({
      language: "es",
      modal: true,
      players: ['html', 'php'],
      overlayColor: "#000",
      overlayOpacity: "0.5"});
</script>

<div class="titulomodulo">Registro de Roles</div>
<form name="formulario" id="formulario" method="POST" action="cpanel.php?k=<?php echo $sMenuLink ?>" >
   <div id="divformulario" >
      <table class="tablaformulario">
         <tr>
            <td valign="bottom"><span class="etiqueta">Nombre</span></td>
            <td><span class="etiqueta">Descripci&oacute;n</span></td>
         </tr>
         <tr>
            <td><input name="nombre" id="nombre" class=":required texto_min" onChange="javascript:this.value = this.value.toUpperCase();" type="text" style="width:200px"  maxlength="80"></td>
            <td><input name="descripcion" id="descripcion" class=":required texto" type="text" style="width:450px" maxlength="200"></td>
         </tr>
         <tr>
            <td  colspan="2" id="chkAdmin">
               <?php
               $str = "";
               if ($RecordUsuario["usuario_administrador"] === "1") {
                  $str = "<br>";
                  $str.="<input name=\"admin\" id=\"admin\" type=\"checkbox\" value=\"1\">Administrador de la Aplicaci&oacute;n";
               }
               echo $str;
               ?>
            </td>
         </tr>
         <tr>
            <td colspan="2" id="selectAdmin">              
               <?php
               if ($RecordUsuario["usuario_administrador"] === "1") {
                  $countUbic = count($arrayUbic);
                  $str = "<tr><td colspan=2 ><span class=\"etiqueta\">Ubicaci&oacute;n T&eacute;cnica</span></td></tr>";
                  $str.="<tr><td colspan=2 ><select id=\"co_ubic_tecnica\" name=\"co_ubic_tecnica\" style=\"width:400px\">";
                  $str.="<option value='0'> Seleccione... </option>";
                  for ($i = 1; $i < $countUbic; $i++) {
                     $str.="<option value='" . $arrayUbic[$i]['TPLNR'] . "'> (" . $arrayUbic[$i]['TPLNR'] . ") " . utf8_encode($arrayUbic[$i]['PLTXU']) . "</option>";
                  }
                  $str.= "</select><td><tr>";
               }
               echo $str;
               ?>
            </td>
         </tr>
         <tr><td colspan="2"><hr></td></tr>

      </table>

      <div id="chkRoles"><span style="color:green;">.... Cargando accesos de men&uacute;s ...<span></div>
               <input type="hidden" id="co_rol" name="co_rol" value="0">
               <div id="grupobtn"  class="grupobotones">
                  <input class="submit boton" name="accion" value="Guardar" type="submit" >
                  <input class="boton"  value="Limpiar" type="reset">
               </div>
               </div>
               <?php if (strlen($msg) > 0) echo " <span id=\"spanMsg\" style=\"color:#f00;font-size:x-small;\">**$msg**</span>"; ?>
               </form>

               <div class="titulodivision">Registro Cargados:</div>
               <div class="marcoContenido">
                  <table id="listaSaldos" class="tabla">
                     <thead>
                        <tr>
                           <th scope="col" style="width:300px" >Nombre</th>
                           <th scope="col">Descripci&oacute;n</th>
                           <th scope="col" style="width:140px">Opci&oacute;n</th>
                        </tr>
                     </thead>
                     <tbody>

                        <?php
                        $rsRoles = $objConfigura->SelecRoles();
                        while (!$rsRoles->EOF) {

                           $str = "<tr>";
                           $str.= "<td>" . utf8_decode($rsRoles->fields['NB_ROL']) . "</td>";
                           $str.="<td title=\"" . utf8_decode($rsRoles->fields['TX_DESCRIPCION']) . "\">" . utf8_decode($rsRoles->fields['TX_DESCRIPCION']) . "</td>";

                           $str.="<td nowrap><a  class=\"enlaceblanco\" onclick=\"CargarRol('{$rsRoles->fields['CO_ROL']}',$('formulario'),'#divformulario','#chkRoles','#co_rol','#grupobtn',{$RecordUsuario["usuario_administrador"]});\" title=\"Modifica los accesos de menu\">Editar</a>";

                           if ($rsRoles->fields['esAsig'] <= 0) {
                              $str.=" / <a class=\"enlaceblanco\" onclick=\"return cliEliminar({$rsRoles->fields['CO_ROL']});\" title=\"Elimina el rol\">Eliminar</a>";
                           } else {
                              $str.=" / <a class=\"enlaceblanco\" onclick=\"return MostrarRol({$rsRoles->fields['CO_ROL']},'".urlencode($rsRoles->fields['NB_ROL'])."');\" title=\"Muestra la lista de usuarios asociados al rol\"><span style=\"color:green;font-style:italic;\">Usuarios</span></a>";
                           }

                           $str.="</td>";
                           $str.="</tr>";

                           echo $str;
                           $rsRoles->MoveNext();
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
               <script type="text/javascript">
                  var adm =<?php echo $RecordUsuario["usuario_administrador"] ?>;
                  var jq = jQuery.noConflict();

                  /*funciones de precarga*/
                  jq(document).ready(function() {

//                     jq('#nombre').bind('keyup blur', function() {
//                        jq(this).val(jq(this).val().replace(/[^a-zA-Z]/g, '').toUpperCase());
//                     });

                     jq("#formulario").attr("onsubmit", "return Enviar();")
                     CargarNuevoRol('#chkRoles', adm);

                     jq("#admin").change(function() {
                        if (!jq(this).is(":checked"))
                           jq("#co_ubic_tecnica").val("0");
                     });
                  });

                  function Enviar()
                  {

                     if (EsCodigoRepetidoRol(jq("#nombre").val(), jq("#co_rol").val()) === 1) {
                        alert("La Nomenclatura para el tipo de labor " + frm.codigo.value + " se encuentra en el sistema");
                        return false;

                     } else if (jq("#nombre").val().length < 5)
                     {
                        alert("El nombre del rol debe contener al menos 5 caracteres alfanumericos");
                        return false;
                     } else if (jq("#descripcion").val().length < 5) {
                        alert("Falta la descripción de la nomenclatura, debe contener al menos cinco caracteres");
                        return false;
                     } else if (jq("#admin").prop("checked") && jq("#co_ubic_tecnica").prop("selectedIndex") < 1)
                     {
                        alert("El rol ha sido marcado como administrador, debe seleccionar la Ubicacion Tecnica a la que pertenece");
                        return false;
                     }

                     ///jq("#formulario").attr('action',"configuracion/registro_roles.php?accion=Guardar");
                     //jq("#formulario").submit();
                     return true;
                  }


                  function Cancelar() {
                     jq('#nombre').val('');
                     jq('#descripcion').val('');
                     jq('#chkRoles').empty();
                     jq('#spanMsg').empty();
                     jq('#co_rol').val('0');
                     jq('#co_ubic_tecnica').val('0');
                     jq('#grupobtn').html('<input class="boton"  id="accion" name="accion" value="Guardar" type="submit" > &nbsp;' +
                             '<input class="boton"  value="Limpiar" type="reset" >');
                     CargarNuevoRol('#chkRoles', adm);
                  }

                  function cliEliminar(co_rol) {

                     if (confirm('Esta seguro de eliminar \u00e9ste codigo')) {
                        if (EliminaRol(co_rol)) {
                           jq("#formulario").removeAttr("onsubmit");
                           jq("#formulario").submit();
                        } else
                           jq("#spanMsg").val("<?php echo MSG_ERROR_TRANSACCION; ?>");
                     }
                     return true;
                  }


                  function MostrarRol(co_rol,nb_rol) {
                        OpenShadow('configuracion/registro_roles_usuarios.php?co_rol=' + co_rol+'&nb_rol='+nb_rol, 350, 310);
                  }

                  function OpenShadow(sPath, pHeight, pWidth) {
                     if (typeof (pHeight) === 'undefined')
                        pHeight = 511;
                     if (typeof (pWidth) === 'undefined')
                        pWidth = 801;

                     Shadowbox.open({
                        content: sPath,
                        player: "iframe",
                        height: pHeight,
                        width: pWidth
                     });
                  }

               </script>


