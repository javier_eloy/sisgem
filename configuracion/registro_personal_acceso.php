<html>
   <?php
   require_once("../aplicacion/configuracion/aut_lib.inc.php");
//require("aplicacion/configuracion/aut_lib.inc.php");
   $rsMenuUsr = new registro_personal();
   $VectorConsulTabla = $rsMenuUsr->listaMenuUsuario($_GET['id']);
   ?>
   <head>
    <script type="text/javascript" src="../aplicacion/cliente/sesion.js"></script><!--*-->
    <link   rel="stylesheet"   type="text/css"  href="../publico/js/Archivos/tools.css">
    <link   rel="stylesheet"   type="text/css"  href="../publico/js/Archivos/estilos.css">
    <link   rel="stylesheet"       type="text/css" href="../publico/estilos/jquery-ui-1.10.3.custom.min.css" >
    <link   rel="stylesheet"       type="text/css" href="../publico/js/Archivos/styleestancia.css" ><!--*-->
   </head>
   <body  bgcolor="#ffffff" >
      <div class="titulomodulo">Configuraci&oacute;n Niveles de Acceso</div>
      <div class="f10bold">PERSONAL: <?php echo utf8_decode($VectorConsulTabla->fields['NB_NOMBRE']) ?> (<?php echo $VectorConsulTabla->fields['NB_INDICADOR']; ?>)</div>
      <form method="POST" action="cpanel.php?sistema=12&accion=Acceso&id=<?php echo $VectorConsulTabla->fields['CO_USUARIO'] ?>">
         <table id="listaSaldos" class="tabla">
            <thead>
               <tr>
                  <th scope="col">Bloque de men&uacute;s</th>
                  <th scope="col">Descripci&oacute;n</th>
                  <th scope="col">Opci&oacute;n</th>
               </tr>
            </thead>
            <tbody>
               <?php
               $conteo = 0;
               while (!$VectorConsulTabla->EOF) {
                  ?>
                  <tr>
                     <td><?php echo $VectorConsulTabla->fields['chg_nombreBloque']; ?> </td>
                     <td WIDTH="100%"><?php echo utf8_decode($VectorConsulTabla->fields['chg_descriBloque']); ?> </td>
                     <td>
                        <input type="checkbox" <?php echo "name=\"vectorbloque[$conteo]\""; ?> value="<?php echo utf8_decode($VectorConsulTabla->fields['ing_idBloque']); ?>"
                               <?php if ($VectorConsulTabla->fields['ing_codigoBloque'] == $VectorConsulTabla->fields['ing_idBloque']) echo 'checked="checked"'; ?>/>
                     </td>
                  </tr>
                  <?php
                  $conteo++;
                  $VectorConsulTabla->MoveNext();
               }
               ?>
            </tbody>
         </table>
         <div class="grupobotones">
            <input class="boton"  value="Limpiar" type="reset" >
            <input class="submit boton" value="Guardar" id="botonsiguiente" type="submit">
            <input class="submit boton" value="Volver" id="botonsiguiente" type="submit" onclick='window.parent.Shadowbox.close();'>
         </div>
      </form>
   </body>
</html>
