<?php
require_once("../aplicacion/configuracion/aut_lib.inc.php");
$msg="";
$id_user = $_REQUEST['id'];
$in_admin = $_SESSION['usuario_administrador'];
$ubic_admin= $_SESSION['usuario_ubic_administrador'];

$rsMenuUsr = new registro_personal();

if (isset($_POST["Acceso"])) {
            $rol_id=$_POST['chkrole'];
            //-***************** Edita los accesos de un usuario ********************
            if ($rsMenuUsr->actualizaRolesUsuario($id_user, $rol_id))
                $msg = "Acceso del Personal Registrado";
            else
                $msg = "<B>ERROR</B> Modificando los Accesos del Personal";
  }
 $rsUsuario = $rsMenuUsr->selectUsuario($id_user);
 $rsRolesUsuarios = $rsMenuUsr->listaRolesUsuario($id_user, $in_admin, $ubic_admin);
?>
<html>
   <head>
      <script type="text/javascript" src="../aplicacion/cliente/sesion.js"></script><!--*-->
      <link   rel="stylesheet"   type="text/css"  href="../publico/js/Archivos/tools.css">
      <link   rel="stylesheet"   type="text/css"  href="../publico/js/Archivos/estilos.css">
      <link   rel="stylesheet"       type="text/css" href="../publico/estilos/jquery-ui-1.10.3.custom.min.css" >
      <link   rel="stylesheet"       type="text/css" href="../publico/js/Archivos/styleestancia.css" ><!--*-->
   </head>
   <body  bgcolor="#ffffff"  onload="setTimeout(function (){document.getElementById('Info').style.display='none'}, 5000); return false" >
      <div class="titulomodulo">Configuraci&oacute;n Niveles de Acceso</div>
      <div class="f10bold">PERSONAL: <?php echo utf8_decode($rsUsuario->fields['NB_NOMBRE']) ?> (<?php echo $rsUsuario->fields['NB_INDICADOR']; ?>)</div>
      <div id="Info" class="mensaje_personal" style="display: block;" ><?php echo $msg;?></div>
      <form method="POST" action="">
         <input type="hidden" name="Acceso" value="Acceso">
         <input type="hidden" name="id" value="<?php echo $id_user; ?>">
         <table id="listaSaldos" class="tabla">
            <thead>
               <tr>
                  <th width="200px" scope="col">Rol</th>
                  <th width="700px" scope="col">Descripci&oacute;n</th>
                  <th scope="col">Opci&oacute;n</th>
               </tr>
            </thead>
            <tbody>
               <?php
               $conteo = 0;
               while (!$rsRolesUsuarios->EOF) {

//                  if ($rsRolesUsuarios->fields['IN_ADMIN'] == 1 && $_SESSION['usuario_administrador'] != 1)
//                  {
//                     $rsRolesUsuarios->MoveNext();
//                     continue;
//                  }
//                  ?>
                  <tr>
                     <td ><?php echo $rsRolesUsuarios->fields['NB_ROL']; ?> </td>
                     <td ><?php echo utf8_decode($rsRolesUsuarios->fields['TX_DESCRIPCION']); ?> </td>
                     <td>
                        <input type="checkbox" <?php echo "name=\"chkrole[$conteo]\""; ?> value="<?php echo utf8_decode($rsRolesUsuarios->fields['CO_ROL']); ?>"
                               <?php if ($rsRolesUsuarios->fields['ES_MARCADO'] > 0) echo 'checked="checked"'; ?>/>
                     </td>
                  </tr>
                  <?php
                  $conteo++;
                  $rsRolesUsuarios->MoveNext();
               }
               ?>
            </tbody>
         </table>
         <div class="grupobotones">
            <input class="boton"  value="Recuperar" type="reset" >
            <input class="submit boton" value="Guardar" id="botonsiguiente" type="submit">
            <input class="submit boton" value="Volver" id="botonsiguiente" type="button" onclick='window.parent.Shadowbox.close();'>
         </div>
         
      </form>
   </body>
</html>
