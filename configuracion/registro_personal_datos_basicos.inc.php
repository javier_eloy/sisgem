<div class="titulomodulo">Registro de Personal: <div id="cargando-ldap" class="loading-circle" style="display:none;"></div></div>
<form name="formulario" id="formulario" <?php echo "action=\"",$action,"\"";?> method='POST' onsubmit="return validar(this);">
    <?php 
      echo "<input type=\"hidden\" id=\"url_foto\" value=\"",SERVIDOR_FOTOS,"\">"; 
      echo "<input type=\"hidden\" id=\"url_foto_nodisp\" value=\"",SERVIDOR_FOTOS_NODISP,"\">";       
    ?>
    <img id="fotopersonal" class="marcofoto" alt="foto" src="#url_foto_nodisp" onError="return asignaNoDisp();">
    <table class="tablaformulario" id="coincide" cellspacing="2" cellpadding="2">
        <tr>
            <td nowrap>
                <span class="etiqueta">Indicador
                <img class="botonimagen" src="publico/imagenes/add_user.png" title="Busca los datos en el dominio de red utilizando el indicador" onclick="return esUsuarioLDAP();"></span>                
            </td>
            <td><span class="etiqueta">Nombre y Apellido</span></td>
            <td><span class="etiqueta">C&eacute;dula</span></td>
        </tr>
        <tr>
            <td>
                <input id="nu_username" name="nu_username" value="<?php  echo $nu_username?>" type="hidden">
                <input id="username" name="indicador" type="text" value="<?php echo $nb_indicador;?>" onChange="javascript:this.value=this.value.toUpperCase();" class=':required texto' />
            </td>
            <td><input  id="nombre_apellido" name="nombre_apellido" value="<?php echo  $nb_nombre;?>"  onChange="javascript:this.value=this.value.toUpperCase();" class='texto_readonly' type="text" readonly></td>
            <td><input  id="cedula" name="cedula"  value="<?php echo $nu_cedula;?>" class='texto_readonly' type="text" onchange="return refrescaFoto();" readonly></td>
        </tr>
        <tr>
            <td><span class="etiqueta">Direcci&oacute;n</span></td>
            <td><span class="etiqueta">Tel&eacute;fono</span></td>
            <td><span class="etiqueta">Cargo</span></td>
            <td style="width:100px"></td>
        </tr>
        <tr>
            <td><input  id="direccion" name="direccion" value="<?php echo $tx_direccion;?>" class='texto_readonly' type="text" readonly></td>
            <td><input  id="telefono" name="telefono" value="<?php echo $nu_telefono;?>" type="text" class='texto_readonly' readonly></td>
            <td>
                <select name="cargo" class=":required texto" id="cargo">
                <?php
                    
                 if(isset($co_cargo)) echo str_replace("<option value={$co_cargo}>","<option value={$co_cargo} selected>",$sHTMLCargos);
                                 else echo $sHTMLCargos;
                ?>
               </select>
            </td>
        </tr>
        <tr>            
            <td><span class="etiqueta">Nivel de Control de Personal</span></td>
            <td colspan="2"><div id="titulo_cargos_control_gestion" style="display:none" class="etiqueta" >M&aacute;ximo cargo para control de gesti&oacute;n</div></td>
        </tr>
        <tr>            
            <td>
                <select name='gestion' class=':required texto' id='id_gestion' onchange="return ActivarCargoGestion(this.value);">
                    <option value='0'<?php if($in_planificador==0) echo "Selected"; ?>> (ninguno)</option>
                    <option value='1'<?php if($in_planificador==1) echo "Selected"; ?>>Control de Gesti&oacute;n</option>
                    <option value='2'<?php if($in_planificador==2) echo "Selected"; ?>>Supervisorio</option>
                </select>
            </td>
            <td colspan="2">
                <div id="cargos_control_gestion" style="display:none">
                   <select name="cargo_controlgestion" class=":required texto" id="cargo_controlgestion">
                      <?php  
                      if(isset($co_cargo_planificador)) echo str_replace("<option value={$co_cargo_planificador}>","<option value={$co_cargo_planificador} selected>",$sHTMLCargos);                      
                                                  else echo $sHTMLCargos; 
                      
                      ?>
                   </select>    
                </div>
            </td>
        </tr>
    </table> 
    <div id="Info" class="mensaje_personal"></div> 
    <div class="grupobotones">
        <input class="submit boton" name="accion" id="accion" value="<?php echo $etiqBoton;?>" type="submit" >
        <input class="boton"  value="Limpiar" type="button" onclick="reiniciarFormulario();" >         
    </div>
</form>
<?php if (strlen($msg) > 0) echo " <span id=\"msgbar\" style=\"color:#f00;font-size:x-small;\">**$msg**</span>"; ?>        