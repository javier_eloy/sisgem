<?php
header("Cache-Control: no-cache,no-store,must-revalidate");
header("Pragma: no-cache");
header("Expires:0");
header("Content-Type: text/html; charset=UTF-8");


include_once("aplicacion/configuracion/aut_config.inc.php");
include_once("aplicacion/configuracion/aut_mensaje_error.inc.php");
include_once("aplicacion/librerias/JSON.php");
include_once("aplicacion/servidor/_comun/librerias.php"); // Para destruir la sesion


DestruirSesion(USUARIOS_SESION);

?>
<!DOCTYPE HTML>
<html >
   <head>
      <title>PDVSA </title>
      <link type="text/css" rel="stylesheet" href="publico/js/Archivos/estilos.css">
      <link rel="stylesheet" type="text/css" href="publico/js/Archivos/styleestancia.css" >
      <link rel="stylesheet" type="text/css" href="publico/js/sexyalertbox/sexyalertbox.css">

      <!-- Includes de Sexy Alert Box -->
      <script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
      <script type="text/javascript" src="publico/js/sexyalertbox/jquery.easing.1.3.js"></script>
      <script type="text/javascript" src="publico/js/sexyalertbox/sexyalertbox.v1.2.jquery.js"></script>
      <script type="text/javascript" src="publico/js/base64.js"></script>
      <script type="text/javascript" src="publico/js/refresh.js"></script>


      <!-- Script JQUERY -->
      <script type="text/javascript" src="aplicacion/cliente/html_login.js"></script>
      <!-- Cliente -->
      <script language="javascript" type="text/javascript">
         var arr_mensajes=<?php echo json_encode($error_login_ms); ?>;
         $(document).ready(function() {
            if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase()) && /Linux/.test(navigator.platform)){
               alert('El navegador Chromium no es soportado por esta aplicación.');
               $("#ingresar").prop('disabled', true);
               $("#e1").prop('disabled', true);
               $("#z7").prop('disabled', true);
               $("<br><span style='color:red'>>> Navegador no soportado. << <br> Utilice: Mozilla o Internet Explorer</span>").insertAfter($("#ingresar"));
            }
            //------ Elimina caracteres no alfabetico del login
            $('#e1').bind('keyup blur',function(){
               $(this).val( $(this).val().replace(/[^a-zA-Z]/g,'').toUpperCase());
            });
            //----- Activa el botón de ingreso si escribe
            $('#e1').change(function(){
               $("#z7").removeAttr("disabled");
               $("#ingresar").show();
               $("#edpwd").hide();
               $('#listaRoles').html("");
            });
            //----- Acciones para el botón de Ingresar
            $('#ingresar').bind('click',function(){
               if( vacio($("#e1").val()) == false )
               { 
                  Sexy.error('<h1>El Campo Indicador debe contener informaci&oacute;n </h1><p>Ejemplo:GONZALEZJOS.</p>');
                  return false;
               }
               if( vacio($("#z7").val()) == false )
               {
                  Sexy.error('Campos Contrase&ntilde;a en blanco.');
                  return false;
               }
               if(SelecRole('#listaRoles','#loading',
                            Base64.encode($("#e1").val()),
                            Base64.encode($("#z7").val()),
                            arr_mensajes)) {
                  if($("#lstrol").children('option').length == 1){
                       $("#listaRoles").hide();
                       validar();
                  }
                  $("#z7").attr("disabled","true");
                  $("#ingresar").hide();
                  $("#edpwd").show();

                  return true;
               } else
                  return false;
            });
            //---- Reedita la clave
            $('#edpwd').bind('click',function(){
               $("#ingresar").show();
               $("#z7").removeAttr("disabled");
               $("#listaRoles").html("");
               $(this).hide();
            });

            //--------- Controles por defecto
            $('#loading').hide();
            $('#edpwd').hide();
         });
            
         function MensajeErrorEntrada(smsg){
            if( refreshCheck())  Sexy.error(smsg);
                                       
         }
         function vacio(q)
         {
            for ( i = 0; i < q.length; i++ )
               if ( q.charAt(i) != " " )
                  return true
            return false
         }

         function validar()
         {  var ejec;

            if($("#lstrol").val() == null) {
               Sexy.alert("Debe seleccionar un rol");
               return false;
            }
            ejec=Autenticar('#loading',
                  Base64.encode($("#e1").val()),
                  Base64.encode($("#z7").val()),
                  Base64.encode($("#lstrol").val()),
                  Base64.encode($("#fz").val()),
                  arr_mensajes);
            switch(ejec) {
               case 0:
                  $("#auth").val("auth");
                  $("#send").submit();
                  return true;
               case 1:
                  if(confirm("Existen otras sesiones abiertas, ¿Desea cerrar la otra sesion?")) {
                     $("#fz").val("1");
                     return validar();
                  }
                  break;
               case -1:
                  return false;
               default:
                  return false;
            }
         }
      </script>
      <!--<body topmargin="0" style="background-image: url(/img/); background-repeat:repeat; background-position:center top;">-->
   </head>
   <body>
      <!-- Forms -->
      <form id="refresh" name="refresh"><input type="hidden" name="loaded" id="loaded" /></form>
      <form id="send" name="send" method="post" action="cpanel.php">
         <input type="hidden" name="auth" id="auth" value=""/>
         <input type="hidden" name="fz" id="fz" value="0"/>
      </form>
      <!--******  -->

      <div class="pagina_index">
         <div style="padding:5px 0px 10px 0px;">
            <img src="publico/imagenes/logo.jpg" alt="Volver a Inicio" >
         </div>
         <!--<div align="center" style="overflow: auto; width: 756px; height: 486px; background-image:url(/images/back_tran.png); background-color:#F9F9F9;">-->
         <div class="encabezado">
            <div class="titulo-usuario">PDVSA - INTRANET</div>
            <div class="titulo-fecha"><?php echo str_replace(' De ', ' de ', ucwords(strftime("%A, %d de %B de %Y"))); ?></div>
         </div>
         <!-- Cuerpo -->
         <div class="cuerpo">
            <div class="izquierda">
               <div>
                  <div><h1>Acceso al sistema</h1></div>
               </div>
               <div>
                  <div class="etiqueta">Indicador: </div>
               </div>
               <div>
                  <div>
                     <input  id="e1"  name="user" class="texto" maxlength="20" type="text"  size="25" autocomplete="off">
                     <div class="ayuda">Ej.: xxxxx </div>
                  </div>
               </div>
               <div class='etiqueta'>Contrase&ntilde;a:</div>
               <div>
                  <input id="z7" name="pass" class="texto" type="password" size="20" autocomplete="off">&nbsp;<input id="edpwd" class="boton_min" type="button" value="..." title="Edita la clave" style="display:none;">
                  <div class="ayuda">Ej.: 123456 </div>
               </div>
               <br>
               <div>
                  <input name="ingresar" id="ingresar" type="submit" class="boton" value="Ingresar">
               </div>
               <!-- Carga los roles -->
               <div id="listaRoles"></div>
               <!-- ****** -->
               <div id="loading" style="padding-top:15px;"><img src="publico/imagenes/ajax-loader.gif" alt="Cargando..."></div>
            </div>
            <div class="centro">
               <div class="tituloindex"><h1>Bienvenidos.</h1></div>
               <div class="etiquetaindex">Estimado usuario(a):</div>
               <div><?php include 'principal.php'; ?> </div>
            </div>
         </div>
         <!-- Fin de Cuerpo -->
         <div class="pie"><span class="etiqueta_index"> Sistema Realizado para Resolucion  1024x768 - Recomendado para Mozilla Firefox</span> </div>
         <div class="pie2" >PROGRAMACI&Oacute;N, PLANIFICACI&Oacute;N Y MANTENIMIENTO / PLATAFORMA CENTRALIZADA / AIT </div>
      </div>
   </body>
</html>