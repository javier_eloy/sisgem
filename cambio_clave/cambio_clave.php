<?php

if ($RecordUsuario["usuario_administrador"] != 1) 
{
    echo "<font style=\"color:red;font-size:13px;font-weight:bold;\">",MSG_ACCESO_ADMINISTRADOR,"</font>";
    exit;
}

$codigo = $RecordUsuario["usuario_id"];

$sMsg = "";
if(isset($_GET['accion']))
switch ($_GET['accion']) 
{
    case "actualizar":
        $pass         = $_POST['pass'];
        $pass_confirm = $_POST['pass_confirm'];

        if ($pass == $pass_confirm) 
        {
          if(strlen($pass) >= 8 && stripos($pass," ") == false ) 
           {
                $passMD5        = md5($pass_confirm);
                $objCambioClave = new Cambioclave();
                if ($objCambioClave->ActClaveAdmin($codigo, $passMD5))
                    $sMsg = "Clave actualizada Satisfactoriamente";
                else
                    $sMsg = "No se pudo cambiar la clave, intente nuevamente";
            } 
            else
                $sMsg="La clave debe ser de 8 caracteres minimo y sin espacios";
        } 
        else
            $sMsg = "Las claves ingresada y la confirmaci&oacute;n son distintas";
        break;
}
?>

<div class="titulomodulo">Cambio de Clave</div>		
<form method="POST" action="cpanel.php?sistema=4&accion=actualizar">
    <table class="tablaformulario">
        <tr>
            <td><span class="etiqueta">Usuario:</span></td>
            <td><span class="etiqueta"><b><?php echo $RecordUsuario["usuario_nombre"]?></b></span></td>
        </tr>
        <tr>
            <td><span class="etiqueta"> Nueva Clave:</span></td>
            <td><input  name="pass" class=":required texto"  maxlength="15" type="password"></td>
        </tr>

        <tr>
            <td><span class="etiqueta"> Repita la Nueva Clave:</span></td>
            <td><input  name="pass_confirm" class=":required texto"  maxlength="15" type="password"></td>
        </tr>
    </table>
    <br>
    <?php if (strlen($sMsg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$sMsg**</span>"; ?>        
    <div class="grupobotones">
        <input class="submit boton"  value="Cambiar" type="submit">	
        <input class="boton"  value="Limpiar" type="reset" >
    </div>
</form>



