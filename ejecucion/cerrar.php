<?php
include ("../aplicacion/configuracion/aut_lib.inc.php");
$co_orden = $_GET['co_orden'];
$sMsg = "";
$script = "";

$objEjecucion = new ejecucion();
if (isset($_POST['accion']))
    switch ($_POST['accion']) {
        case "Cerrar Orden":
            $tx_notas = $_POST['_tx_notas'];
            $pc_avance = $_POST['_pc_avance'];
            if ($pc_avance < 100 && $tx_notas == "") $sMsg = "El porcentaje es menor al 100%, debe describir un motivo para el cierre";

            if ($sMsg == "") {
                if ($objEjecucion->CerrarOrden($co_orden, $tx_notas)) $script = "window.parent.Cargar(); window.parent.Shadowbox.close();";
                else $sMsg = MSG_ERROR_TRANSACCION;
            }
            break;
    }

$rsEjecucion = $objEjecucion->SelecDatosOrden($co_orden);
?>
<script type="text/javascript">
<?php echo $script ?>
</script>
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">   
<script type="text/javascript" src="../publico/js/jquery-1.7.2.js"></script>
<form <?php echo "action=\"cerrar.php?co_orden={$co_orden}\""; ?> method='POST' onsubmit="return confirm('¿Está seguro de cerrar la orden?');" >
    <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="10" width="592" height="292">
        <tbody>
            <tr>
                <td valign="top" width="756">
                    <div class="titulomodulo">Orden de Trabajo.</div>
                    <div class="tabla">
                        <?php
                        if (!$rsEjecucion->EOF) {
                            $fechaProgram = new DateTime($rsEjecucion->fields['FE_ASIGNACION']);
                            echo "<b>Fecha de Creaci&oacute;n de Orden de Trabajo:</b> ", utf8_encode(ucfirst(strftime("%A, %d de %B", $fechaProgram->getTimestamp())));
                            echo "<br>";
                            echo "<b>No de Orden: </b>", $rsEjecucion->fields['NU_ORDEN'];
                            echo "<br>";
                            echo "<b>Fecha de Asignacion a Orden de Trabajo: </b>", date_format(new DateTime($rsEjecucion->fields['FE_ASIG_ORDEN']), "d/m/Y");
                        }
                        ?>
                    </div>
                    <table class="tabla">
                        <tr>
                            <td>
                                <input type="hidden" name="_pc_avance" value="<?php $rsEjecucion->fields['PC_AVANCE_ORDEN'] ?>">
                                Porcentaje de Ejecuci&oacute;n:   <?php echo $rsEjecucion->fields['PC_AVANCE_ORDEN']; ?>%
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Motivo del Cierre:
                                <textarea  style="width:760px;" cols="100"  id="_tx_notas" name="_tx_notas"  rows="4" maxlength="250" ></textarea>
                            </td>
                        </tr>
                    </table>
                    <?php if (strlen($sMsg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$sMsg**</span>"; ?>      
                    <div class="grupobotones">
                        <input class="submit boton" name="accion" value="Cerrar Orden" type="submit" />
                        <input class="boton" name="Volver" value="Volver" type="button" onclick="window.parent.Shadowbox.close();"/>
                    </div>
                </td>                
            </tr>       
        </tbody>
    </table>
</form>
