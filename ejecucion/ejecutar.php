<!DOCTYPE html>
<html>
    <?php
    include ("../aplicacion/configuracion/aut_lib.inc.php");
    header('Content-Type: text/html; charset=UTF-8');

    $co_actividad = $_GET['co_actividad'];
    $co_usuario   = $_GET['co_usuario'];
    
    

    $objEjecucion = new ejecucion();
    $objInventario = new inventarioSAP();
    $sMsg = "";
    $script = "";

    if (isset($_REQUEST['accion'])) {
        $accion = $_REQUEST['accion'];
        switch ($accion) {
            case "Cerrar Actividad":
                $total_chk     = $_POST['total_chk'];
                $total_sel_chk = count($_POST['chkProcedimiento']);
                $tx_notas      = (isset($_POST['_tx_notas'])) ? $_POST['_tx_notas'] : "";
//                $fe_fin = ($_POST['_fe_fin']=='') ?  '00-00-0000' : $_POST['_fe_fin'];
                $fe_fin = $_POST['_fe_fin'];
                $motivoCierre  = $_POST['motivoCierre'];
                if(($total_chk != $total_sel_chk) && (strlen($tx_notas) <= 10) ) {
                   $script="alert('Esta intentando cerrar una actividad con algunos procedimientos no ejecutados, debe colocar una explicación en el campo de [Notas]. (Minimo 10 caracteres)');";
                   break;
                }
                if($fe_fin == '') {
                   $script="alert('Debe ingresar una fecha cierre para poder procesar');";
                   break;
                }
                
                if(($total_chk != $total_sel_chk) && $motivoCierre == 0) {
                   $script="alert('Debe seleccionar un motivo de cierre para poder procesar');";
                   break;
                }

            case "Actualizar" :
                if (array_key_exists('_fe_inicio', $_POST) && array_key_exists('_hh_total_horas_hombre', $_POST) &&
                        array_key_exists('_hh_viaje', $_POST)) {
                    $fe_inicio = $_POST['_fe_inicio'];
                    $fe_fin = ($_POST['_fe_fin']=='') ?  '00-00-0000' : $_POST['_fe_fin'];
                    //*--- Calcular Horas Hombre (Suma)
                    $hh_total_horas_hombre = (empty($_POST['_hh_total_horas_hombre']))? '0:00' : $_POST['_hh_total_horas_hombre'];
                    $hh_viaje = (empty($_POST['_hh_viaje'])) ? '0:00' : $_POST['_hh_viaje'];
                    
                    $arrayChkProc = (isset($_POST['chkProcedimiento'])) ? $_POST['chkProcedimiento'] : array();
                    $tx_notas = (isset($_POST['_tx_notas'])) ? $_POST['_tx_notas'] : "";

                    //* Obtiene las fechas y los procedimientos
                    $j = 0;
                    $conjunto_procedimientos = "";
                    $arrayEjecucion = array();
                    foreach ($_POST As $key => $values) {
                        if (stripos($key, "_fe_ejecucion_") === 0) {
                            $arrayEjecucion[$j] = array("VA_PROCEDIMIENTO" => $arrayChkProc[$j],
                                "FE_EJECUCION" => $values);
                            if ($conjunto_procedimientos != "")
                                $conjunto_procedimientos.=",'{$arrayChkProc[$j]}'";
                            else
                                $conjunto_procedimientos = "'{$arrayChkProc[$j]}'";
                            $j++;
                        }
                    }
                    //*--  Ejecuta la Accion           
                    if ($objEjecucion->ActualizaEjecucion($co_actividad, $co_usuario, $fe_inicio, $hh_total_horas_hombre, $hh_viaje, $fe_fin, $tx_notas, $conjunto_procedimientos, $arrayEjecucion)) {
                        $sMsg = "Se actualizó el avance.";
                        if ($accion == "Cerrar Actividad") {
                            if ($objEjecucion->CerrarActividad($co_actividad,$motivoCierre))
                                $script = "window.parent.Cargar(); window.parent.Shadowbox.close();";
                            else
                                $sMsg = "No se pudo cerrar la actividad, intente de nuevo";
                        }
                    } else
                        $sMsg = "Error Interno de Base de Datos: No se puede actualizar";
                } else
                    $sMsg = "Faltan datos, revise la ejecucion";
                break;
        }
    }

    $rsEjecucion = $objEjecucion->SelActividad($co_actividad);
    $fe_inicio = $rsEjecucion->fields['FE_INICIO'];
    $hh_total_horas_hombre = substr($rsEjecucion->fields['HH_TOTAL_HORAS_HOMBRE'], 0, 5);
    $hh_viaje = substr($rsEjecucion->fields['HH_VIAJE'], 0, 5);
    $fe_fin = $rsEjecucion->fields['FE_FIN'];
    $tx_notas = $rsEjecucion->fields['TX_OBSERVACION'];

    $rsProcedimiento = $objEjecucion->SelProcedimiento($co_actividad, $co_usuario);
    $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
    
//** Obtiene los datos de los procedimientos
    if ($rsEjecucion->fields['VA_TIPO_PLAN'] == "E") {
        $arrayDatos = $objInventario->SelecDetalleEqpSAP($rsEjecucion->fields['VA_DETALLE']);
        $Desc = $arrayDatos[1]['EQKTU'];
        $sCodigo = $arrayDatos[1]['TPLNR'];
//    list($ubic, $loc) = explode("-", $sCodigo);
        $arrayDatos = $objInventario->SelecUbicacionTecnica($sCodigo);
        $sUbic = $arrayDatos[1]['TXT_TPLMA'];
    } else {
        $arrayDatos = $objInventario->SelecUbicacionTecnica($rsEjecucion->fields['VA_DETALLE']);
        $sUbic = $arrayDatos[1]['TXT_TPLMA'];
        $Desc = $arrayDatos[1]['PLTXU'];
    }
    
//** Obtiene los motivos de cierre de actividad  
    $rsMotivoCierre = $objEjecucion->SelectMotivoCierre(0);  
    
    ?>
    <script type="text/javascript">
    <?php echo $script ?>
    </script>

    <head>
        <link type="text/css" rel="stylesheet" href="../publico/js/Archivos/tools.css">
        <link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">                    
        <link type="text/css" rel="stylesheet" href="../publico/js/Archivos/styleestancia.css" >
        <link type="text/css" rel="stylesheet" href="../publico/estilos/jquery-ui-1.10.3.custom.min.css" >
        <script type="text/javascript" src="../publico/js/jquery-1.7.2.js"></script>
        <script type="text/javascript" src="../publico/js/jquery-ui.js"></script>	
        <script type="text/javascript" src="../publico/js/jquery.ui-1.10.3.datepicker-es.js"></script>
        <script type="text/javascript" src="../publico/js/jquery.maskedinput.js" ></script>
        <script type="text/javascript" src="../aplicacion/cliente/texto_flotante_td.js"></script>
        <style type="text/css">
            /* Estilo que muestra la capa flotante */
            #flotante
            {
                position: absolute;
                display:none;
                font-family:Arial;
                font-size:0.7em;
                width:280px;
                border:0.1px solid #808080;
                background-color:#FFFFD0;
                padding:5px;
            }
        </style>
    </head>
    <body bgcolor="#ffffff">
        <!-- style PARA MOSTRAR UN TEXTO FLOTANTE SOBRE COMPONENTES --> 
        <div class="f10bold">
            <strong>CARACTERISTICAS: </strong> <?php echo $Desc; ?>
        </div>
        <div class="f10bold">
            <strong>UBICACI&Oacute;N: </strong> <?php echo utf8_encode($sUbic); ?>
            <strong>NIVEL :</strong> <?php echo $textoActividad; ?>
        </div>
<?php if (strlen($sMsg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$sMsg**</span>"; ?> 
        <div class="titulodivision">Ejecuci&oacute;n de las actividades:</div>
        <form  method="post" name="formulario" onsubmit="return Validar();" id="formulario" action="ejecutar.php<?php echo "?co_usuario=$co_usuario&co_actividad=$co_actividad" ?>">
            <table class="tabla">
                <thead>
                    <tr>
                        <th scope="col" align="left">Fecha de inicio (DD/MM/AAAA):</th>
                        <th scope="col" align="left">Horas Hombre (HH:MM):</th>        
                        <th scope="col" align="left">Tiempo de Viaje de ida (HH:MM):</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>

                        <td align="left" nowrap>
                            <span>
<?php if ($fe_inicio == '0000-00-00') { ?>
                                    <input type="text" id="_fe_inicio" class=":required textofecha_avanzado" onchange="" name="_fe_inicio" readonly value="" onchange="sincronizafecha();"/>
                                <?php } else { ?>   
                                    <input type="text" id="_fe_inicio_m" class="textofecha_avanzado" name ="_fe_inicio" readonly value="<?php echo date_format(new DateTime($fe_inicio), "d/m/Y"); ?>" /> 
                                <?php } ?>   
                            </span>
                        </td>
                        <td align="left">
<?php echo 'H/H acumuladas:', $hh_total_horas_hombre; ?><br />
                            <input id="_hh_total_horas_hombre" name="_hh_total_horas_hombre" class=":required texto_min" type="text" maxlength="6" size="6">
                        </td>
                        <td align="left"> 
<?php echo 'Tiempo de viaje acumulado: ', $hh_viaje ?><br />
                            <input  id="_hh_viaje" name="_hh_viaje" class=":required texto_min" type="text" maxlength="6" size="6" value="0:00">

                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- ACTIVIDADES -->
            
            <div class="titulodivision">Actividad a Completar:</div>
            <div id="flotante"></div>
            <table id="listaSaldos" class="tabla">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Nro.</th>
                        <th scope="col">Procedimiento</th>
                        <th scope="col">Fecha de ejecuci&oacute;n</th>            
                    </tr>
                </thead>	
                <tbody>
<?php
$contar = 1;
while (!$rsProcedimiento->EOF) {
    $ejecutado = "onclick=\"habilitarFecha('$contar');\"";
    $divAyuda = "";
    if ($rsProcedimiento->fields['TOT_EJECUTADO'] > 0) {
        $rsUsuarioEjecutado = $objEjecucion->SelEjecutores($co_actividad, $co_usuario, $rsProcedimiento->fields['VA_PROCEDIMIENTO']);
        if (!$rsUsuarioEjecutado->EOF) {
            $divAyuda = "<strong>Actividad ejecutada por:</strong><br>";
            $divAyuda.="<table>";
            while (!$rsUsuarioEjecutado->EOF) {
               
                $divAyuda.="<tr>";
                $divAyuda.="<td>
                                           {$rsUsuarioEjecutado->fields['NB_NOMBRE']} ({$rsUsuarioEjecutado->fields['NB_INDICADOR']}) <br>
                                           Fecha de Ejecucion: ".date_format(new DateTime($rsUsuarioEjecutado->fields['FE_EJECUCION']),"d/m/Y")."
                                           </td>    
                                           <td>
                                              <img src='" . SERVIDOR_FOTOS . "/0{$rsUsuarioEjecutado->fields['NU_CEDULA']}.jpg' height='48px' width='48px'> 
                                           </td>";
                $divAyuda.="</tr>";
                $rsUsuarioEjecutado->MoveNext();
            }
            $divAyuda.="</table>";
            $divAyuda = "<div class='referencia' style='display: none;' content=\"" . $divAyuda . "\"></div>";
        }

    }
    if (isset($rsProcedimiento->fields['FE_EJECUCION'])) {
        $fe_ejecucion = date_format(new DateTime($rsProcedimiento->fields['FE_EJECUCION']), "d/m/Y");
        $esDisabled = "";
        $esCheck = "checked";
    } else {
        $fe_ejecucion = "";
        $esDisabled = "disabled";
        $esCheck = "";
    }
    echo "<tr>";
    echo "<td style='width:10px'><input $esCheck type=\"checkbox\" name=\"chkProcedimiento[]\" value=", $rsProcedimiento->fields['VA_PROCEDIMIENTO'], " $ejecutado></td>";
    echo "<td style='width:10px;text-align:center;' >", $rsProcedimiento->fields['NU_NODO'], "</td>";
    echo "<td nowrap onmouseover='return onMouseEnterProg(this,event);' onmouseout='return onMouseLeaveProg(this);' style=\"width:70%\">", $divAyuda, ucwords(utf8_decode($rsProcedimiento->fields['NB_TEXTO'])), "</td>";
    echo "<td> <input $esDisabled id=\"_fe_ejecucion_{$contar}\" readonly name=\"_fe_ejecucion_{$contar}\" class=\"textofecha_avanzado\" type=\"text\" value=\"{$fe_ejecucion}\"> </td>";
    echo "</tr>";
    $contar++;
    $rsProcedimiento->MoveNext();
}
?>
                </tbody>
            </table>
            <!-- NOTAS -->
            <div class="titulodivision">Notas:</div>
            <table class="tabla">
                <tbody>             
                    <tr>
                        <td>
                            <textarea style="width:760px;" cols="100"  id="_tx_notas" name="_tx_notas"  rows="4" maxlength="250"><?php echo $tx_notas; ?></textarea>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- FECHA DE CULMINACION -->
            <table class="tabla">
                <thead>
                    <tr>
                        <th align="left">Fecha de culminaci&oacute;n (DD-MM-AA):</th>            
                        <th align="left">Motivo de Cierre</th>            
                        <!--<th align="left"><input type="checkbox" id="omitir" name="omitir" onchange="habMotivoOmision();">Omitir Actividad</th>-->            
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="left" >
                            <span><input type="text" id="_fe_fin" class=":required textofecha_avanzado" onchange="" name="_fe_fin" readonly value="<?php if ($fe_fin !== '0000-00-00') echo date_format(new DateTime($fe_fin), "d/m/Y"); ?>" /></span>
                        </td>
                         <td align="left" >
                            <span>                                                                    
                                <?php
                                                                          
                                    echo '<select name="motivoCierre" class=":required texto" id="motivoCierre" >
                                            <option value = "0">Seleccione una opci&oacute;n...</option>';
                                            while(!$rsMotivoCierre->EOF){
                                                echo '<option value='. $rsMotivoCierre->fields["NU_MOTIVO_CIERRE"].'>' . $rsMotivoCierre->fields["TX_DESCRIPCION"] .'</option>';
                                                $rsMotivoCierre->MoveNext();
                                            }
                                    echo '</select>';
                                ?>                       
                            </span>
                        </td>
<!--                        <td align="left" >
                            <span>
                                <select name="motivoOmision" class=":required texto" id="motivoOmision" >disabled>
                                    <option value = '0'>Seleccione una opci&oacute;n...</option>
                                    <option value = '1'>A</option>
                                    <option value = '2'>B</option>                        
                                </select>	
                            </span>
                        </td>-->
                    </tr>
                </tbody>
            </table>

            <!-- BOTONES -->
            <div class="grupobotones">
                <input type="hidden" id="total_chk"  name="total_chk" value="0"/>
                <input type="submit" class="submit boton"  name="accion" value="Actualizar"/>
                <input type="submit" class="submit boton"  name="accion" value="Cerrar Actividad"/>
                <input class="boton" name="Volver" value="Volver" type="button" onclick="window.parent.Cargar();window.parent.Shadowbox.close();"/>
            </div>
        </form>
        <script> 
            var jq=jQuery.noConflict();
            jq(function() {
                /*/-Fecha de Inicio-/*/
                jq("#_fe_inicio").datepicker({
                    maxDate:"+1d",
                    showOn: "both",
                    buttonImage: "../publico/imagenes/calendar-icon.png",
                    buttonImageOnly: true});
                jq("#_fe_inicio" ).datepicker( "option","es");
                jq("#_fe_inicio").change(function() {
                    jq('[id^="_fe_ejecucion_"]').datepicker('option','minDate',jq('#_fe_inicio').datepicker('getDate'));
                })
                /*/-Fecha Fin-/*/                
                jq("#_fe_fin").datepicker({
                    maxDate:"+1d",
                    showOn: "both",
                    buttonImage: "../publico/imagenes/calendar-icon.png",
                    buttonImageOnly: true
                });
                jq("#_fe_fin").change(function() {
                    jq('[id^="_fe_ejecucion_"]').datepicker('option','maxDate',jq('#_fe_fin').datepicker('getDate'));
                })

                jq("#_fe_fin").datepicker( "option","es");
                /*/-Formato de Horas-/*/
                jq("#_hh_total_horas_hombre").mask("99:99");
                jq("#_hh_viaje").mask("99:99");
                jq('[id^="_fe_ejecucion_"]').datepicker({maxDate:"+1d"});
            }); 
            
            function habilitarFecha(cont_fecha) 
            {   var sobj='#_fe_ejecucion_'+cont_fecha;
                var objProc=jq(sobj)[0];
                var curPHPdate='<?php echo date_format(new DateTime(), "d/m/Y"); ?>';
                

                if (objProc.disabled==true) {
                    objProc.disabled=false;
                    if (typeof jq('#_fe_inicio')[0]!=='undefined') {
                        if (jq('#_fe_inicio').datepicker('getDate'))  
                            jq(sobj).datepicker('setDate',jq('#_fe_inicio').datepicker('getDate'));
                    }  else if(typeof jq('#_fe_inicio_m')[0]!=='undefined') 
                        jq(sobj).datepicker('setDate',jq('#_fe_inicio_m')[0].value);
                    else  
                        jq(sobj).datepicker('setDate',curPHPdate);
                } else  { 
                    objProc.disabled=true;
                    objProc.value='';
                }
            }
//            FUNCION QUE SE UTILIZARA PARA LA OMISION DE LA ACTIVIDAD , POR AHORA QUEDA SIN EFECTO
            function habMotivoOmision(cont){  
                var marcado = false;
//                jq('#chkProcedimiento').prop('checked') ? true : false;
                for(i=1;i<=cont;i++)
                    if(document.formulario.chkProcedimiento[i].checked){
                        marcado = true;
                        break;
                    }
                if(!marcado){
                    if (document.formulario.omitir.checked){
                            document.formulario.motivoOmision.disabled  = false;
                            document.formulario.motivoOmision.focus();
                    }
                    else 
                             document.formulario.motivoOmision.disabled  = true;
                }
                else
                    alert('No puede cancelar la actividad');
            }
            
            function Validar(){

                if(!confirm('¿Está seguro de ejecutar esta operación?')) 
                    return false;


                if(!jq('#_fe_inicio') && jq('#_fe_inicio').datepicker('getDate') == null) {
                    alert("Debe ingresar la fecha de inicio");
                    return false;
                } else if(jq('#_hh_total_horas_hombre').val()== null) {
                    alert("Debe ingresar el total de horas hombre");
                    return false;
                } else if(jq("#_hh_viaje").val()==null) {
                    alert("Debe ingresar el tiempo de viaje");
                    return false;
                } else {

                    var fe_ini;                
                    if (typeof jq('#_fe_inicio')!=='undefined') fe_ini= jq('#_fe_inicio').datepicker('getDate');
                    else fe_ini= jq.datepicker.parseDate("dd/mm/yy",jq('#_fe_inicio_m').val());
                                  
                    var fe_fin= jq('#_fe_fin').datepicker('getDate');               
               
                    if(fe_fin != null) {
                        var fechas=jq('[id^="_fe_ejecucion_"]:enabled');
                        for (var i=0; i < fechas.length; i++) {
                            var cur_fec=jq.datepicker.parseDate("dd/mm/yy",fechas[i].value);
                            if ( fe_ini >  cur_fec || cur_fec > fe_fin ) {
                                fechas[i].focus();
                                alert("Existen fechas de la ejecución de los procedimientos que no se encuentran dentro del rango de la actividad (fecha de Inicio y fecha fin)");
                                return false;
                            }
                        }
                    }
                    /*Valida que el campo nota tea*/
                    var chk_total=jq('[name="chkProcedimiento[]"]').length;
                    jq("#total_chk").val(chk_total);
                }
                return true;                
            } 
            
          //FUNCION QUE ACTIVA EL COMBO PARA EL MITVO DE OMISION  
            jq(document).ready(function() {
                    var select =  jq("select[name='motivoOmision']");
                    select.attr('disabled','disabled');
                });
                            
        </script>   
    </body>

</html>
