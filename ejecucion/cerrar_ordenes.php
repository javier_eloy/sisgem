<?php
$fecha_tope= new DateTime("NOW");
$diasemana=$fecha_tope->format("N");
if( $diasemana > 1  && $diasemana < 5) $diasemana = 14 - $diasemana;
                                  else $diasemana=14;
    
$fecha_tope->add(new DateInterval('P'.$diasemana.'D'));
?>
<!-- script JQUERY -->
<script type="text/javascript" src="aplicacion/cliente/html_tablas_ejecucion.js"></script>
<!-- script PARA REALIZAR TEXTO FLOTANTE -->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript" src="publico/js/jquery-ui.js"></script>	
<script type="text/javascript" src="publico/js/jquery.ui-1.10.3.datepicker-es.js"></script>
<link href="publico/estilos/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript"> 
        Shadowbox.init({
        language: "es",
        modal:true,
        players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv', 'php'],
        overlayColor: "#000",
        overlayOpacity: "0.5"}); 
</script>

<!-- style PARA MOSTRAR UN EXTO FLOTANTE SOBRE COMPONENTES --> 
<style type="text/css">
    /* Estilo que muestra la capa flotante */
    #flotante
    {
        position: absolute;
        display:none;
        font-family:Arial;
        font-size:0.7em;
        width:280px;
        border:0.1px solid #808080;
        background-color:#FFFFD0;
        padding:5px;
    }
</style>
<script type="text/javascript" src="aplicacion/cliente/texto_flotante_td.js"></script>
<div class="titulomodulo">
    Ordenes de Mantenimiento al: &nbsp;<span><input type="text" id="datepicker" class=":required textofecha" name="fecha" readonly value="<?php echo $fecha_tope->format("d/m/Y"); ?>" size="7" onchange="return Cargar();"/>  </span>
</div>

<div class="etiqueta" id="Cargando" ></div>
<div id="shadow_hidden"></div>
<div id="flotante"></div>
<div id="loading"></div>
<div id="resultado_cerrarorden"  class="resultado"></div>
<!-- script PARA CAMBIAR EL BOT�N O EL LINK DEPENDIENDO DE LA SELECCI�N DEL SELECT-->
<script type="text/javascript">
    Cargar();
     //**--------------------------------------------------**-----------------------------
    var jq=jQuery.noConflict();
    jq(document).ready(function(){      
        initBindLocal();
    });
    
    function initBindLocal(){
        jq("tbody#detalle").hide();
        jq("tbody#orden .ClassMas").click(function(event){
            var desplegable = jq(this).parents('#orden').get(0).next('#detalle')/*jq(this).next()*/;
            desplegable.toggle();
            event.preventDefault();
        });
        jq("#datepicker").datepicker({ 
            minDate: new Date(),
            showWeek:true,
           weekHeader:"Sem.",
            showOn: "both",
            buttonImage: "publico/imagenes/calendar-icon.png",
            buttonImageOnly: true
           });
        
    }
    
     function Cargar() {
        jq("#resultado_cerrarorden").html("");
        SelecOrdenes("resultado_cerrarorden","loading",jq("#datepicker").val(),<?php echo $RecordUsuario["usuario_id"] ?>);
    }
    
    function OpenShadow(sPath) {
	Shadowbox.open({
	        content:    sPath,
	        player:     "iframe",
	        height:     330,
		width:      805
	});
    }
</script>