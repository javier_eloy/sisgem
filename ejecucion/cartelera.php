<?php
$fecha_tope= new DateTime("NOW");
$diasemana=$fecha_tope->format("N");
if( $diasemana > 1  && $diasemana < 5) $diasemana = 14 - $diasemana;
                                  else $diasemana=14;
    
$fecha_tope->add(new DateInterval('P'.$diasemana.'D'));
?>
<!-- script PARA REALIZAR TEXTO FLOTANTE -->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript" src="publico/js/jquery-ui.js"></script>	
<script type="text/javascript" src="publico/js/jquery.ui-1.10.3.datepicker-es.js"></script>
<script type="text/javascript" src="aplicacion/cliente/html_tablas_ejecucion.js"></script>
<script type="text/javascript"> 
        Shadowbox.init({
        language: "es",
        modal:true,
        players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv', 'php'],
        overlayColor: "#000",
        overlayOpacity: "0.5"}); 
</script>
<link href="publico/estilos/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css">
<!-- style PARA MOSTRAR UN EXTO FLOTANTE SOBRE COMPONENTES --> 
<style type="text/css">
    /* Estilo que muestra la capa flotante */
    #flotante
    {
        position: absolute;
        display:none;
        font-family:Arial;
        font-size:0.7em;
        width:280px;
        border:0.1px solid #808080;
        background-color:#FFFFD0;
        padding:5px;
    }
    .text {}
</style>
<script type="text/javascript" src="aplicacion/cliente/texto_flotante_td.js"></script>
<div class="titulomodulo">
    Actividades a Ejecutar al: &nbsp;<span><input type="text" id="datepicker" class=":required textofecha" name="fecha" readonly value="<?php echo $fecha_tope->format("d/m/Y"); ?>" size="7" onchange="return Cargar();"/>  </span>
</div>
<div id="loading"></div>   
<div id="shadow_hidden"></div>
<div id="flotante"></div>
<div id="resultado_cartelera" class="resultado_cartelera"></div>

<!-- script PARA CAMBIAR EL BOT�N O EL LINK DEPENDIENDO DE LA SELECCI�N DEL SELECT-->
<script type="text/javascript">

    
    //**--------------------------------------------------**-----------------------------
    var jq=jQuery.noConflict();
    jq(document).ready(function(){      
        initBindLocal();
       /** Primera carga **/
        Cargar();
    });
    function initBindLocal(){
        jq("tbody#detalle").hide();
        jq("tbody#orden .ClassMas").click(function(event){
            var desplegable = jq(this).parents('#orden').get(0).next('#detalle')/*jq(this).next()*/;
            desplegable.toggle();
            event.preventDefault();
        });
        jq("#datepicker").datepicker({ 
            minDate: new Date(),
            showWeek:true,
           weekHeader:"Sem.",
            showOn: "both",
            buttonImage: "publico/imagenes/calendar-icon.png",
            buttonImageOnly: true
           });
        
    }
    
   //** Se coloca en funcion para llamarese en ejecutar.php
   function Cargar() {
         jq("#resultado_cartelera").html("");
         SelecCartelera("resultado_cartelera","loading",jq("#datepicker").val(),<?php echo $RecordUsuario["usuario_id"] ?>);
    }
    
    function ActualizarEstado(obj,idplan,idnomenclatura) {
        var lastvalue=obj.value;
        obj.value="Ejecutando...";
        if(ActualizarAplazada(idplan,idnomenclatura)) { 
            CargarProgramacion($("filtroprograma").value);
            return true;
        } else {
            obj.value=lastvalue;
            return false;
        }
    }
    
   
    
    function OpenShadow(sPath,pHeight,pWidth) {
        if(typeof(pHeight)==='undefined') pHeight=501;
        if(typeof(pWidth)==='undefined') pWidth= 801;
        
	Shadowbox.open({
	        content:    sPath,
	        player:     "iframe",
	        height:     pHeight,
		width:      pWidth
	});
    }
    

</script>
