<?php

error_reporting(E_ERROR);
require('../../aplicacion/librerias/fpdf/fpdf.php');
require('../../aplicacion/configuracion/aut_lib.inc.php');

class PDF extends FPDF {

   private $objEjecucion;
   private $objInventario;
   private $objComun;
   private $rsEjecucion;
   private $rsEjecucion2;
   private $rsEjecucion3;
   private $actividad;
   private $lugar;
   private $mantenedor_nombre;
   private $mantenedor_indicador;
   private $supervisor_indicador;
   private $fecha_asignacion;
   private $fecha_programacion;
   private $motivo_cierre;

   /*    * *
    *  Obtiene los parametros y se conecta
    */

   public function GetRequest() {
      /*$p = decryptLink($_REQUEST['p']);
      parse_str($p);*/
      $this->actividad=$_REQUEST['p'];

      $this->objComun = new comun();
      $this->objEjecucion = new ejecucion();
      $this->objInventario = new inventarioSAP();
      $this->rsEjecucion = $this->objEjecucion->SelFormatoActividad($_REQUEST['p']);
   }

   /*    * *
    * Prepara los datos de la cabecera
    */

   public function GetHeader() {


      //* Lugar
      list($grupohr, $operacionhr) = explode("-", $this->rsEjecucion->fields['VA_HOJARUTA']);
      if ($this->rsEjecucion->fields['VA_TIPO_PLAN'] == "E") {
         $arrayDatos = $this->objInventario->SelecDetalleEqpSAP($this->rsEjecucion->fields['VA_DETALLE']);
         $sCodigo = $arrayDatos[1]['TPLNR'];
         $arrayDatos = $this->objInventario->SelecUbicacionTecnica($sCodigo);
         $this->lugar = $arrayDatos[1]['TXT_TPLMA'];
      } else {
         $arrayDatos = $this->objInventario->SelecUbicacionTecnica($this->rsEjecucion->fields['VA_DETALLE']);
         $this->lugar = $arrayDatos[1]['TXT_TPLMA'];
      }
      $this->lugar= substr($this->lugar,0,38);
      $this->orden_trabajo = $this->rsEjecucion->fields['NU_ORDEN'];  
//* Mantenedor
      $this->rsEjecucion2 = $this->objEjecucion->SelMantSuperv($this->rsEjecucion->fields['CO_USUARIO']);
      $this->rsEjecucion3 = $this->objEjecucion->SelMotivoCierre($this->rsEjecucion->fields['NU_MOTIVO_CIERRE']);
      $this->mantenedor_indicador = $this->rsEjecucion2->fields['NB_INDICADOR'];
      //* Fecha de Programacion
      $this->fecha_programacion = $this->rsEjecucion->fields['FE_ASIG_ORDEN'];
      //* Fecha de Asignacion
      $this->fecha_asignacion = $this->rsEjecucion->fields['FE_ASIG_ORDEN'];
      //* Supervisor
      $this->supervisor_indicador=$this->rsEjecucion2->fields['NB_INDICADOR_SUPER'];
      //*Motivo cierre
      $this->motivo_cierre = $this->rsEjecucion3->fields['TX_DESCRIPCION'];;
   }

   /*    * *
    * Funcion Interna para pintar el Header
    */

   function Header() {
      //Logo

      $this->Image('../../aplicacion/librerias/fpdf/logopdvsa.jpg', 10, 3, 50);
      $this->Ln(15);
      $this->SetFont('Arial', 'B', 11);
      $this->Cell(180, 5, ' ACTIVIDADES DE MANTENIMIENTO EN EJECUCION ', 0, 0, 'C');
      $this->Ln(8);
      $this->Cell(180, 5, ' DATOS DE LA ORDEN DE TRABAJO ', 0, 0, 'C');
      $this->Ln();
      $this->Cell(50, 5, utf8_decode('N° de Orden de Trabajo: '), 'LTR', 0, 'L');
      $this->Cell(90, 5, 'Lugar:', 'LTR', 0, 'L');
      $this->Cell(50, 5, 'Mantenedor:', 'LTR', 0, 'L');
      $this->Ln();
      $this->SetFont('Arial', '', 11);
      $this->Cell(50, 5, "{$this->orden_trabajo}", 'LRB', 0, 'L');
      $this->Cell(90, 5, "{$this->lugar}", 'LRB', 0, 'L');
      $this->Cell(50, 5, "{$this->mantenedor_indicador}", 'LRB', 0, 'L');
      $this->Ln();
      $this->SetFont('Arial', 'B', 11);
      $this->Cell(50, 5, utf8_decode('Fecha de Programación: '), 'LTR', 0, 'L');
      $this->Cell(90, 5, utf8_decode('Fecha de Asignación:'), 'LTR', 0, 'L');
      $this->Cell(50, 5, 'Supervisor:', 'LTR', 0, 'L');
      $this->Ln();
      $this->SetFont('Arial', '', 11);
      $this->Cell(50, 5, date_format(new DateTime($this->fecha_programacion), "d/m/Y"), 'LRB', 0, 'L');
      $this->Cell(90, 5, date_format(new DateTime($this->fecha_asignacion), "d/m/Y"), 'LRB', 0, 'L');
      $this->Cell(50, 5, $this->supervisor_indicador, 'LRB', 0, 'L');
      $this->Ln(15);
      $this->SetFont('Arial', 'B', 11);
      $this->Cell(180, 5, ' ACTIVIDADES DE MANTENIMIENTO ', 0, 0, 'C');
      $this->Ln(10);
   }

   /*    * *
    * Funcion interna para pintar el pie de pagina
    */

   function Footer() {
      //Posición: a 1,5 cm del final
      $this->Ln();
      $this->Ln();
      $this->Ln();
      $this->Ln();
      $this->SetFont('Arial', 'B', 11);
      $this->Cell(63, 5, "Ejecutado:", 'LTR', 0, 'L');
      $this->Cell(63, 5, "Supervisado:", 'LTR', 0, 'L');
      $this->Cell(64, 5, "Aprobado:", 'LTR', 0, 'L');
      $this->Ln();
      $this->Cell(63, 5, "Cedula:", 'LTR', 0, 'L');
      $this->Cell(63, 5, "Cedula:", 'LTR', 0, 'L');
      $this->Cell(64, 5, "Cedula:", 'LTR', 0, 'L');
      $this->Ln();      
      $this->Cell(63, 5, "Firma:", 'LTRB', 0, 'L');
      $this->Cell(63, 5, "Firma:", 'LTRB', 0, 'L');
      $this->Cell(64, 5, "Firma:", 'LTRB', 0, 'L');
      $this->SetY(-15);
      //Arial italic 8
      $this->SetFont('Arial', 'I', 8);
      //Número de página
      $this->Cell(0, 10, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
   }

   /*    * *
    * Imprime el contenido
    */

   function PrintContent() {
      $this->rsEjecucion->MoveFirst();

      $this->AddPage();

      while (!$this->rsEjecucion->EOF) {
         //* Datos del Mantenimiento

         $fecha_plan = new DateTime($this->rsEjecucion->fields['FE_PROXIMA_PLAN']);
         $equipo = "(" . ltrim($this->rsEjecucion->fields['VA_DETALLE'],'0'). ") ";
         if ($this->rsEjecucion->fields['VA_TIPO_PLAN'] == "E") {
            $arrayDatos = $this->objInventario->SelecDetalleEqpSAP($this->rsEjecucion->fields['VA_DETALLE']);
            $Desc = $arrayDatos[1]['EQKTU'];
         } else {
            $arrayDatos = $this->objInventario->SelecUbicacionTecnica($this->rsEjecucion->fields['VA_DETALLE']);
            $Desc = $arrayDatos[1]['PLTXU'];
         }
         $equipo=$equipo.$Desc;
         $actividad=split("-",$this->rsEjecucion->fields['TX_HOJARUTA'],2);
         list($anoi, $mesi, $diai) = explode("-",$this->rsEjecucion->fields["FE_INICIO"]);
         if ($diai == "00"){
            $fecha_inicio = "NO EJECUTADO"; 
         }
         else{
            $fecha_inicio = $diai."-".$mesi."-". $anoi;  
         }
         list($anof, $mesf, $diaf) = explode("-",$this->rsEjecucion->fields["FE_FIN"]);
         if ($diaf == "00"){
            $fecha_fin = "NO EJECUTADO"; 
         }
         else{
            $fecha_fin = $diaf."-".$mesf."-". $anof;
         }
         $this->SetFont('Arial', 'B', 11);
         $this->Cell(40, 5, utf8_decode('Fecha de Plan: '), 'LTR', 0, 'L');
         $this->Cell(80, 5, utf8_decode('Equipo, Instalación o Sistema:'), 'LTR', 0, 'L');
         $this->Cell(70, 5, utf8_decode('Actividad de Mantenimiento:'), 'LTR', 0, 'L');
         $this->Ln();
         $this->SetFont('Arial', '', 10);
         $this->Cell(40, 5, date_format($fecha_plan, "d/m/Y"), 'LRB', 0, 'L');
         $this->Cell(80, 5, $equipo, 'LRB', 0, 'L');
         $this->Cell(70, 5, substr($actividad[1],0,33), 'LRB', 0, 'L');
         $this->Ln();
         //* Fecha de Inicio y Labor
         $this->SetFont('Arial', 'B', 11);
         $this->Cell(40, 5, utf8_decode('Fecha de Inicio: '), 'LTR', 0, 'L');
         $this->Cell(50, 5, utf8_decode('H/H de Labor (HH:MM):'), 'LTR', 0, 'L');
         $this->Cell(50, 5, utf8_decode('Tiempo de Viaje (HH:MM):'), 'LTR', 0, 'L');
         $this->Cell(50, 5, utf8_decode('Fecha de Culminación:'), 'LTR', 0, 'L');
         $this->Ln();
         $this->SetFont('Arial', '', 10);
         $this->Cell(40, 8, $fecha_inicio, 'LRB', 0, 'L');
         $this->Cell(50, 8, $this->rsEjecucion->fields['HH_TOTAL_HORAS_HOMBRE'], 'LRB', 0, 'L');
         $this->Cell(50, 8, $this->rsEjecucion->fields['HH_VIAJE'], 'LRB', 0, 'L');
         $this->Cell(50, 8, $fecha_fin, 'LRB', 0, 'L');
         $this->Ln();
         $this->SetFont('Arial', 'B', 11);
         $this->Cell(60, 5, 'PROCEDIMIENTOS:');
         $this->Ln();

         //* Lista de Procedimientos
         $this->SetFont('Arial', 'B', 11);
         $this->Cell(10, 5, utf8_decode('N°:'), 1, 0, 'L');
         $this->Cell(140, 5, utf8_decode('Descripción:'), 1, 0, 'L');
         $this->Cell(40, 5, utf8_decode('Fecha de Ejecución:'), 1, 0, 'L');
         $this->Ln();
         $this->SetFont('Arial', '', 10);
         
         $rsProcedimiento = $this->objComun->SelecLocalProcedimientos($this->rsEjecucion->fields['CO_PLAN']);
         
         while (!$rsProcedimiento->EOF) {
            $actividad = $this->rsEjecucion->fields['CO_ACTIVIDAD'];
            $procedimiento = $rsProcedimiento->fields['VA_PROCEDIMIENTO'];
            $rsEjecucion4 = $this->objEjecucion->SelEjecucion($procedimiento,$actividad);
            list($anoe, $mese, $diae) = explode("-",$rsEjecucion4->fields['FE_EJECUCION']);
            if ($diae == ""){
                $fecha_ejecucion = "NO EJECUTADO";
            }
            else{
                $fecha_ejecucion = $diae."-".$mese."-". $anoe;
            }
            $this->Cell(10, 5, $rsProcedimiento->fields['NU_NODO'], 1, 0, 'L');
            $this->Cell(140, 5, $rsProcedimiento->fields['NB_TEXTO'], 1, 0, 'L');
            $this->Cell(40, 5, $fecha_ejecucion, 1, 0, 'L');
            if ($diae == ""){
                $this->Rect(157, $this->GetY() + 1, 3, 3);
            }
            else{
                $this->Rect(157, $this->GetY() + 1, 3, 3,'F');
            }
            $this->Ln();
            $rsProcedimiento->MoveNext();
         }
         //* Notas
         $this->Cell(190, 5, 'Motivo de Cierre:', 'LTR', 0, 'L');
         $this->Ln();
         $this->Cell(190, 5, $this->motivo_cierre , 'LRB', 0, 'L');
         $this->Ln();
         $this->Cell(190, 5, 'Notas:', 'LTR', 0, 'L');
         $this->Ln();
         $this->Cell(190, 20, $this->rsEjecucion->fields['TX_OBSERVACION'], 'LRB', 0, 'L');
         $this->Ln();
         //* Fin- Lista de Procedimiento
         $this->rsEjecucion->MoveNext();
      }
   }

}

//* Creación del objeto de la clase heredada PDF
$pdf = new PDF();
$pdf->SetTitle("Formato para Registro de Procedimientos");
$pdf->AliasNbPages();
$pdf->SetMargins(12, 5, 12);
$pdf->GetRequest();
$pdf->GetHeader();
$pdf->PrintContent();
$pdf->Output("Formato_001.pdf", "I");
?>