<?php

error_reporting(E_ERROR);
require('../../aplicacion/librerias/fpdf/fpdf.php');
require('../../aplicacion/configuracion/aut_lib.inc.php');

class PDF extends FPDF {

   private $objReporte;
   private $objUsuario;
   private $rsReporte;
   private $rsUsuario;
   private $usuario;
   private $fecha_inicio;
   private $fecha_fin;
   private $orden;

   /*    * *
    *  Obtiene los parametros y se conecta
    */

   public function GetRequest() {
      /*$p = decryptLink($_REQUEST['p']);
      parse_str($p);*/
      $this->usuario=$_REQUEST['co_usuario'];
      $this->fecha_inicio=$_REQUEST['fecha_ini'];
      $this->fecha_fin=$_REQUEST['fecha_fin'];
      $this->orden=$_REQUEST['orden'];
      //$this->Cell(20,10,$_REQUEST['fecha_ini'],0,0);

      $this->objReporte = new reporte();
      $this->objUsuario = new registro_personal();
      $this->objInventario = new inventarioSAP();
      $this->rsReporte = $this->objReporte->SelecHistPersonal($_REQUEST['co_usuario'],$_REQUEST['fecha_ini'],$_REQUEST['fecha_fin'],$_REQUEST['orden']);      
      $this->rsUsuario = $this->objUsuario->selectUsuario($_REQUEST['co_usuario']);      
      
   }

   
   /*    * *
    * Funcion Interna para pintar el Header
    */
//Cabecera de página
 //Variable para cabecera de tabla
 var $col = array ("Nro Orden","Codigo / Caract.    ","                    Lugar                                ","          Actividades                          ","S. Plan","S. Prog ","F. Asig  ","F. Ini    ","F. Fin   ","H/H ","T/V","% Ejec");
   
   function Header() {

    //Logo
    $this->Image('../../aplicacion/librerias/fpdf/logopdvsa.jpg',10,8,40);
    $this->SetFont('Arial','B',12);
    $this->SetFillColor(224,235,255);
	$this->SetTextColor(0);
	$this->SetDrawColor(0,0,0);
	$this->SetLineWidth(.3);
        $this->Ln(5);
	$this->SetX(230);
        $this->Cell(50,10,' Detalle de actividades del mantenedor ',0,0,'R');
	$this->Ln(10);
        $this->SetFont('Arial','B',10);
        $this->SetTextColor(204,0,0);
        $this->Cell(20,10,'Mantenedor: ',0,0,'L');
        $this->SetTextColor(0);
        $this->SetX(33);
        $this->Cell(20,10,$this->rsUsuario->fields['NB_NOMBRE'],0,0);
        $this->Ln(5);
        $this->SetTextColor(204,0,0);
        $this->Cell(20,10,'Rango de fecha:  ',0,0,'L');
        $this->SetTextColor(0);
        $this->SetX(40);
        $this->Cell(20,10, ' del '.$_REQUEST['fecha_ini'].' al '.$_REQUEST['fecha_fin'],0,0);
        $this->SetFont('Arial','B',9);
	$this->Ln(15);
        $this->SetDrawColor(205,205,205);
        $this->SetLineWidth(.4);
        $this->Line(8, 43, 285, 43);
	$this->SetX(0);
        $this->Ln(4);
       	for($i=0;$i<count($this->col);$i++){
		$tam[$i] = $this->GetStringWidth($this->col[$i])+4;
		$this->Cell($tam[$i],5,$this->col[$i],0,0,'C',0);
	}
        $this->Ln(1);
        $this->Line(8, 52, 285, 52);
        $this->Ln(4);

   }

   /*    * *
    * Funcion interna para pintar el pie de pagina
    */

   function Footer() {
      //Posición: a 1,5 cm del final
      $this->SetY(-32); 
      $this->Ln();
      $this->Ln();
      $this->Ln();
      $this->Ln();
      $this->SetFont('Arial', 'B', 9);
      $this->Cell(90, 6, "Ejecutado:", 'LTR', 0, 'L');
      $this->Cell(90, 6, "Supervisado:", 'LTR', 0, 'L');
      $this->Cell(90, 6, "Aprobado:", 'LTR', 0, 'L');
      $this->Ln();
      $this->Cell(90, 6, "Cedula:", 'LTR', 0, 'L');
      $this->Cell(90, 6, "Cedula:", 'LTR', 0, 'L');
      $this->Cell(90, 6, "Cedula:", 'LTR', 0, 'L');
      $this->Ln();      
      $this->Cell(90, 6, "Firma:", 'LTRB', 0, 'L');
      $this->Cell(90, 6, "Firma:", 'LTRB', 0, 'L');
      $this->Cell(90, 6, "Firma:", 'LTRB', 0, 'L');
      $this->Ln();
//      $this->SetY(-15);
      //Arial italic 8
      $this->SetFont('Arial', 'I', 8);
      //Número de página
      $this->Cell(0, 10, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
   }

   /*    * *
    * Imprime el contenido
    */

   function PrintContent() {

      $this->rsReporte->MoveFirst();

      $this->AddPage('L');
      $this->SetFont('Arial', '', 6);
      $this->Ln(4);
      while (!$this->rsReporte->EOF) {
          
          //Detalles de la actividad
          
            list($anoi, $mesi, $diai) = explode("-",$this->rsReporte->fields['FE_INICIO']);
            if ($diai == "00"){
                $fecha_inicio = "NO EJEC."; 
            }
            else{
               $fecha_inicio = $diai."-".$mesi."-". $anoi;  
            }
            list($anof, $mesf, $diaf) = explode("-",$this->rsReporte->fields['FE_FIN']);
            if ($diaf == "00"){
                $fecha_fin = "NO EJEC."; 
            }
            else{
                $fecha_fin = $diaf."-".$mesf."-". $anof;
            }
            list($anop, $mesp, $diap) = explode("-",$this->rsReporte->fields['FE_PROXIMA_PLAN']);            
            $fecha_plan = $diap."-".$mesp."-". $anop; 
            $semana_plan = date( 'W-o', strtotime($this->rsReporte->fields['FE_PROXIMA_PLAN']));
            list($anog, $mesg, $diag) = explode("-",$this->rsReporte->fields['PROG']);
            $fecha_programacion = $diag."-".$mesg."-". $anog; 
            $semana_prog = date( 'W-o', strtotime($this->rsReporte->fields['PROG']));
            list($anoa, $mesa, $diaa) = explode("-",$this->rsReporte->fields['ASIG']);
            $fecha_asignacion = $diaa."-".$mesa."-". $anoa; 
            if ($this->rsReporte->fields['VA_TIPO_PLAN'] == "E") {
                $arrayDatos = $this->objInventario->SelecDetalleEqpSAP($this->rsReporte->fields['VA_DETALLE']);
                $Desc = $arrayDatos[1]['EQKTU'];
                $sCodigo = $arrayDatos[1]['TPLNR'];
                $codigo = ltrim($this->rsReporte->fields['VA_DETALLE'],'0');
                
                $arrayDatos = $this->objInventario->SelecUbicacionTecnica($sCodigo);
                $sUbic = $arrayDatos[1]['TXT_TPLMA'];
                
                $s2Codigo = $arrayDatos[1]['TPLMA'];
                $arrayDatos = $this->objInventario->SelecUbicacionTecnica($s2Codigo);
                $s2Ubic = $arrayDatos[1]['TXT_TPLMA'];

                
            } else {
                $arrayDatos = $this->objInventario->SelecUbicacionTecnica($this->rsReporte->fields['VA_DETALLE']);
                $cUbic = $arrayDatos[1]['TPLNR'];
                $sUbic = $arrayDatos[1]['TXT_TPLMA'];
                $Desc = $arrayDatos[1]['PLTXU'];
                $codigo = $arrayDatos[1]['TPLNR'];
                $s2Codigo = $arrayDatos[1]['TPLMA'];
                $arrayDatos = $this->objInventario->SelecUbicacionTecnica($s2Codigo);
                $s2Ubic = $arrayDatos[1]['TXT_TPLMA'];

                
            }
            $param=$this->rsReporte->fields['co_actividad'];

          //Comienza a Impriir el contenido  
          $this->Cell(20, 3, $this->rsReporte->fields['NU_ORDEN'], 0,0,'C',0);
          $this->Cell(37, 2, '('.$codigo.')', 0,0,'L',0);
          $this->Cell(55, 2, utf8_encode($sUbic), 0,0,'L',0);
          $this->Cell(55, 3, $this->rsReporte->fields['TX_HOJARUTA'], 0,0,'L',0);
          $this->Cell(14, 3, $semana_plan, 0,0,'L',0);
          $this->Cell(14, 3, $semana_prog, 0,0,'L',0);
          $this->Cell(16, 3, $fecha_asignacion, 0,0,'L',0);
          $this->Cell(16, 3, $fecha_inicio, 0,0,'L',0);
          $this->Cell(16, 3, $fecha_fin, 0,0,'L',0);
          list($horah, $minh, $segh) = explode(":",$this->rsReporte->fields['H_H']);
          $H_H = $horah.":".$minh;
          $this->Cell(10, 3, $H_H, 0,0,'L',0);
          list($horav, $minv, $segv) = explode(":",$this->rsReporte->fields['T_V']);
          $T_V = $horav.":".$minv;
          $this->Cell(10, 3, $T_V, 0,0,'L',0);
          $this->Cell(9, 3, $this->rsReporte->fields['NU_PORCENTAJE_AVANCE'], 0,0,'R',0);
          $this->Ln();
          $this->SetX(31);
          $this->Cell(37, 1, $Desc, 0,0,'L',0);
          $this->Cell(55, 1, $s2Ubic, 0,0,'L',0);

          $this->Ln(3);
          $this->rsReporte->MoveNext();
      }
//      $this->rsEjecucion->MoveFirst();
//      while (!$this->rsEjecucion->EOF) {
//         //* Datos del Mantenimiento
//
//         $fecha_plan = new DateTime($this->rsEjecucion->fields['FE_PROXIMA_PLAN']);
//         $equipo = "(" . ltrim($this->rsEjecucion->fields['VA_DETALLE'],'0'). ") ";
//         if ($this->rsEjecucion->fields['VA_TIPO_PLAN'] == "E") {
//            $arrayDatos = $this->objInventario->SelecDetalleEqpSAP($this->rsEjecucion->fields['VA_DETALLE']);
//            $Desc = $arrayDatos[1]['EQKTU'];
//         } else {
//            $arrayDatos = $this->objInventario->SelecUbicacionTecnica($this->rsEjecucion->fields['VA_DETALLE']);
//            $Desc = $arrayDatos[1]['PLTXU'];
//         }
//         $equipo=$equipo.$Desc;
//         $actividad=split("-",$this->rsEjecucion->fields['TX_HOJARUTA'],2);
//         list($anoi, $mesi, $diai) = explode("-",$this->rsEjecucion->fields["FE_INICIO"]);
//         if ($diai == "00"){
//            $fecha_inicio = "NO EJECUTADO"; 
//         }
//         else{
//            $fecha_inicio = $diai."-".$mesi."-". $anoi;  
//         }
//         list($anof, $mesf, $diaf) = explode("-",$this->rsEjecucion->fields["FE_FIN"]);
//         if ($diaf == "00"){
//            $fecha_fin = "NO EJECUTADO"; 
//         }
//         else{
//            $fecha_fin = $diaf."-".$mesf."-". $anof;
//         }
//         $this->SetFont('Arial', 'B', 11);
//         $this->Cell(40, 5, utf8_decode('Fecha de Plan: '), 'LTR', 0, 'L');
//         $this->Cell(80, 5, utf8_decode('Equipo, Instalación o Sistema:'), 'LTR', 0, 'L');
//         $this->Cell(70, 5, utf8_decode('Actividad de Mantenimiento:'), 'LTR', 0, 'L');
//         $this->Ln();
//         $this->SetFont('Arial', '', 10);
//         $this->Cell(40, 5, date_format($fecha_plan, "d/m/Y"), 'LRB', 0, 'L');
//         $this->Cell(80, 5, $equipo, 'LRB', 0, 'L');
//         $this->Cell(70, 5, substr($actividad[1],0,33), 'LRB', 0, 'L');
//         $this->Ln();
//         //* Fecha de Inicio y Labor
//         $this->SetFont('Arial', 'B', 11);
//         $this->Cell(40, 5, utf8_decode('Fecha de Inicio: '), 'LTR', 0, 'L');
//         $this->Cell(50, 5, utf8_decode('H/H de Labor (HH:MM):'), 'LTR', 0, 'L');
//         $this->Cell(50, 5, utf8_decode('Tiempo de Viaje (HH:MM):'), 'LTR', 0, 'L');
//         $this->Cell(50, 5, utf8_decode('Fecha de Culminación:'), 'LTR', 0, 'L');
//         $this->Ln();
//         $this->SetFont('Arial', '', 10);
//         $this->Cell(40, 8, $fecha_inicio, 'LRB', 0, 'L');
//         $this->Cell(50, 8, $this->rsEjecucion->fields['HH_TOTAL_HORAS_HOMBRE'], 'LRB', 0, 'L');
//         $this->Cell(50, 8, $this->rsEjecucion->fields['HH_VIAJE'], 'LRB', 0, 'L');
//         $this->Cell(50, 8, $fecha_fin, 'LRB', 0, 'L');
//         $this->Ln();
//         $this->SetFont('Arial', 'B', 11);
//         $this->Cell(60, 5, 'PROCEDIMIENTOS:');
//         $this->Ln();
//
//         //* Lista de Procedimientos
//         $this->SetFont('Arial', 'B', 11);
//         $this->Cell(10, 5, utf8_decode('N°:'), 1, 0, 'L');
//         $this->Cell(140, 5, utf8_decode('Descripción:'), 1, 0, 'L');
//         $this->Cell(40, 5, utf8_decode('Fecha de Ejecución:'), 1, 0, 'L');
//         $this->Ln();
//         $this->SetFont('Arial', '', 10);
//         
//         $rsProcedimiento = $this->objComun->SelecLocalProcedimientos($this->rsEjecucion->fields['CO_PLAN']);
//         
//         while (!$rsProcedimiento->EOF) {
//            $actividad = $this->rsEjecucion->fields['CO_ACTIVIDAD'];
//            $procedimiento = $rsProcedimiento->fields['VA_PROCEDIMIENTO'];
//            $rsEjecucion4 = $this->objEjecucion->SelEjecucion($procedimiento,$actividad);
//            list($anoe, $mese, $diae) = explode("-",$rsEjecucion4->fields['FE_EJECUCION']);
//            if ($diae == ""){
//                $fecha_ejecucion = "NO EJECUTADO";
//            }
//            else{
//                $fecha_ejecucion = $diae."-".$mese."-". $anoe;
//            }
//            $this->Cell(10, 5, $rsProcedimiento->fields['NU_NODO'], 1, 0, 'L');
//            $this->Cell(140, 5, $rsProcedimiento->fields['NB_TEXTO'], 1, 0, 'L');
//            $this->Cell(40, 5, $fecha_ejecucion, 1, 0, 'L');
//            if ($diae == ""){
//                $this->Rect(157, $this->GetY() + 1, 3, 3);
//            }
//            else{
//                $this->Rect(157, $this->GetY() + 1, 3, 3,'F');
//            }
//            $this->Ln();
//            $rsProcedimiento->MoveNext();
//         }
//         //* Notas
//         $this->Cell(190, 5, 'Motivo de Cierre:', 'LTR', 0, 'L');
//         $this->Ln();
//         $this->Cell(190, 5, $this->motivo_cierre , 'LRB', 0, 'L');
//         $this->Ln();
//         $this->Cell(190, 5, 'Notas:', 'LTR', 0, 'L');
//         $this->Ln();
//         $this->Cell(190, 20, $this->rsEjecucion->fields['TX_OBSERVACION'], 'LRB', 0, 'L');
//         $this->Ln();
//         //* Fin- Lista de Procedimiento
//         $this->rsEjecucion->MoveNext();
//      }
   }

}

//* Creación del objeto de la clase heredada PDF
$pdf = new PDF();
$pdf->SetTitle("Formato para Registro de Procedimientos");
$pdf->AliasNbPages();
$pdf->SetMargins(11, 5, 11);
$pdf->GetRequest();
//$pdf->GetHeader();
$pdf->PrintContent();
$pdf->Output("Formato_001.pdf", "I");
?>