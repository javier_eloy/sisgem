<?php

//************************** IMPRIME LA TABLA DE PERSONAL **********************************************/
?>
<script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="publico/js/prototype.js"></script>
<script type="text/javascript" src="aplicacion/cliente/html_reportes.js"></script>
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<div class="titulomodulo" align="center">Personal Asignado</div>	 
<form method="POST" action="cpanel.php?sistema=4004" name="planificar" id="planificar">
    <input type="hidden" id="page" name="page" value="" />
    <table id="texto_informativo">
        <tr>
            <td>
                <input type="text" class="texto" name="search"   value="" id="search" />
                <input class="boton"  id="cmdbuscar"  name="cmdbuscar"  value="Buscar"   type="button" onclick="MostrarTablaConsultaPersonal(1,'TablaDatos',<?php echo ("'".session_id()."'"); ?>,$('search').value,1);"> 
                <input class="boton"  id="cmdbuscar"  name="cmdLimpiar" value="Limpiar"  type="button" onclick="MostrarTablaConsultaPersonal(1,'TablaDatos',<?php echo ("'".session_id()."'"); ?>,$('search').value='',1);">                                       
            </td>
            <td><div id="loading" class="etiqueta"> </div></td>
            <!-- IMAGEN QUE SOLO PERMITE  width=25% height=25% REFERENCIAR UNA CARGA PARA IMPRIMIR LA TABLA ANTERIOR -->
        </tr>
    </table>            
</form>

<div id="TablaDatos"> </div>
<script type="text/javascript">     
    var jq=jQuery.noConflict();     
    jq('#search').keydown(function(e){         
        if (e.keyCode == 13) {
            e.preventDefault();
             jq("#cmdbuscar").click();
           }
    });

     Shadowbox.init({
        language: "es",
        modal:true,
        players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv', 'php'],
        overlayColor: "#000",
        overlayOpacity: "0.5"}); 
    
   function OpenShadow(sPath,pHeight,pWidth) {
        if(typeof(pHeight)==='undefined') pHeight=501;
        if(typeof(pWidth)==='undefined') pWidth= 801;
        
	Shadowbox.open({
	        content:    sPath,
	        player:     "iframe",
	        height:     pHeight,
		width:      pWidth
	});

     }      
 <?php echo "MostrarTablaConsultaPersonal(1,'TablaDatos','".session_id()."','',1)";?>   
</script>
