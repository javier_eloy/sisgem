<html>
<meta http-equiv="Content-Type" content="text/html"; charset=utf-8"/> 
<link rel="stylesheet" type="text/css" media="all" href="../../publico/js/sexyalertbox/sexyalertbox.css"/>
<title>PDVSA</title>
<!-- validacion de campos Obligatorios -->
<!--<script type="text/javascript" src="../publico/js/vanadium/jquery.min.js"></script>
<script type="text/javascript" src="../publico/js/vanadium/vanadium.js"></script>-->
<link href="../../publico/js/vanadium/style.css" rel="stylesheet" type="text/css" />

<link href="../../publico/js/Archivos/ddaccordion.css" rel="stylesheet" type="text/css">
<link href="../../publico/js/Archivos/styleestancia.css" rel="stylesheet" type="text/css">
<link href="../../publico/js/Archivos/contenido_style.css" rel="stylesheet" type="text/css">
<link href="../../publico/js/Archivos/contentslider.css" rel="stylesheet" type="text/css">
<!--<link href="../publico/js/Archivos/virtualpaginator.css" rel="stylesheet" type="text/css">-->
<link href="../../publico/js/Archivos/image-slideshow-vertical.css" rel="stylesheet" type="text/css">
   <script type="text/javascript" src="../../publico/js/jquery-1.7.2.js"></script>
   <script type="text/javascript" src="../../aplicacion/cliente/sesion.js"></script><!--*-->
   <script type="text/javascript" src="../../publico/js/Archivos/funciones.js"></script><!--*-->
   <script type="text/javascript" src="../../publico/js/jquery-ui.js"></script>
   <script type="text/javascript" src="../../publico/js/jquery.ui-1.10.3.datepicker-es.js"></script>
<link type="text/css" rel="stylesheet" href="../../publico/js/Archivos/tools.css">
<link type="text/css" rel="stylesheet" href="../../publico/js/Archivos/estilos.css">
<link type="text/css" rel="stylesheet" href="../../publico/js/Archivos/estilosui.css">


   <link   rel="stylesheet"       type="text/css" href="../../publico/estilos/jquery-ui-1.10.3.custom.min.css" >
   <script type="text/javascript" src="../../publico/js/prototype.js"></script>
   <link type="text/css"  href="../../publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
    <!--Clase para para manejo de encabezado de tabla fija--> 
   <link type="text/css"  href="../../publico/estilos/encabezado_fijo_tabla.css" rel="stylesheet"/>
<!--   <script type="text/javascript" src="../../publico/js/shadowbox/shadowbox_1.js"></script>
   <script type="text/javascript" src="../../publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
   <script type="text/javascript">
      Shadowbox.init({
         language: "es",
         modal: true,
         players: ['html', 'php'],
         overlayColor: "#000",
         overlayOpacity: "0.5"});
   </script>-->
<?php 
require_once("../../aplicacion/configuracion/aut_lib.inc.php");


// Se obtiene el periodo inicial de la consulta, que por descarte es:
// la fecha inicio el 1er dia del mes y la fecha fin, el día de hoy
if (!isset($_GET["fecha_ini"]) OR !isset($_GET["fecha_fin"]) OR !isset($_GET["orden"])) {
    $fecha_ini='01/'.date("m").'/'.date("Y");
    $fecha_fin=date("d/m/Y");
    $orden='0';
}  else {
    $fecha_ini=$_GET["fecha_ini"];
    $fecha_fin=$_GET["fecha_fin"];
    $orden=$_GET["orden"];
}

$datosReporte    = new reporte;
$objInventario   = new inventarioSAP();
$arrDatosReporte = $datosReporte->SelecHistPersonal($_GET["co_usuario"],$fecha_ini,$fecha_fin,$orden);

$estilo='style="BORDER-RIGHT: Silver solid; BORDER-TOP: Silver solid; BORDER-LEFT: Silver solid;
	BORDER-BOTTOM: Silver solid" align="center" width="20" height="24" bgcolor="FFFFFF"';

header('Content-Type: text/html; charset=UTF-8');
?>
<link href="../../publico/js/shadowbox/blccbx.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="../../publico/js/vanadium/jquery.min.js"></script>
<script type="text/javascript" src="../../publico/js/vanadium/vanadium.js"></script>-->

<body  topmargin="0" bgcolor="#ffffff">
    <div class="titulomodulo">Detalle de hist&oacute;rico de Personal</div>	 
    <input type="hidden" id="page" name="page" value="" />
<form method="POST" name="formulario" id="formulario" action="detalle_historico_personal.php">    
    <input type="hidden" name="co_usuario" id="co_usuario" value="<?php echo $_GET["co_usuario"] ?>">
    <table id="texto_informativo">
        <tr>
            <td>
                <span class="etiqueta">Ordenar por:</span> 
            </td>
            <td colSpan="2">
                <select name="orden" class=":required texto" id="tipo_fecha" >
                    <option value =  "<?php if (!isset($_GET["orden"])) echo '0';
                                            Else echo $_GET["orden"];?>
                                     ">
                        <?php
                            if (!isset($_GET["orden"])) echo 'Fecha de Inicio';
                            Else 
                                switch ($orden) {
                                    case 0;
                                        echo 'Fecha de inicio';
                                    break;
                                    case 1;
                                        echo 'Fecha de culminaci&oacute;n';
                                    break;
                                    case 2;
                                        echo 'Fecha de asignaci&oacute;n';
                                    break;                            
                                    case 3;
                                        echo 'Fecha de programaci&oacute;n';
                                    break;  
                                    case 4;
                                        echo 'Fecha de planificaci&oacute;n';
                                    break;                              
                                }
                        ?>
                    </option>
                    <option value =  "0">Fecha de inicio</option>
                    <option value =  "1">Fecha de culminaci&oacute;n</option>
                    <option value =  "2">Fecha de asignaci&oacute;n</option>   
                    <option value =  "3">Fecha de programaci&oacute;n</option> 
                    <option value =  "4">Fecha de planificaci&oacute;n</option> 
                </select>
            </td>
        </tr>        
        <tr class="etiqueta">
            <td>
                <span class="etiqueta">Rango de fecha: </span> 
            </td>            
            <td align="left" nowrap>
                <span><input type="text" id="datepicker" class=":required textofecha_avanzado" name="fechai" readonly value="<?php if(!isset($_GET["fecha_ini"])){echo $fecha_ini;} Else {echo $_GET["fecha_ini"];}?>" size="7"/></span>
            </td>
            <td align="left" nowrap>    
                <span><input type="text" id="datepicker2" target="_blank" class=":required textofecha_avanzado" name="fechaf" readonly value="<?php if(!isset($_GET["fecha_fin"])){echo $fecha_fin;} Else {echo $_GET["fecha_fin"];}?>" size="7"/></span>
            </td>
        </tr>
    </table> 
</form>

<table id="texto_informativo" width="20%">
    <tr>
        <td>
            <span><input class="boton" type="button" name="btnUpdate"  id="btnUpdate"  value="Actualizar"></span>
        </td>
<!--    <input class="submit boton" name="Reporte" value="Imprimir reporte" type="button" onclick="Enviar();"/>-->
        <td align="rigth">
            <span><a class='printButton' target='_blank' href='detalle_historico_impreso.php?p=51&orden=<?php echo $orden;?>&fecha_ini=<?php echo $fecha_ini;?>&fecha_fin=<?php echo $fecha_fin;?>&co_usuario=<?php echo $_GET["co_usuario"];?>'></a></span>
        </td>
    </tr>
</table>
<div class="etiqueta" id="Cargando">Cargando...</div>
<!--<div id="shadow_hidden"></div>
<div id="flotante"></div>
<div id="loading"></div>
<div id="resultado_cerrarorden"  class="resultado"></div>-->
<br><br>
    <center>
    <table class="tablaformulario" id="table-1">
        <!--<tbody>-->
        <thead>         
        <tr>
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>N° de Orden</font></th>
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>C&oacute;digo / Caracter&iacute;sticas</font></th>
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>Lugar</font></th>
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>Hoja de Ruta</font></th>
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>Sem Plan</font></th>  
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>Sem Programaci&oacute;n</font></th>  
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>Fecha Asignaci&oacute;n</font></th> 
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>Fecha Inicio</font></th>  
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>Fecha Fin</font></th>  
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>Notas</font></th>  
            <th scope="col" class="headcol" bgcolor=Gray align="center"><font color=White>Opci&oacute;n</font></th>  
        </tr>
        </thead>
        
        <tbody>
        <?php 
        while(!$arrDatosReporte->EOF)
        {  
            list($anoi, $mesi, $diai) = explode("-",$arrDatosReporte->fields["FE_INICIO"]);
            if ($diai == "00"){
                $fecha_inicio = "NO EJECUTADO"; 
            }
            else{
               $fecha_inicio = $diai."-".$mesi."-". $anoi;  
            }
            list($anof, $mesf, $diaf) = explode("-",$arrDatosReporte->fields["FE_FIN"]);
            if ($diaf == "00"){
                $fecha_fin = "NO EJECUTADO"; 
            }
            else{
                $fecha_fin = $diaf."-".$mesf."-". $anof;
            }             
            list($anop, $mesp, $diap) = explode("-",$arrDatosReporte->fields["FE_PROXIMA_PLAN"]);            
            $fecha_plan = $diap."-".$mesp."-". $anop; 
            $semana_plan = date( 'W-o', strtotime($arrDatosReporte->fields["FE_PROXIMA_PLAN"]));
            list($anog, $mesg, $diag) = explode("-",$arrDatosReporte->fields["PROG"]);
            $fecha_programacion = $diag."-".$mesg."-". $anog; 
            $semana_prog = date( 'W-o', strtotime($arrDatosReporte->fields["PROG"]));
            list($anoa, $mesa, $diaa) = explode("-",$arrDatosReporte->fields["ASIG"]);
            $fecha_asignacion = $diaa."-".$mesa."-". $anoa; 
            if ($arrDatosReporte->fields['VA_TIPO_PLAN'] == "E") {
                $arrayDatos = $objInventario->SelecDetalleEqpSAP($arrDatosReporte->fields['VA_DETALLE']);
                $Desc = $arrayDatos[1]['EQKTU'];
                $sCodigo = $arrayDatos[1]['TPLNR'];
                $arrayDatos = $objInventario->SelecUbicacionTecnica($sCodigo);
                $sUbic = $arrayDatos[1]['TXT_TPLMA'];
                $equipo = '('.ltrim($arrDatosReporte->fields['VA_DETALLE'], "0") .')<br>'. $Desc;
                $s2Codigo = $arrayDatos[1]['TPLMA'];
                $arrayDatos = $objInventario->SelecUbicacionTecnica($s2Codigo);
                $s2Ubic = $arrayDatos[1]['TXT_TPLMA'];
                $sUbic = $sUbic. '.<br>'.$s2Ubic;
            } else {
                $arrayDatos = $objInventario->SelecUbicacionTecnica($arrDatosReporte->fields['VA_DETALLE']);
                $cUbic = $arrayDatos[1]['TPLNR'];
                $sUbic = $arrayDatos[1]['TXT_TPLMA'];
                $Desc = $arrayDatos[1]['PLTXU'];
                $equipo = '('.$cUbic .')<br>'. $Desc;
                $s2Codigo = $arrayDatos[1]['TPLMA'];
                $arrayDatos = $objInventario->SelecUbicacionTecnica($s2Codigo);
                $s2Ubic = $arrayDatos[1]['TXT_TPLMA'];
                $sUbic = $sUbic. '.<br>'.$s2Ubic;
            }
            $param=$arrDatosReporte->fields['co_actividad'];
            echo '
            <tr>
                <td scope="col" class="titulomodulo">'.$arrDatosReporte->fields["NU_ORDEN"]   .'</td>
                <td nowrap scope="col" class="titulomodulo">'. $equipo .'</td>
                <td scope="col" class="titulomodulo">'.utf8_encode($sUbic)                        .' </td>
                <td scope="col" class="titulomodulo">'.$arrDatosReporte->fields["TX_HOJARUTA"]    .'</td>
                <td scope="col" class="titulomodulo">'.$semana_plan                               .'</td>
                <td scope="col" class="titulomodulo">'.$semana_prog                               .'</td>
                <td scope="col" class="titulomodulo">'.$fecha_asignacion                          .'</td>
                <td scope="col" class="titulomodulo">'.$fecha_inicio                              .'</td> 
                <td scope="col" class="titulomodulo">'.$fecha_fin                                 .'</td> 
                <td scope="col" class="titulomodulo">'.$arrDatosReporte->fields["TX_OBSERVACION"] .'</td> 
                <td scope="col" class="titulomodulo"><br><a target="_blank" href="formato_actividad.php?p='.$param.'">Ver Detalle</a></br> </td>    
            </tr>';   
            $arrDatosReporte->MoveNext();
        }
        ?>
        </tbody>
    </table>
    <table class="tablaformulario" id="header-fixed"></table>

      <script type="text/javascript">
         var jq = jQuery.noConflict();
         var tableOffset = jq("#table-1").offset().top;
         var $header = jq("#table-1 > thead").clone();
         var $fixedHeader = jq("#header-fixed").append($header);         
         
         jq(function() {
             
            
            // Configuración de calendarios             
              jq("#datepicker").datepicker({ 
                  maxDate:"+0d",
                  showWeek:true,
                 weekHeader:"Sem.",
                  showOn: "both",
                  buttonImage: "../../publico/imagenes/calendar-icon.png",
                  buttonImageOnly: true
                 });

              jq("#datepicker2").datepicker({ 
                  maxDate:"+0d",
                  showWeek:true,
                 weekHeader:"Sem.",
                  showOn: "both",
                  buttonImage: "../../publico/imagenes/calendar-icon.png",
                  buttonImageOnly: true
                 });
                 
         });

       //Deshabilita y habilita "Cargando..." Mientras se procesa al darle clic a "Actualizar"
         jq('#Cargando')
			.hide()
			.ajaxStart(function() {
				$(this).show();
			})
			.ajaxStop(function() {
				$(this).hide();
			})
		;     


       //Habilitar botón actualizar al cambiar algún valor del calendario o menú desplegable
         jq("#datepicker").change(function() {
            jq("#btnUpdate").removeAttr("disabled");
         });
         
         jq("#datepicker2").change(function() {
            jq("#btnUpdate").removeAttr("disabled");
         });

        jq("#tipo_fecha").change(function() {
           jq("#btnUpdate").removeAttr("disabled");
        });  

      // Evento del Boton Actualizar
      jq("#btnUpdate").click(function() {
         var dateini = jq("#datepicker").datepicker('getDate');
         var datefin = jq("#datepicker2").datepicker('getDate');
         var tipo_fecha = jq("#tipo_fecha").val();
         var co_usuario = jq("#co_usuario").val();
         jq('#formulario').attr('action', "detalle_historico_personal.php?orden="+tipo_fecha+"&fecha_ini=" + jq.datepicker.formatDate("dd/mm/yy", dateini) + "&fecha_fin=" + jq.datepicker.formatDate("dd/mm/yy", datefin)+"&co_usuario=" + co_usuario);
         jq('#formulario').submit();         
      });
      
      //Cabecera Fija
      jq(window).bind("scroll", function() {
          
            var offset = jq(this).scrollTop();
            
            if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                $fixedHeader.show();
                //alert('Prueba1');
            }
            else if (offset < tableOffset) {
                $fixedHeader.hide();
                //alert('Prueba2');
            }          

      });      
      

      
      </script>
</body>
</html>
