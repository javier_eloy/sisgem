<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Grafico PPAE</title>
	<link rel="stylesheet" type="text/css" href="../aplicacion/librerias/flot/jquery-ui/jquery-ui.min.css">
	<script language="javascript" type="text/javascript" src="../aplicacion/librerias/flot/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="../aplicacion/librerias/flot/jquery-ui/jquery-ui.min.js"></script>
	<script language="javascript" type="text/javascript" src="../aplicacion/librerias/flot/jquery.flot.js"></script>
	<script language="javascript" type="text/javascript" src="../aplicacion/librerias/flot/jquery.flot.orderBars.js"></script>
	<script type="text/javascript" language="javascript" src="../aplicacion/librerias/flot/jquery.flot.axislabels.js"></script>
	<script language="javascript" type="text/javascript" src="../aplicacion/librerias/flot/jquery.flot.resize.js"></script>
    <script type="text/javascript">
    
    $(document).ready(function () {
 



            var planificado=JSON.parse(window.localStorage.getItem("V_planificado"));
			var asignado=JSON.parse(window.localStorage.getItem("V_asignado"));
			var ejecutado=JSON.parse(window.localStorage.getItem("V_ejecutado"));
			var programado=JSON.parse(window.localStorage.getItem("V_programado"));
			var info_x = JSON.parse(window.localStorage.getItem("V_x"));
 		

            var Month_planificado=JSON.parse(window.localStorage.getItem("Month_planificado"));
            var Month_asignado=JSON.parse(window.localStorage.getItem("Month_asignado"));
            var Month_ejecutado=JSON.parse(window.localStorage.getItem("Month_ejecutado"));
            var Month_programado=JSON.parse(window.localStorage.getItem("Month_programado"));
            var Month_info_x = JSON.parse(window.localStorage.getItem("Month_x"));
            
           

            /*

                planificado   = cantidad de actividades planificadas por semanas
                asignado      = cantidad de actividades asignadas por semanas
                ejecutado     = cantidad de actividades ejecuatas por semanas
                programado    = cantidad de actividades programadas por semanas
                info_x             = eje X por semanas

                Month_planificado   = cantidad de actividades planificadas por meses
                Month_asignado      = cantidad de actividades asignadas por meses
                V_ejecutado          = cantidad de actividades ejecuatas por meses
                Month_programado    = cantidad de actividades programadas por meses
                Month_info_x        = eje X por meses
            */


 			//crea el vetor con la informacion necesaria
            var data = {
                "planificado":{label: "planificado", data: planificado, bars: {fillColor: "#336600"}, color: "#336600"},
                "programado":{label: "programado", data: programado, bars: {fillColor: "#4572A7"}, color: "#4572A7"},
                "asignado":{label: "asignado", data: asignado, bars: {fillColor: "#AA4643"}, color: "#AA4643"},
                "ejecutado":{label: "ejecutado", data: ejecutado, bars: {fillColor: "#89A54E"}, color: "#89A54E"}
                
            };


           


         

            // las opciones del grafico

            var options = {
                xaxis: {
                    mode: null,
                    ticks: info_x,
                    tickLength: 0,
                    axisLabel: "Semanas-Mes-Año",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                }, yaxis: {
                    axisLabel: "Cantidad",
                    tickDecimals: 0,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                    axisLabelPadding: 5
                }, grid: {
                    hoverable: true,
                    clickable: false,
                    borderWidth: 1
                }, legend: {
                    labelBoxBorderColor: "none",
                    position: "right"
                }, series: {
                    shadowSize: 1,
                    bars: {
                        show: true,
                        barWidth: 0.06,
                        order: 1
                    }
                }
            };


            //crea los cheecks para el grafico
            var choiceContainer = $("#choices");
            $.each(data, function(key, val) {

                choiceContainer.append("<div class ='vals'><input type='checkbox' name='" + val["label"] +
                    "' checked='checked' id='id" + val["label"] + "'></input>" +
                    "<label for='id" + val["label"] + "'>"
                    + val["label"] + "</label><div>");
            });
 

            //modifica la fecha del grafico
            $("#button_date").click(function()
                {
                    if($("#button_date").val()==="Semanas")
                    {
                        data = {
                        "planificado":{label: "planificado", data: planificado, bars: {fillColor: "#336600"}, color: "#336600"},
                        "programado":{label: "programado", data: programado, bars: {fillColor: "#4572A7"}, color: "#4572A7"},
                        "asignado":{label: "asignado", data: asignado, bars: {fillColor: "#AA4643"}, color: "#AA4643"},
                        "ejecutado":{label: "ejecutado", data: ejecutado, bars: {fillColor: "#89A54E"}, color: "#89A54E"}};
                        $("#button_date").val("Meses");
                         options = {
                                xaxis: {
                                    mode: null,
                                    ticks: info_x,
                                    tickLength: 0,
                                    axisLabel: "Semanas-Mes-Año",
                                    axisLabelUseCanvas: true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                    axisLabelPadding: 5
                                }, yaxis: {
                                    axisLabel: "Cantidad",
                                    tickDecimals: 0,
                                    axisLabelUseCanvas: true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                    axisLabelPadding: 5
                                }, grid: {
                                    hoverable: true,
                                    clickable: false,
                                    borderWidth: 1
                                }, legend: {
                                    labelBoxBorderColor: "none",
                                    position: "right"
                                }, series: {
                                    shadowSize: 1,
                                    bars: {
                                        show: true,
                                        barWidth: 0.06,
                                        order: 1
                                    }
                                }
                        };

                        plotAccordingToChoices();

                    }
                    else
                    {
                        data = {
                        "planificado":{label: "planificado", data: Month_planificado, bars: {fillColor: "#336600"}, color: "#336600"},
                        "programado":{label: "programado", data: Month_programado, bars: {fillColor: "#4572A7"}, color: "#4572A7"},
                        "asignado":{label: "asignado", data: Month_asignado, bars: {fillColor: "#AA4643"}, color: "#AA4643"},
                        "ejecutado":{label: "ejecutado", data: Month_ejecutado, bars: {fillColor: "#89A54E"}, color: "#89A54E"}};
                         $("#button_date").val("Semanas");
                         options = {
                                xaxis: {
                                    mode: null,
                                    ticks: Month_info_x,
                                    tickLength: 0,
                                    axisLabel: "Mes-Año",
                                    axisLabelUseCanvas: true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                    axisLabelPadding: 5
                                }, yaxis: {
                                    axisLabel: "Cantidad",
                                    tickDecimals: 0,
                                    axisLabelUseCanvas: true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelFontFamily: "Verdana, Arial, Helvetica, Tahoma, sans-serif",
                                    axisLabelPadding: 5
                                }, grid: {
                                    hoverable: true,
                                    clickable: false,
                                    borderWidth: 1
                                }, legend: {
                                    labelBoxBorderColor: "none",
                                    position: "right"
                                }, series: {
                                    shadowSize: 1,
                                    bars: {
                                        show: true,
                                        barWidth: 0.06,
                                        order: 1
                                    }
                                }
                        };
                         plotAccordingToChoices();
                    }
                });


            choiceContainer.find("input").click(plotAccordingToChoices);



            //imprime el grafico acorde a las opciones dadas
            function plotAccordingToChoices() {

            var datasets = [];

            choiceContainer.find("input:checked").each(function () {
                var key = $(this).attr("name");
                
                datasets.push(data[key]);
                
            });

            
            if (datasets.length > 0) {
                
                $.plot($("#placeholder"), datasets, options);
            }
        }


      
        //son los valores extremos que puede alcanzar el grafico
        $(".demo-container").resizable({
            maxWidth: 3000,
            maxHeight: 500,
            minWidth: 450,
            minHeight: 250
        });

        plotAccordingToChoices();
           
 
        });

	</script>
</head>
<body style="background-color:white;">
<div class="settings">
    <p id="text">Cambiar formato de la fecha a :</p>
    <input id="button_date" type="button" value="Meses" class="button" />
    <div id="choices"></div>
</div>
<div id="message">
    <p>puede cambiar el tamaño del grafico arrastrando el lado inferior </br> y derecho del mismo</p>
</div>

	<div id="content" >

		<div class="demo-container">
			
            <div id="placeholder" class="demo-placeholder" style="float:left;"></div>
			
		</div>

		
	</div>

</body>
</html>
<style type="text/css">
	
        #text
        {
            font-size: 15px;
            padding-left: 17px;
        }

        #message
        {
            font-size: 13px;
            color: #808080;
            padding-top: 39px;
            padding-left: 20px;

        }

        .settings
        {
            margin-top: 25px;
        }
        .settings *
        {
            float: left;
            
        }
        .settings input[type=button]
        {
            margin-left: 10px;
            margin-bottom: 10px;
        }
		p 
		{
			margin-top: 10px;
		}

        #choices
        {
            float: left;
            position: relative;
           
        }

		input[type=checkbox] {
			margin: 7px;
		}


		#content {
			width: 800px;
			margin: 0;
			padding: 10px;
		}

		.demo-container {
			box-sizing: border-box;
			width: 1500px;
			height: 400px;
			padding: 20px 15px 15px 15px;
			margin: 15px auto 30px auto;
			border: 1px solid #ddd;
			background: #fff;
			background: linear-gradient(#f6f6f6 0, #fff 50px);
			background: -o-linear-gradient(#f6f6f6 0, #fff 50px);
			background: -ms-linear-gradient(#f6f6f6 0, #fff 50px);
			background: -moz-linear-gradient(#f6f6f6 0, #fff 50px);
			background: -webkit-linear-gradient(#f6f6f6 0, #fff 50px);
			box-shadow: 0 3px 10px rgba(0,0,0,0.15);
			-o-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
			-ms-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
			-moz-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
			-webkit-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
            margin-top: 30px;
		}

		.demo-placeholder {
			width: 100%;
			height: 100%;
			font-size: 14px;
			line-height: 1.2em;
		}

		.legend table {
			border-spacing: 5px;
		}
</style>
