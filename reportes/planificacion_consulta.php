<meta http-equiv="Content-Type" content="text/html"; charset=utf-8"/> 
<link rel="stylesheet" type="text/css" media="all" href="../publico/js/sexyalertbox/sexyalertbox.css"/>
<title>PDVSA</title>
<!-- validacion de campos Obligatorios -->

<script type="text/javascript" src="../publico/js/vanadium/jquery.min.js"></script>
<script type="text/javascript" src="../publico/js/vanadium/vanadium.js"></script>
<link href="../publico/js/vanadium/style.css" rel="stylesheet" type="text/css" />

<link href="../publico/js/Archivos/ddaccordion.css" rel="stylesheet" type="text/css">
<link href="../publico/js/Archivos/styleestancia.css" rel="stylesheet" type="text/css">
<link href="../publico/js/Archivos/contenido_style.css" rel="stylesheet" type="text/css">
<link href="../publico/js/Archivos/contentslider.css" rel="stylesheet" type="text/css">
<link href="../publico/js/Archivos/virtualpaginator.css" rel="stylesheet" type="text/css">
<link href="../publico/js/Archivos/image-slideshow-vertical.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/tools.css">
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilosui.css">		
</head>

<?php 
include("../aplicacion/librerias/adodb5/adodb.inc.php");
include("../aplicacion/librerias/adodb5/adodb-exceptions.inc.php");
include("../aplicacion/configuracion/aut_config.inc.php");
include("../aplicacion/servidor/_conexion/conexion.class.php");
include("../aplicacion/librerias/saprfc/saprfc.class.php");
include_once ("../aplicacion/servidor/inventario/inventarioSAP.class.php");
include("../aplicacion/servidor/reporte/reporte.class.php");
require_once 'consulta_pdf.php';
$objInventario = new inventarioSAP();

// FUNCI�N DE SUMA DE FECHAS
function suma_fechas($fecha,$ndias)
    {
        if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha))
            list($dia,$mes,$ano)=split("/", $fecha);
        if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha))
            list($dia,$mes,$ano)=split("-",$fecha);
        $nueva = mktime(0,0,0, $mes,$dia,$ano) + $ndias * 24 * 60 * 60;
        $nuevafecha=date("d-m-Y",$nueva);
        return ($nuevafecha);  
    }
header('Content-Type: text/html; charset=UTF-8');
?>
<link href="../publico/js/shadowbox/blccbx.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../publico/js/vanadium/jquery.min.js"></script>
<script type="text/javascript" src="../publico/js/vanadium/vanadium.js"></script>

<body topmargin="0" bgcolor="#ffffff">
<br><br>
 <?php
 if (isset($_GET['Desd'])){
    $sem_anno        = $_GET['Desd'];
    $objReporte      = new reporte();
    $ActivoManten    = $objReporte->SelecPlan($sem_anno);	   


    ?>	
    <center>
    <table >
    <thead>
        <tr>
            <th scope="col" class="titulomenu">c&oacute;digo / Caracter&iacute;sticas</th>
            <th scope="col" class="titulomenu">Lugar</th>
            <th scope="col" class="titulomenu">Actividades<br /></th>
            <th scope="col" class="titulomenu">Fecha Plan</th>   
            <th scope="col" class="titulomenu">Fecha Programaci&oacute;n</th>
            <th scope="col" class="titulomenu">Fecha Asignaci&oacute;n</th>
        </tr>
    </thead>
    <tbody>

    <?php
    $cont=0;
    $fill=0;
    $i=0;
    $pag = 1;
    $pdf->Ln();
    $conteo = 0; 
    while(!$ActivoManten->EOF){
        list($ano, $mes, $dia) = explode("-", $ActivoManten->fields['Proximo1']);
        $fecha_prox_mtto = $dia."-".$mes."-". $ano;
        list($anog, $mesg, $diag) = explode("-",$ActivoManten->fields["PROG"]);
        $fecha_programacion = $diag."-".$mesg."-". $anog; 
        list($anoa, $mesa, $diaa) = explode("-",$ActivoManten->fields["ASIG"]);
        $fecha_asignacion = $diaa."-".$mesa."-". $anoa; 
        // conteo PERMITE VERIFICAR LAS VECES CUANTOS PLANES SE HAN IMPRIMIDO
        $conteo = $conteo + 1;
        $coplan = $ActivoManten->fields['CoPlan'];
        $ubicacion = $ActivoManten->fields['Ubicacion'];
        $arrayDatos = $objInventario->SelecUbicacionTecnica($ubicacion);
        $lugar = $arrayDatos[1]['TXT_TPLMA'];
        $Desc = $arrayDatos[1]['PLTXU'];
        if ($ActivoManten->fields['TipoPlan'] == "E") {
           $arrayDatos = $objInventario->SelecDetalleEqpSAP($ActivoManten->fields['Equipo']);
           $Desc = $arrayDatos[1]['EQKTU'];
        }
        ?>	<tr> 
                    <td  class="titulomodulo"> <?php echo ltrim($ActivoManten->fields['Equipo'],"0").'<br>'.$Desc; ?> </td>
                    <td  class="titulomodulo"> <?php echo $lugar;?> </td>
                    <td  class="titulomodulo"> <?php echo $ActivoManten->fields['Actividad']; ?> </td>
                    <td  class="titulomodulo"><?php echo $fecha_prox_mtto; ?></td>
                    <td  class="titulomodulo"><?php echo $fecha_programacion; ?></td>
                    <td  class="titulomodulo"><?php echo $fecha_asignacion; ?></td>
                </tr>                	                
        <?php                                
         if($i==37){
            $i=0;
            $pag++;
            $pdf->SetX(75);
            $esp = 20;
            $pdf->Cell(65,6,'',0,0,'C');
            $pdf->Ln(5);
            $pdf->SetX(10);

        }
        $pdf->SetX(10);
        $pdf->Cell($tam[0],5,ltrim($ActivoManten->fields['Equipo'],"0").' '.$Desc,0,0,'L',$fill);
        $pdf->Cell($tam[1],5,$lugar,0,0,'L',$fill);
        $pdf->Cell($tam[2],5,$ActivoManten->fields['Actividad'],0,0,'L',$fill);
        $pdf->Cell($tam[3],5,$fecha_prox_mtto,0,0,'C',$fill);
        $pdf->Cell($tam[4],5,$fecha_programacion,0,0,'C',$fill);
        $pdf->Cell($tam[5],5,$fecha_asignacion,0,1,'C',$fill);
        $i++;
        $cont++;
        $ActivoManten->MoveNext();
    }
    ?>
            </tbody>
        </table>
        </center>
        <?php

}
        $pdf->Close();
        $pdf->ruta = "../aplicacion/librerias/fpdf/reports/".$_SESSION["usuario_id"]."_reporte_plan.PDF";
        $pdf->Output($pdf->ruta,"F");
?>
<script>
         URL = '<?php echo $pdf->ruta; ?>';
	 day = new Date();
         id = day.getTime();
         //alert("entro")
         Centrada=true;
         Posx=0;
         Posy=0;
         Ancho=980;
         Alto=650;
         OpcionesVentana="toolbar=yes,scrollbars=yes,location=0,statusbar=0,status=1,menubar=0,resizable=0";
	 OpcionesVentana="";
         if (Centrada){
          PosX = (screen.availWidth - Ancho)/2;
          PosY = (screen.availHeight - Alto)/2;
          }
	 PosX = (screen.availWidth - Ancho)/2;
	 PosY = Posy;
	 if (OpcionesVentana == ''){ 
             OpcionesVentana = 'width=' + Ancho;
         }
         else{
             OpcionesVentana += ',width=' + Ancho;
         }
         OpcionesVentana += ',height=' + Alto + ',left=' + PosX + ',top=' + PosY;
         window.open(URL,id,OpcionesVentana);
	 //location.href='pt_rol.php?ROL=' + rol + '&CO_CATALOGO_ROL=' + co_cat + '&NB_CATALOGO_ROL='+nb_cat;
</script>
</body>
