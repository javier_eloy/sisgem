<html>
<meta http-equiv="Content-Type" content="text/html"; charset=utf-8"/> 
<link rel="stylesheet" type="text/css" media="all" href="../../publico/js/sexyalertbox/sexyalertbox.css"/>
<title>PDVSA</title>
<!-- validacion de campos Obligatorios -->
<!--<script type="text/javascript" src="../publico/js/vanadium/jquery.min.js"></script>
<script type="text/javascript" src="../publico/js/vanadium/vanadium.js"></script>-->
<link href="../../publico/js/vanadium/style.css" rel="stylesheet" type="text/css" />

<link href="../../publico/js/Archivos/ddaccordion.css" rel="stylesheet" type="text/css">
<link href="../../publico/js/Archivos/styleestancia.css" rel="stylesheet" type="text/css">
<link href="../../publico/js/Archivos/contenido_style.css" rel="stylesheet" type="text/css">
<link href="../../publico/js/Archivos/contentslider.css" rel="stylesheet" type="text/css">
<!--<link href="../publico/js/Archivos/virtualpaginator.css" rel="stylesheet" type="text/css">-->
<link href="../../publico/js/Archivos/image-slideshow-vertical.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="../../publico/js/Archivos/tools.css">
<link type="text/css" rel="stylesheet" href="../../publico/js/Archivos/estilos.css">
<link type="text/css" rel="stylesheet" href="../../publico/js/Archivos/estilosui.css">		

<?php 

require_once("../../aplicacion/configuracion/aut_lib.inc.php");

$datosReporte    = new reporte;
$arrDatosReporte = $datosReporte->SelecHistEquipoInstalacion($_GET["objeto_tecnico"]);

$estilo='style="BORDER-RIGHT: Silver solid; BORDER-TOP: Silver solid; BORDER-LEFT: Silver solid;
	BORDER-BOTTOM: Silver solid" align="center" width="20" height="24" bgcolor="FFFFFF"';

header('Content-Type: text/html; charset=UTF-8');
?>
<link href="../../publico/js/shadowbox/blccbx.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../publico/js/vanadium/jquery.min.js"></script>
<script type="text/javascript" src="../../publico/js/vanadium/vanadium.js"></script>

<body topmargin="0" bgcolor="#ffffff">
<br><br>
    <center>
    <table class="tablaformulario">
        <thead>         
        <tr>
            <th scope="col" class="titulomenu" bgcolor=Gray align="center"><font color=White>N° Orden</font></th>
            <th scope="col" class="titulomenu" bgcolor=Gray align="center"><font color=White>Hoja de Ruta</font></th>
            <th scope="col" class="titulomenu" bgcolor=Gray align="center"><font color=White>Fecha Plan</font></th>
            <th scope="col" class="titulomenu" bgcolor=Gray align="center"><font color=White>Fecha Prog</font></th>
            <th scope="col" class="titulomenu" bgcolor=Gray align="center"><font color=White>Fecha Asig</font></th>
            <th scope="col" class="titulomenu" bgcolor=Gray align="center"><font color=White>Fecha Ini</font></th>
            <th scope="col" class="titulomenu" bgcolor=Gray align="center"><font color=White>Fecha Fin</font></th>  
            <th scope="col" class="titulomenu" bgcolor=Gray align="center"><font color=White>Notas</font></th>
        </tr>
        </thead>
        <?php 
        while(!$arrDatosReporte->EOF)
        { 
            if($arrDatosReporte->fields["FE_PLAN"] === '00-00-0000') 
                $fecha_plan = '&nbsp;';
            else{
                $fecha_plan = $arrDatosReporte->fields["FE_PLAN"]; 
            }
            if($arrDatosReporte->fields["FE_PROG"] === '00-00-0000') 
                $fecha_progr = '&nbsp;';
            else{
                $fecha_progr = $arrDatosReporte->fields["FE_PROG"]; 
            }
            if($arrDatosReporte->fields["FE_ASIG"] === '00-00-0000') 
                $fecha_asign = '&nbsp;';
            else{
                $fecha_asign = $arrDatosReporte->fields["FE_ASIG"]; 
            }
            if($arrDatosReporte->fields["FE_INICIO"] === '00-00-0000') 
                $fecha_inicio = '&nbsp;';
            else{
                $fecha_inicio = $arrDatosReporte->fields["FE_INICIO"]; 
            }
            if($arrDatosReporte->fields["FE_FIN"] === '00-00-0000') 
                $fecha_fin = '&nbsp;';
            else{
                $fecha_fin = $arrDatosReporte->fields["FE_FIN"]; 
            }
            echo '
            <tr>
                <td scope="col" class="titulomodulo">'.$arrDatosReporte->fields["NU_ORDEN"]      .'</td>
                <td scope="col" class="titulomodulo">'.$arrDatosReporte->fields["TX_HOJARUTA"]   .'</td>
                <td scope="col" class="titulomodulo">'.$fecha_plan                               .'</td>
                <td scope="col" class="titulomodulo">'.$fecha_progr                              .'</td>
                <td scope="col" class="titulomodulo">'.$fecha_asign                              .'</td>
                <td scope="col" class="titulomodulo">'.$fecha_inicio                             .'</td>
                <td scope="col" class="titulomodulo">'.$fecha_fin                                .'</td>  
                <td scope="col" class="titulomodulo">'.$arrDatosReporte->fields["TX_OBSERVACION"].'</td>
            </tr>';   
            $arrDatosReporte->MoveNext();
        }
        ?>
    </table>
</body>
</html>