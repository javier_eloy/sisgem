<?php
require('../aplicacion/librerias/fpdf/fpdf.php');
class PDF extends FPDF
{
//Cabecera de página
 var $ruta;
 var $col = array ("Codigo / Caracteristica    ","                         Lugar                            ","                  Actividades                 ","      Responsable / Fecha      ");

function Header()
{
    //Logo
    $this->Image('../aplicacion/librerias/fpdf/logopdvsa.jpg',10,8,40);
    $this->SetFont('Arial','B',12);
    $this->SetFillColor(224,235,255);
	$this->SetTextColor(0);
	$this->SetDrawColor(0,0,0);
	$this->SetLineWidth(.3);
        $this->Ln(5);
	$this->SetX(230);
        $this->Cell(50,10,' Asignacion por semana ',0,0,'R');
	$this->Ln(10);
        $this->SetFont('Arial','B',10);
        $this->SetTextColor(204,0,0);
        $this->Cell(20,10,'Usuario: ',0,0,'L');
        $this->SetTextColor(0);
        $this->SetX(27);
        $this->Cell(20,10,$_SESSION['usuario_nombre'],0,0);
        $this->Ln(5);
        $this->SetTextColor(204,0,0);
        $this->Cell(20,10,'Fecha: ',0,0,'L');
        $this->SetTextColor(0);
        $this->SetX(27);
        $this->Cell(20,10,date("d/m/Y"),0,0);
        $this->SetFont('Arial','B',11);
	$this->Ln(15);
        $this->SetDrawColor(205,205,205);
        $this->SetLineWidth(.4);
        $this->Line(10, 43, 280, 43);
	$this->SetX(10);
       	for($i=0;$i<count($this->col);$i++){
		$tam[$i] = $this->GetStringWidth($this->col[$i])+6;
		$this->Cell($tam[$i],5,$this->col[$i],0,0,'C',0);
	}
    $this->Ln(1);
    $this->Line(10, 52, 280, 52);
    $this->Ln(3);
}

//Pie de página
function Footer()
{
    //Posición: a 1,5 cm del final
    $this->SetY(-15);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('L');
$pdf->SetFont('Times','',12);
$columna = $pdf->col;
$pdf->SetFont('Arial','B',11);

$pdf->SetFillColor(224,235,255);
$pdf->SetTextColor(0);

$pdf->SetDrawColor(0,0,0);
$pdf->SetLineWidth(.3);
$pdf->SetX(10);
for($i=0;$i<count($columna);$i++){
	$tam[$i] = $pdf->GetStringWidth($columna[$i])+6;
}
//restore color
$pdf->SetFont('Times','',8);
$pdf->SetFillColor(224,235,255);
$pdf->SetTextColor(0);
?>
