<?php
error_reporting(E_ERROR);
require('../aplicacion/librerias/fpdf/fpdf.php');
require('../aplicacion/configuracion/aut_lib.inc.php');

class PDF extends FPDF{
   private $objReporte;
   private $rsReporte;
   private $sem_anno;
   private $utUsuario;
   private $usuarioCargo;
   private $objInventario;
   private $rsInventario;



   /*    * *
    *  Obtiene los parametros y se conecta
    */

   public function GetRequest() {
      /*$p = decryptLink($_REQUEST['p']);
      parse_str($p);*/
      $this->sem_anno=$_REQUEST['sem_anno'];
      $this->utUsuario=$_REQUEST['utUsuario'];
      $this->usuarioCargo=$_REQUEST['usuarioCargo'];

      $this->objReporte = new reporte();
      $this->objInventario = new inventarioSAP();
      $this->rsReporte = $this->objReporte->SelecPlan($_REQUEST['sem_anno'],$_REQUEST['utUsuario'],$_REQUEST['usuarioCargo']);
      
   }   

   
//Cabecera de página
 //var $ruta;
 var $col = array ("Codigo / Caracteristica    ","                Lugar                                    ","     Actividades                            ","Fecha Plan   ","Fecha Prog  ","Fecha Asig");
 


function Header()
{
    //Logo
    $this->Image('../aplicacion/librerias/fpdf/logopdvsa.jpg',10,8,40);
    $this->SetFont('Arial','B',12);
    $this->SetFillColor(224,235,255);
	$this->SetTextColor(0);
	$this->SetDrawColor(0,0,0);
	$this->SetLineWidth(.3);
        $this->Ln(5);
	$this->SetX(230);
        $this->Cell(50,10,' Planificacion por semana ',0,0,'R');
	$this->Ln(10);
        $this->SetFont('Arial','B',10);
        $this->SetTextColor(204,0,0);
        $this->Cell(20,10,'Semana: ',0,0,'L');
        $this->SetTextColor(0);
        $this->SetX(27);
        $this->Cell(20,10,$_REQUEST['sem_anno'],0,0);
        $this->Ln(5);
        $this->SetTextColor(204,0,0);
        $this->Cell(20,10,'Fecha: ',0,0,'L');
        $this->SetTextColor(0);
        $this->SetX(27);
        $this->Cell(20,10,date("d/m/Y"),0,0);
        $this->SetFont('Arial','B',11);
	$this->Ln(15);
        $this->SetDrawColor(205,205,205);
        $this->SetLineWidth(.4);
        $this->Line(10, 43, 280, 43);
	$this->SetX(10);
        $this->Ln(5);
       	for($i=0;$i<count($this->col);$i++){
		$tam[$i] = $this->GetStringWidth($this->col[$i])+5;
		$this->Cell($tam[$i],5,$this->col[$i],0,0,'L',0);
	}
    $this->Ln(1);
    $this->Line(10, 52, 280, 52);
    $this->Ln(3);
}

    //Pie de página
    function Footer()
    {
        //Posición: a 1,5 cm del final
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //Número de página
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
    }
    
   /*    * *
    * Imprime el contenido
    */

   function PrintContent() {

      $this->rsReporte->MoveFirst();

      $this->AddPage('L');
      $this->SetFont('Arial', '', 8);
      $this->Ln(4);
      
        $cont=0;
        $fill=0;
        $i=0;
        $j=0;        
        $pag = 1;
        $conteo = 0; 
       	for($j=0;$j<count($this->col);$j++){
		$tam[$j] = $this->GetStringWidth($this->col[$j])+19;
	}          
        while(!$this->rsReporte->EOF){
            list($ano, $mes, $dia) = explode("-", $this->rsReporte->fields['Proximo1']);
            $fecha_prox_mtto = $dia."-".$mes."-". $ano;
            if ($this->rsReporte->fields["PROG"]){
               list($anog, $mesg, $diag) = explode("-",$this->rsReporte->fields["PROG"]);
                $fecha_programacion = $diag."-".$mesg."-". $anog;
            }  else $fecha_programacion='SIN PROGRAMAR';
            if ($this->rsReporte->fields["ASIG"]){
                list($anoa, $mesa, $diaa) = explode("-",$this->rsReporte->fields["ASIG"]);
                $fecha_asignacion = $diaa."-".$mesa."-". $anoa;             
            }else $fecha_asignacion = 'SIN ASIGNAR';

            // conteo PERMITE VERIFICAR LAS VECES CUANTOS PLANES SE HAN IMPRIMIDO
            $conteo = $conteo + 1;
            $coplan = $this->rsReporte->fields['CoPlan'];
            $ubicacion = $this->rsReporte->fields['Ubicacion'];
            $this->rsInventario = $this->objInventario->SelecUbicacionTecnica($ubicacion);
            $this->rsInventario = $this->objInventario->SelecUbicacionTecnica($ubicacion);
            $sUbic = $this->rsInventario[1]['TXT_TPLMA'];
            $Desc = $this->rsInventario[1]['PLTXU'];
            $s2Codigo = $this->rsInventario[1]['TPLMA'];
            $this->rsInventario = $this->objInventario->SelecUbicacionTecnica($s2Codigo);
            $s2Ubic = $this->rsInventario[1]['TXT_TPLMA'];
            //$lugar = $sUbic. '.<br>'.$s2Ubic;           
            if ($this->rsReporte->fields['TipoPlan'] == "E") {
               $this->rsInventario = $this->objInventario->SelecDetalleEqpSAP($this->rsReporte->fields['Equipo']);
               $Desc = $this->rsInventario[1]['EQKTU'];
            }
         if($i==37){
            $i=0;
            $pag++;
            $this->SetX(75);
            $esp = 20;
            $this->Cell(65,6,'',0,0,'C');
            $this->Ln(5);
            $this->SetX(10);

        }
      
        $this->SetX(10);
        $this->Cell($tam[0],2,ltrim($this->rsReporte->fields['Equipo'],"0"),0,0,'L',$fill);
        $this->Cell($tam[1],2,$sUbic.'.',0,0,'L',$fill);
        $this->Cell($tam[2],3,$this->rsReporte->fields['Actividad'],0,0,'L',$fill);
        $this->Cell($tam[3],3,$fecha_prox_mtto,0,0,'C',$fill);
        $this->Cell($tam[4],3,$fecha_programacion,0,0,'C',$fill);
        $this->Cell($tam[5],3,$fecha_asignacion,0,1,'C',$fill);
        $this->SetX(10);
        $this->Ln();        
        $this->Cell($tam[0],0,$Desc,0,0,'L',$fill);
        $this->Cell($tam[1],0,$s2Ubic.'.',0,0,'L',$fill);
        $this->Ln(6);        
        $i++;
        $cont++;
        $this->rsReporte->MoveNext();
    }
      
   }
    
}

//* Creación del objeto de la clase heredada PDF
$pdf = new PDF();
$pdf->SetTitle("Planificacion por Semana");
$pdf->AliasNbPages();
$pdf->SetMargins(11, 5, 11);
$pdf->GetRequest();
//$pdf->GetHeader();
$pdf->PrintContent();
$pdf->Output("Plan_Semanal_001.pdf", "I");
?>