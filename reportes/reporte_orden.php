<script type="text/javascript" src="aplicacion/cliente/html_tablas_ejecucion.js"></script>
<!-- script PARA REALIZAR TEXTO FLOTANTE -->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript" src="publico/js/jquery-ui.js"></script>	
<script type="text/javascript" src="publico/js/jquery.ui-1.10.3.datepicker-es.js"></script>
<link href="publico/estilos/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript"> 
        Shadowbox.init({
        language: "es",
        modal:true,
        players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv', 'php'],
        overlayColor: "#000",
        overlayOpacity: "0.5"}); 
</script>

<!-- style PARA MOSTRAR UN EXTO FLOTANTE SOBRE COMPONENTES --> 
<style type="text/css">
    /* Estilo que muestra la capa flotante */
    #flotante
    {
        position: absolute;
        display:none;
        font-family:Arial;
        font-size:0.7em;
        width:280px;
        border:0.1px solid #808080;
        background-color:#FFFFD0;
        padding:5px;
    }
</style>
<script type="text/javascript" src="aplicacion/cliente/texto_flotante_td.js"></script>
<div class="titulomodulo">Historico de Ordenes</div>	 
    <input type="hidden" id="page" name="page" value="" />
    <table id="texto_informativo">
        <tr>
            <td>
                <span class="etiqueta"> Fecha de la orden:</span> 
            </td>
            <td align="left" nowrap>
                <span><input type="text" id="datepicker" class=":required textofecha_avanzado" name="fechai" readonly value="" size="7"/></span>
            </td>
            <td align="left" nowrap>    
                <span><input type="text" id="datepicker2" class=":required textofecha_avanzado" name="fechaf" readonly value="" size="7"/></span>
            </td>
        </tr>
        <tr>
            <td>
                <span class="etiqueta"> Estado de la orden:</span> 
            </td>
            <td colSpan="2">
                <select name="estatus" class=":required texto" id="estatus" >
                    <option value = "-1">Seleccione una opci&oacute;n...</option>
                    <option value =  "0">Creada sin asignaci&oacute;n</option>
                    <option value =  "1">En ejecuci&oacute;n</option>   
                    <option value =  "2">Asignaci&oacute;n de personal</option> 
                    <option value =  "3">Cerrada</option> 
                </select>
            </td>
        </tr>
        <tr>
            <td><input class="boton"  id="cmdbuscar"  name="cmdbuscar"  value="Buscar"   type="button" onclick="return Cargar();"> 
            </td>
            <!-- IMAGEN QUE SOLO PERMITE  width=25% height=25% REFERENCIAR UNA CARGA PARA IMPRIMIR LA TABLA ANTERIOR -->
        </tr>
    </table> 
<div class="etiqueta" id="Cargando" ></div>
<div id="shadow_hidden"></div>
<div id="flotante"></div>
<div id="loading"></div>
<div id="resultado_cerrarorden"  class="resultado"></div>
<!-- script PARA CAMBIAR EL BOT�N O EL LINK DEPENDIENDO DE LA SELECCI�N DEL SELECT-->
<script type="text/javascript">
     //**--------------------------------------------------**-----------------------------
    var jq=jQuery.noConflict();
    jq(document).ready(function(){      
        initBindLocal();
    });
    
    function initBindLocal(){
        jq("tbody#detalle").hide();
        jq("tbody#orden .ClassMas").click(function(event){
            var desplegable = jq(this).parents('#orden').get(0).next('#detalle')/*jq(this).next()*/;
            desplegable.toggle();
            event.preventDefault();
        });
        jq("#datepicker").datepicker({ 
            maxDate:"+1d",
            showWeek:true,
           weekHeader:"Sem.",
            showOn: "both",
            buttonImage: "publico/imagenes/calendar-icon.png",
            buttonImageOnly: true
           });
        jq("#datepicker2").datepicker({ 
            maxDate:"+1d",
            showWeek:true,
           weekHeader:"Sem.",
            showOn: "both",
            buttonImage: "publico/imagenes/calendar-icon.png",
            buttonImageOnly: true
           });
        
    }
    
     function Cargar() {
        jq("#resultado_cerrarorden").html("");
        SelecOrdenesR("resultado_cerrarorden","loading",jq("#datepicker").val(),jq("#datepicker2").val(),jq("#estatus").val());
    }
    
    function OpenShadow(sPath) {
	Shadowbox.open({
	        content:    sPath,
	        player:     "iframe",
	        height:     330,
		width:      805
	});
    }
</script>