
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<!-- <script type="text/javascript" src="aplicacion/cliente/html_tablas_ejecucion.js"></script> -->
<!--script type="text/javascript" src="aplicacion/cliente/html_tablas_gestionpersonal.js"></script-->

<!-- GRAFICOS DE BARRAS POR AE-->

  <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="../excanvas.js"></script><![endif]-->
  
  <link rel="stylesheet" type="text/css" href="publico/js/grafico/jquery.jqplot.css" />
  <link rel="stylesheet" type="text/css" href="publico/js/grafico/examples/examples.css" />
  
  <!-- BEGIN: load jquery -->
  <!--script language="javascript" type="text/javascript" src="publico/js/grafico/jquery.js"></script-->
  <!-- END: load jquery -->  
  
  <!-- BEGIN: load jqplot -->
  <script language="javascript" type="text/javascript" src="publico/js/grafico/jquery.jqplot.js"></script>
  <script language="javascript" type="text/javascript" src="publico/js/grafico/plugins/jqplot.pieRenderer.js"></script>
  <!-- END: load jqplot -->
  

<!-- script PARA REALIZAR TEXTO FLOTANTE -->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript" src="publico/js/jquery-ui.js"></script>  
<script type="text/javascript" src="publico/js/jquery.ui-1.10.3.datepicker-es.js"></script>
<link href="publico/estilos/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript"> 
        Shadowbox.init({
        content: 'reportes/grafico_grafico_ppae.php',    
        language: "es",
        modal:true,
        players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv', 'php'],
        overlayColor: "#000",
        overlayOpacity: "0.5"
        }); 
</script>



<div class="titulomodulo">Cantidad de mantenimientos (PPAE)</div>	 
    
    <input type="hidden" id="page" name="page" value="" />
    <?php

   

    if(isset($_SESSION['usuario_id']))
    {
        echo '<input id="U1" type="hidden" value='.base64_encode($_SESSION["usuario_id"]).'>';
    }
    else
    {
        echo '<input id="U1" type="hidden" value="-8a">';      
    }

        
    ?>
    
    <div id="test_w"></div>
    
    <table id="texto_informativo"  >
        

        <tr>
            <td>
                <span class="etiqueta"> Fecha de la orden: (<span class="required">*</span>)</span> 
            </td>
            
            <td align="left" nowrap>
                <!-- Fecha de Inicio -->
                <span><input type="text" id="datepicker" class=":required textofecha_avanzado" name="fechai" readonly value="" size="7"/></span>
            </td>
            
            <td align="left" nowrap id="img_spam">
                <!-- Fecha de Fin -->    
                <span id="img_spam2"><input type="text" id="datepicker2" class=":required textofecha_avanzado" name="fechaf" readonly value="" size="7"/></span>
                <div id="img_con">
                    <!-- Imprimir PDF -->
                    <img  id="impr" src="publico/imagenes/print.png">
                    <!-- Grafico de solucion-->
                    <a id="chart_a" href="reportes/grafico_grafico_ppae.php" rel="shadowbox;width=1000px;height=600px">
                     <img  id="chart" src="publico/imagenes/chart.png">
                    </a>
                </div>
                
            </td>
            
        </tr>
        
        <tr>

            <td>
                <label class="etiqueta" for="Grupo_solucionador">Grupo Solucionador: (<span class="required">*</span>)</label>
                <!-- Grupo Solucionador -->
                <select id="Grupo_solucionador" name="Grupo_solucionador" >
                    <option value="0">Seleccione un valor...</option>
                </select>
            </td>

            <td>
                <label class="etiqueta" for="Grupo_planificador" multiple="multiple">Grupo Planificador: (<span class="required">*</span>)</label>
                <!-- Grupo Planificador  -->
                <select id="Grupo_planificador" name="Grupo_planificador"  ></select>
            </td>
            
            <td>
                <label class="etiqueta" for="Puesto_trabajador" multiple="multiple">Puesto Trabajador: (<span class="required">*</span>)</label>
                <!-- Puesto de trabajo -->
                <select id="Puesto_trabajador" name="Puesto_trabajador"  >
                </select>
            </td>
            
        </tr>
        <tr>
            <!-- mantenedor oculto solo visible por tipo de grupo planificador -->
            <td id="test_s">
                <label class="etiqueta" for="Mantenedor" multiple="multiple">Mantenedor: (<span class="required">*</span>)</label>
                <select id="Mantenedor" name="Mantenedor"  id="Mantenedor" >
                </select>
            </td>
            <td>
                <!-- ubicacion tecnica  -->
                <label class="etiqueta" for="Ubicacion_tec">Ubicacion Tecnica:</label>
                <div id="unique_td">
                 <input type="text" name="Ubicacion_tec" id="Ubicacion_tec" placeholder="en caso de aplicar">    
                 <input type="checkbox" name="restric_ubi" id="rest_ubi" checked>
                 <label class="etiqueta" for="restric_ubi" style=" margin: 0px; font-size: 6pt;" >Restrictivo</label>
                </div>
            </td>
            

        </tr>
        <tr>
            <td>
                <input class="boton"  id="cmdbuscar"  name="cmdbuscar"  value="Buscar"   type="button"> 
            </td>
        </tr>
        
    </table> 
<!-- Ventana de espera -->
<div class="etiqueta" id="Cargando" ></div>
<div id="shadow_hidden"></div>
<div id="loading">
    <img src="publico/imagenes/ajax-loader.gif">
    <label >por favor espere</label>
</div>

<!-- espacio para el reporte -->
<div id="resultado_cerrarorden"  class="resultado">
    <div id="report_title"></div> 
    <div id="gridbox" style="width:735px;height:400px;background-color:white;"></div>
</div>


    


<link type="text/css" rel="stylesheet" href="publico/js/Archivos/tools.css">
<link type="text/css" rel="stylesheet" href="publico/js/Archivos/estilos.css">                    
<link href="publico/estilos/jquery.multiselect.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="aplicacion/librerias/dhtmlx/dhtmlx.css"/>
<style type="text/css">
    
    #resultado_cerrarorden
    {
        width: 735px;
        height: 700px;
        margin-top: 5px;
    }


    #texto_informativo
    {
        margin-top: 15px;
    }

    #texto_informativo *
    {
        margin: 5px;
    }

    #loading 
    {
        position: absolute;
        top: 200px;
        left: 400px;
        z-index: 1;
        background-color: rgba(174, 172, 172, 0.67);
        width: auto;
        height: auto;
        padding: 15px;
        border-radius: 10px;          
    }

    #loandig label
    {
        font-size: 15px;
    }

    #loading * ,#impr,#img_spam2,#unique_td *
    {
        float: left;

    }

    #impr
    {
        margin-left: 35px;
        margin-top: 10px;
        cursor: pointer;
    }

    #chart
    {
        margin-top: -4px;
        cursor: pointer;
    }

    #cmdbuscar
    {
        margin-top: 20px;
    }

    #cmdbuscar:hover
    {
        background-color: rgba(24, 21, 22, 0.63);
        color: white;
    }

    #img_con img
    {
        width: 25px;
        
    }

    td
    {
        width: 250px;
    }
    #img_spam2
    {
        margin-top: 17px;
    }


    .required
    {
        color: red;
        font-size: 13px;
    }

    #report_title p
    {
        font: bold 15px Arial,Helvetica,sans-serif;
        color: #C00;;
        margin-left: 29px;
    }

    #report_title 
    {
        padding-bottom: 10px;
    }
</style>



<script src="aplicacion/cliente/html_graficoPPAE.js"></script>
<script src="aplicacion/librerias/dhtmlx/dhtmlxgrid.js"></script>
<script type="text/javascript" src="publico/js/jquery.multiselect.js"></script>
<script type="text/javascript">



var jq=jQuery.noConflict();
    jq(document).ready(function(){      
        initBindLocal();

        data_selectList('#loading',"#texto_informativo",jq('#U1').val(),'#Grupo_solucionador');
    });
    
</script>


















