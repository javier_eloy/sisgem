<meta charset="UTF-8">

<?php
//header("Cache-Control: public, must-revalidate");
//header("Content-Transfer-Encoding:gzip;q=1.0,identity;q=0.5,*;q=0");
//header('Content-Type: text/html; charset=UTF-8');

error_reporting(E_ERROR);
include("aplicacion/configuracion/aut_lib.inc.php");

$navegador = $_SERVER['HTTP_USER_AGENT'];
if (strpos($navegador, "Chrome") > 0 && strpos($navegador, "Linux") > 0) {
   header("Location:nosoportado.php");
}

if ($_GET['k'])
   DecodificarGet($_GET['k']);
$url = (isset($_GET['sistema'])) ? $_GET['sistema'] : 0;

//
// DEFINICI�N DE CARGO DEL USUARIO CONECTADO
$fecha_actual = new DateTime("NOW");
$codigo = $_SESSION['usuario_id'];
$RecordUsuario["usuario_id"] = $_SESSION['usuario_id'];
$RecordUsuario["usuario_nombre"] = $_SESSION['usuario_nombre'];
$RecordUsuario["usuario_co_cargo_completo"] = $_SESSION['usuario_co_cargo_completo'];
$RecordUsuario["usuario_co_cargo_completo_plan"] = $_SESSION['usuario_co_cargo_completo_plan'];
$RecordUsuario["usuario_ubic_tecnica"] = $_SESSION['usuario_ubic_tecnica'];
$RecordUsuario["usuario_grp_plan"] = $_SESSION['usuario_grp_plan'];
$RecordUsuario["usuario_pto_plan"] = $_SESSION['usuario_pto_plan'];
$RecordUsuario["usuario_cod_cargos"] = $_SESSION['usuario_cod_cargos'];
$RecordUsuario["usuario_disponible"] = $_SESSION['usuario_disponible'];
$RecordUsuario["usuario_planificador"] = $_SESSION['usuario_planificador'];
$RecordUsuario["usuario_administrador"] = $_SESSION['usuario_administrador'];
$RecordUsuario["usuario_ubic_administrador"] = $_SESSION['usuario_ubic_administrador'];


// CORRE EL ARCHIVO QUE CONTIENE LAS FUNCIONES DE CONEXI�N A LAS LOS BD (inventario y actividades)

/* * ************ Estabbelcer como tarea ejecutable y no cada vez que se carge la pagina ************** 
 * 
 */
//        include "inicio_programacion.php";
?>
<!DOCTYPE HTML>
<html>
   <head>
      <title>PDVSA</title>
      <script type="text/javascript" src="publico/js/base64.js"></script>
      <script type="text/javascript" src="publico/js/jquery-1.7.2.js"></script>
      <script type="text/javascript" src="publico/js/prototype.js"></script>
      <script type="text/javascript" src="aplicacion/cliente/sesion.js"></script>
      <script type="text/javascript" src="publico/js/menu.js"></script>
      <script type="text/javascript" src="publico/js/backfix.min.js"></script>
      <script type="text/javascript" src="publico/js/Archivos/funciones.js"></script>
      <link type="text/css" rel="stylesheet" href="publico/js/Archivos/estilos.css">
      <link type="text/css" rel="stylesheet" href="publico/js/Archivos/styleestancia.css" >
      <script type="text/javascript">
         var jq=jQuery.noConflict();
         jq(document).ready(function()
         {
            jq('#box-link').click(function()
            {
                
               if(jq(this).parent().hasClass('dock'))
                  jq(this).parent().attr('class','undock');
               else
                  jq(this).parent().attr('class','dock');
            }          
         );
            history.replaceState({},'Sisgem','/<?php if (URL_NAME != '/') echo URL_NAME; ?>index.php');

            bajb_backdetect.OnBack = function()
            {
               alert('No est\u0301 permitido el uso del boton atras!');
            }
         }
      );
     

      </script>
   </head>
   <!--Cuerpo-->
   <body onload="mostrarMenu();init();" >
      <div id="Main-externo" >
         <div style="padding:5px 0px 10px 0px;">
            <img src="publico/imagenes/logo.jpg" alt="Volver a Inicio" >
         </div>
         <!--<div align="center" style="overflow: auto; width: 756px; height: 486px; background-image:url(/images/back_tran.png); background-color:#F9F9F9;">-->
         <div class="encabezado">
            <div class="titulo-usuario">PDVSA - INTRANET</div>
            <div class="titulo-fecha"><?php echo str_replace(' De ', ' de ', ucwords(strftime("%A, %d de %B de %Y"))); ?></div>
         </div>
         <div class="contenido">
            <div class="izquierdaplantilla">
               <div class="bienvenido">Bienvenido(a)</div>
               <div class="nombredeusuario"><h1><?php echo $_SESSION['usuario_nombre'] ?></h1></div>
               <div id="menuDinamico"><?php include 'menu.php'; ?></div>
            </div>
            <div class="centroplantilla" >
               <div id="dockeable"  class="dock">
                  <div id="box-link" title="Desacoplar/Acoplar"></div>
                  <?php
                  switch ($url) {
                     case 0:
                        include"panel.php";
                        break;
                     case 4:
                        include"cambio_clave/cambio_clave.php";
                        break;
                     case 5:
                        include"administrador/administrador.php";
                        break;
                     // Submodulo de Registro de Cargos
                     case 6:
                        include"configuracion/registro_cargos.php";
                        break;
                     case 66:
                        include"configuracion/registro_cargos_grupos.php";
                        break;
                     // Submodulo de Registro de Personal
                     case 12:
                        include"configuracion/registro_personal.php";
                        break;
//                     case 121:
//                        include"configuracion/registro_personal_acceso.php";
//                        break;
                     case 123:
                        include "configuracion/registro_roles.php";
                        break;
                     // Submodulo Registro de Clases
                     case 125:
                        include"configuracion/registro_clases.php";
                        break;
                     // NOMENCLATURAS
                     case 127:
                        include"configuracion/registro_cod_orden.php";
                        break;
                     // MOTIVOS DE CIERRE
                     case 129:
                        include"configuracion/registro_cod_cierre_actividad.php";
                        break;

                     //------------ M�DULO DE GESTI�N DE PERSONAL ---------------
                     // Dependencia Gr�fico
                     case 23:
                        include"gestion_personal/grafico_personal.php";
                        break;
                     case 24:
                        include"gestion_personal/gestion_personal_disponible.php";
                        break;
                     case 25:
                        include"gestion_personal/gestion_personal_disponibilidad.php";
                        break;
                     case 26:
                        include"gestion_personal/gestion_personal_historial.php";
                        break;

                     //------------ M�DULO DE PLANIFICACI�N DEL MANTENIMIENTO ---------------
                     case 2000:
                        include"planificacion_mtto/planificacion.php";
                        break;
                     case 2100:
                        include"planificacion_mtto/plan.php";
                        break;
                     //------------ M�DULO CARGA MASIVA DE DATOS ---------------
                     case 3000:
                        include"carga_masiva/carga_masiva_plan.php";
                        break;
                     //------------ M�DULO DE PROGRAMACION DEL MANTENIMIENTO ---------------
                     case 800:
                        include"programacion/programacion_act.php";
                        break;
                     case 830:
                        include"programacion/actividades_asignadas.php";
                        break;

                     //----------- MODULO DE ASIGNACION DE ACTIVIDADES ------------------
                     case 30:
                        include"asignacion_activ/asignacion_activ.php";
                        break;

                     case 3001:
                        include"asignacion_activ/detalle_activ.php";
                        break;

                     //------------ M�DULO DE EJECICI�N DEL MANTENIMIENTO ---------------
                     case 16:
                        include "ejecucion/cartelera.php";
                        break;
                     case 161:
                        // Ejecuci�n de actividad a traves de los procedimientos
                        include "ejecucion/ejecutar_actividad.php";
                        break;

                     case 162:
                        include "ejecucion/cerrar_ordenes.php";
                        break;
                     case 9999:
                        include"formulario.php";
                        break;
                     case 19999:
                        include"programacion/orden_trabajo.php";
                        break;
                     //------------------- REPORTES ------------------
                     case 4000:
                        include"reportes/reporte_planificacion.php";
                        break;
                     case 4001:
                        include"reportes/reporte_programacion.php";
                        break;
//                                case 4002:
//                                    include"reportes/reporte_ejecucion.php";
//                                    break;
                     case 4003:
                        include"reportes/historico/lista_equipos.php";
                        break;
                     case 4004:
                        include"reportes/historico/lista_stma_instalacion.php";
                        break;
                     case 4005:
                        include"reportes/historico_personal/lista_personal.php";
                        break;
                    case 4006:
                        include"reportes/reporte_orden.php";
                        break;
                    case 4007:
                        include"reportes/grafico_ppae.php";
                        break;
                    case 4008:
                        include"plan_anual/plan_anual.php";
                        break;                    

                  }
                  ?>
               </div>
            </div>
         </div>
         <div class="pie"><span class="etiqueta_index"> Sistema Realizado para Resolucion  1024x768 </span></div>
         <!--Fin de Cuerpo-->
         <div class="pie2">PROGRAMACI&Oacute;N, PLANIFICACI&Oacute;N Y MANTENIMIENTO / PLATAFORMA CENTRALIZADA / AIT </div>
      </div>
   </body>
</html>
