<?php
$Date = new DateTime();

$dia_inicio = clone $Date;
if ($dia_inicio->format("N") > 1) {
   $dia = $dia_inicio->format("N");
   $dia = 7 - $dia;
   $dia_inicio->sub(new DateInterval('P' . $dia . 'D'));
}
$semana_actual = $Date->format("W-Y");
$Date->add(new DateInterval('P8D'));
$dia_fin = clone $Date;
if ($dia_fin->format("N") < 7) {
   $dia = $dia_fin->format("N");
   $dia = 7 - $dia;
   $dia_fin->add(new DateInterval('P' . $dia . 'D'));
   
}
$semana_proxima = $Date->format("W-Y");
?>
<!-- script JQUERY -->
<script type="text/javascript" src="aplicacion/cliente/html_tablas_programacion.js"></script>
<!-- script PARA REALIZAR TEXTO FLOTANTE -->
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript"> 
   Shadowbox.init({
      language: "es",
      modal:true,
      players:  ['html', 'php'],
      overlayColor: "#000",
      overlayOpacity: "0.5"});
</script>


<!-- style PARA MOSTRAR UN EXTO FLOTANTE SOBRE COMPONENTES --> 
<style type="text/css">
   /* Estilo que muestra la capa flotante */
   #flotante
   {
      position: absolute;
      display:none;
      font-family:Arial;
      font-size:0.7em;
      width:480px;
      border:0.1px solid #808080;
      background-color:#FFFFD0;
      padding:5px;
   }
   .text {}
</style>
<script type="text/javascript" src="aplicacion/cliente/texto_flotante_td.js"></script>
<div class="titulosubmodulo">Programaci&oacute;n del Mantenimiento Planificado</div>
<div>   
   <SELECT id="filtroprograma" name="filtroprograma" onchange="return CargarProgramacion(this.value);">
      <option value="0">Seleccione Vista...</option>
      <option value="1">Programaci&oacute;n</option>
      <option value="2">Pendientes por Programar</option>
      <option value="3">Actividades Programadas</option>
      <option value="4">Actividades en Espera</option>
   </SELECT>
   <div class="etiqueta" id="Cargando" ></div>
</div>
<div id="titulosemana" class="titulomodulo" style="display:none;">Semananas en curso a Programar: Actual (N&deg; <?php echo $semana_actual, " - ", $dia_inicio->format("d/m/Y"); ?> ) y la pr&oacute;xima semana (N&deg; <?php echo $semana_proxima, " - ", $dia_fin->format("d/m/Y"); ?>):</div>
<div id="shadow_hidden"></div>
<div id="flotante"></div>

<div id="resultado"  class="resultado"></div>
<!-- script PARA CAMBIAR EL BOT�N O EL LINK DEPENDIENDO DE LA SELECCI�N DEL SELECT-->
<script type="text/javascript">
   var jq=jQuery.noConflict();
   jq(document).ready(function(){
      jq("#titulosemana").hide();
   });
   
   window.CargarProgramacionModificada = function () {
      CargarProgramacion(jq('#filtroprograma').val());
      
   }
   
   function CargarProgramacion(value){
      jq("#resultado").html("");
      switch(value){
         case "1":
            SelecProgramacion("resultado","Cargando","titulosemana",'<?php echo $RecordUsuario["usuario_ubic_tecnica"] ?>','<?php echo $RecordUsuario["usuario_grp_plan"] ?>','<?php echo $RecordUsuario["usuario_pto_plan"] ?>');
            break;
         case "2":
            SelecProgramacionPendiente("resultado","Cargando",'<?php echo $RecordUsuario["usuario_ubic_tecnica"] ?>','<?php echo $RecordUsuario["usuario_grp_plan"] ?>','<?php echo $RecordUsuario["usuario_pto_plan"] ?>');
            jq("#titulosemana").hide();
            break;
         case "3":
            SelecActividadProgramada("resultado","Cargando",'<?php echo $RecordUsuario["usuario_ubic_tecnica"] ?>','<?php echo $RecordUsuario["usuario_grp_plan"] ?>');
            jq("#titulosemana").hide();
            break;
         case "4":
            SelecActividadAplazada("resultado","Cargando",'<?php echo $RecordUsuario["usuario_ubic_tecnica"] ?>','<?php echo $RecordUsuario["usuario_grp_plan"] ?>');
            jq("#titulosemana").hide();
            break;
         default:
            jq("#titulosemana").hide();
         }

         ajustarPaneles();
      }
      function ActualizarEstado(obj,idplan,idnomenclatura) {
         var lastvalue=obj.value;
         obj.value="Ejecutando...";
         if(ActualizarAplazada(idplan,idnomenclatura)) {
            CargarProgramacion(jq("#filtroprograma").val());
            return true;
         } else {
            obj.value=lastvalue;
            return false;
         }
      }
    
      function cargaEspera(idSelectOrigen)
      {
         // Obtener el ID del elemento
         var id = idSelectOrigen;
         var objprog=null;
         var objcont=null;
         // DESCONCATENO PARA OBTENER EL NUMERO DE PROCEDIMIENTO
         id = id.split('_');
         id = id[1];

         // Obtengo el select nivel, que fueron modificado anteriormente
         var IdSelectMotivo=document.getElementById("idmotivo_"+id);

         // Obtengo la opci�n que el usuario selecciono en el select nivel
         var ValorSelectMotivo = IdSelectMotivo.options[IdSelectMotivo.selectedIndex].value;
         objprog=document.getElementById('programar_'+id);
         objcont=document.getElementById('continuar_'+id);
        
         if(ValorSelectMotivo!=0)
         {
            // Oculto el link y muestro el boton con style="display: none;"
            if(objprog) objprog.setAttribute('style','display: none;');
            if(objcont) objcont.setAttribute('style','display: none;');
            document.getElementById('detener_'+id).setAttribute('style','');

         }
         if(ValorSelectMotivo==0)
         {
            // Oculto el bot�n y muestro el link con style="display: none;"
            document.getElementById('detener_'+id).setAttribute('style','display: none;');        
            if(objprog) objprog.setAttribute('style','');
            if(objcont) objcont.setAttribute('style','');
         }
         return true;
      }
    
      function OpenShadow(sPath) {
         Shadowbox.open({
            content:    sPath,
            player:     "iframe",
            height:     511,
            width:      801
         });
      }
</script>
