<?php
error_reporting(E_ERROR | E_PARSE);
include_once("../aplicacion/configuracion/aut_lib.inc.php");

$co_plan = $_GET['co_plan'];
$grp_plan = $_GET['grp_plan'];
$pto_plan = $_GET['pto_plan'];
$ubic_tec = $_GET['ubic_tec'];
$co_plan_pto = $_GET['co_plan_pto'];
$co_grp_plan = explode('-', $co_plan_pto);

$sMsg = "";
$script = "";
$sfechaMinima = "";

/* * * Obtiene Todos los Puesto de Trabajo ** */
$objInventario = new inventarioSAP();
$arrayPto = $objInventario->SelecPtoTrabajo($co_grp_plan[1], '1A00');
$countArrayPto = count($arrayPto);
$PtoAll = "";
$pto_all_desc = array();
$pto_all_val = array();
for ($j = 1; $j <= $countArrayPto; $j++) {
   $val = $arrayPto[$j]["ARBPL"];
   $ktext = $arrayPto[$j]["KTEXT"];
   $PtoAll.=($PtoAll == "") ? "'$val'" : ",'$val'";
   $pto_all_val[] = $val;
   $pto_all_desc[] = $val . " - " . $ktext;
}

/** fix para mostrar todos los puesto de trabajo y no los asignados* */
$pto_plan = $PtoAll;


if (isset($_GET['accion'])) {
   $objProgramacion = new programacion();
   switch ($_GET['accion']) {
      case 'Guardar' :
         $conteo = 1;
         $lstDocSGE = "";
         $lstPlan = "";

         if (isset($_POST['planes'])) {
            $planes = explode(",", $_POST['planes']);
            $countplanes = count($planes);
            // --- Separa los planes de los documentos
            for ($i = 0; $i < $countplanes; $i++) {
               $val = explode("-", $planes[$i]);

               $lstPlan.= (($i > 0) ? "," : "") . $val[0];
               $lstDocSGE.= (($i > 0) ? "," : "") . $val[1];
            }

            $fe_asignacion = date("d/m/Y");
            // Crea la Orden
            if ($objProgramacion->InsOrdenTrabajoDocumento($fe_asignacion, $conteo, $lstPlan, $lstDocSGE)) {
               $script = "window.parent.CargarProgramacionModificada(); window.parent.Shadowbox.close();";
            } else
               $sMsg = "Error Interno de Base de Datos: No se puede crear la orden";
         }
         break;
   }
}
?>
<script type="text/javascript">
<?php echo $script ?>
</script>
<!-- Script de la Aplicacion -->
<script type="text/javascript" src="../aplicacion/cliente/html_tablas_programacion.js"></script>

<!--Cuerpo-->
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/tools.css">
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">                    
<script type="text/javascript" src="../publico/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="../publico/js/jquery-ui.js"></script>
<script type="text/javascript" src="../publico/js/jquery.multiselect.js"></script>

<script type="text/javascript" src="../aplicacion/cliente/texto_flotante_td.js"></script>
<script type="text/javascript" src="../publico/js/jquery.ui-1.10.3.datepicker-es.js"></script>
<link href="../publico/estilos/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css">
<link href="../publico/estilos/jquery.multiselect.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../publico/js/prototype.js"></script>


<!-- style PARA MOSTRAR UN TEXTO FLOTANTE SOBRE COMPONENTES --> 
<style type="text/css">
   /* Estilo que muestra la capa flotante */
   #flotante
   {
      position: absolute;
      display:none;
      font-family:Arial;
      font-size:0.7em;
      width:280px;
      border:0.1px solid #808080;
      background-color:#FFFFD0;
      padding:5px;
   }
   .text {}
</style>
<body  bgcolor="#ffffff" onload="jQuery('#datepicker').datepicker('refresh');">

   <table align="center" style="background-color:#fffff; border:0;	border-collapse: collapse; padding:0; border-spacing:10px; width:760; height:401;" >
      <tbody>
         <tr>
            <td height="401" valign="top" width="756">
               <form method="POST" name="formulario" id="formulario" action="orden_trabajo.php">
                  <input type="hidden" name="co_plan" id="co_plan" value="<?php echo $co_plan ?>">
                  <input type="hidden" name="grp_plan" id="grp_plan" value="<?php echo $grp_plan ?>">
                  <input type="hidden" name="planes" id="planes" value="">

                  <div class="titulomodulo">Las siguientes actividades est&aacute;n pendientes por ejecutar o se tienen planificadas para las pr&oacute;ximas semanas en el mismo lugar. Seleccionelas si desea incorporarla a esta misma orden de trabajo</div>
                  <div class="tabla">
                     Actividades a Programar hasta el <input type="text" id="datepicker" class=":required textofecha" name="fecha" readonly size="7"/>
                  </div>

                  <div class="tabla">
                     <span style="display:inline-block !important;width:130px">Ubicaci&oacute;n T&eacute;cnica: </span>
                     <select name="ubic_tec[]" id="ubic_tec" class="textofecha" style="width:280px" multiple="multiple" >
                        <?php
                        ;
                        /** Muestra las ubicaciones tecnicas * */
                        $nivel = 2;
                        if (substr_count($ubic_tec, "-") >= $nivel) {
                           $t_ubic_tec = $ubic_tec;
                           while (substr_count($t_ubic_tec, "-") >= $nivel) {
                              $t_ubic_tec = substr($t_ubic_tec, 0, strrpos($t_ubic_tec, "-"));
                              echo "<option value=\"$t_ubic_tec\">$t_ubic_tec</option>";
                           }
                        }

                        echo "<option value=\"$ubic_tec\" selected>$ubic_tec</option>";
                        ?>
                     </select>
                     <input type="checkbox" name="chkNivelUbic" id="chkNivelUbic" value="1"> Solo nivel de la ubicaci&oacuten
                  </div>

                  <div class="tabla">                     
                     <span style="display:inline-block !important;width:130px;">Puestos de Trabajo:</span>
                     <select name="pto_list[]" id="pto_list" multiple="multiple">
                        <?php
                        /** Muestra los puesto de trabajo * */
                        $str = "";
                        $countList = count($pto_all_val);
                        for ($j = 0; $j < $countList; $j++) {
                           $val = $pto_all_val[$j];
                           $text = $pto_all_desc[$j];
                           if ($val == $co_plan_pto)
                              $str.= "<option value=\"'$val'\" selected disabled> $text</option>";
                           else
                              $str.= "<option value=\"'$val'\"> $text </option>";
                        }
                        echo $str;
                        ?>
                     </select>
                  </div>

               </form>
               <input class="boton" type="button" name="btnUpdate"  id="btnUpdate"  value="Actualizar">

               <table id="listaSaldos" class="tabla">
                  <thead>
                     <tr>
                        <th scope="col">No.</th>
                        <th scope="col">C&oacute;digo / Caracter&iacute;sticas</th>
                        <th scope="col">Actividad<br /><font face="Arial" size="0.2em">(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></th>
                        <th scope="col">Min. Personas Requeridas</th>
                        <th scope="col">Max. Personas Requeridas</th>
                        <th scope="col">Fecha de Plan</th>
                        <th scope="col">Opci&oacute;n</th>
                        <th scope="col">Doc. SGE</th>
                     </tr>
                  </thead>
                  <!-- En este div se muestra la capa emergente -->
                  <div id="flotante"></div>
                  <tbody id="detalleOrden">
                  </tbody>
               </table>
               <?php if (strlen($sMsg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$sMsg**</span>"; ?>
               <div id="validaDocSgeDisp"></div>
               <div class="grupobotones">
                  <input class="submit boton" name="Guardar" value="Guardar" type="button" onclick="Enviar(<?php echo $co_plan . ",'" . urlencode($grp_plan) . "','" . urlencode($pto_plan) . "','" . urlencode($ubic_tec) . "','" . urlencode($co_plan_pto) . "'" ?>);"/>
                  <input class="boton" name="Volver" value="Cerrar" type="button" onclick="window.parent.Shadowbox.close();"/>
               </div>
            </td>
         </tr>
      </tbody>
   </table>

</body>

<!--Fin de Cuerpo-->
<script type="text/javascript">
   var jq = jQuery.noConflict();

   jq(document).ready(function() {

      jq("#pto_list").multipleSelect({
         allSelected: '(Todos)',
         selectAllText: 'Seleccionar Todos',
         countSelected: '# de % seleccionados',
         multipleWidth: '100%',
         width: '70%',
         onCheckAll: function() {
            jq("#btnUpdate").removeAttr("disabled");
         },
         onUncheckAll: function() {
            jq("#btnUpdate").removeAttr("disabled");
         },
         onClick: function() {
            jq("#btnUpdate").removeAttr("disabled");
         }});

      jq("#ubic_tec").multipleSelect({
         noMatchesFound: '(No seleccionado)',
         single: true,
         onCheckAll: function() {
            jq("#btnUpdate").removeAttr("disabled");
         },
         onUncheckAll: function() {
            jq("#btnUpdate").removeAttr("disabled");
         },
         onClick: function() {
            jq("#btnUpdate").removeAttr("disabled");
         }});

      // Valores por Defecto de los Controles 
      /* jq("#pto_list").multipleSelect("checkAll");*/
      jq("#chkNivelUbic").prop('checked', true);
      jq("#chkNivelUbic").change(function() {
         jq("#btnUpdate").removeAttr("disabled");
      });
      // Campos de Fecha
      jq("#datepicker").datepicker({minDate: new Date()});
      jq("#datepicker").change(function() {
         jq("#btnUpdate").removeAttr("disabled");
      });
      // Evento del Boton Actualizar
      jq("#btnUpdate").click(function() {
         var datenow = jq("#datepicker").datepicker('getDate');
         var l_ubic_tec = jq("#ubic_tec").multipleSelect("getSelects");
         var l_pto_list = jq("#pto_list").multipleSelect("getSelects");
         var l_grp_list = jq("#grp_plan").val();

         if (l_ubic_tec == "" || l_pto_list == "") {
            alert("Deben existir los campos de Ubicacion Tecnica y Puestos de trabajo");
         } else
            SelecOrdActivosUbicacion('detalleOrden', jq("#co_plan").val(),
                    jq.datepicker.formatDate("d/mm/yy", datenow),
                    l_ubic_tec, l_grp_list, l_pto_list,
                    jq("#chkNivelUbic").is(":checked"));
      });

      //*** Carga de datos Inicial
      SelecOrdActivosUbicacion('detalleOrden', jq("#co_plan").val(), '',
              jq("#ubic_tec").multipleSelect("getSelects"),
              jq("#grp_plan").val(),
              jq("#pto_list").multipleSelect("getSelects"),
              jq("#chkNivelUbic").is(":checked"));
   });
//
//   function Actualizar(co_plan, grp_plan, ubic_tec) {
//      var datenow = jq("#datepicker").datepicker('getDate');
//      var datemin = new Date('<?php echo $sfechaMinima ?>');
//      if (datenow < datemin) {
//         alert("La fecha es menor al plan que se seleccionado, intente con una fecha posterior");
//         return false;
//      }
//      jq('#formulario').attr('action', "orden_trabajo.php?fecha_tope=" + jq.datepicker.formatDate("d/mm/yy", datenow) + "&co_plan=" + co_plan + "&grp_plan=" + grp_plan + "&ubic_tec=" + ubic_tec);
//      jq('#formulario').submit();
//      return true;
//   }


   function Enviar(co_plan, grp_plan, pto_plan, ubic_tec_prg, co_plan_pto) {

      var datenow = jq("#datepicker").datepicker('getDate');
      if (validaFormatArregDocSGE()) {

         var valuesSGE = jq("input[name='DocSGE[]']");
         var valuesPlanes = jq("input[name='ListaPlanes[]']");
         var lengthPlanes = valuesPlanes.length;
         var sResult = '';

         for (i = 0; i < lengthPlanes; i++) {
            var objPlanes = valuesPlanes[i];
            if (objPlanes.checked) {
               if (sResult != "")
                  sResult += ",";
               sResult += objPlanes.value + "-" + valuesSGE[i].value;
            }
         }
         jq("#planes").val(sResult);
         jq('#formulario').attr('action', "orden_trabajo.php?accion=Guardar&fecha_tope=" + jq.datepicker.formatDate("d/mm/yy", datenow) + "&co_plan=" + co_plan + "&pto_plan=" + pto_plan + "&grp_plan=" + grp_plan + "&ubic_tec=" + ubic_tec_prg + "&co_plan_pto=" + co_plan_pto);
         jq('#formulario').submit();
         return true;
      }
   }

   /** validaFormatArregDocSGE()
    * Valida el formato de cada cadena de caracteres corresondiente a DocSGE[], sin validar contra SAP.
    * Esta funci贸n se utiliza para validar al presioar el bot贸n guardar
    * La cadena debe ser s贸lo n煤meros positivos con una longitud de 10 caracteres.
    *
    */
   function validaFormatArregDocSGE() {

      var msgeError = "";
      var valuesSGE = jq("input[name='DocSGE[]']");
      var lengthSGE = valuesSGE.length;

      for (i = 0; i < lengthSGE; i++) {
         var objSGE = valuesSGE[i];
         if (!jq(objSGE).prop('disabled') && (objSGE.value.length < 10 || objSGE.value < 0))
            msgeError = msgeError + "　RROR!!! formato del numero de doc. SGE para la actividad " + (i + 1) + " Invalido '"
                    + objSGE.value + "' debe tener exactamente 10 caracteres numericos positivos. <br>";

      }

      if (msgeError != "") {

         muestraMsgeValidaDocSgeDisp(msgeError);
         return false;
      }

      return true;
   }

   /** validaFormatDocSGE
    * Valida el formato de la cadena introducida por teclado en la posici贸n pos del arreglo DocSGE.
    * Esta funci贸n se utiliza para la validaci贸n individual de cada caja de texto perteneciente a DocSGE.
    * La cadena debe ser s贸lo n煤meros positivos con una longitud de 10 caracteres.
    *
    */
   function validaFormatDocSGE(obj)
   {

      if (isNaN(obj.value) || obj.value.length != 10 || obj.value < 0) {
         var msge = "Error en formato, El numero de doc. SGE '" + obj.value +
                 "' debe contener exactamente 10 caracteres numericos positivos.<br>";
         muestraMsgeValidaDocSgeDisp(msge);
      }
      else
      {
         validaDocSGE(obj.value);
      }
   }


   function muestraMsgeValidaDocSgeDisp(msge) {

      var pars1 = 'fnc=msge_valida_formato_doc_sge&msge=' + msge;
      new Ajax.Updater("validaDocSgeDisp", "../aplicacion/servidor/_ajax/controlador.programacion.jquery.php",
              {
                 method: 'get',
                 parameters: pars1,
                 asynchronous: false,
                 evalScripts: false,
                 onComplete: function() {
                    jq("#validaDocSgeDisp").val('');
                    jq("#validaDocSgeDisp").show();
                 }

              });
   }

   /** validaDocSGE
    * Valida que el n煤mero intriducido en la variable coDocSge sea un n煤mero de documento v谩lido en SAP.
    *
    */
   function validaDocSGE(coDocSge) {

      var pars1 = 'fnc=valida_doc_sge&co_doc_sge=' + coDocSge;
      new Ajax.Updater("validaDocSgeDisp", "../aplicacion/servidor/_ajax/controlador.programacion.jquery.php",
              {
                 method: 'get',
                 parameters: pars1,
                 asynchronous: false,
                 evalScripts: false,
                 onComplete: function(request) {
                    jq("#validaDocSgeDisp").show();
                 }

              });
   }

   /**getEnter
    * Obtiene el evento de pulsar la tecla enter
    *
    */
   function getEnter(e) {
      var key = e.keyCode || e.which;
      if (key == 13) {
         return true;
      } else {
         return false;
      }
   }

   /**activaCampo
    * Activa el campo de texto en caso de existir en la posici贸n pos dada
    *
    */
   function activaCampo(posCheck) {
      var valDocSGE = jq("input[name='DocSGE[]']");

      jq("#validaDocSgeDisp").hide();
      valDocSGE[posCheck - 1].removeAttr("disabled");

   }
</script> 

