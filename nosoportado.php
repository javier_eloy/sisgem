<!DOCTYPE HTML>
<html >
   <head>
      <title>PDVSA </title>
      <link type="text/css" rel="stylesheet" href="publico/js/Archivos/estilos.css">
      <link rel="stylesheet" type="text/css" href="publico/js/Archivos/styleestancia.css" >
   <body>
      <div class="pagina_index">
         <div style="padding:5px 0px 10px 0px;">
            <img src="publico/imagenes/logo.jpg" alt="Volver a Inicio" >
         </div>
         <div class="encabezado">
            <div class="titulo-usuario">PDVSA - INTRANET</div>
            <div class="titulo-fecha"><?php echo str_replace(' De ', ' de ', ucwords(strftime("%A, %d de %B de %Y"))); ?></div>
         </div>
         <!--Cuerpo-->
         <div class="cuerpo">
            <div style="margin-left:30px;font-style:bold;font-size:18px;">
               <span >Navegador no soportado, comuniquese con soporte 105 para solicitar algun navegador oficial, tales como:</span>
               <div>
                  <ul>
                     <li>Firefox 12.0+</li>
                     <li>Internet Explorer 9+</li>
                     <li>Chrome</li>
                  </ul>
               </div>
            </div>
         </div>
         <!--Fin de Cuerpo-->
         <div class="pie"><span class="etiqueta_index"> Sistema Realizado para Resolucion  1024x768 - Recomendado para Mozilla Firefox</span> </div>
         <div class="pie2" >PROGRAMACI&Oacute;N, PLANIFICACI&Oacute;N Y MANTENIMIENTO / PLATAFORMA CENTRALIZADA / AIT </div>
      </div>
   </body>
</html>