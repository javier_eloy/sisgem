<?php
//  Autentificator
//  Gesti�n de Usuarios PHP+Mysql
//  ------------------------------
// Mensajes de error.

define("ERR_BD",0);
define("ERR_BDUSUARIOS",1);
define("ERR_USUARIOCLAVE",2);
define("ERR_USUARIONOLDAP",3);
define("OK_USUARIO",4);
define("ERR_NOAUTORIZADO",5);
define("ERR_USUARIONOREGISTRADO",6);
define("ERR_SESIONEXPIRADA",7);
define("ERR_LDAP",8);
define("ERR_NAVEGADOR",9);
define("ERR_DESCONOCIDO",10);
define("ERR_USUARIOVACIO",11);


$error_login_ms[ERR_BD]="No se pudo conectar con Base de datos de la Aplicacion";
$error_login_ms[ERR_BDUSUARIOS]="No se pudo realizar consulta a la Base de datos Usuarios";
$error_login_ms[ERR_USUARIOCLAVE]="Usuario/Clave Invalido"; // Usuario y Clave Invalido
$error_login_ms[ERR_USUARIONOLDAP]="Usuario/Clave Invalido"; // No es Usuario LDAP
$error_login_ms[OK_USUARIO]="Usuario Validado";
$error_login_ms[ERR_NOAUTORIZADO]="No esta autorizado para realizar esta accion o entrar en esta pagina";
$error_login_ms[ERR_USUARIONOREGISTRADO]="Usuario/Clave Invalido"; // Cuenta no registrada en SISGEM
$error_login_ms[ERR_SESIONEXPIRADA]="Sesion Expirada. Por Favor Inicie Sesion";
$error_login_ms[ERR_LDAP]="Problemas de Conexion con el Directorio Activo";
$error_login_ms[ERR_NAVEGADOR]="Navegador no soportado";
$error_login_ms[ERR_DESCONOCIDO]="Error Desconocido";
$error_login_ms[ERR_USUARIOVACIO]="Debe ingresasr Usuario/Clave";


define("BD_DESCONECTADO","Aplicación no conectada a Base de Datos");
define("MSG_DATOS_EDITADOS","Datos Editados...");
define("MSG_DATOS_ELIMINADOS","Datos Eliminados...");
define("MSG_DATOS_REGISTRADOS","Datos Registrados...");
define("MSG_ERROR_TRANSACCION","Error completando la operacion, revise los datos.");
define("MSG_NO_INSERTA_CARGOS","No se puede insertar cargos con el mismo codigo");
define("MSG_ACCESO_ADMINISTRADOR","Este modulo solo puede ser accesado por administradores.");

define("ERROR_SGE_SININTERVALO_PLAN","No existen intervalos asociados a los procedimientos en el SGE");
define("ERROR_SGE_SINHOJARUTA_PLAN","No existen hojas de rutas asociados en el SGE");
?>
