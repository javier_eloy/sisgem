<?php

//include_once('aut_config.inc.php');
include_once('aut_lib_min.inc.php');

session_name(USUARIOS_SESION);
session_start();

if (empty($_SESSION['usuario_login'])) {
   echo "1";
   exit();
}

$sesiontrabajo = new WorkSession($_SESSION['usuario_login']);

/* Sesion Cerrada forzada o no existe */
if (is_null($sesiontrabajo->connectSession())) {
   echo "2";
} else
if (!$sesiontrabajo->compareSessionId(session_id())) {
   echo "2";
} else
/* Revisa si expiro */
if ($sesiontrabajo->isExpirated()) {
   $sesiontrabajo->removeSession();
   session_destroy();
   session_unset();
   echo "1";
} else
/* Todo en orden */
   echo "0";
?>
