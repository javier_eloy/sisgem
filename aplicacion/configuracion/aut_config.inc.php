<?php
/*
 Variables de Configuracion para todo el sistema  
*/
//-------------- Nombre de App
if (!defined('APP_NAME')) define('APP_NAME','sisgem');
if (!defined('URL_NAME')) define('URL_NAME','sisgem/');

//-------------- SISGEM BD
//-------- CALIDAD ------------
//define('SQL_HOST','10.117.50.105');
//define('SQL_USUARIO','sisgemapl');
//define('SQL_PASS','sisgemapl');
//define('SQL_DB','sisgem_qa');
//define('SQL_PORT','3308');
//define('SQL_TABLA','I004T_USUARIO');
//define('USUARIOS_SESION', 'autentificator');

//------------ PRUEBA -------------------
define('SQL_HOST','10.117.50.105');
define('SQL_USUARIO','sisgemapl');
define('SQL_PASS','sisgemapl');
define('SQL_DB','sisgem');
define('SQL_PORT','3308');
define('SQL_TABLA','I004T_USUARIO');
define('USUARIOS_SESION', 'autentificator');



//-------------- Variables para ADODB
define('ADODB_CACHE_PATH',$_SERVER['DOCUMENT_ROOT'].'/cache');
define('ADODB_TIEMPO_CACHE',30);
//-------------- Variables Generales
define('SESION_EXPIRACION',900); // Expiracion en segundos
define('CACHE_EXPIRACION',300); // Espiracion en Segundos para el cache

//-------------- Variables de SAP
//------------- PRODUCCION PR3 -----------------------
//define("SAP_HOST","167.134.155.121");   // application server
//define("SAP_SYS","00");                 // system number
//define("SAP_CLIENT","510");             // client
//define("SAP_USER", "VALEROJP");         // user
//define("SAP_PWD","ENE2015");           // password
//define("SAP_CODEPAGE","4110");
//define("SAP_UNICODE","1");                //SAP R/3 4.6 no soporta unicode.

//------------- DESARROLLO PR3 (DV3) -----------------------
//define("SAP_HOST","167.134.155.171");   // application server
//define("SAP_SYS","00");                 // system number
//define("SAP_CLIENT","110");             // client
//define("SAP_USER", "PORTALWEB");         // user
//define("SAP_PWD","lara_12");           // password
//define("SAP_CODEPAGE","4110");
//define("SAP_UNICODE","1");                //SAP R/3 4.6 no soporta unicode.

//------------ CALIDAD ----

//define("SAP_HOST","167.134.155.139");   // application server
//define("SAP_SYS","00");                 // system number
//define("SAP_CLIENT","516");             // client
//define("SAP_USER", "VALEROJP");         // user
//define("SAP_PWD","MARIJO05");           // password
//define("SAP_CODEPAGE","1160");
//define("SAP_UNICODE","0");                //SAP R/3 4.6 no soporta unicode.


//------------- PRUEBA (DV1)-----------------------
define("SAP_HOST","167.134.155.125");   // application server
define("SAP_SYS","00");                 // system number
define("SAP_CLIENT","100");             // client
define("SAP_USER", "SGEPMAIT");         // user
define("SAP_PWD","sucre_12");           // password
define("SAP_CODEPAGE","1160");
define("SAP_UNICODE","0");                //SAP R/3 4.6 no soporta unicode.



//-------------- VARIABLES DE SEGLOG, SE COLOCAN ACA MIENTRAS NO SEA UN SERVICIO
define("LENEL_SERVER","ccscam90.pdvsa.com");
define("LENEL_USER","Co_Lenel_Occ");
define("LENEL_PASSWD","Co_Lenel_Occ");
define("LENEL_CATALOG","Lenel_Autentica");

#define("LDAP_SERVER",'172.28.209.250');
define("LDAP_SERVER",'162.122.42.211');
define("LDAP_DOMAIN",'pdvsa.com');
define("BASE_DN",'dc=pdvsa,dc=com');

define("AUDIT_SERVER",'10.121.52.157:5432');
define("AUDIT_USER","auditoria");
define("AUDIT_PASSWD","auditoria");
define("AUDIT_CATALOG","pg_occd02");

//-------------- FIN VARIABLES DE SEGLOG

define("KEY_START","8xw2g!q");
define("LEN_KEY_START",7);

//------------- SERVIDOR DE FOTOS PDVSA 
define("SERVIDOR_FOTOS","http://ccschu14.pdvsa.com/PHOTOS/");
define("SERVIDOR_FOTOS_NODISP","http://intranet.pdvsa.com/++resource++foto-faltante.png");

//------------- CODIGO DE LA APLICACION PARA LA BITACORA
define("CO_APLIC","1");

//------------- BANDERA PARA ACTIVAR/INACTIVAR AUDITORIA
define("AUDIT_ACTIVA",false);


//--- Otras Definiciones
date_default_timezone_set('America/Caracas');
setlocale(LC_TIME, 'esp','es','spanish','es_VE.UTF-8');


//---- Constantes de Sistema
define("ASIGNACION_PROGRAMACION","1");
define("ASIGNACION_ESPECIFICA","2");

define("TIPO_NIVEL_PLAN","MPP,OPT");
define("NIVEL_PLAN_CICLO","MPP");

define("STATUS_ACTIVIDAD_CERRADA","3");

//---- Variables de Seguridad
define("KEYSALT","pdvsa2000");
        
//---- Ruta carga de datos   
define("FUENTES_IMPORT","/var/www/tmp");     
//define("FUENTES_IMPORT","files");


header("Cache-Control: no-cache, no-store"); // HTTP 1.1.
header("Cache-Control: post-check=0, pre-check=0", FALSE );
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: -1"); // Proxies.
?>
