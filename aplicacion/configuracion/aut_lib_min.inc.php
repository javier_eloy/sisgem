<?php

//Asigna dinamicamente el path del INCLUDE
define('APP_NAME','sisgem');
define('URL_NAME','sisgem/');

$ruta=getcwd();
$pos_app=strpos($ruta,APP_NAME);
$pos_slash=strpos($ruta,DIRECTORY_SEPARATOR,$pos_app);

if($pos_slash) $ruta=substr($ruta,0,$pos_slash);
set_include_path(get_include_path().PATH_SEPARATOR.$ruta);

//---- LIBRERIAS GENERALES
include("aplicacion/librerias/adodb5/adodb.inc.php");
include("aplicacion/librerias/adodb5/adodb-exceptions.inc.php");
//include("aplicacion/servidor/SEGLOG/SegLog.inc.php");

//--------- OTROS INCLUDE DE CONFIGURACION
include("aplicacion/configuracion/aut_mensaje_error.inc.php");
include("aplicacion/configuracion/aut_config.inc.php");

//-------- RUTAS FIJAS TEMPORALES PARA ACCEDER A SEGLOG, MIENTRAS NO SEA UN SERVICIO
include("aplicacion/servidor/SEGLOG/aplicacion/modelo/user_ldap.class.php");
include("aplicacion/servidor/SEGLOG/aplicacion/modelo/user_audit.class.php");

//----CLASES BASE PARA EL FUNCIONAMIENTO
include("aplicacion/servidor/_conexion/conexion.class.php");
include("aplicacion/servidor/seguridad/seguridad.class.php");
include("aplicacion/servidor/reporte/reporte.class.php");

//----- Librerias
include("aplicacion/librerias/saprfc/saprfc.class.php");
include("aplicacion/librerias/JSON.php");
include("aplicacion/librerias/cacheAPC.class.php");
include("aplicacion/librerias/PaginationArray.class.php");
include("aplicacion/librerias/WorkSession.class.php");
?>
