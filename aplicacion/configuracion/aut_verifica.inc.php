<?php

/* ======================================================================= */

function RedirectApp($type) {
   switch ($type) {
      case 'index':
         header("Location:/" . URL_NAME . "index.php");
         break;
      case 'timeout' :
         header("Location:/" . URL_NAME . "sesionexpirada.php", true, "301");
         break;
   }
   echo '<script type="text/javascript"> window.parent.Shadowbox.close();</script>';
   exit();
}

/* ======================================================================= */
if (empty($_SERVER['HTTP_REFERER'])) {
   $redir = "index.php";
} else {
   $url = explode("?", $_SERVER['HTTP_REFERER']);
   $pag_referida = $url[0];
   $redir = $pag_referida;
}

session_cache_limiter('nocache,private');
session_name(USUARIOS_SESION);
session_start();

/* Intento de Ingreso a la aplicación no autorizado */
if (!isset($_SESSION['usuario_login'])) {
   $usuario = new seguridad();
   $usuario->traza('Desconocido', 'true', 'Intento de acceso no autorizado', 0, '');
   RedirectApp("index");
   exit;
}

$sesiontrabajo = new WorkSession($_SESSION['usuario_login']);

if (is_null($sesiontrabajo->connectSession())) {
   /* No hay una sesión de trabajo activa */
   RedirectApp("index");
} else if (!$sesiontrabajo->compareSessionId(session_id())) {
   /* Existe otra sesión abierta */
   RedirectApp("timeout");
} else if (!$sesiontrabajo->isExpirated()) {
   /* La sesión no ha expirado, actualiza la proxima hora de expiración */
   $sesiontrabajo->setNextTimeExpired(SESION_EXPIRACION);
   $sesiontrabajo->updateSession();
} else { /* Expiro la sesión de trabajo */
   $usuario = new seguridad();
   $usuario->traza('no inf', 'true', 'Salida forzada de SISGEM, Sesion expirada', 1, '');
   RedirectApp("timeout");
}
?>
