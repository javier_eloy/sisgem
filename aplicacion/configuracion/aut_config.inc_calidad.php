<?php
/*
 Variables de Configuracion para todo el sistema
*/

//-------------- SISGEM BD
define('SQL_HOST','162.122.59.35');
define('SQL_USUARIO','sisgem_apl');
define('SQL_PASS','sisgem_apl');
define('SQL_DB','sisgem2');
define('SQL_TABLA','I004T_USUARIO');
define('USUARIOS_SESION', 'autentificator');


//-------------- Inventario BD
define('SQL_INV_HOST', "170.179.1.208");
define('SQL_INV_USUARIO',"inventario_apl");
define('SQL_INV_PASS',"inventario_apl");
define('SQL_INV_DB',"inventario");

//-------------- Variables para ADODB
define('ADODB_CACHE_PATH',$_SERVER['DOCUMENT_ROOT'].'cache');
define('ADODB_TIEMPO_CACHE',30);
//-------------- Variables Generales
define('SESION_EXPIRACION',3600); // Expiracion en segundos
define('CACHE_EXPIRACION',300); // Espiracion en Segundos para el cache

//-------------- Variables de SAP
define("SAP_HOST","167.134.155.139");   // application server
define("SAP_SYS","00");                 // system number
define("SAP_CLIENT","516");             // client
define("SAP_USER", "REVEROLE");         // user
define("SAP_PWD","pr21013");           // password
define("SAP_CODEPAGE","1160");
define("SAP_UNICODE","0");                //SAP R/3 4.6 no soporta unicode. 


//-------------- VARIABLES DE SEGLOG, SE COLOCAN ACÁ MIENTRAS NO SEA UN SERVICIO
define("LENEL_SERVER","ccscam90.pdvsa.com");
define("LENEL_USER","Co_Lenel_Occ");
define("LENEL_PASSWD","Co_Lenel_Occ");
define("LENEL_CATALOG","Lenel_Autentica");

define("LDAP_SERVER",'172.28.209.250');
define("LDAP_DOMAIN",'pdvsa.com');
define("BASE_DN",'dc=pdvsa,dc=com');

//------------- SERVIDOR DE FOTOS PDVSA 
define("SERVIDOR_FOTOS","http://ccschu14.pdvsa.com/PHOTOS/");
define("SERVIDOR_FOTOS_NODISP","http://intranet.pdvsa.com/++resource++foto-faltante.png");

//------------- CODIGO DE LA APLICACION PARA LA BITACORA
define("CO_APLIC","1");

//------------- BANDERA PARA ACTIVAR/INACTIVAR AUDITORIA
define("AUDIT_ACTIVA",false);


//--- Otras Definiciones
date_default_timezone_set('America/Caracas');
setlocale(LC_TIME, 'esp','es','sp');


//---- Constantes de Sistema
define("ASIGNACION_PROGRAMACION","1");
define("ASIGNACION_ESPECIFICA","2");
define("TIPO_NIVEL_PLAN","MPP,OPT");
define("NIVEL_PLAN_CICLO","MPP");
?>
