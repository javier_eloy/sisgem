<?php
/*
 * Esta libreria configura los include de la aplicación
 * Para cada modulo se debe incluir está libreria, no incluir librerias independientes de las presentada en la lista
 */
//
////---- LIBRERIAS GENERALES
//include("aplicacion/librerias/adodb5/adodb.inc.php");
//include("aplicacion/librerias/adodb5/adodb-exceptions.inc.php");
////include("aplicacion/servidor/SEGLOG/SegLog.inc.php");
//
////--------- OTROS INCLUDE DE CONFIGURACION
//include("aplicacion/configuracion/aut_mensaje_error.inc.php");
//include("aplicacion/configuracion/aut_config.inc.php");
//
////-------- RUTAS FIJAS TEMPORALES PARA ACCEDER A SEGLOG, MIENTRAS NO SEA UN SERVICIO
//include("aplicacion/servidor/SEGLOG/aplicacion/modelo/user_ldap.class.php");
//include("aplicacion/servidor/SEGLOG/aplicacion/modelo/user_audit.class.php");
//
////----CLASES BASE PARA EL FUNCIONAMIENTO
//include("aplicacion/servidor/_conexion/conexion.class.php");
//include("aplicacion/servidor/seguridad/seguridad.class.php");
//
////----- Librerias
//include("aplicacion/librerias/saprfc/saprfc.class.php");
//include("aplicacion/librerias/JSON.php");
//include("aplicacion/librerias/cacheAPC.class.php");
//include("aplicacion/librerias/PaginationArray.class.php");
//include("aplicacion/librerias/WorkSession.class.php");
//include('aplicacion/librerias/Classes/PHPExcel.php');
//include('aplicacion/librerias/Classes/PHPExcel/Reader/Excel5.php');


include("aut_lib_min.inc.php");

//------ Incluir Clases Controladoras
include_once ("aplicacion/servidor/_comun/librerias.php");
include_once ("aplicacion/servidor/_comun/comun.class.php");
include_once ("aplicacion/servidor/administrador/administrador.class.php");
include_once ("aplicacion/servidor/menu/menu.class.php");
include_once ("aplicacion/servidor/cambio_clave/cambioclave.class.php");
include_once ("aplicacion/servidor/gestion_personal/gestion_personal.class.php");
include_once ("aplicacion/servidor/planificacion_mtto/planificacion.class.php");
include_once ("aplicacion/servidor/planificacion_mtto/plan.class.php");
include_once ("aplicacion/servidor/programacion/programacion.class.php");
include_once ("aplicacion/servidor/asignacion_activ/asignacion_activ.class.php");
include_once ("aplicacion/servidor/ejecucion/ejecucion.class.php");
include_once ("aplicacion/servidor/configuracion/configuracion.class.php");
include_once ("aplicacion/servidor/configuracion/registro_personal.class.php");
include_once ("aplicacion/servidor/inicio_programacion/inicio_programacion.class.php");
include_once ("aplicacion/servidor/reporte/reporte.class.php");

//------- Incluir Clases Funcionalidades
include_once ("aplicacion/servidor/inventario/inventarioSAP.class.php");
include_once ("aplicacion/servidor/fecha/Fecha.class.php");


//------- carga masiva
include_once ("aplicacion/servidor/carga_masiva/carga_masiva.class.php");
include_once ("aplicacion/librerias/PHPExcel.php");
include_once ("aplicacion/librerias/PHPExcel/IOFactory.php");

//------ Verifica el acceso
include("aplicacion/configuracion/aut_verifica.inc.php");

?>
