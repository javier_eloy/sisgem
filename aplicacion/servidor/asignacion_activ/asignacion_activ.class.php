<?php

class asignacion_activ {

   private $MySQL = null;
   private $rs = null;

   //================= METODOS PRIVADOS ================
   public function __construct() {
      $this->MySQL = new conexion();
      $this->MySQL->ConnectMySQL();
   }

   public function __destruct() {
      $this->MySQL->CloseConnect();
   }

   //===============  METODOS PUBLICOS =================

   /*
    *  Obtiene las actividades asignadas
    */
   public function SelecActividadAsignada($co_estatus, $semanas, $ubic_tec, $no_aplazada, $pto_plan) {
      $where_aplazada = "";
      if ($no_aplazada)
         $where_aplazada = "AND COALESCE(PL.CO_ESTATUS_ORDEN,0) = 0";

      $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                    PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    PL.CO_ESTATUS_ORDEN,
                                                    PR.CO_ORDEN,
                                                    PR.FE_ASIG_ORDEN,
                                                    ORN.NU_ORDEN
                    FROM I037T_PLAN As PL  INNER JOIN C004T_PLAN_ORDEN PR ON  PL.CO_PLAN = PR.CO_PLAN
                                           INNER JOIN I036T_ORDEN ORN ON ORN.CO_ORDEN = PR.CO_ORDEN
                    WHERE PL.CO_TIPO_ASIGNACION = 1  
                    AND PL.CO_ESTATUS IN ({$co_estatus}) 
                    AND PL.NU_SEMANA_PROXIMA IN({$semanas}) 
                    AND PL.CO_EJEC_PLAN IN ({$pto_plan})    
                    AND PL.CO_UBIC_TEC LIKE '{$ubic_tec}%'
                    ORDER BY ORN.NU_ORDEN, PL.CO_PLAN;");

      return $this->rs;
   }

   /*
    *  Obtiene los planes asociados a una Orden
    */

   public function SelecPlanesAsignar($co_orden) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                    PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    PL.CO_ESTATUS_ORDEN,
                                                    PR.CO_ORDEN,
                                                    PR.FE_ASIG_ORDEN,
                                                    ORN.NU_ORDEN
                    FROM I037T_PLAN As PL  INNER JOIN C004T_PLAN_ORDEN PR ON  PL.CO_PLAN = PR.CO_PLAN
                                           INNER JOIN I036T_ORDEN ORN ON ORN.CO_ORDEN = PR.CO_ORDEN
                    WHERE PR.CO_ORDEN = {$co_orden}
                    ORDER BY ORN.NU_ORDEN, PL.CO_PLAN;");

      return $this->rs;
   }

   /*
    *  Obtiene los usuarios en los puestos de trabajo
    */

   public function SelecUsuarioPto($pto_plan, $co_cargo) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT  US.CO_USUARIO,
                                                      US.NB_INDICADOR,
                                                      US.NB_NOMBRE, 
                                                      US.TX_DIRECCION, 
                                                      CR.NB_CARGO, 
                                                      CR.CO_CARGO_PADRE 
                                            FROM I005T_CARGO CR 
                                            INNER JOIN I004T_USUARIO US ON US.NU_CARGO = CR.NU_CARGO 
                                            WHERE CR.NU_CARGO IN
                                                (SELECT PTO_T.NU_CARGO 
                                                 FROM C003T_CARGO_PUESTO_TRAB PTO_T 
                                                 WHERE PTO_T.CO_PUESTO_TRAB IN ({$pto_plan})) 
                                            AND CR.CO_CARGO_PADRE LIKE '{$co_cargo}%' OR CONCAT(CR.CO_CARGO_PADRE,CR.CO_CARGO) = '{$co_cargo}'
                                            AND US.IN_DISPONIBLE=0 
                                            AND COALESCE(US.IN_ADMIN,0) = 0");
      return $this->rs;
   }

   /*
    * Obtiene las asignaciones pendientes
    */

   public function SelecAsignacionPendiente($fecha_tope, $ubic_tec, $pto_plan) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                    PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    PL.CO_ESTATUS_ORDEN,
                                                    PR.CO_ORDEN,
                                                    PR.FE_ASIG_ORDEN,
                                                    ORN.NU_ORDEN
                    FROM I037T_PLAN As PL  INNER JOIN C004T_PLAN_ORDEN PR ON  PL.CO_PLAN = PR.CO_PLAN
                                           INNER JOIN I036T_ORDEN ORN ON ORN.CO_ORDEN = PR.CO_ORDEN
                    WHERE PL.CO_TIPO_ASIGNACION = 1  
                    AND PL.CO_ESTATUS IN (2)
                    AND PL.FE_PROXIMA_PLAN < STR_TO_DATE('$fecha_tope','%d/%m/%Y')
                    AND PL.CO_EJEC_PLAN IN ({$pto_plan})    
                    AND PL.CO_UBIC_TEC LIKE '{$ubic_tec}%'
                    ORDER BY ORN.NU_ORDEN, PL.CO_PLAN;");
      return $this->rs;
   }

   /*
    * Agrega los Usuarios con los planes de la Orden
    */

   public function InsUsuariosOrden($fe_asignacion, $usuarios_list, $co_orden) {
      $trans = false;

      $this->MySQL->BeginTrans();
      try {
         // Anexa los usuarios
         foreach ($usuarios_list as $usuario) {
            $sql = "INSERT INTO I022T_ACTIVIDAD(CO_ORDEN,CO_PLAN,CO_TIPO_ASIGNACION,FE_ASIGNACION,CO_ESTATUS,CO_USUARIO,
                                                  FE_INICIO,FE_FIN,HH_TOTAL_HORAS_HOMBRE,NU_PORCENTAJE_AVANCE,TX_OBSERVACION) 
                                        SELECT {$co_orden},ORN.CO_PLAN,1,STR_TO_DATE('$fe_asignacion', '%d/%m/%Y'),2,{$usuario},
                                                  '0000-00-00', '0000-00-00', 0, 0 , ''
                                        FROM  C004T_PLAN_ORDEN As ORN
                                        INNER JOIN I037T_PLAN As PL ON ORN.CO_PLAN = PL.CO_PLAN
                                        WHERE ORN.CO_ORDEN = {$co_orden};";
            $bres = (boolean) $this->MySQL->ExecuteSQL($sql);
            if (!$bres)
               throw new Exception("No se pudo insertar el usuario");
         }
         // Actualiza el estado de los planes a asignado
         $this->MySQL->ExecuteSQL("UPDATE I037T_PLAN SET CO_ESTATUS=1
                                      WHERE CO_PLAN IN (SELECT CO_PLAN FROM C004T_PLAN_ORDEN  WHERE CO_ORDEN = {$co_orden})");
         // Actualiza el estado de la orden a 2 = Asignacion de personal
         $this->MySQL->ExecuteSQL("UPDATE I036T_ORDEN SET TX_ESTATUS=2 WHERE CO_ORDEN={$co_orden}");
         $trans = true;
      } catch (ADODB_Exception $e) {
         $this->MySQL->RollbackTrans();
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
      }

      $this->MySQL->CommitTrans();
      return $trans;
   }

   /**
    *  Obtiene las actividades asignadas que no tienen el 100%
    */
   public function SelecActividadesProgramadas($pto_plan, $ubic_tec) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT DISTINCT PL.CO_PLAN,PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    PL.CO_ESTATUS_ORDEN,
                                                    PR.CO_ORDEN,
                                                    PR.FE_ASIG_ORDEN,
                                                    ORN.NU_ORDEN
                    FROM I037T_PLAN As PL  INNER JOIN C004T_PLAN_ORDEN PR ON  PL.CO_PLAN = PR.CO_PLAN
                                           INNER JOIN I036T_ORDEN ORN ON ORN.CO_ORDEN = PR.CO_ORDEN
                                           INNER JOIN I022T_ACTIVIDAD ACT ON ACT.CO_ORDEN = ORN.CO_ORDEN AND
                                                                      ACT.CO_PLAN = PR.CO_PLAN
                    WHERE ACT.NU_PORCENTAJE_AVANCE < 100 
                      AND PL.CO_ESTATUS = 1
                      AND ACT.CO_ESTATUS = 2
                      AND PL.CO_EJEC_PLAN IN ({$pto_plan})
                      AND PL.CO_UBIC_TEC LIKE '{$ubic_tec}%'
                    ORDER BY ORN.NU_ORDEN,PL.CO_PLAN,PL.VA_HOJARUTA");
      return $this->rs;

      /* ,
        ACT.FE_ASIGNACION,
        ACT.NU_PORCENTAJE_AVANCE */
   }

   /**
    *
    * @param type $co_orden
    * @return type
    */
   public function SelectPersonalActividadesProgramadas($co_orden, $va_hojaruta) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    PL.CO_ESTATUS_ORDEN,
                                                    PR.CO_ORDEN,
                                                    PR.FE_ASIG_ORDEN,
                                                    ORN.NU_ORDEN,
                                                    ACT.CO_ACTIVIDAD,
                                                    ACT.CO_USUARIO,
                                                    ACT.FE_ASIGNACION,
                                                    ACT.NU_PORCENTAJE_AVANCE,
                                                    US.NB_NOMBRE,
                                                    CRG.NB_CARGO
                    FROM I037T_PLAN As PL  INNER JOIN C004T_PLAN_ORDEN PR ON  PL.CO_PLAN = PR.CO_PLAN
                                           INNER JOIN I036T_ORDEN ORN ON ORN.CO_ORDEN = PR.CO_ORDEN
                                           INNER JOIN I022T_ACTIVIDAD ACT ON ACT.CO_ORDEN = ORN.CO_ORDEN AND
                                                                      ACT.CO_PLAN = PR.CO_PLAN
                                           INNER JOIN I004T_USUARIO US ON US.CO_USUARIO = ACT.CO_USUARIO
                                           INNER JOIN I005T_CARGO CRG ON CRG.NU_CARGO = US.NU_CARGO
                    WHERE ACT.NU_PORCENTAJE_AVANCE < 100 
                     AND PL.CO_ESTATUS = 1
                     AND ACT.CO_ESTATUS = 2
                     AND ORN.NU_ORDEN = {$co_orden}
                     AND PL.VA_HOJARUTA = '{$va_hojaruta}'
                    ORDER BY ORN.NU_ORDEN,PL.CO_PLAN,PL.VA_HOJARUTA");
      return $this->rs;
   }

   /**
    *
    * @param type $co_actividad
    * @return type
    */
   function SelecDatosActividad($co_actividad) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                    PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    PL.CO_EJEC_PLAN,
                                                    PL.CO_ESTATUS_ORDEN,
                                                    PR.CO_ORDEN,
                                                    PR.FE_ASIG_ORDEN,
                                                    ORN.NU_ORDEN,  
                                                    ORN.CO_ORDEN,
                                                    ACT.CO_ACTIVIDAD,
                                                    ACT.CO_USUARIO,
                                                    ACT.FE_ASIGNACION,
                                                    ACT.NU_PORCENTAJE_AVANCE,
                                                    US.NB_NOMBRE,
                                                    US.CO_USUARIO
                    FROM I037T_PLAN As PL  INNER JOIN C004T_PLAN_ORDEN PR ON  PL.CO_PLAN = PR.CO_PLAN
                                           INNER JOIN I036T_ORDEN ORN ON ORN.CO_ORDEN = PR.CO_ORDEN
                                           INNER JOIN I022T_ACTIVIDAD ACT ON ACT.CO_ORDEN = ORN.CO_ORDEN AND
                                                                      ACT.CO_PLAN = PR.CO_PLAN
                                           INNER JOIN I004T_USUARIO US ON US.CO_USUARIO = ACT.CO_USUARIO                          
                    WHERE ACT.CO_ACTIVIDAD = {$co_actividad}");
      return $this->rs;
   }

   /**
    *
    * @param type $co_orden
    * @param type $co_cargo
    * @return type
    */
   function SelecUsuariosDisponibleOrden($co_orden, $co_cargo,$pto_plan) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT US.CO_USUARIO,
                                                        US.NB_NOMBRE,
                                                        US.NB_INDICADOR,
                                                        CR.NB_CARGO
                                                FROM I004T_USUARIO US 
                                                  INNER JOIN I005T_CARGO CR ON CR.NU_CARGO = US.NU_CARGO
                                                WHERE CR.NU_CARGO IN (SELECT PTO_T.NU_CARGO
                                                                  FROM C003T_CARGO_PUESTO_TRAB PTO_T
                                                                  WHERE CO_PUESTO_TRAB IN  ($pto_plan))
                                                  AND CR.CO_CARGO_PADRE LIKE '{$co_cargo}%'
                                                  AND US.CO_USUARIO NOT IN (SELECT CO_USUARIO FROM I022T_ACTIVIDAD
                                                                               WHERE CO_ORDEN = $co_orden)");

      return $this->rs;
   }

   public function ActUsuarioActividad($co_actividad, $co_usuario_nuevo, $hoy) {

      $this->MySQL->BeginTrans();
      try {
         /* Intenta asignar el nuevo usuario si el avance es 0 */
         $bres = (boolean) $this->MySQL->ExecuteSQL("UPDATE I022T_ACTIVIDAD
                                                         SET CO_USUARIO = {$co_usuario_nuevo},
                                                            CO_TIPO_ASIGNACION = 4,
                                                            FE_ASIGNACION = CURDATE()
                                                         WHERE CO_ACTIVIDAD={$co_actividad} AND NU_PORCENTAJE_AVANCE = 0");
         $isUpdated = $this->MySQL->LastAffectedRow();
         if ($isUpdated === false)
            throw new Exception("Funcion no soportada, affectedRow");
         if ($isUpdated === 0) {
            /* Reinserta Historico */
            $bres = (boolean) $this->MySQL->ExecuteSQL("INSERT INTO I022T_ACTIVIDAD(CO_ORDEN,CO_PLAN,FE_ASIGNACION,
                                                                                        FE_INICIO,FE_FIN, HH_TOTAL_HORAS_HOMBRE,
                                                                                        NU_PORCENTAJE_AVANCE,CO_USUARIO,TX_OBSERVACION,
                                                                                        NU_PORCENTAJE_ANTERIOR,CO_ESTATUS,CO_TIPO_ASIGNACION)
                                                                      SELECT CO_ORDEN,CO_PLAN,FE_ASIGNACION,
                                                                             FE_INICIO,FE_FIN, HH_TOTAL_HORAS_HOMBRE,
                                                                             NU_PORCENTAJE_AVANCE,CO_USUARIO,TX_OBSERVACION,
                                                                             NU_PORCENTAJE_ANTERIOR,5,CO_TIPO_ASIGNACION
                                                                      FROM I022T_ACTIVIDAD 
                                                                      WHERE CO_ACTIVIDAD={$co_actividad} AND CO_ESTATUS = 2");
            if (!$bres)
               throw new Exception("No se puede copiar la actividad");
            /* Asigna el nuevo usuario */
            $bres = (boolean) $this->MySQL->ExecuteSQL("UPDATE I022T_ACTIVIDAD SET CO_USUARIO = {$co_usuario_nuevo},
                                                                                       CO_TIPO_ASIGNACION = 4,
                                                                                       FE_ASIGNACION = CURDATE(),
                                                                                       NU_PORCENTAJE_ANTERIOR=NU_PORCENTAJE_AVANCE    
                                                                WHERE CO_ACTIVIDAD={$co_actividad} AND CO_ESTATUS = 2");
            if (!$bres)
               throw new Exception("La actividad no se pudo reasignar");
         }

         $trans = true;
      } catch (ADODB_Exception $e) {
         $this->MySQL->RollbackTrans();
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
      }

      $this->MySQL->CommitTrans();
      return $trans;
   }

   /**
    * 
    * @param type $co_orden
    */
   public function SelecDatosOrden($co_orden) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT  ORN.NU_ORDEN,ORN.FE_ASIGNACION,
        PLORD.CO_PLAN, PLORD.PC_AVANCE, PLORD.FE_ASIG_ORDEN,
        PL.CO_UBIC_TEC, PL.CO_ESTATUS, PL.VA_DETALLE, PL.VA_HOJARUTA, PL.VA_TIPO_PLAN, PL.TX_HOJARUTA,
        ACT.CO_ESTATUS As ACT_STATUS,
        PRC.VA_PROCEDIMIENTO, PRC.NB_TEXTO,PRC.NU_NODO
        FROM I036T_ORDEN ORN
            INNER JOIN C004T_PLAN_ORDEN PLORD ON ORN.CO_ORDEN = ORN.CO_ORDEN
            INNER JOIN I037T_PLAN PL ON PL.CO_PLAN = PLORD.CO_PLAN
            INNER JOIN I022T_ACTIVIDAD ACT ON ACT.CO_ORDEN = ORN.CO_ORDEN AND ACT.CO_PLAN = PL.CO_PLAN
            INNER JOIN I020T_PROCEDIMIENTO_PLAN PRC ON PRC.CO_PLAN = PL.CO_PLAN
         WHERE ORN.CO_ORDEN=$co_orden
         GROUP BY ORN.CO_ORDEN,PL.CO_PLAN, PRC.VA_PROCEDIMIENTO
         ORDER BY CAST(PRC.NU_NODO AS UNSIGNED) ASC");
      return $this->rs;
   }

}

?>
