<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of seguridad
 *
 * @author portilloga
 */
//include ("aplicacion/configuracion/aut_lib.inc.php");

class seguridad {
   /*
    * ATRIBUTOS DE RESPUESTA
    */

   //
   //================= METODOS PRIVADOS ================
   //

   /*
    *
    */
   private function usuarioLdapValido($indicador, $clave) {
        $tmpLdap = new user_ldap();
        return $tmpLdap->login($indicador, $clave);
//       return 1;
   }

   //
   //================= METODOS PUBLICOS ================
   //
    
    /*
    *
    */
   public function __construct() {
      $this->MySQL = new conexion();
      $this->MySQL->ConnectMySQL();
   }

   /*
    *
    */

   public function __destruct() {
      $this->MySQL->CloseConnect();
   }

   /*
    *
    */

   public function esUsuarioLDAP($username) {
      $ldap = new user_ldap();
   }

   /*
    * Obtiene datos del LDAP
    */

   public function selDatosLDAP($username) {
      $ldap = new user_ldap();
      $arrayLDAP = $ldap->getDetailFromUsername($username, array("sn", "givenName", "middleName", "l", "pdvsacom-AD-organization", "pdvsacom-ad-Cedula"));
      return $arrayLDAP;
   }

   /*
    *
    */

   public function checkUser($indicador, $clave) {
      $usrLdap = $this->usuarioLdapValido($indicador, $clave);
      switch ($usrLdap) {
         case 1: 
            $sql = "SELECT count(co_usuario) as ACC
                    FROM I004T_USUARIO I004
                    WHERE I004.nb_indicador = '{$indicador}'";
            $rs=$this->MySQL->ExecuteSQL($sql);
            if ($rs->fields["ACC"]=="1")
                 return OK_USUARIO;
            else return ERR_USUARIONOREGISTRADO;
         case -1: return ERR_LDAP; /* Problema de Conexión */
         case 0: return ERR_USUARIONOLDAP; /* No es usuario LDAP */
         default: return ERR_DESCONOCIDO; /* Error desconocido */
      }
   }
 /*
  *
  */

   public function listarRol($indicador){
       $sql = "SELECT ROL.CO_ROL,ROL.NB_ROL, ROL.TX_DESCRIPCION
                FROM I004T_USUARIO US
                INNER JOIN C001T_ROL_USUARIO RUS ON RUS.CO_USUARIO = US.CO_USUARIO
                INNER JOIN I003T_ROL ROL ON ROL.CO_ROL = RUS.CO_ROL
                WHERE US.NB_INDICADOR ='{$indicador}'";
       $rs=$this->MySQL->ExecuteSQL($sql);
       return $rs;
   }
   /*
    *
    */

//   public function esAdmin($indicador) {
//      $sql = "select count(co_usuario) as X
//                from I004T_USUARIO I004
//                where I004.nb_indicador = '" . $indicador . "'
//                and I004.in_admin = 1";
//      return $this->MySQL->ExecuteSQL($sql);
//   }

   /*
    *
    */

//   public function claveAdminValida($indicador, $clave) {
//      $sql = "select count(co_usuario) as X
//                from I004T_USUARIO I004
//                where I004.nb_indicador = '" . $indicador . "'
//                and I004.TX_CLAVE = md5('" . $clave . "')
//                and I004.in_admin = 1";
//
//      return $this->MySQL->ExecuteSQL($sql);
//   }

   public function getDatosUsuario($indicador, $co_rol) {
      $sql = "SELECT I004.CO_USUARIO,
                        NB_INDICADOR,
                        IN_ESTADO_EMPLEADO,
                        NU_CEDULA,
                        NB_NOMBRE,
                        NU_TELEFONO,
                        TX_DIRECCION,
                        CONCAT(I005.CO_CARGO_PADRE,I005.CO_CARGO) as CO_CARGO,
                        I004.NU_CARGO,
                        IN_DISPONIBLE,
                        IN_PLANIFICADOR,
                        I003.IN_ADMIN,
                        I003.CO_UBIC_TECNICA_ADMIN,
                        GRP.GRP_PLAN,
                        PTO.PTO_PLAN,
                        I005.CO_UBIC_TECNICA,
                        CONCAT(I005R.CO_CARGO_PADRE,I005R.CO_CARGO)  As CO_CARGO_PLANIFICA
                FROM I004T_USUARIO I004
                    INNER JOIN C001T_ROL_USUARIO C001 ON C001.CO_USUARIO = I004.CO_USUARIO
                    INNER JOIN I003T_ROL I003 ON I003.CO_ROL = C001.CO_ROL
                    LEFT JOIN    I005T_CARGO I005 ON I004.NU_CARGO = I005.NU_CARGO
                    LEFT JOIN    I005T_CARGO I005R ON I004.NU_CARGO_PLANIFICADOR = I005R.NU_CARGO
                    LEFT JOIN (SELECT nu_cargo,GROUP_CONCAT(CO_GRUPO_PLAN) As GRP_PLAN
     							FROM C002T_CARGO_GRUPO_PLAN
                                GROUP BY nu_cargo) As GRP ON GRP.NU_CARGO = I005.NU_CARGO
                    LEFT JOIN (SELECT nu_cargo,GROUP_CONCAT(CO_PUESTO_TRAB) As PTO_PLAN
     							FROM C003T_CARGO_PUESTO_TRAB
                                GROUP BY nu_cargo) As PTO ON PTO.NU_CARGO = I005.NU_CARGO
                WHERE  I004.NB_INDICADOR ='$indicador' AND C001.CO_ROL=$co_rol";
      return $this->MySQL->ExecuteSQL($sql);
   }

   public function traza($coIndicador, $inFallido, $txInfo, $coAccion, $txTablas) {

      if (AUDIT_ACTIVA) {
         $auditoria = new user_audit();
         $auditoria->escribeBitacora($coIndicador, $inFallido, $txInfo, $coAccion, $txTablas);
      }
   }

}

?>
