<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menu
 *
 * @author hernandezjnz
 */
class MenuSISGEM {

   private $MySQL = null;
   private $rs = null;

   //================= METODOS PRIVADOS ================
   public function __construct() {
      $this->MySQL = new conexion();
      $this->MySQL->ConnectMySQL();
   }

   public function __destruct() {
      $this->MySQL->CloseConnect();
   }

   //=============== METODOS PUBLICOS ==================

   public function getUserMenu($usuario_id, $rol) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT  B.CO_BLOQUE,
        B.in_es_link,
        B.NU_SISTEMA As BNU_SISTEMA,
        B.NB_BLOQUE,
        CASE WHEN ISNULL(CO_MENU) THEN B.TX_DESCRIPCION ELSE M.TX_DESCRIPCION END As TX_DESCRIPCION,
        M.CO_MENU,
        M.NU_SISTEMA As MNU_SISTEMA,
        M.NB_MENU ,
        M.NU_POSICION_MENU,
        ROL.IN_ADMIN
            FROM I003T_ROL as ROL
            INNER JOIN J035T_MENU_ROL As MROL ON MROL.CO_ROL = ROL.CO_ROL
            INNER JOIN C001T_ROL_USUARIO As ROLU ON ROLU.CO_ROL = MROL.CO_ROL
            INNER join I024T_BLOQUE As B on B.CO_BLOQUE= MROL.CO_BLOQUE AND B.NU_POSICION > 0
            LEFT join I033T_MENU As M ON M.CO_BLOQUE= MROL.CO_BLOQUE AND M.NU_POSICION_MENU > 0
         WHERE ROLU.CO_USUARIO = $usuario_id  AND  ROL.co_rol = $rol
         ORDER BY B.NU_POSICION, B.CO_BLOQUE");
      return $this->rs;
   }

}

?>
