<?php

class administrador {

    private $MySQL = null;
    private $rs = null;
    private $esTmpGrpPto = false;

    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQL->ConnectMySQL();
    }

    public function __destruct() {
        $this->MySQL->CloseConnect();
    }

    //===============  METODOS PUBLICOS =================

    public function SelecAdministrador($co_usuario) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT  CO_USUARIO,
                                                      NB_INDICADOR,
                                                      TX_CLAVE,
                                                      NB_NOMBRE,
                                                      CO_UBIC_TECNICA_ADMIN
                                              FROM I004T_USUARIO 
                                              WHERE IN_ADMIN = 1 
                                                AND CO_USUARIO !=" . $co_usuario);
        return $this->rs;
    }

    /*
     * Inserta un administrador
     */

    public function InsertAdministrador($nb_indicador, $tx_clave, $nb_nombre, $co_ubic_tecnica_admin) {
        $tx_clave = md5($tx_clave);
        $f = true;

        $this->MySQL->BeginTrans();
        try {
            $this->MySQL->ExecuteSQL("INSERT INTO I004T_USUARIO(NB_INDICADOR,TX_CLAVE,NB_NOMBRE,CO_UBIC_TECNICA_ADMIN,IN_ADMIN)
                                                   VALUES('$nb_indicador','$tx_clave','$nb_nombre','$co_ubic_tecnica_admin',1)");
            $co_usuario = $this->MySQL->LastInsertId("I004T_USUARIO");
            $this->MySQL->ExecuteSQL("INSERT INTO J034T_MENU_USUARIO (CO_USUARIO,CO_BLOQUE)
                                            SELECT $co_usuario,CO_BLOQUE
                                               FROM I024T_BLOQUE 
                                               WHERE NB_BLOQUE LIKE 'CONFIGURA%'");
        } catch (Exception $e) {
            $f = false;
        } catch (ADODB_Exception $e) {
            $f = false;
        }
        $this->MySQL->CommitTrans();
        return $f;
    }

    /*
     * Elimina un administrador
     */

    public function EliminarAdministrador($co_usuario) {
        return (boolean) $this->MySQL->ExecuteSQL("DELETE FROM I004T_USUARIO WHERE CO_USUARIO = $co_usuario");
    }

}

?>
