<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Fecha
 *
 * @author portilloga
 */
class Fecha 
{
     /*
     * 
     */
    function sumaFechas($fecha,$ndias)
    {
      if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha))
              list($dia,$mes,$ano)=split("/", $fecha);
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha))
              list($dia,$mes,$ano)=split("-",$fecha);
        $nueva = mktime(0,0,0, $mes,$dia,$ano) + $ndias * 24 * 60 * 60;
        $nuevafecha=date("d-m-Y",$nueva);
      return ($nuevafecha);  
    }
     /*
     * 
     */
    function concatenaDatos($d1,$d2,$d3)
    {
        return $d1.'-'.$d2.'-'.$d3;
    }
     /*
     * 
     */
    function semanaAnio($dia,$mes,$anio)
    {
        $ns_ultimo = strftime("%W", mktime(0,0,0,$mes,$dia,$anio));
        if(strftime("%W", mktime(0,0,0,$mes,$dia,$anio)) == '00')
        {
            $ns_ultimo = 53;
            $anio      = ((int) $anio) - 1;
        }
        return $ns_ultimo."-".$anio;
    }
}

?>
