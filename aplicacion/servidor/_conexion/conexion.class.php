<?php

/**
 *
 * @author hernandezjnz
 */
global $ADODB_COUNTRECS,
       $ADODB_CACHE_DIR;

class conexion {
  private $con = null;
  private $msg_error = "";

  //=============== Funciones Privadas ======================
  public function  __construct() {
      $ADODB_COUNTRECS = false;
      $ADODB_CACHE_DIR = ADODB_CACHE_PATH;
      $GLOBALS['ADODB_CACHE_DIR'] = ADODB_CACHE_PATH;  
  }
  public function __destructor() {
      
  } 
 //=============== Funciones Publicas ===================== 
  /*
   * Funcion específica para conectarse a MYSQL o BD Local
   */
 public function ConnectMySQL() {
    
//     $flags =  MYSQLI_CLIENT_COMPRESS;
     $dsn = "mysqli://".SQL_USUARIO.":".SQL_PASS."@".SQL_HOST."/".SQL_DB."?persist&port=".SQL_PORT."&clientflags=$flags";
//$this->con= NewADOConnection('mysqli');
     try {
        $this->con=ADONewConnection($dsn);
//$this->con->PConnect(SQL_HOST,SQL_USUARIO,SQL_PASS,SQL_DB);
     } catch(ADODB_Exception $e) {
//        $this->con= NewADOConnection("mysqli");
//        $this->con->NConnect(SQL_HOST.":".SQL_PORT,SQL_USUARIO,SQL_PASS,SQL_DB);
       $this->con=ADONewConnection(str_replace("persist","",$dsn));
    }
     $this->con->Execute("SET NAMES utf8");
     $this->con->CacheFlush();    
     $this->con->multiQuery = true;
 }
 
 public function CloseConnect() {
     $this->con->Disconnect();
     $this->con = null;
 }
 
/*
* Funcion específica para conectarse a MYSQL Inventario (temporal)
*/
// public function ConnectInventario (){
//     $this->con= NewADOConnection('mysqli');
//     $this->con->PConnect(SQL_INV_HOST,SQL_INV_USUARIO,SQL_INV_PASS,SQL_INV_DB);
//     $this->con->Execute("SET NAMES utf8");
//     $this->con->multiQuery = true;
// }
//
//  public function CloseConnectInventario() {
//     $this->con->Disconnect();
// }
 /*
  * Función específica para conectarse al SAPRFC 
  */
 public function ConnectSAP (){
     
 }
 /*
  * Ejecuta un comienzo de transacción
  */
 public function BeginTrans() {
      if ($this->con) $this->con->StartTrans();
                else throw new Exception(__METHOD__,BD_DECONECTADO);
  }
  /*
   * Realiza un COMMIT a la base de datos
   */
  public function CommitTrans() {
      if ($this->con) $this->con->CompleteTrans();
                else throw new Exception(__METHOD__,BD_DECONECTADO);                
  }
  /*
   * Realiza un deshacer a la transacción
   */
  public function RollbackTrans() {
      if($this->con) {
          if (!$this->hasFailed()) $this->con->FailTrans();
      } else 
           throw new Exception(__METHOD__,BD_DESCONECTADO);
  }
  /*
   * Devuelve el estado de la ultima transacción ejecutada
   */
  public function hasFailed() {
     return $this->con->HasFailedTrans();
  }
  /*
   * Funcion que ejecuta un SQL con la conexión actual
   */
  public function ExecuteSQL($sql) {
     return $this->con->Execute($sql);
  }
  /*
   *  Devuelve el ultimo id manejado por la conexión
   */
  public function LastInsertId($tabla) {
      return $this->con->Insert_id($tabla);
  }
  
  /*
   * Devuelve el numero de afectadas filas en la ultima sentencia
   */
  public function LastAffectedRow(){
      return $this->con->Affected_Rows();
  }
  /*
   * Funcion que ejecuta y devuelve la primeras fila
   */
  public function ExecuteRow($sql) {
      return $this->con->GetRow($sql);
  }
  /*
   * Ejecuta consulta con el uso del cache 
   */
  public function ExecuteCacheSQL($sql){
      return $this->con->CacheExecute(ADODB_TIEMPO_CACHE,$sql);
  }
  /*
   * Devuelve el ultimo ID
   */
  public function getLastID(){
      return $this->con->Insert_ID();
      
  }
}

?>
