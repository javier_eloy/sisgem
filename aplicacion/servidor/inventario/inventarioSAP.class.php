<?php

/**
 * Description of InventarioSAP 
 *
 * @author portilloga
 */
class inventarioSAP {
    /*
     * ATRIBUTOS DE RESPUESTA
     */

    private $arrayRes = null; //Arreglo con resultado de la consulta
    private $sapResult = null;
    private $sapObj = null; //Objeto de conexion saprfc
    //
    //================= METODOS PRIVADOS ================
    // 

    public function __construct() {
        $this->sapObj = new saprfc(array
                    (
                    "logindata" => array
                        (
                        "ASHOST" => SAP_HOST
                        , "SYSNR" => SAP_SYS
                        , "CLIENT" => SAP_CLIENT
                        , "USER" => SAP_USER
                        , "PASSWD" => SAP_PWD
                        , "CODEPAGE" => SAP_CODEPAGE
                        , "UNICODE" => SAP_UNICODE
                    )
                    , "show_errors" => true   // el despliegue de los errores viene de la clase directo de SAP.
                    , "debug" => false
                        )
        );
        
//        $this->sapObj = new saprfc(array
//                    (
//                    "logindata" => array
//                        (
//                        "ASHOST" => SAP_HOST
//                        , "SYSNR" => SAP_SYS
//                        , "CLIENT" => SAP_CLIENT
//                        , "USER" => SAP_USER
//                        , "PASSWD" => SAP_PWD 
//                    )
//                    , "show_errors" => false   // el despliegue de los errores viene de la clase directo de SAP.
//                    , "debug" => false
//                        )
//        );
    }

    public function __destruct() {
        $this->sapObj->logoff();
        unset($this->sapObj);
    }
    
    /**
     * Encapsula las llamadas a SAP 
     * y retorna los resultados 
     */
    private function ExecuteSAP($func, $prm) {
        if ($this->sapObj->getStatus() == SAPRFC_OK) { //Si la llamada fue exitosa
            $Result = $this->sapObj->callFunction($func, $prm);
            $this->arrayRes = $Result;
        } else
            return null;

        return $this->arrayRes;
    }

    /**
     * Encapsula las llamadas a SAP 
     * y retorna los resultados en una tabla (matriz)
     */
    private function ExecuteSAP_Tabla($func, $prm, $tbl) {
        if ($this->sapObj->getStatus() == SAPRFC_OK) { //Si la llamada fue exitosa
            $Result = $this->sapObj->callFunction($func, $prm);
            $this->arrayRes = $Result[$tbl];
        } else
            return null;

        return $this->arrayRes;
    }

    //================================  METODOS PUBLICOS ===============================   
    /*     * ****************************************************************************
     *
     *                         SelecUbicacionTecnica
     * Devuelve las Ubicaciones Tecnicas
     * ENTRADA: Ubicación Tecnica Raíz 
     * RETORNA: Lista con las regiones de SAP 
     *
     * *************************************************************************** */
    public function SelecUbicacionTecnica($coUbicTecnica) {
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_TPLNR", array(array("IMPORT", "PR_TPLNR", $coUbicTecnica),
            array("TABLE", "TI_TPLNR", array())), "TI_TPLNR");

        return $this->sapResult;
    }

    /*     * *******************************************************************************
     *  
     *                          SelecGrupoPlanificador
     * Devuelve los grupos de planficación
     * ENTRADA: Centro de Emplazamiento
     * RETORNA: Lista de grupos de planificacion
     * 
     * ***************************************************************************** */

    public function SelecGrupoPlanificador($coCentroEmp) {
        /* Centro de Emplazamiento AIT =1A00 */
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_GRUPLA_CP", array(array("IMPORT", "PR_IWERK", $coCentroEmp),
            array("TABLE", "TI_GRUPLA", array())), "TI_GRUPLA");

        return $this->sapResult;
    }

    /*     * ****************************************************************************
     *                          SelecPtoTrabajo
     * Devuelve los grupos de planficación
     * ENTRADA: Centro de Emplazamiento
     * RETORNA: Lista de grupos de planificacion
     * 
     * ************************************************************************** */

    public function SelecPtoTrabajo($coGrupoPlan, $coCentroEmp) {
        /* Centro de Emplazamiento pr_werks= AIT =1A00 , pr_arbpl
         */
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_PUESTO_GP", array(array("IMPORT", "PR_ARBPL", "*-" . $coGrupoPlan),
            array("IMPORT", "PR_WERKS", $coCentroEmp),
            array("TABLE", "TI_PUERE", array())), "TI_PUERE");

        return $this->sapResult;
    }

    /*     * ****************************************************************************
     *
     *                         SelecDetalleEqpSAP
     * Devuelve el detalle de un equipo
     * ENTRADA: Codigo conformado con la estructura de ubicación técnica del equipo en SAP
     * RETORNA: Detalle del equipo de SAP 
     *
     * *************************************************************************** */

    public function SelecDetalleEqpSAP($coEquipo) {

        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_EQUNR", array(array("IMPORT", "PR_EQUNR", $coEquipo),
            array("TABLE", "TI_EQUNR", array())), "TI_EQUNR");

        return $this->sapResult;
    }

    /*     * ****************************************************************************
     *
     *                         SelecGrpPlanSAP
     * Devuelve el grupo planificador en SAP asociado a una ubicación técnica
     * ENTRADA: Código de ubicación téncica
     * RETORNA: Lista con las gerencias o negocios de SAP 
     *
     * *************************************************************************** */

    public function SelecGrpPlanSAP($coUbicTecnica) {
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_EQUI_TPL", array(array("IMPORT", "PR_TPLNR", $coUbicTecnica),
            array("TABLE", "TI_EQUNR", array())), "TI_EQUNR");

        return $this->sapResult;
    }

    /*     * ****************************************************************************
     *
     *                         SelectEstacionGrpPlanUbicTec
     * Devuelve las ubicaciones técnicas asociadas a un grupo de planificacion dado
     * ENTRADA: Código grupo de planificacion y ubicación técnica
     * RETORNA: Arreglo con el detalle de las ubicaciones técnicas asociadas al grupo planificador 
     *
     * *************************************************************************** */

    public function SelectEstacionGrpPlanUbicTec($coGrupoPlan, $coUbicTecnica) {
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_TPLNR_GP", array(array("IMPORT", "PR_INGRP", $coGrupoPlan),
            array("IMPORT", "PR_TPLMA", $coUbicTecnica),
            array("TABLE", "TI_TPLNR", array())), "TI_TPLNR");

        return $this->sapResult;
    }
    
        /*     * ****************************************************************************
     *
     *                         SelectEstacionUbicTecUbicTec
     * Devuelve las ubicaciones técnicas 
     * ENTRADA: Ubicación técnica desde que se va hacer la busqueda
     * RETORNA: Arreglo con el detalle de las ubicaciones técnicas asociadas a la UT que se plantea en la busqueda 
     *
     * *************************************************************************** */

    public function SelectEstacionUbicTecUbicTec($coUbicTecnica) {
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_TPLNR", array(array("IMPORT", "PR_TPLNR", $coUbicTecnica),
            array("TABLE", "TI_TPLNR", array())), "TI_TPLNR");

        return $this->sapResult;
    }

    /*     * ****************************************************************************
     *
     *                         SelecTEquipoGrpPlan
     * Devuelve los equipos asociados a un grupo de planificacion dado
     * ENTRADA: Código grupo de planificacion
     * RETORNA: Arreglo con el detalle de los equipos asociados al grupo planificador 
     *
     * *************************************************************************** */

    public function SelectEquipoGrpPlan($coGrupoPlan) {
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_EQUNR", array(array("IMPORT", "PR_INGRP", $coGrupoPlan),
            array("TABLE", "TI_EQUNR", array())), "TI_EQUNR");

        return $this->sapResult;
    }

    /*     * ****************************************************************************
     *
     *                         SelecTEquipoGrpPlanUbicTec
     * Devuelve los equipos asociados a un grupo de planificacion y ubicación técnica dados
     * ENTRADA: Código grupo de planificacion, Código Ubicación Tecnica
     * RETORNA: Arreglo con el detalle de los equipos asociados al grupo planificador 
     *
     * *************************************************************************** */

    public function SelectEquipoGrpPlanUbicTec($coGrupoPlan, $coUbicTecnica) {
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_EQUNR", array(array("IMPORT", "PR_INGRP", $coGrupoPlan),
            array("IMPORT", "PR_TPLNR", $coUbicTecnica),
            array("TABLE", "TI_EQUNR", array())), "TI_EQUNR");

        return $this->sapResult;
    }
    
      /*     * ****************************************************************************
     *
     *                         SelectHojaRutaEquipo
     * Devuelve la hoja de ruta asociada a un codigo de equipo especifico
     * ENTRADA: Código de equipo
     * RETORNA: Arreglo con el detalle de la hoja de ruta asociada al equipo 
     *
     * *************************************************************************** */

    public function SelectHojaRutaEquipo($coEquipo, $operacion= null) {
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_EQUI_PLNNR", array(array("IMPORT", "PR_EQUNR", $coEquipo),
            array("TABLE", "TI_PLPO", array())), "TI_PLPO");
        if (isset($operacion)) {
            $arrayProcedimiento = array_values(array_filter($this->sapResult, function($row) use($operacion) {
                                return (($row["PLNAL"] == $operacion) && ($row["PLNNR"] != "") && ($row["PLNAL"] != ""));
                            }));
            return $arrayProcedimiento;
        } else
            return $this->sapResult;
    }

    /*     * ****************************************************************************
     *
     *                         SelectHojaRutaInstalacion
     * Devuelve la hoja de ruta asociada a un codigo de equipo especifico
     * ENTRADA: Código de equipo
     * RETORNA: Arreglo con el detalle de la hoja de ruta asociada al equipo 
     *
     * *************************************************************************** */

    public function SelectHojaRutaInstalacion($coInstalacion,$operacion = null) {
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_TPL_PLNNR", array(array("IMPORT", "PR_TPLNR", $coInstalacion),
            array("TABLE", "TI_PLPO", array())), "TI_PLPO");

        if (isset($operacion)) {
            $arrayProcedimiento = array_values(array_filter($this->sapResult, function($row) use($operacion) {
                                return (($row["PLNAL"] == $operacion) && ($row["PLNNR"] != "") && ($row["PLNAL"] != ""));
                            }));
            return $arrayProcedimiento;
        } else        
            return $this->sapResult;
    }

    /*     * ****************************************************************************
     *
     *                         SelecTEquipoGrpPlanUbicTec
     * Devuelve los equipos asociados a un grupo de planificacion y ubicación técnica dados
     * ENTRADA: Código grupo de planificacion, Código Ubicación Tecnica
     * RETORNA: Arreglo con el detalle de los equipos asociados al grupo planificador 
     *
     * *************************************************************************** */

    public function SelectTxtUbicTec($coUbicTecnica) {
        $this->sapResult = $this->ExecuteSAP_Tabla("Z9PMF_TPLNR", array(array("IMPORT", "PR_TPLNR", $coUbicTecnica),
            array("TABLE", "TI_TPLNR", array())), "TI_TPLNR");

        return $this->sapResult;
    }
    
     /**     
     ** ****************************************************************************
     *
     *                         SelectDetalleDocSAP
     * Devuelve la información asociada a un número de contrato/orden de trabajo/SOLPE dado
     * ENTRADA:
      * PR_EVRTN = Número de contrato / Número de pedido / Número de solped
     * SALIDA: 
      * V_VALID = validación de PR_EVRTN (0 vacio, 1 contrato, 2 pedido, 3 solped, 4 no encontrado)
      * V_VIGEN = 1 vigente, 0 no vigente
      * TI_EKKO = Datos de cabecera del contrato (arreglo)
      * TI_EKPO = Posiciones del contrato (matriz)
      * TI_EKAB = Datos del pedido (arreglo)
      * TI_ESSR = Datos de las HES (Hoja de Entrada de Servicios ) (arreglo)
      * TI_EBAN = Datos de la solped (arreglo)
     *
     * *************************************************************************** */

    public function SelectDetalleDocSAP($coDocSAP) {
        

         $this->sapResult = $this->ExecuteSAP("Z9PMF_CONT_PED_SOLP", 
                                               array(array("IMPORT", "PR_EVRTN", $coDocSAP),
                                                     array("EXPORT", "V_VALID"),
                                                     array("EXPORT", "V_VIGEN"),
                                                     array("TABLE", "TI_EKKO", array()),
                                                     array("TABLE", "TI_EKPO", array()),
                                                     array("TABLE", "TI_EKAB", array()),
                                                     array("TABLE", "TI_ESSR", array()),
                                                     array("TABLE", "TI_EBAN", array())));
        return $this->sapResult;

    }
}

?>
