<?php
//include_once('aplicacion/librerias/adodb5/adodb.inc.php');

/**
 * Description de user_ldap.class
 *
 * @author hernandezjnz
 */
class user_ldap {
    private $connection = null;
  
    function __construct () {
 
    }
     /*==================================== PRIVATE METHODS ====================================*/
    /**
     * Metodo que conecta al ldap server
     */
    private function conect(){
        $this->connection = @ldap_connect(LDAP_SERVER);
        ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, 3 );     
        ldap_set_option($this->connection, LDAP_OPT_REFERRALS, 0);
    }
     private function close(){
        ldap_close($this->connection);
        
    }
    private function is_connected(){
        return $this->connection!= NULL; 
    }
          
    
    /*====================================== PUBLIC METHODS ======================================*/  
    /**
     * Metodo que envia el usuario y clave a ldap server/domain
     *  
     * @param string $username
     * @param string $password
     */
    public function login ($username, $password) {
        $login = false;
        $full_username = $username."@".LDAP_DOMAIN;
        $this->conect();
        try {
           if ($this->is_connected()) {
             $login = @ldap_bind( $this->connection, $full_username, $password );
             if ($login){ return 1;}
             else {return 0;}
           }      
        } catch (Exception $e) {
            return -1;
        }         
        finally; 
        {        
//        finally {
          $this->close();  
        }        
      }
   /**
     * Metodo que entrega un arreglo de datos del ldap 
     * @param string $username
     */
    public function getDetailFromUsername ($username,$elements) {
        $this->conect();
       try { 
         $sr = @ldap_search( $this->connection, BASE_DN, "sAMAccountName=$username" );
         if($sr===false) return null;
         
         $entry = @ldap_first_entry ( $this->connection, $sr );         
         if($entry ===false) return null;
         
         foreach($elements as $elm){
             $value = @ldap_get_values( $this->connection, $entry, $elm );             
             $detail[$elm] = trim($value[0]);    
         }             
         return $detail;
         
       } catch (Exception $e) {
          return -1;
        } finally; {          
//       } finally {
          $this->close();
       }
    }
    /**
     * Metodo que entrega el nombre del ldap user
     * @param string $username
     */
    public function getFullNameFromUsername ($username) {
        $this->conect();
       try { 
         $sr = @ldap_search( $this->connection, BASE_DN, "sAMAccountName=$username" );
         $entry = @ldap_first_entry ( $this->connection, $sr );
         $full_name = @ldap_get_values( $this->connection, $entry, "displayName" );
         return $full_name[0];
       } catch (Exception $e) {
          return -1;
       
        } finally; {          
//       } finally {
          $this->close();
       }
    }

    /**
     * metodo que retorna el email address de un usuario ldap
     * @param string $username
     */
    public function getEmailFromUsername ($username) {
        $this->conect();
        try {
        $sr = @ldap_search ( $this->connection, BASE_DN, "sAMAccountName=$username" );
        $entry = @ldap_first_entry ( $this->connection, $sr );
        $email = @ldap_get_values ( $this->connection, $entry, "mail" );
        return $email [0];
        } catch (Exception $e) {
            return -1;
        } finally; {
//        } finally {
            $this->close;
        }
    }
 /**
     * metodo que retorna el email address de un usuario ldap
     * @param string $username
     */
    public function getIDFromUsername ($username) {
        $this->conect();
        try {
        $sr = @ldap_search ( $this->connection, BASE_DN, "sAMAccountName=$username" );
        $entry = @ldap_first_entry ( $this->connection, $sr );
        $id = @ldap_get_values ( $this->connection, $entry, "pdvsacom-ad-Cedula" );
        return $id[0];
        } catch (Exception $e) {
            return -1;
        } finally; {
//         } finally {    
            $this->close;
        }
    }
}
?>
