<?php
require_once('aplicacion/librerias/adodb5/adodb.inc.php');

/**
 * Description of user_lenel
 *
 * @author hernandezjnz
 */
class user_lenel {

     private $connection = NULL;
             
    function __construct () {
        $this->connection = NewADOConnection('mssql');
        $this->connection->Connect(LENEL_SERVER,LENEL_USER,LENEL_PASSWD,LENEL_CATALOG); 
    }
    
    function __destruct() {
        $this->connection->Close();        
    }
     /*==================================== PRIVATE METHODS ====================================*/
        /**
     * Metodo que conecta al servidor LENEL
     */
    private function is_connected(){
        return $this->connection->IsConnected();
    }
     /*====================================== PUBLIC METHODS ======================================*/  
    /**
     * Metodo que envia el usuario y clave a ldap server/domain
     *  
     * @param string $username
     * @param string $password
     */
    public function getLenelFromUsername($cedula) {
        if($this->is_connected()) {
            $this->connection->Execute("SELECT * FROM Results WHERE cedula=$cedula");
        }
        
    }
    
    
            
    
}

?>
