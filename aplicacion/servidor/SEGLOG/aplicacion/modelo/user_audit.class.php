<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user_log
 *
 * @author portilloga
 */

class user_audit {
    
   private $con = null;

   function __construct () {

        $this->con= NewADOConnection('postgres');
        $this->con->PConnect(AUDIT_SERVER,AUDIT_USER,AUDIT_PASSWD,AUDIT_CATALOG);
//        $this->con->Execute("SET NAMES utf8");
        $this->con->multiQuery = true;        
    }


        
    /************************************************************************************
    ************ FUNCION: getIpCliente()  *************************************************
    ************ FECHA DE CRACION: 14/08/2013 *******************************************
    ************ AUTOR: PORTILLOGA ********** *******************************************
    ************ PARAMETROS: NINGUNO ****************************************************
    ************ DESCRIPCION: OBTENER LA DIRECCION IP REAL DEL CLIENTE ******************/
    private function getIpCliente() 
    {
      if (!empty($_SERVER['HTTP_CLIENT_IP']))
          return $_SERVER['HTTP_CLIENT_IP'];
      if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
          return $_SERVER['HTTP_X_FORWARDED_FOR'];
      return $_SERVER['REMOTE_ADDR'];
    }

    /************************************************************************************
    ************ FUNCION: escribeBitacora()  ********************************************
    ************ FECHA DE CRACION: 14/08/2013 *******************************************
    ************ AUTOR: PORTILLOGA ********** *******************************************
    ************ PARAMETROS: $coIndicador: Indicador de Usuario
    ************             $infallido:   true si falla 
    ************             $txInfo: comentario
    ************             $coAccion: codigo de accion: 0: ingreso, 1: salida,  2: insert 
    *                                                     3: update, 4: delete                                           
    ************             $txTablas: Tablas tocadas por la transaccion 
    ************ DESCRIPCION: Ingresar registro descriptivo de una accion de usuario ******
    ************              en la bitacora                                   ************/
    public function escribeBitacora ($coIndicador,$inFallido,$txInfo,$coAccion,$txTablas)
    {
         $tabla = '"auditoria"."I001T_BITACORA"'; //--- ARTIFCIO POR EL USO DE " Y ' EN POSTGRES
         $sql   ="insert into  ".$tabla."(co_aplicacion,
                                          fe_actual,
                                          nb_indicador,
                                          nu_ip_origen,
                                          in_fallido,
                                          tx_info,
                                          co_accion,
                                          tx_tablas_procesadas)
                                   values(".CO_APLIC.",                                                          
                                          'now',
                                         '".$coIndicador."',
                                         '".$this->getIpCliente()."',
                                          '".$inFallido."',
                                          '".$txInfo."',
                                          '".$coAccion."',
                                          '".$txTablas."')";
//         echo $sql;
         $this->con->Execute($sql); 
    }
}
?>
