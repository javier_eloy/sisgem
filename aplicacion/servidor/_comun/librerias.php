<?php

function sumaDiasFecha($fecha, $ndias) {
    if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/", $fecha))
        list($dia, $mes, $ano) = split("/", $fecha);
    if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/", $fecha))
        list($dia, $mes, $ano) = split("-", $fecha);
    $nueva = mktime(0, 0, 0, $mes, $dia, $ano) + $ndias * 24 * 60 * 60;
    $nuevafecha = date("d-m-Y", $nueva);
    return ($nuevafecha);
}

function semanaAnio($dia, $mes, $anio) {
    $ns_ultimo = strftime("%W", mktime(0, 0, 0, $mes, $dia, $anio));
    if (strftime("%W", mktime(0, 0, 0, $mes, $dia, $anio)) == '00') {
        $ns_ultimo = 53;
        $anio = ((int) $anio) - 1;
    }
    return $ns_ultimo . "-" . $anio;
}

function EnviarMsgHTML($msg) {
    return "<center>
         <table width='100%' height='15' >
         <tr><td width='100%' height='15'>
             <p align='center'>
             <div class='titulomenu'><font face='Arial' size='2' color='#FFFFFF'>&nbsp; $msg</font></div>
             </td>
         </tr>
         </table>
      </center>";
}

/*** FUNCION QUE ADICIONA DIAS A UNA FECHA DADA ***/
function suma_fechas($fecha,$ndias)
{
    if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha))
        list($dia,$mes,$ano)=split("/", $fecha);
    if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha))
        list($dia,$mes,$ano)=split("-",$fecha);
    $nueva = mktime(0,0,0, $mes,$dia,$ano) + $ndias * 24 * 60 * 60;
    $nuevafecha = date("d-m-Y",$nueva);
    return ($nuevafecha);  
}
function DestruirSesion($sesion_nom) {
    session_cache_limiter('nocache,private');
    session_name($sesion_nom);
    session_id();
    session_start();
    
    session_destroy();
    session_unset();
}

function PalabraExisteInicio($string, $words) {
    foreach($words as $word) {
        if(stripos($string, $word) === 0) {
            
            return true;
        }
    }
    return false;
}

function DecodificarGet($value){
   $response=base64_decode(substr($value,LEN_KEY_START));
   parse_str($response,$_GET);
}

function CodificarGet($value) {
   return KEY_START.base64_encode($value);
}
/**
 * Encripta un query string para usarse por $_GET
 * @param type $qryStr
 * @return type
 */
function encryptLink($qryStr){
	$query = base64_encode(urlencode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(KEYSALT), $qryStr,
                           MCRYPT_MODE_CBC, md5(md5(KEYSALT)))));
	return $query;
}
/**
 * Decrypta un query string para usarse por $_GET
 * @param type $qryStr, no se usa
 */
function decryptLink($qryStr) {
   $queryString = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(KEYSALT),
                        urldecode(base64_decode($qryStr)), MCRYPT_MODE_CBC,
                        md5(md5(KEYSALT))), "\0");
  return $queryString;
}

/**Para manejo de sesiones**/
//function saveSession($user,$sesion){
//   $path=sys_get_temp_dir().DIRECTORY_SEPARATOR.base64_encode($user).".wsgm";
//   $res=file_put_contents($path,base64_encode($sesion));
//
//   return ($res !== false);
//}
//
//function getSession($user) {
//   $path=sys_get_temp_dir().DIRECTORY_SEPARATOR.base64_encode($user).".wsgm";
//
//   $ret= "-1";
//   if (file_exists($path)){
//       $texto=file_get_contents($path);
//         if($texto !== false)  return base64_decode($texto);
//   }
//  return $ret;
//}
//
//function removeSession($user){
//  $path=sys_get_temp_dir().DIRECTORY_SEPARATOR.base64_encode($user).".wsgm";
//  if(file_exists($path)) {
//     return unlink($path);
//  } else
//     return false;
//}
?>
