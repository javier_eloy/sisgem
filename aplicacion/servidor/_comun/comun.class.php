<?php

/*
 * Lista de Funciones comunes para todos los modulos
 */

class comun {

   //put your code here
   private $MySQL = null;
   private $rs = null;

   //================= METODOS PRIVADOS ================
   public function __construct() {
      $this->MySQL = new conexion();
      $this->MySQL->ConnectMySQL();
   }

   public function __destruct() {
      $this->MySQL->CloseConnect();
   }

   //===============  METODOS PUBLICOS =================
   public function SelecLocalProcedimientos($co_plan) {
      $this->rs = $this->MySQL->ExecuteCacheSQL("SELECT PRP.CO_PLAN,
                                                       PRP.VA_PROCEDIMIENTO,
                                                       PRP.NB_TEXTO,
                                                       PRP.NU_POSICION,
                                                       PRP.NU_NODO,
                                                       PRP.CAPACIDAD,
                                                       PL.VA_HOJARUTA,
                                                       PL.TX_HOJARUTA                                                     
                                                   FROM I020T_PROCEDIMIENTO_PLAN PRP 
                                                     INNER JOIN  I037T_PLAN PL ON PRP.CO_PLAN = PL.CO_PLAN
                                                     WHERE  PRP.CO_PLAN = {$co_plan};");

      return $this->rs;
   }

   public function SelecListaEstadoOrden() {
      $this->rs = $this->MySQL->ExecuteCacheSQL("SELECT CO_ESTATUS_ORDEN,NB_CO_ESTATUS_ORDEN
                                                 FROM I025T_ESTATUS_ORDEN 
                                                 WHERE co_tipo_orden = '1'");
      return $this->rs;
   }

   public function SelectUbicDesc($objInventario, $va_hojaruta, $va_detalle, $va_plan, &$Desc, &$sUbic) {
      list($grupohr, $operacionhr) = explode("-", $va_hojaruta);
      if ($va_plan == "E") {
         $arrayDatos = $objInventario->SelecDetalleEqpSAP($va_detalle);
         $Desc = $arrayDatos[1]['EQKTU'];
         $sCodigo = $arrayDatos[1]['TPLNR'];
         $arrayDatos = $objInventario->SelecUbicacionTecnica($sCodigo);
         $sUbic = $arrayDatos[1]['TXT_TPLMA'];
      } else {
         $arrayDatos = $objInventario->SelecUbicacionTecnica($va_detalle);
         $sUbic = $arrayDatos[1]['TXT_TPLMA'];
         $Desc = $arrayDatos[1]['PLTXU'];
      }
   }

}

?>
