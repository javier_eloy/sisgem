<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of configuracion
 *
 * @author hernandezjnz
 */
class configuracion {

   private $MySQL = null;
   private $rs = null;
   private $esTmpGrpPto = false;

//================= METODOS PRIVADOS ================
   public function __construct() {
      $this->MySQL = new conexion();
      $this->MySQL->ConnectMySQL();
   }

   public function __destruct() {
      $this->MySQL->CloseConnect();
   }

//===============  METODOS PUBLICOS =================

   public function SelecRegiones() {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT CO_ZONA,NB_ZONA,TX_DESCRIPCION,
                COALESCE((SELECT 1 FROM I002T_LOCALIDAD WHERE I002T_LOCALIDAD.co_zona =I001T_ZONA.CO_ZONA LIMIT 1),0) +
                COALESCE((SELECT 1 FROM I003T_NEGOCIO WHERE I003T_NEGOCIO.co_zona =I001T_ZONA.CO_ZONA LIMIT 1),0)+
                COALESCE((SELECT 1 FROM I004T_USUARIO WHERE I004T_USUARIO.co_zona =I001T_ZONA.CO_ZONA LIMIT 1),0)+
                COALESCE((SELECT 1 FROM I017T_EQUIPO WHERE I017T_EQUIPO.co_zona =I001T_ZONA.CO_ZONA LIMIT 1),0) As hasRelacion
                FROM I001T_ZONA");
      return $this->rs;
   }

   public function SelecRegionesEdit($id) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT * from I001T_ZONA WHERE co_zona ='$id' LIMIT 1");
      return $this->rs;
   }

   public function SelecListaCargo($ubic_tec) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT CONCAT(CR.CO_CARGO_PADRE,CR.CO_CARGO) As Codigo,CR.*
                                              FROM I005T_CARGO As CR 
                                              WHERE CR.CO_UBIC_TECNICA LIKE '$ubic_tec%'
                                              ORDER BY Codigo");
      return $this->rs;
   }

   public function SelecListaCargoEditable($ubic_tecnica) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT CONCAT(COALESCE(CR.CO_CARGO_PADRE,''),CO_CARGO) As Codigo, CR.*,
                    (SELECT COUNT(ICR.NU_CARGO) FROM I005T_CARGO ICR WHERE ICR.CO_CARGO_PADRE = CONCAT(CR.CO_CARGO_PADRE,CR.CO_CARGO)) As esSup,
                    (SELECT COUNT(USR.CO_USUARIO) FROM I004T_USUARIO USR WHERE USR.NU_CARGO = CR.NU_CARGO) As esAsg
                     FROM I005T_CARGO As CR
                     WHERE CR.CO_UBIC_TECNICA LIKE '{$ubic_tecnica}%'");
      return $this->rs;
   }

   public function SelecRegistroCargo($nu_cargo) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT CONCAT(CR.CO_CARGO_PADRE,CR.CO_CARGO) As Codigo,
                                               CR.*,  
                                              COALESCE(CR_PADRE.CO_UBIC_TECNICA,'') As CO_UBIC_TECNICA_PADRE,
                                              COALESCE(CR_PADRE.NU_CARGO,'') As NU_CARGO_PADRE
                                              FROM I005T_CARGO As CR 
                                              LEFT JOIN I005T_CARGO As CR_PADRE ON CONCAT(CR_PADRE.CO_CARGO_PADRE,CR_PADRE.CO_CARGO) = CR.CO_CARGO_PADRE
                                               WHERE CR.NU_CARGO = $nu_cargo");
      return $this->rs;
   }

   /*
    * Revisa si existe el código del cargo en base de datos
    */

   public function EsCodigoRegistroCargo($co_cargo_estructura, $nu_cargo) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT * FROM I005T_CARGO
                                           WHERE CONCAT(CO_CARGO_PADRE,CO_CARGO) = '$co_cargo_estructura' AND NU_CARGO <> $nu_cargo");
      return !$this->rs->EOF;
   }

   /*
    * Revisa si existe el código de la nomenclatura en base de datos
    */

   public function EsCodigoRegistroNomen($co_estado_orden, $codigo) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT * FROM I025T_ESTATUS_ORDEN
                                           WHERE VA_ESTATUS_ORDEN = '$codigo' AND CO_ESTATUS_ORDEN <> $co_estado_orden");
      return !$this->rs->EOF;
   }

   /*
    * Revisa si existe el código de la clase en base de datos
    */

   public function EsCodigoRegistroClase($co_clase, $codigo) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT * FROM I009T_CLASE
                                           WHERE TX_SIGLAS_CLASE = '$codigo' AND CO_CLASE <> $co_clase");
      return !$this->rs->EOF;
   }

   /**
    *
    * @param type $nb_rol
    * @param type $co_rol
    */
   public function EsNombreRolRepetido($nb_rol, $co_rol) {

      $this->rs = $this->MySQL->ExecuteSQL("SELECT * FROM I003T_ROL
                                          WHERE CO_ROL <> $co_rol AND NB_ROL = '$nb_rol'");
      return !$this->rs->EOF;
   }

   /*
    * Inserta un cargo a la base de datos
    */

   public function InsRegistroCargo($nb_cargo, $tx_descripcion, $co_cargo, $co_cargo_padre, $co_ubic_tecnica, $in_requerido) {
      $resultado = $this->MySQL->ExecuteSQL("INSERT INTO I005T_CARGO(nb_cargo,tx_descripcion,co_cargo,co_cargo_padre,co_ubic_tecnica,in_requerido)
                                          VALUES('$nb_cargo','$tx_descripcion','$co_cargo','$co_cargo_padre','$co_ubic_tecnica',$in_requerido); ");
      return $resultado;
   }

   /*
    * Elimina un Cargo
    */

   public function DelRegistroCargo($nu_cargo) {
      $resultado = $this->MySQL->ExecuteSQL("DELETE FROM I005T_CARGO WHERE NU_CARGO = $nu_cargo");
      return $resultado;
   }

   /*    * ***************************************************************************************
    * Obtiene una Nomenclatura
    */

   public function SelecRegistroNomenclatura($co_estatus_orden) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT ST.*
                                             FROM I025T_ESTATUS_ORDEN As ST
                                             WHERE CO_ESTATUS_ORDEN = $co_estatus_orden");
      return $this->rs;
   }

   /*
    * Obtiene la lista de Nomenclaturas
    */

   public function SelecListaNomenclatura() {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT ST.*,
                                             (SELECT COUNT(ORD.CO_ORDEN) FROM I036T_ORDEN ORD WHERE ORD.CO_ESTATUS_ORDEN = ST.CO_ESTATUS_ORDEN) As esAsig
                                             FROM I025T_ESTATUS_ORDEN As ST");
      return $this->rs;
   }

   /*
    * Inserta una Nomenclatura a la base de datos
    */

   public function InsRegistroNomenclatura($va_estatus_orden, $nb_co_estatus_orden, $tx_descripcion, $co_tipo_orden) {
      $resultado = $this->MySQL->ExecuteSQL("INSERT INTO I025T_ESTATUS_ORDEN(VA_ESTATUS_ORDEN,NB_CO_ESTATUS_ORDEN,TX_DESCRIPCION,CO_TIPO_ORDEN)
                                               VALUES ('$va_estatus_orden','$nb_co_estatus_orden','$tx_descripcion',$co_tipo_orden);");
      return $resultado;
   }

   /*
    * Elimina un Nomenclatura
    */

   public function DelRegistroNomenclatura($co_estatus_orden) {
      $resultado = $this->MySQL->ExecuteSQL("DELETE FROM I025T_ESTATUS_ORDEN WHERE co_estatus_orden = $co_estatus_orden");
      return $resultado;
   }

   /*
    * Update un Nomenclatura
    */

   public function UpdRegistroNomenclatura($co_estatus_orden, $va_estatus_orden, $nb_co_estatus_orden, $tx_descripcion, $co_tipo_orden) {
      $resultado = $this->MySQL->ExecuteSQL("UPDATE I025T_ESTATUS_ORDEN
                                               SET VA_ESTATUS_ORDEN='$va_estatus_orden',
                                                   NB_CO_ESTATUS_ORDEN='$nb_co_estatus_orden',
                                                   TX_DESCRIPCION='$tx_descripcion',
                                                   CO_TIPO_ORDEN=$co_tipo_orden     
                                               WHERE CO_ESTATUS_ORDEN=$co_estatus_orden");
      return $resultado;
   }

   /*    * ***************************************************************************************
    * Obtiene un  motivo de cierre
    */

   public function SelecRegistroMotivoCierre($nu_motivo_cierre) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT *
                                              FROM J041T_MOTIVO_CIERRE J041T
                                              WHERE J041T.NU_MOTIVO_CIERRE = $nu_motivo_cierre");
      return $this->rs;
   }

   /*
    * Obtiene la lista de motivos de cierre
    */

   public function SelecListaMotivoCierre() {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT *
                                              FROM J041T_MOTIVO_CIERRE J041T
                                              ORDER BY J041T.IN_ACTIVO");
      return $this->rs;
   }

   /*
    * Revisa si existe el código del motivo de cierre en base de datos
    */

   public function EsCodigoRegistroMotivoCierre($co_motivo_cierre) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT *
                                              FROM J041T_MOTIVO_CIERRE J041T
                                              WHERE J041T.CO_MOTIVO_CIERRE = $co_motivo_cierre");
      return !$this->rs->EOF;
   }

   /*
    * Inserta un motivo de cierre a la base de datos
    */

   public function InsRegistroMotivoCierre($co_motivo_cierre, $tx_descripcion, $co_tipo, $in_activo) {
      $resultado = $this->MySQL->ExecuteSQL("INSERT INTO J041T_MOTIVO_CIERRE(CO_MOTIVO_CIERRE,
                                                                               TX_DESCRIPCION,
                                                                               CO_TIPO,
                                                                               IN_ACTIVO)
                                                                      VALUES ('$co_motivo_cierre',
                                                                              '$tx_descripcion',
                                                                              '$co_tipo',
                                                                               $in_activo);");
      return $resultado;
   }

   /*
    * Elimina un motivo de cierre
    */

   public function DelRegistroMotivoCierre($co_motivo_cierre) {
      $resultado = $this->MySQL->ExecuteSQL("DELETE FROM J041T_MOTIVO_CIERRE WHERE co_estatus_orden = $co_motivo_cierre");
      return $resultado;
   }

   /*
    * Update un motivo de cierre
    */

   public function UpdRegistroMotivoCierre($co_motivo_cierre, $tx_descripcion, $co_tipo, $in_activo) {
      $resultado = $this->MySQL->ExecuteSQL("UPDATE J041T_MOTIVO_CIERRE
                                               SET TX_DESCRIPCION='$tx_descripcion',
                                                   CO_TIPO='$co_tipo',
                                                   IN_ACTIVO='$in_activo'    
                                               WHERE CO_MOTIVO_CIERRE='$co_motivo_cierre'");
      return $resultado;
   }

   /*    * ********************************************************************** */
   /*
    * Obtiene una Clase
    */

   public function SelecRegistroClase($co_clase) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT CL.*
                                             FROM I009T_CLASE As CL
                                             WHERE CO_CLASE= $co_clase");
      return $this->rs;
   }

   /*
    * Obtiene la lista de Clase
    */

   public function SelecListaClase() {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT CL.*,
                                             (1) As esAsig
                                             FROM I009T_CLASE As CL ");
      return $this->rs;
   }

   /*
    * Inserta una Clase la base de datos
    */

   public function InsRegistroClase($tx_siglas_clase, $nb_clase, $tx_descripcion, $co_tipo_orden) {
      $resultado = $this->MySQL->ExecuteSQL("INSERT INTO I009T_CLASE(TX_SIGLAS_CLASE,NB_CLASE,TX_DESCRIPCION,CO_TIPO_CLASE)
                                               VALUES ('$tx_siglas_clase','$nb_clase','$tx_descripcion',$co_tipo_orden);");
      return $resultado;
   }

   /*
    * Elimina un Clase
    */

   public function DelRegistroClase($co_clase) {
      $resultado = $this->MySQL->ExecuteSQL("DELETE FROM I009T_CLASE WHERE co_clase = $co_clase");
      return $resultado;
   }

   /*
    * Update un Clase
    */

   public function UpdRegistroClase($co_clase, $tx_siglas_clase, $nb_clase, $tx_descripcion, $co_tipo_orden) {
      $resultado = $this->MySQL->ExecuteSQL("UPDATE I009T_CLASE
                                               SET TX_SIGLAS_CLASE='$tx_siglas_clase',
                                                   NB_CLASE='$nb_clase',
                                                   TX_DESCRIPCION='$tx_descripcion',
                                                   CO_TIPO_CLASE=$co_tipo_orden     
                                               WHERE CO_CLASE=$co_clase");
      return $resultado;
   }

   /*    * ***********************************************************************
    * Actualiza un Cargo en la base de datos
    */

   public function UpdRegistroCargo($nu_cargo, $nb_cargo, $tx_descripcion, $co_cargo, $co_cargo_padre, $codigo_original, $co_ubic_tecnica, $co_ubic_tecnica_original, $in_requerido) {

      $this->MySQL->BeginTrans();
      try {
         $codigo = $co_cargo_padre . $co_cargo;
         $this->MySQL->ExecuteSQL("UPDATE I005T_CARGO
                                          SET nb_cargo='$nb_cargo',tx_descripcion='$tx_descripcion',
                                              co_cargo='$co_cargo',co_cargo_padre='$co_cargo_padre',
                                              co_ubic_tecnica='$co_ubic_tecnica',
                                             in_requerido=$in_requerido
                                         WHERE nu_cargo=$nu_cargo");
// Actualiza el campo codigo padre que se modifico anteriormente y que son sus hijos
         $this->MySQL->ExecuteSQL("UPDATE I005T_CARGO
                                    SET co_cargo_padre='$codigo', 
                                        co_ubic_tecnica=IF(INSTR(co_ubic_tecnica,'$co_ubic_tecnica_original')=1,co_ubic_tecnica,'') 
                                    WHERE co_cargo_padre LIKE '$codigo_original%'");
      } catch (ADODB_Exception $e) {
         echo("ERROR ADODB:" . $e->getMessage());
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
         echo("ERROR Exception:" . $e->getMessage());
      }
      $hasError = $this->MySQL->hasFailed();
      $this->MySQL->CommitTrans();

      return !$hasError;
   }

   /*
    * Crea una Tabla Temporal para Grupos y Puestos de Trabajo
    */

   public function CrearTmpGrpPto($nu_cargo) {

      $rsl = $this->MySQL->ExecuteSQL("CREATE TEMPORARY TABLE IF NOT EXISTS tmp_grp_pto (
          co_grp_plan VARCHAR(30),
          co_grp_plan_desc VARCHAR(100) NOT NULL DEFAULT '',
          co_pto_trab VARCHAR(30),
          co_pto_trab_desc VARCHAR(100) NOT NULL DEFAULT '',
          in_select bit,
          nu_cargo int(11) NOT NULL DEFAULT $nu_cargo,
          es_ultimo bit
        ) ENGINE=MEMORY DEFAULT CHARSET=latin1;");
      $this->esTmpGrpPto = $rsl;
      return $rsl;
   }

   /*
    *   Inserta un elemento en la tabla temporal (ésta debe existir)
    */

   public function InsTmpGrpPto($nu_cargo, $co_grp_plan, $co_grp_plan_desc, $co_pto_trab, $co_pto_trab_desc, $es_ultimo) {
      if ($this->esTmpGrpPto) {
         $isSelect = 0;

         if ($co_pto_trab == "") {
            $rstmp = $this->MySQL->ExecuteSQL("SELECT  *
                                               FROM C002T_CARGO_GRUPO_PLAN 
                                               WHERE NU_CARGO=$nu_cargo AND CO_GRUPO_PLAN='$co_grp_plan' 
                                               LIMIT 1");
            $isSelect = ($rstmp->EOF) ? 0 : 1;
         } else {
            $rstmp = $this->MySQL->ExecuteSQL("SELECT  *
                                               FROM C003T_CARGO_PUESTO_TRAB
                                               WHERE NU_CARGO=$nu_cargo AND CO_GRUPO_PLAN='$co_grp_plan' AND CO_PUESTO_TRAB = '$co_pto_trab'
                                               LIMIT 1");
            $isSelect = ($rstmp->EOF) ? 0 : 1;
         }

         $rsl = $this->MySQL->ExecuteSQL("INSERT INTO tmp_grp_pto(co_grp_plan,
                                                         co_grp_plan_desc,
                                                         co_pto_trab,
                                                         co_pto_trab_desc,
                                                         in_select,
                                                         es_ultimo)
                                        VALUES('$co_grp_plan',
                                               '$co_grp_plan_desc', 
                                               '$co_pto_trab',
                                               '$co_pto_trab_desc',
                                                $isSelect,
                                               $es_ultimo);");
         return $rsl;
      } else
         return false;
   }

   /*
    *  Devuelve la tabla temporal utilizada para los grupos y puestos de trabajo
    */

   public function GetTmpGtpPto($nu_cargo) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT * FROM tmp_grp_pto");
      return $this->rs;
   }

   /*
    * Inserta un Grupo Planificador y Puestos de Trabajo para un cargo:
    *   $arrayGrpPto = Arreglo conformado con codigo del grupo planificador y Puesto de Trabajo
    *   $nu_cargo = Numero del Cargo en Base de Datos
    */

   public function InsCargoGrupoPuesto($nu_cargo, $arrayGrpPto) {

      $this->MySQL->BeginTrans();
      try {
         $this->MySQL->ExecuteSQL("DELETE FROM C002T_CARGO_GRUPO_PLAN WHERE nu_cargo = $nu_cargo");
         $this->MySQL->ExecuteSQL("DELETE FROM C003T_CARGO_PUESTO_TRAB WHERE nu_cargo = $nu_cargo");

         for ($i = 0; $i < count($arrayGrpPto); $i++) {
            $values = explode("|", $arrayGrpPto[$i]);
            if (count($values) == 1)
               $sql = "INSERT INTO C002T_CARGO_GRUPO_PLAN(nu_cargo, co_grupo_plan) VALUES($nu_cargo,'" . $values[0] . "')";
            else
               $sql = "INSERT INTO C003T_CARGO_PUESTO_TRAB(nu_cargo, co_grupo_plan, co_puesto_trab) VALUES($nu_cargo,'" . $values[0] . "','" . $values[1] . "')";

            $this->MySQL->ExecuteSQL($sql);
         }
      } catch (ADODB_Exception $e) {
         echo "ERROR ADODB:" . $e->getMessage();
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
         echo "ERROR Exception:" . $e->getMessage();
      }
      $hasError = $this->MySQL->hasFailed();
      $this->MySQL->CommitTrans();

      return !$hasError;
   }

   /*
    * Obtiene los datos de Grupos de Planificación y Puestos de Trabajo
    */

   public function SelecCargoGrupoPuesto($nu_cargo) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT NU_CARGO, CO_GRUPO_PLAN, '' As CO_PUESTO_TRAB
                                            FROM  C002T_CARGO_GRUPO_PLAN
                                            WHERE nu_cargo = $nu_cargo
                                            UNION ALL
                                            SELECT NU_CARGO, CO_GRUPO_PLAN, CO_PUESTO_TRAB 
                                            FROM C003T_CARGO_PUESTO_TRAB
                                            WHERE nu_cargo = $nu_cargo
                                            ORDER BY CO_GRUPO_PLAN,CO_PUESTO_TRAB");
      return $this->rs;
   }

   /*
    *
    */

   public function SelecRoles() {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT DISTINCT RL.CO_ROL, RL.CO_UBIC_TECNICA_ADMIN,RL.TX_DESCRIPCION,
                                                            RL.IN_ADMIN, RL.NB_ROL, COUNT(RUS.CO_USUARIO) As esAsig
                                           FROM I003T_ROL As RL
                                             LEFT JOIN C001T_ROL_USUARIO RUS ON RUS.CO_ROL = RL.CO_ROL
                                           GROUP BY RL.CO_ROL, RL.CO_UBIC_TECNICA_ADMIN,
                                                RL.IN_ADMIN, RL.NB_ROL");
      return $this->rs;
   }

   /*
    * Obtiene una Rol
    */

   public function SelecRegistroRol($co_rol) {
      $this->rs = $this->MySQL->ExecuteSQL("SELECT RL.CO_UBIC_TECNICA_ADMIN, RL.TX_DESCRIPCION,RL.IN_ADMIN,
                                                    RL.NB_ROL
                                             FROM I003T_ROL As RL
                                             WHERE CO_ROL=$co_rol");
      return $this->rs;
   }

   /**
    *
    * @param type $co_rol
    * @param type $in_admin
    * @return type
    */
   public function SelecPermisoRol($co_rol, $in_admin = 0) {
      $andWhere = "";
      if ($in_admin != 1) $andWhere = " AND IN_ADMINISTRADOR = 0";

      $this->rs = $this->MySQL->ExecuteSQL("SELECT DISTINCT PRM.CO_ROL, PRM.CO_BLOQUE,PRM.NB_BLOQUE, PRM.TX_DESCRIPCION, COALESCE(MRL.CO_ROL,0) As MARCADO
                                             FROM (SELECT RL.CO_ROL,BLK.CO_BLOQUE,BLK.NB_BLOQUE, BLK.TX_DESCRIPCION, BLK.IN_ADMINISTRADOR
                                                   FROM I024T_BLOQUE BLK
                                                   CROSS JOIN I003T_ROL RL)  As PRM
                                             LEFT JOIN  J035T_MENU_ROL As MRL ON (MRL.CO_ROL = PRM.CO_ROL AND MRL.CO_BLOQUE = PRM.CO_BLOQUE)
                                             WHERE PRM.CO_ROL = $co_rol" . $andWhere. " ORDER BY PRM.NB_BLOQUE");

      return $this->rs;
   }

   /**
    *
    * @param type $in_admin : 1 si desea agregar a los menu de administradores
    * @return type
    */
   function SelecPermiso($in_admin = 0) {
      $swhere = "";
      if ($in_admin != 1)
         $swhere = "WHERE IN_ADMINISTRADOR = 0";

      $this->rs = $this->MySQL->ExecuteCacheSQL("SELECT CO_BLOQUE,NB_BLOQUE,TX_DESCRIPCION
                                                      FROM I024T_BLOQUE " . $swhere. " ORDER BY NB_BLOQUE");

      return $this->rs;
   }

   /**
    *
    * @param type $co_rol
    * @param type $nb_rol
    * @param type $tx_descripcion
    * @param type $in_admin
    * @param type $lroles
    * @return type
    */
   public function UpdateRole($co_rol, $nb_rol, $tx_descripcion, $co_ubic_tec_admin, $in_admin, $lroles) {

      $this->MySQL->BeginTrans();
      try {
         /** Actualiza el nombre del rol * */
         $this->MySQL->ExecuteSQL("UPDATE I003T_ROL SET NB_ROL= '$nb_rol',
                                                        TX_DESCRIPCION = '$tx_descripcion',
                                                        IN_ADMIN=$in_admin,
                                                        CO_UBIC_TECNICA_ADMIN='$co_ubic_tec_admin'
                                   WHERE CO_ROL = $co_rol");

         /*          * Actualiza los permisos * */
         $this->MySQL->ExecuteSQL("DELETE FROM J035T_MENU_ROL WHERE co_rol= $co_rol");
         foreach ($lroles as $rol) {
            $sql = "INSERT INTO J035T_MENU_ROL (CO_ROL,CO_BLOQUE) VALUES($co_rol,$rol)";
            $this->MySQL->ExecuteSQL($sql);
         }
      } catch (ADODB_Exception $e) {
         echo "ERROR ADODB:" . $e->getMessage();
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
         echo "ERROR Exception:" . $e->getMessage();
      }
      $hasError = $this->MySQL->hasFailed();
      $this->MySQL->CommitTrans();

      return !$hasError;
   }

   /**
    *
    * @param type $nb_rol
    * @param type $tx_descripcion
    * @param type $in_admin
    * @param type $lroles
    * @return type
    */
   public function InsertRole($nb_rol, $tx_descripcion, $co_ubic_tec_admin,$in_admin,$lroles) {

      $this->MySQL->BeginTrans();
      try {
         /** Actualiza el nombre del rol * */
         $this->MySQL->ExecuteSQL("INSERT INTO I003T_ROL (NB_ROL,TX_DESCRIPCION,CO_UBIC_TECNICA_ADMIN,IN_ADMIN)
                                           VALUES('$nb_rol','$tx_descripcion','$co_ubic_tec_admin',$in_admin)");

         $co_rol = $this->MySQL->LastInsertId("I003T_ROL");
         /*          * Inserta los permisos * */
         foreach ($lroles as $rol) {
            $sql = "INSERT INTO J035T_MENU_ROL (CO_ROL,CO_BLOQUE) VALUES($co_rol,$rol)";
            $this->MySQL->ExecuteSQL($sql);
         }
      } catch (ADODB_Exception $e) {
         echo "ERROR ADODB:" . $e->getMessage();
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
         echo "ERROR Exception:" . $e->getMessage();
      }
      $hasError = $this->MySQL->hasFailed();
      $this->MySQL->CommitTrans();

      return !$hasError;
   }

   /**
    *
    * @param type $co_rol
    * @return type
    */
   public function DelRole($co_rol) {
      $this->MySQL->BeginTrans();
      try {
         /** Elimina los menus del rol * */
         if (!$this->MySQL->ExecuteSQL("DELETE FROM J035T_MENU_ROL WHERE CO_ROL = $co_rol"))
            new Exception("No se puede eliminar los menus del rol");

         /** Elimina el rol **/
         if (!$this->MySQL->ExecuteSQL("DELETE FROM I003T_ROL WHERE CO_ROL = $co_rol"))
            new Exception("El rol no se puede eliminar");
         
      } catch (ADODB_Exception $e) {
         echo "ERROR ADODB:" . $e->getMessage();
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
         echo "ERROR Exception:" . $e->getMessage();
      }
      $hasError = $this->MySQL->hasFailed();
      $this->MySQL->CommitTrans();

      return !$hasError;
   }
   /**
    *
    * @param type $co_rol
    * @return type
    */
   public function MostrarRolUsuario($co_rol){
      $this->rs = $this->MySQL->ExecuteSQL("SELECT USU.NB_INDICADOR, USU.NB_NOMBRE
                     FROM C001T_ROL_USUARIO RUS
                     INNER JOIN I004T_USUARIO USU ON RUS.CO_USUARIO = USU.CO_USUARIO
                     WHERE RUS.CO_ROL = $co_rol ");
      return $this->rs;
      
   }
}


