<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registro_personal
 *
 * @author portilloga
 */
class registro_personal {

   private $MySQL = null;
   private $rs = null;

   //================= METODOS PRIVADOS ================
   public function __construct() {
      $this->MySQL = new conexion();
      $this->MySQL->ConnectMySQL();
   }

   public function __destruct() {
      $this->MySQL->CloseConnect();
   }

   /*
    * 
    */

   private function ejecutaTransaccion($sentencia) {
      $rsl = true;
      $this->MySQL->BeginTrans();
      try {
         $this->MySQL->ExecuteSQL($sentencia);
      } catch (ADODB_Exception $e) {
         $rsl = false;
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
         $rsl = false;
      }
      $this->MySQL->CommitTrans();
      return $rsl;
   }

   //===============  METODOS PUBLICOS =================

   /*
    * 0 NO EXISTE
    * 1 EXISTE
    */
   public function usuarioExiste($coUsuario) {
      $sql = "SELECT count(CO_USUARIO) as cant FROM I004T_USUARIO WHERE NB_INDICADOR = '" . $coUsuario . "'";
//        try 
//        {                
      $this->rs = $this->MySQL->ExecuteSQL($sql);
      return $this->rs->fields['cant'];

//        }
//       catch(Exception $e)
//       { return 'ERROR '.$e;}
   }

   /*
    *
    */

   public function selectCargos($ubic_tec = '') {
      $sql = "SELECT NU_CARGO,NB_CARGO,CO_CARGO FROM I005T_CARGO
                WHERE CO_UBIC_TECNICA LIKE '{$ubic_tec}%'";
      return $this->MySQL->ExecuteSQL($sql);
   }

   /*
    *
    */

   public function listaUsuarios($ubic_tec, $bMostrarAdmin = false) {

      if (!$bMostrarAdmin)
         $sWhere = "WHERE (I004T.IN_ADMIN <> 1) AND (I005T.CO_UBIC_TECNICA LIKE '{$ubic_tec}%')";
      else
         $sWhere = "WHERE I005T.CO_UBIC_TECNICA LIKE '{$ubic_tec}%'";

      $sql = "SELECT I004T.CO_USUARIO,
                       I004T.NB_INDICADOR, 
                       I004T.NU_CEDULA,
                       I004T.NB_NOMBRE,
                       I004T.NU_TELEFONO,
                       I005T.NB_CARGO, 
                       COUNT(J034T.CO_MENU_USER) As TOT_MENU, 
                       COUNT(I023T.CO_ACTIVIDAD_FALLA)+COUNT(I022T.CO_ACTIVIDAD) As TOT_ACTIVIDAD                       
                FROM I004T_USUARIO I004T
                    INNER JOIN I005T_CARGO I005T ON (I004T.NU_CARGO = I005T.NU_CARGO)
                    LEFT JOIN J034T_MENU_USUARIO J034T ON (I004T.CO_USUARIO = J034T.CO_USUARIO)
                    LEFT JOIN I022T_ACTIVIDAD I022T ON (I004T.CO_USUARIO = I022T.CO_USUARIO)
                    LEFT JOIN I023T_ACTIVIDAD_FALLA I023T ON (I004T.CO_USUARIO = I023T.CO_USUARIO)                
                  {$sWhere}   
                GROUP BY I004T.CO_USUARIO";
      return $this->MySQL->ExecuteSQL($sql);
   }

   /*
    * Inserta un usuario
    */

   public function insertUsuario($indicador, $nombre, $cedula, $direccion, $telefono, $cargo, $planifica, $nu_cargo_planificador) {


      $passmy = md5('123456'); //CLAVE TEMPORAL, ESTA NO SE INCLUIRÁ EN TABLA YA QUE SE AUTENTICARÁ CON DIRECTORIO ACTIVO
      $sql = "INSERT INTO  I004T_USUARIO(NB_INDICADOR,
                                           TX_CLAVE,
                                           NB_NOMBRE,
                                           NU_CEDULA,
                                           TX_DIRECCION,
                                           NU_TELEFONO,
                                           NU_CARGO,
                                           IN_PLANIFICADOR,
                                           NU_CARGO_PLANIFICADOR) 
                                VALUES  ('$indicador',
                                         '$passmy',
                                         '$nombre',
                                         '$cedula',
                                         '$direccion',
                                         '$telefono',
                                         '$cargo',
                                          $planifica,
                                         '$nu_cargo_planificador')";
      return $this->ejecutaTransaccion($sql);
   }

   /*
    *
    */

   public function deleteUsuario($coUsuario) {
      $sql = "DELETE FROM I004T_USUARIO WHERE CO_USUARIO='" . $coUsuario . "'";
      return $this->ejecutaTransaccion($sql);
   }

   /*
    *
    */

   public function selectUsuario($coUsuario) {
      $sql = "SELECT I004T.NB_INDICADOR,
                      I004T.NU_CEDULA,
                      I004T.NB_NOMBRE,
                      I004T.NU_TELEFONO,
                      I004T.TX_DIRECCION,
                      I004T.NU_CARGO,
                      I004T.IN_PLANIFICADOR,
                      I004T.NU_CARGO_PLANIFICADOR
               FROM I004T_USUARIO I004T
               INNER  JOIN I005T_CARGO I005T  ON I004T.NU_CARGO = I005T.NU_CARGO
               WHERE I004T.CO_USUARIO = $coUsuario";
      return $this->MySQL->ExecuteSQL($sql);
   }

   /*
    *
    */

   public function editUsuario($coUsuario, $indicador, $nombre, $cedula, $direccion, $telefono, $cargo, $planifica, $nu_cargo_planficador) {


      $sql = "UPDATE I004T_USUARIO SET NB_INDICADOR    = '$indicador',
                                         NU_CEDULA       = '$cedula',
                                         NB_NOMBRE       = '$nombre',
                                         NU_TELEFONO     = '$telefono',
                                         TX_DIRECCION    = '$direccion',
                                         NU_CARGO        = '$cargo',                      
                                         IN_PLANIFICADOR = '$planifica',
                                         NU_CARGO_PLANIFICADOR = '$nu_cargo_planficador'
                WHERE CO_USUARIO = '$coUsuario'";
      return $this->ejecutaTransaccion($sql);
   }

   /*
    *
    *
    */

   public function listaMenuUsuario($coUsuario) {

      $sql = "SELECT  I004.CO_USUARIO,
                        I004.NB_NOMBRE,
                        I004.NB_INDICADOR, 
                        I024.CO_BLOQUE AS ing_idBloque, 
                        I024.NB_BLOQUE AS chg_nombreBloque, 
                        I024.TX_DESCRIPCION AS chg_descriBloque,
                        J034.CO_BLOQUE AS ing_codigoBloque
                FROM I004T_USUARIO I004
                CROSS JOIN I024T_BLOQUE I024
                LEFT JOIN J034T_MENU_USUARIO J034 ON (I024.CO_BLOQUE=J034.CO_BLOQUE 
                AND I004.CO_USUARIO = J034.CO_USUARIO AND J034.NU_POS_MENU_USER='0')
                WHERE I004.CO_USUARIO = '" . $coUsuario . "'
                ORDER BY I024.CO_BLOQUE";
      return $this->MySQL->ExecuteSQL($sql);
   }

   /**
    *
    * @param type $coUsuario
    * @return type
    */
   public function listaRolesUsuario($coUsuario,$in_admin,$ubic_admin) {

      if($in_admin == "1") $sWhere = " AND ((RL.IN_ADMIN = '0') OR (RL.IN_ADMIN = '1' AND RL.CO_UBIC_TECNICA_ADMIN LIKE '$ubic_admin%'))";
                      else $sWhere = " AND RL.IN_ADMIN = '0'";

      $sql = "SELECT  DISTINCT US.CO_USUARIO, RL.CO_ROL,RL.NB_ROL, RL.TX_DESCRIPCION, RL.IN_ADMIN, COALESCE(RUS.CO_USUARIO,0) As ES_MARCADO
               FROM I004T_USUARIO US
               CROSS JOIN I003T_ROL RL
               LEFT JOIN C001T_ROL_USUARIO RUS ON (RL.CO_ROL  = RUS.CO_ROL  and US.CO_USUARIO = RUS.CO_USUARIO)
               WHERE US.CO_USUARIO = $coUsuario ";

      $sOrderBy=" ORDER BY RL.NB_ROL";

      return $this->MySQL->ExecuteSQL($sql.$sWhere.$sOrderBy);
   }

   /**
    *
    * @param type $coUsuario
    * @param type $accesos
    * @return boolean
    */
   public function modifAccesoUsuario($coUsuario, $accesos) {
//                CODIGO ORIGINAL
      ///FOREACH HACE UN BARRIDO POR TODOS LOS BLOQUES SELECCIONADOS
//        foreach($accesos as $x)       
//            $condicion[] = "J034T.CO_BLOQUE!=".$x;
//       
//        // CONCATENO LA CONSULTA CON TODOS LOS BLOQUES SELECCIONADOS
//        $tCond = ' AND ('.implode(" AND ",$condicion).')';    
//
//
//        // ELIMINO LOS BLOQUES NO SELECCIONADOS O DESELECCIONADOS EN LA EDICIÓN
//        $delete ="DELETE FROM J034T
//                  USING J034T_MENU_USUARIO J034T
//                  INNER JOIN J034T_MENU_USUARIO J034T_2 ON (J034T.CO_BLOQUE = J034T_2.CO_BLOQUE 
//                                                          AND J034T_2.CO_USUARIO='".$coUsuario."' 
//                                                          AND J034T_2.NU_POS_MENU_USER='0' ".$tCond.") 
//                  WHERE J034T.CO_USUARIO='".$coUsuario."' 
//                  AND   J034T.NU_POS_MENU_USER = '0'";
      $bres = false;
      $delete = "DELETE FROM J034T_MENU_USUARIO
                   WHERE CO_USUARIO='" . $coUsuario . "' 
                   AND   NU_POS_MENU_USER = '0'";
      $this->MySQL->BeginTrans();
      try {
         $this->MySQL->ExecuteSQL($delete);
         ///FOREACH INSERTA EL C�DIGO DE BLOQUE POR USUARIO
         foreach ($accesos as $key => $valor) {
            $insert = "INSERT INTO J034T_MENU_USUARIO(CO_USUARIO,
                                                            CO_BLOQUE,
                                                            NU_POS_MENU_USER)
                                                    VALUES($coUsuario,
                                                            '$valor',
                                                            0)";
            $res = (boolean) $this->MySQL->ExecuteSQL($insert);
         }
         $bres = true;
      } catch (ADODB_Exception $e) {
         $bres = false;
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
         $bres = false;
      }
      $this->MySQL->CommitTrans();
      return $bres;
   }

   /**
    * Actualiza los roles asignados al usuario
    * @param type $coUsuario
    * @param type $accesos
    * @return boolean
    */
   public function actualizaRolesUsuario($coUsuario, $role_id) {
      $bres = false;
      $delete = "DELETE FROM C001T_ROL_USUARIO
                 WHERE CO_USUARIO='$coUsuario'";

      $this->MySQL->BeginTrans();
      try {
         $res = (boolean) $this->MySQL->ExecuteSQL($delete);
         if ($res)
            foreach ($role_id as $key => $valor) {
               $insert = "INSERT INTO C001T_ROL_USUARIO(CO_USUARIO,CO_ROL)
                                                    VALUES($coUsuario,$valor)";
               $res = (boolean) $this->MySQL->ExecuteSQL($insert);
            }
         $bres = $res;
      } catch (ADODB_Exception $e) {
         $bres = false;
      } catch (Exception $e) {
         $this->MySQL->RollbackTrans();
         $bres = false;
      }
      $this->MySQL->CommitTrans();
      return $bres;
   }

}

?>
