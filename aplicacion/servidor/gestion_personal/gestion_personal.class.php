<?php

/**
 * Description of gestion_personal
 *
 * @author hernandezjnz
 */
class gestion_personal {

    private $MySQL = null;
    private $rs = null;

    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQL->ConnectMySQL();
    }

    public function __destruct() {
        $this->MySQL->CloseConnect();
    }

    //===============  METODOS PUBLICOS =================
    /*
     * Elimina la suplencia 
     */
    public function EliminarPersonalDisponibilidad($co_suplencia) {
        $FechaActual = new DateTime("NOW");
        $bres = false;
        $this->MySQL->BeginTrans();
        try {
            //* Actualiza el estado a disponible 
            $this->MySQL->ExecuteSQL("UPDATE I004T_USUARIO 
                                      SET IN_DISPONIBLE='0'
                                      WHERE CO_USUARIO=
                                            (SELECT CO_USUARIO_AUSENTE 
                                               FROM I042T_SUPLENCIA 
                                               WHERE CO_SUPLENCIA={$co_suplencia}
                                                   AND STR_TO_DATE('{$FechaActual->format("d/m/Y")}','%d/%m/%Y') BETWEEN FE_INICIO AND FE_FIN)");

            //* Se elimina la suplencia
            $this->MySQL->ExecuteSQL("DELETE FROM I042T_SUPLENCIA 
                                       WHERE CO_SUPLENCIA='$co_suplencia'");

            $bres = true;
        } catch (ADODB_Exception $e) {
            $this->MySQL->RollbackTrans();
        } catch (Exception $e) {
            $this->MySQL->RollbackTrans();
        }
        $this->MySQL->CommitTrans();
        return $bres;
    }

    /*
     * 
     */

    public function UpdatePersonalDisponibilidad($id, $cod_suplente, $fechaInicio_c, $fecha_actual_c, $fechaFinal_c, $cod_ausente, $descripcion, $cod_clases) {

        $this->MySQL->BeginTrans();
        try {
            $row = $this->MySQL->ExecuteRow("SELECT CO_SUPLENCIA 
                            FROM I042T_SUPLENCIA 
                            WHERE CO_USUARIO_SUPLENTE='$cod_suplente'
                            AND CO_SUPLENCIA='$id'");

            // VERIFICA SI LA FECHA ACTUAL EST� DENTRO DEL PER�ODO DEL REGISTRO DE LA AUSENCIA: 
            if ($fecha_actual_c >= $fechaInicio_c AND $fecha_actual_c <= $fechaFinal_c) {
                if ($row['CO_SUPLENCIA'] == "") {
                    $sql = "DELETE FROM b USING I042T_SUPLENCIA s
                INNER JOIN J034T_MENU_USUARIO b ON (s.CO_USUARIO_SUPLENTE  = b.CO_USUARIO AND b.NU_POS_MENU_USER ='1') 
                WHERE s.CO_SUPLENCIA='$id'";
                    // INSERTO TODOS LOS BLOQUES QUE ACCESAR� EL REEMPLAZO
                    $sql.="; INSERT INTO J034T_MENU_USUARIO (CO_BLOQUE,CO_USUARIO,NU_POS_MENU_USER)
                        SELECT td2.CO_BLOQUE,'$cod_suplente','1' FROM J034T_MENU_USUARIO td2 
                        WHERE td2.CO_USUARIO='$cod_ausente'";
                }
                // SE EDITA EL ESTADO DE DISPONIBILIDAD DEL USUARIO QUE SE AUSENTARA:        
                $sql.="; UPDATE I004T_USUARIO SET IN_DISPONIBLE='1' 
                    WHERE CO_USUARIO='$cod_ausente'";
            } elseif ($fecha_actual_c < $fechaInicio_c AND $fecha_actual_c > $fechaFinal_c) {
                $sql = "DELETE FROM b USING I042T_SUPLENCIA s
                 INNER JOIN J034T_MENU_USUARIO b ON (s.CO_USUARIO_SUPLENTE  = b.CO_USUARIO AND b.NU_POS_MENU_USER='1') 
                 WHERE s.CO_SUPLENCIA='$id'";
            }

            // SE EDITA LA SUPLENCIA Y LA AUSENCIA
            $sql.="UPDATE 042T_SUPLENCIA 
            SET CO_USUARIO_AUSENTE='$cod_ausente',TX_ DESCRIPCION='$descripcion',
                CO_USUARIO_SUPLENTE='$cod_suplente',CO_CLASE='$cod_clases',CO_TIPO ='1',
                FE_INICIO='$fechaInicio_c',FE_FIN='$fechaFinal_c' 
                WHERE CO_SUPLENCIA='$id'";

            $this->MySQL->ExecuteSQL($sql);
        } catch (ADODB_Exception $e) {
            return false;
        } catch (Exception $e) {
            $this->MySQL->RollbackTrans();
            return false;
        }
        $this->MySQL->CommitTrans();
        return true;
    }

    /*
     * 
     */

    public function InsertarPersonalDisponibilidad($cod_ausente, $descripcion, $cod_suplente, $cod_clases, _
    $fechaInicio_c, $fechaFinal_c) {

        $this->MySQL->BeginTrans();
        try {
            $sql = "INSERT INTO I042T_SUPLENCIA (CO_USUARIO_AUSENTE,TX_ DESCRIPCION,CO_USUARIO_SUPLENTE,CO_CLASE,CO_TIPO,FE_INICIO,FE_FIN) 
                 VALUES ('$cod_ausente','$descripcion','$cod_suplente','$cod_clases',
                         '1','$fechaInicio_c','$fechaFinal_c')";

            // VERIFICA SI LA FECHA ACTUAL EST� DENTRO DEL PER�ODO DEL REGISTRO DE LA AUSENCIA: 
            if ($fecha_actual_c >= $fechaInicio_c AND $fecha_actual_c <= $fechaFinal_c) {
                // SE EDITA EL ESTADO DE DISPONIBILIDAD DEL USUARIO QUE SE AUSENTARA:
                $sql.="; UPDATE I004T_USUARIO SET IN_DISPONIBLE='1' WHERE CO_USUARIO='$cod_ausente'";
                // INSERTO TODOS LOS 
                $sql.="; INSERT INTO J034T_MENU_USUARIO (CO_BLOQUE,CO_USUARIO,NU_POS_MENU_USER)
                  SELECT td2.CO_BLOQUE,'$cod_suplente','1' FROM J034T_MENU_USUARIO td2 
                  WHERE td2.CO_USUARIO='$cod_ausente'";
            }
            $this->MySQL->ExecuteSQL($sql);
        } catch (ADODB_Exception $e) {
            return false;
        } catch (Exception $e) {
            $this->MySQL->RollbackTrans();
            return false;
        }
        $this->MySQL->CommitTrans();
        return true;
    }

    /*
     * 
     */

    public function SelecPersonalDisponible() {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT i004t.nu_cedula,
                                    i004t.nb_nombre,
                                    i004t.nu_telefono,
                                    i005t.nb_cargo,
                                    i010t.nb_departamento
                          from I004T_USUARIO i004t,
                                  I005T_CARGO i005t,
                                  I010T_DEPARTAMENTO i010t,
                                  I030T_DISPONIBILIDAD_PERSONAL i030t  
                          WHERE i004t.co_cargo= i005t.co_cargo
                          AND   i004t.co_departamento= i010t.co_departamento 
                          AND   i004t.co_usuario= i030t.co_usuario
                          AND   i030t.in_disponible= '1'	
                          GROUP BY i004t.nu_cedula ");
        return $this->rs;
    }

    /*
     * Utilizado para ejecutar el grafico del personal
     */

    public function SelecGrafPersonal() {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT SUM(CASE in_disponible WHEN '1' THEN 1 ELSE 0 END ) As NoDisp,
                                            SUM(CASE in_disponible WHEN '0' THEN 1 ELSE 0 END) As Disp
                                            FROM I004T_USUARIO");
        return $this->rs;
    }

    /*
     * Utilizado para ejecutar el grafico del personal
     */

    public function SelecGrafPersonalVaca($year) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT MONTH(PERIODOS.STARTDATE) As MesReporte,
                                                     COUNT(SP.CO_USUARIO_AUSENTE) As TotalReporte
                                              FROM(
                                                   SELECT ('$year-01-01')+INTERVAL mes MONTH as STARTDATE,
                                                   LAST_DAY(('$year-01-01')+INTERVAL mes MONTH) As ENDDATE
                                                   FROM (select meses.mes from
                                                            (select 0 As mes union select 1 union select 2 union select 3 union 
                                                             select 4 union select 5 union select 6 union select 6 union 
                                                              select 7 union select 8 union select 9 union select 10 union select 11) meses) d1
                                                ) PERIODOS 
                                             LEFT JOIN I042T_SUPLENCIA As SP ON PERIODOS.STARTDATE BETWEEN SP.FE_INICIO AND SP.FE_FIN
                                                        OR PERIODOS.ENDDATE BETWEEN SP.FE_INICIO AND SP.FE_FIN
                                             GROUP BY MONTH(PERIODOS.STARTDATE)
                                             ORDER BY STARTDATE");
        return $this->rs;
    }

    /*
     *  Busqueda de usuarios filtrando por codigo de cargo, usado en Registro de Ausencia
     *  Usado en la busqueda....
     */

    public function SelecConsultaPersonal($busqueda, $co_cargo) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT USU.CO_USUARIO ,USU.NB_INDICADOR,USU.NB_NOMBRE,USU.IN_DISPONIBLE,
                                                     CR.NB_CARGO 
                                             FROM I004T_USUARIO As USU
                                             INNER JOIN I005T_CARGO As CR ON CR.NU_CARGO= USU.NU_CARGO
                                             WHERE (CAST(USU.NU_CEDULA AS CHAR)='$busqueda' OR USU.NB_INDICADOR='$busqueda')
                                                        AND CONCAT(CR.CO_CARGO_PADRE,CR.CO_CARGO) LIKE '$co_cargo%'");
        return $this->rs;
    }

    /*
     *  Lista de casos de ausencia
     */

    public function SelecClase() {
        $this->rs = $this->MySQL->ExecuteCacheSQL("SELECT CO_CLASE,NB_CLASE,TX_DESCRIPCION
                                                  FROM I009T_CLASE WHERE CO_TIPO_CLASE='3'");
        return $this->rs;
    }

    /*
     * Obtiene la lista de personal
     */

    public function SelecPersonal($co_cargo) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT SPL.CO_SUPLENCIA, SPL.FE_INICIO, SPL.FE_FIN, SPL.TX_DESCRIPCION,
                                             USU.CO_USUARIO AS CO_USUARIO_AUSENTE,USU.NB_INDICADOR AS NB_INDICADOR_AUSENTE,        
                                             US_SUP.CO_USUARIO AS CO_USUARIO_SUPLENTE, COALESCE(US_SUP.NB_INDICADOR,'SIN REMPLAZO') AS NB_INDICADOR_SUPLENTE,            
                                             CLA.TX_SIGLAS_CLASE, CLA.NB_CLASE
                                            FROM I042T_SUPLENCIA SPL
                                            INNER JOIN I004T_USUARIO USU ON (SPL.CO_USUARIO_AUSENTE = USU.CO_USUARIO)
                                            INNER JOIN I005T_CARGO CR ON (CR.NU_CARGO = USU.NU_CARGO)
                                            INNER JOIN I009T_CLASE CLA ON (CLA.CO_CLASE = SPL.CO_CLASE)
                                            LEFT OUTER JOIN I004T_USUARIO US_SUP  ON (SPL.CO_USUARIO_SUPLENTE = US_SUP.CO_USUARIO)
                                            WHERE CONCAT(CR.CO_CARGO_PADRE,CR.CO_CARGO) LIKE '$co_cargo%'");
        return $this->rs;
    }

    /**
     * Obtiene los datos de una persona para registrar la ausencia
     * @param $co_usuario
     * @return recordset
     */
    public function SelecDatosPersonal($co_usuario) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT  USU.CO_USUARIO AS CO_USUARIO_AUSENTE,
                                                        USU.NB_INDICADOR AS NB_INDICADOR_AUSENTE,   
                                                        USU.NB_NOMBRE AS NB_NOMBRE_AUSENTE,
                                                        CR.NB_CARGO,
                                                        CR.IN_REQUERIDO
                                            FROM I004T_USUARIO USU 
                                            INNER JOIN I005T_CARGO As CR ON (CR.NU_CARGO=USU.NU_CARGO)
                                            WHERE USU.CO_USUARIO=$co_usuario");
        return $this->rs;
    }

    /**
     * Obtiene la lista de fechas no disponibles
     */
     public function SelecNoDisponible($co_usuario) {
          $y=date("Y");
          $years = ($y-1).",".$y.",".($y+1);
          $this->rs = $this->MySQL->ExecuteSQL("SELECT SPL.FE_INICIO,
                                                         SPL.FE_FIN
                                           FROM I042T_SUPLENCIA SPL
                                           WHERE SPL.CO_USUARIO_AUSENTE=$co_usuario
                                                 AND (YEAR(SPL.FE_INICIO) IN ($years)
                                                     OR YEAR(SPL.FE_FIN) IN ($years))");
        return $this->rs;
     }

}

?>
