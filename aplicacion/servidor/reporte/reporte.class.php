<?php

class reporte {

    private $MySQL = null;
    private $rs = null;

    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQL->ConnectMySQL();
    }

    public function __destruct() {
        $this->MySQL->CloseConnect();
    }
    
    /*
     * Obtiene La proxima fecha y numero de semana de mantenimiento  de los planes dado un año especifico
     */
    public function SelecPlanes($ano) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT p1.nu_semana_proxima, 
                                                p1.fe_proxima_plan AS Proximo1
                                                FROM I037T_PLAN p1  
                                                WHERE p1.nu_semana_proxima LIKE '%".$ano."'
                                                ORDER by p1.fe_proxima_plan");
        return $this->rs;
    }
    
    /*
     * Obtiene el numero de semana programado y el codigo de actividad  de los planes dado un año especifico
     */
    public function SelecProgramas($ano) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT p1.nu_semana_proxima, 
                                                a1.co_actividad
                                                FROM  I022T_ACTIVIDAD a1                                                
                                                INNER JOIN I037T_PLAN p1 ON (p1.co_plan = a1.co_plan)        
                                                WHERE p1.nu_semana_proxima LIKE '%".$ano."'
                                                ORDER BY a1.co_actividad");
        return $this->rs;
    }
    
    /*
     * Obtiene datos basicos de planes asociados a un numero de semana especifico
     */
    public function SelecPlan($semana) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT  p1.co_plan AS CoPlan, p1.va_detalle AS Equipo, 
                                                p1.co_ubic_tec AS Ubicacion, p1.fe_proxima_plan AS Proximo1,
                                                p1.tx_hojaruta AS Actividad, p1.va_tipo_plan AS TipoPlan,
                                                I036T.FE_ASIGNACION AS PROG, I022T.FE_ASIGNACION AS ASIG
                                                FROM I037T_PLAN p1
                                                LEFT OUTER JOIN I022T_ACTIVIDAD I022T                                                  
                                                  ON p1.CO_PLAN = I022T.CO_PLAN		
                                                LEFT OUTER JOIN I036T_ORDEN I036T
						  ON I022T.CO_ORDEN = I036T.CO_ORDEN
                                                WHERE p1.nu_semana_proxima = '".$semana."'
                                                GROUP BY p1.co_plan 
                                                ORDER BY p1.fe_proxima_plan DESC, I022T.FE_ASIGNACION DESC");
        return $this->rs;
    }
    
    /*
     * Obtiene los datos basicos asociados a la actividad y su responsable dado un codigo deplan especifico
     */
    public function SelecActividad($plan) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT  a1.co_actividad, a1.fe_asignacion AS Fecha,
                                                u1.nb_indicador AS Responsable, u1.nb_nombre AS Nombre
                                                FROM I022T_ACTIVIDAD a1
                                                INNER JOIN I004T_USUARIO u1 ON (u1.co_usuario = a1.co_usuario)
                                                WHERE a1.co_plan = ".$plan."
                                                ORDER BY a1.co_actividad");
        return $this->rs;
    }
    
    /*
     * Obtiene la descripción de un procedimiento dado un codigo deplan especifico
     */
    public function SelecProcedimiento($plan) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT  p1.nb_texto AS Actividad
                                           FROM I020T_PROCEDIMIENTO_PLAN p1
                                           WHERE p1.co_plan = ".$plan."
                                           ORDER BY p1.nu_posicion");
        return $this->rs;
    }
    
    /*
     * Obtiene los datos historicos de un equipo/estación/instalacion especifico a partir de un codigo dado
     */
    public function SelecHistEquipoInstalacion($coObjTecnico){      
       $this->rs = $this->MySQL->ExecuteSQL('SELECT I037T.CO_PLAN, I036T.NU_ORDEN, 
                                                    I037T.TX_HOJARUTA,
                                                    DATE_FORMAT(I037T.FE_PROXIMA_PLAN,"%d-%m-%Y") AS FE_PLAN,
                                                    DATE_FORMAT(I036T.FE_ASIGNACION,"%d-%m-%Y") AS FE_PROG,
                                                    DATE_FORMAT(I022T.FE_ASIGNACION,"%d-%m-%Y") AS FE_ASIG,
                                                    DATE_FORMAT(I022T.FE_INICIO,"%d-%m-%Y") AS FE_INICIO,
                                                    DATE_FORMAT(I022T.FE_FIN, "%d-%m-%Y") AS FE_FIN,
                                                    I022T.TX_OBSERVACION                                                    
                                             FROM   I037T_PLAN I037T,
                                                    I022T_ACTIVIDAD I022T,
                                                    I036T_ORDEN I036T	  
                                             where  I037T.VA_DETALLE = \''.$coObjTecnico.'\'
                                             and    I022T.CO_PLAN = I037T.CO_PLAN
                                             AND    I022T.CO_ORDEN = I036T.CO_ORDEN
                                             and    I022T.FE_FIN  IS NOT NULL
                                             and    DATE_FORMAT(I022T.FE_FIN, "%d-%m-%Y") <> "00-00-0000"
                                             group by I037T.CO_PLAN
                                             ORDER BY I022T.FE_INICIO ASC');
        return $this->rs;
    }
    
     /*
     * Obtiene los datos del personal supervisado por un cargo dado
     */
    public function SelecPersonCargo($coCargoPadre){      
       $this->rs = $this->MySQL->ExecuteSQL('SELECT   I004.CO_USUARIO, 
                                                      I004.NB_INDICADOR,
                                                      I004.NB_NOMBRE As NB_USUARIO, 
                                                      I004.NU_CEDULA,
                                                      I005.NB_CARGO
                                             FROM     I004T_USUARIO I004,
                                                           I005T_CARGO I005  	
                                             WHERE I004.NU_CARGO = I005.NU_CARGO 
                                             AND (I005.CO_CARGO_PADRE = \''.$coCargoPadre.'\'
                                                 OR I004.CO_USUARIO = '.$_SESSION['usuario_id'].')
                                             ORDER BY I004.NB_NOMBRE');
//       if($this->rs->NumRows() == 0){
//           $this->rs = $this->MySQL->ExecuteSQL('SELECT   I004.CO_USUARIO, 
//                                                      I004.NB_INDICADOR,
//                                                      I004.NB_NOMBRE As NB_USUARIO, 
//                                                      I004.NU_CEDULA,
//                                                      I005.NB_CARGO
//                                             FROM     I004T_USUARIO I004,
//                                                           I005T_CARGO I005  	
//                                             WHERE I004.NU_CARGO = I005.NU_CARGO 
//                                             AND I004.CO_USUARIO = '.$_SESSION['usuario_id']);
//       }
       $i   = 0; 
       $res = array();
       while (!$this->rs->EOF) {
         $res[$i]['CO_USUARIO'] = $this->rs->fields['CO_USUARIO'];
         $res[$i]['NB_INDICADOR'] = $this->rs->fields['NB_INDICADOR'];
         $res[$i]['NB_USUARIO'] = $this->rs->fields['NB_USUARIO']; 
         $res[$i]['NU_CEDULA'] = $this->rs->fields['NU_CEDULA'];
         $res[$i]['NB_CARGO'] = $this->rs->fields['NB_CARGO'];
         ++$i;
         $this->rs->MoveNext();
       }
       return $res;                       
    }
    
    /*
     * Obtiene los datos historicos de planes y actividades asociados a un codigo de usuario dado
     */
    public function SelecHistPersonal($coUsuario,$fecha_ini,$fecha_fin,$orden){
        /* Verificación la fecha por la cual se va a ordenar: 
         *      "0" Fecha de inicio
         *      "1" Fecha de culminación
         *      "2" Fecha de asignación
                "3" Fecha de programación
                "4" Fecha de planificación */
        switch ($orden) {
            case 0;
                $ordenar='I022T.FE_INICIO';
            break;
            case 1;
                $ordenar='I022T.FE_FIN';
            break;
            case 2;
                $ordenar='I022T.FE_ASIGNACION';
            break;                            
            case 3;
                $ordenar='I036T.FE_ASIGNACION';
            break;  
            case 4;
                $ordenar='I037T.FE_PROXIMA_PLAN';
            break;                              
        }
        
        // Cambio de formatos de fecha:
        $ArrayFecha =explode('/', $fecha_fin);
        $fecha_fin = $ArrayFecha[2] ."-".$ArrayFecha[1] ."-".$ArrayFecha[0];
        $ArrayFecha =explode('/', $fecha_ini);
        $fecha_ini = $ArrayFecha[2] ."-".$ArrayFecha[1] ."-".$ArrayFecha[0];
        $ordenar;
       $this->rs = $this->MySQL->ExecuteSQL('SELECT I037T.CO_PLAN, 
                                                    I037T.VA_DETALLE,
                                                    I037T.VA_HOJARUTA, 
                                                    I037T.TX_HOJARUTA,
                                                    I037T.FE_PROXIMA_PLAN,
                                                    I037T.VA_TIPO_PLAN,
                                                    I022T.co_actividad,
                                                    I036T.NU_ORDEN,
                                                    I036T.FE_ASIGNACION AS PROG,
                                                    I022T.FE_ASIGNACION AS ASIG,
                                                    I022T.FE_INICIO, 
                                                    I022T.FE_FIN,
                                                    I022T.TX_OBSERVACION,
                                                    I022T.HH_TOTAL_HORAS_HOMBRE AS H_H,
                                                    I022T.HH_VIAJE T_V,
                                                    I022T.NU_PORCENTAJE_AVANCE
                                             FROM I037T_PLAN I037T, 
                                                  I022T_ACTIVIDAD I022T,
                                                  I036T_ORDEN I036T 
                                             WHERE I037T.CO_PLAN = I022T.CO_PLAN
                                             AND   I022T.CO_ORDEN = I036T.CO_ORDEN
                                             AND   I022T.CO_ESTATUS = 3
                                             AND   I022T.CO_USUARIO = "'.$coUsuario.'"
                                             AND '.$ordenar.' BETWEEN "'.$fecha_ini.'" AND "'.$fecha_fin.'"
                                             ORDER BY '.$ordenar.' DESC');
       return $this->rs;                        
    }

    /*
     * Obtiene los datos historicos de planes y actividades asociados a un codigo de usuario dado
     */
    // $CoUsuario: codigo de usuario
    public function SelecGroup_maintainer_ubication($coUsuario)
    {
       
        
       $this->rs = $this->MySQL->ExecuteSQL("SELECT distinct  * 
                                             FROM (
                                                SELECT CONCAT(USUHIJ.CO_CARGO_PADRE,USUHIJ.CO_CARGO) as GRUPO_SOLUCIONADOR,C003.CO_GRUPO_PLAN AS GRUPO_PLAN,C003.CO_PUESTO_TRAB AS PUESTO_TRAB,I004R2.NB_INDICADOR AS INDICADOR
                                                FROM sisgem.I004T_USUARIO I004
                                                INNER JOIN sisgem.I005T_CARGO I005 ON I004.NU_CARGO = I005.NU_CARGO
                                                INNER JOIN  (
                                                    SELECT I005G.CO_CARGO_PADRE As CO_CARGO_PADRE,I005G.CO_CARGO As CO_CARGO,I005G.NU_CARGO
                                                    FROM sisgem.I005T_CARGO I005G  
                                                    INNER JOIN sisgem.I004T_USUARIO I004R ON I004R.NU_CARGO = I005G.NU_CARGO
                                                    INNER JOIN sisgem.C001T_ROL_USUARIO C001 ON C001.CO_USUARIO = sisgem.I004R.CO_USUARIO 
                                                    INNER JOIN sisgem.J035T_MENU_ROL J035 ON J035.CO_ROL = sisgem.C001.CO_ROL 
                                                    INNER JOIN sisgem.I024T_BLOQUE I024 ON I024.CO_BLOQUE = J035.CO_BLOQUE AND I024.CO_BLOQUE='10'
                                                            )
                                                AS USUHIJ ON USUHIJ.CO_CARGO_PADRE = CONCAT(I005.CO_CARGO_PADRE,I005.CO_CARGO)
                                                INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003 ON C003.NU_CARGO = USUHIJ.NU_CARGO
                                                INNER JOIN sisgem.I004T_USUARIO I004R2 ON I004R2.NU_CARGO = USUHIJ.NU_CARGO
                                                WHERE  I004.CO_USUARIO =".$coUsuario."

                                                UNION

                                                SELECT CONCAT(I005U.CO_CARGO_PADRE,I005U.CO_CARGO) as GRUPO_SOLUCIONADOR,C003U.CO_GRUPO_PLAN AS GRUPO_PLAN, C003U.CO_PUESTO_TRAB AS PUESTO_TRAB,null AS INDICADOR
                                                FROM sisgem.I004T_USUARIO I004U
                                                INNER JOIN sisgem.C002T_CARGO_GRUPO_PLAN C002U ON I004U.NU_CARGO = C002U.NU_CARGO
                                                INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003U ON C003U.CO_GRUPO_PLAN = C002U.CO_GRUPO_PLAN
                                                INNER JOIN sisgem.I005T_CARGO I005U ON I005U.NU_CARGO = C003U.NU_CARGO
                                                INNER JOIN sisgem.I004T_USUARIO I004W ON I004W.NU_CARGO = I005U.NU_CARGO
                                                INNER JOIN sisgem.C001T_ROL_USUARIO C001U ON C001U.CO_USUARIO = I004W.CO_USUARIO 
                                                INNER JOIN sisgem.J035T_MENU_ROL J035U ON J035U.CO_ROL = C001U.CO_ROL 
                                                INNER JOIN sisgem.I024T_BLOQUE I024U ON I024U.CO_BLOQUE = J035U.CO_BLOQUE AND I024U.CO_BLOQUE='10'
                                                WHERE  I004U.CO_USUARIO =".$coUsuario." 
                                                GROUP BY PUESTO_TRAB ORDER BY GRUPO_SOLUCIONADOR

                                                  ) AS TESTS");
       return $this->rs;                        
    }

    /*
     * Obtiene los datos historicos de planes y actividades asociados a un codigo de usuario dado
     */
     // $CoUsuario: codigo de usuario
    
    public function SelecGroup($coUsuario)
    {
       
        
      $this->rs = $this->MySQL->ExecuteSQL("SELECT distinct GRUPO_SOLUCIONADOR 
                                             FROM (
                                                SELECT CONCAT(USUHIJ.CO_CARGO_PADRE,USUHIJ.CO_CARGO) as GRUPO_SOLUCIONADOR,C003.CO_GRUPO_PLAN AS GRUPO_PLAN,C003.CO_PUESTO_TRAB AS PUESTO_TRAB,I004R2.NB_INDICADOR AS INDICADOR
                                                FROM sisgem.I004T_USUARIO I004
                                                INNER JOIN sisgem.I005T_CARGO I005 ON I004.NU_CARGO = I005.NU_CARGO
                                                INNER JOIN  (
                                                    SELECT I005G.CO_CARGO_PADRE As CO_CARGO_PADRE,I005G.CO_CARGO As CO_CARGO,I005G.NU_CARGO
                                                    FROM sisgem.I005T_CARGO I005G  
                                                    INNER JOIN sisgem.I004T_USUARIO I004R ON I004R.NU_CARGO = I005G.NU_CARGO
                                                    INNER JOIN sisgem.C001T_ROL_USUARIO C001 ON C001.CO_USUARIO = sisgem.I004R.CO_USUARIO 
                                                    INNER JOIN sisgem.J035T_MENU_ROL J035 ON J035.CO_ROL = sisgem.C001.CO_ROL 
                                                    INNER JOIN sisgem.I024T_BLOQUE I024 ON I024.CO_BLOQUE = J035.CO_BLOQUE AND I024.CO_BLOQUE='10'
                                                            )
                                                AS USUHIJ ON USUHIJ.CO_CARGO_PADRE = CONCAT(I005.CO_CARGO_PADRE,I005.CO_CARGO)
                                                INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003 ON C003.NU_CARGO = USUHIJ.NU_CARGO
                                                INNER JOIN sisgem.I004T_USUARIO I004R2 ON I004R2.NU_CARGO = USUHIJ.NU_CARGO
                                                WHERE  I004.CO_USUARIO =".$coUsuario."

                                                UNION

                                                SELECT CONCAT(I005U.CO_CARGO_PADRE,I005U.CO_CARGO) as GRUPO_SOLUCIONADOR,C003U.CO_GRUPO_PLAN AS GRUPO_PLAN, C003U.CO_PUESTO_TRAB AS PUESTO_TRAB,null AS INDICADOR
                                                FROM sisgem.I004T_USUARIO I004U
                                                INNER JOIN sisgem.C002T_CARGO_GRUPO_PLAN C002U ON I004U.NU_CARGO = C002U.NU_CARGO
                                                INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003U ON C003U.CO_GRUPO_PLAN = C002U.CO_GRUPO_PLAN
                                                INNER JOIN sisgem.I005T_CARGO I005U ON I005U.NU_CARGO = C003U.NU_CARGO
                                                INNER JOIN sisgem.I004T_USUARIO I004W ON I004W.NU_CARGO = I005U.NU_CARGO
                                                INNER JOIN sisgem.C001T_ROL_USUARIO C001U ON C001U.CO_USUARIO = I004W.CO_USUARIO 
                                                INNER JOIN sisgem.J035T_MENU_ROL J035U ON J035U.CO_ROL = C001U.CO_ROL 
                                                INNER JOIN sisgem.I024T_BLOQUE I024U ON I024U.CO_BLOQUE = J035U.CO_BLOQUE AND I024U.CO_BLOQUE='10'
                                                WHERE  I004U.CO_USUARIO =".$coUsuario." 
                                                GROUP BY PUESTO_TRAB ORDER BY GRUPO_SOLUCIONADOR

                                                  ) AS TESTS");
       return $this->rs;                          
    }
    /*
     * Obtiene los datos historicos de planes y actividades asociados a un codigo de usuario dado
     */
    // $CoUsuario: codigo de usuario
    // $group: grupo solucionador
    public function Select_plam($coUsuario,$group)
    {
       
        
      $this->rs = $this->MySQL->ExecuteSQL("SELECT distinct  GRUPO_PLAN
                                            FROM (
                                            SELECT CONCAT(USUHIJ.CO_CARGO_PADRE,USUHIJ.CO_CARGO) as GRUPO_SOLUCIONADOR,C003.CO_GRUPO_PLAN AS GRUPO_PLAN,C003.CO_PUESTO_TRAB AS PUESTO_TRAB,I004R2.NB_INDICADOR AS INDICADOR
                                            FROM sisgem.I004T_USUARIO I004
                                            INNER JOIN sisgem.I005T_CARGO I005 ON I004.NU_CARGO = I005.NU_CARGO
                                            INNER JOIN  (
                                            SELECT I005G.CO_CARGO_PADRE As CO_CARGO_PADRE,I005G.CO_CARGO As CO_CARGO,I005G.NU_CARGO
                                            FROM sisgem.I005T_CARGO I005G  
                                            INNER JOIN sisgem.I004T_USUARIO I004R ON I004R.NU_CARGO = I005G.NU_CARGO
                                            INNER JOIN sisgem.C001T_ROL_USUARIO C001 ON C001.CO_USUARIO = sisgem.I004R.CO_USUARIO 
                                            INNER JOIN sisgem.J035T_MENU_ROL J035 ON J035.CO_ROL = sisgem.C001.CO_ROL 
                                            INNER JOIN sisgem.I024T_BLOQUE I024 ON I024.CO_BLOQUE = J035.CO_BLOQUE AND I024.CO_BLOQUE='10'
                                              )
                                            AS USUHIJ ON USUHIJ.CO_CARGO_PADRE = CONCAT(I005.CO_CARGO_PADRE,I005.CO_CARGO)
                                            INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003 ON C003.NU_CARGO = USUHIJ.NU_CARGO
                                            INNER JOIN sisgem.I004T_USUARIO I004R2 ON I004R2.NU_CARGO = USUHIJ.NU_CARGO
                                            WHERE  I004.CO_USUARIO ='".$coUsuario."'

                                            UNION

                                            SELECT CONCAT(I005U.CO_CARGO_PADRE,I005U.CO_CARGO) as GRUPO_SOLUCIONADOR,C003U.CO_GRUPO_PLAN AS GRUPO_PLAN, C003U.CO_PUESTO_TRAB AS PUESTO_TRAB,null AS INDICADOR
                                            FROM sisgem.I004T_USUARIO I004U
                                            INNER JOIN sisgem.C002T_CARGO_GRUPO_PLAN C002U ON I004U.NU_CARGO = C002U.NU_CARGO
                                            INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003U ON C003U.CO_GRUPO_PLAN = C002U.CO_GRUPO_PLAN
                                            INNER JOIN sisgem.I005T_CARGO I005U ON I005U.NU_CARGO = C003U.NU_CARGO
                                            INNER JOIN sisgem.I004T_USUARIO I004W ON I004W.NU_CARGO = I005U.NU_CARGO
                                            INNER JOIN sisgem.C001T_ROL_USUARIO C001U ON C001U.CO_USUARIO = I004W.CO_USUARIO 
                                            INNER JOIN sisgem.J035T_MENU_ROL J035U ON J035U.CO_ROL = C001U.CO_ROL 
                                            INNER JOIN sisgem.I024T_BLOQUE I024U ON I024U.CO_BLOQUE = J035U.CO_BLOQUE AND I024U.CO_BLOQUE='10'
                                            WHERE  I004U.CO_USUARIO ='".$coUsuario."'
                                            GROUP BY PUESTO_TRAB ORDER BY GRUPO_SOLUCIONADOR

                                            ) AS TESTS where GRUPO_SOLUCIONADOR ='".$group."'");
       return $this->rs;                       
    }

    //obtiene los puestos de trbajo apartir de un grupo soluciobnador y un grupo de planificadores
    // $CoUsuario: codigo de usuario
    // $group: grupo solucionador
    // $Plam_post:grupo planificado

    public function Select_work($coUsuario,$group,$Plam_post)
    {
       
        
      $this->rs = $this->MySQL->ExecuteSQL("SELECT distinct  PUESTO_TRAB
                                            FROM (
                                            SELECT CONCAT(USUHIJ.CO_CARGO_PADRE,USUHIJ.CO_CARGO) as GRUPO_SOLUCIONADOR,C003.CO_GRUPO_PLAN AS GRUPO_PLAN,C003.CO_PUESTO_TRAB AS PUESTO_TRAB,I004R2.NB_INDICADOR AS INDICADOR
                                            FROM sisgem.I004T_USUARIO I004
                                            INNER JOIN sisgem.I005T_CARGO I005 ON I004.NU_CARGO = I005.NU_CARGO
                                            INNER JOIN  (
                                            SELECT I005G.CO_CARGO_PADRE As CO_CARGO_PADRE,I005G.CO_CARGO As CO_CARGO,I005G.NU_CARGO
                                            FROM sisgem.I005T_CARGO I005G  
                                            INNER JOIN sisgem.I004T_USUARIO I004R ON I004R.NU_CARGO = I005G.NU_CARGO
                                            INNER JOIN sisgem.C001T_ROL_USUARIO C001 ON C001.CO_USUARIO = sisgem.I004R.CO_USUARIO 
                                            INNER JOIN sisgem.J035T_MENU_ROL J035 ON J035.CO_ROL = sisgem.C001.CO_ROL 
                                            INNER JOIN sisgem.I024T_BLOQUE I024 ON I024.CO_BLOQUE = J035.CO_BLOQUE AND I024.CO_BLOQUE='10'
                                              )
                                            AS USUHIJ ON USUHIJ.CO_CARGO_PADRE = CONCAT(I005.CO_CARGO_PADRE,I005.CO_CARGO)
                                            INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003 ON C003.NU_CARGO = USUHIJ.NU_CARGO
                                            INNER JOIN sisgem.I004T_USUARIO I004R2 ON I004R2.NU_CARGO = USUHIJ.NU_CARGO
                                            WHERE  I004.CO_USUARIO ='".$coUsuario."'

                                            UNION

                                            SELECT CONCAT(I005U.CO_CARGO_PADRE,I005U.CO_CARGO) as GRUPO_SOLUCIONADOR,C003U.CO_GRUPO_PLAN AS GRUPO_PLAN, C003U.CO_PUESTO_TRAB AS PUESTO_TRAB,null AS INDICADOR
                                            FROM sisgem.I004T_USUARIO I004U
                                            INNER JOIN sisgem.C002T_CARGO_GRUPO_PLAN C002U ON I004U.NU_CARGO = C002U.NU_CARGO
                                            INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003U ON C003U.CO_GRUPO_PLAN = C002U.CO_GRUPO_PLAN
                                            INNER JOIN sisgem.I005T_CARGO I005U ON I005U.NU_CARGO = C003U.NU_CARGO
                                            INNER JOIN sisgem.I004T_USUARIO I004W ON I004W.NU_CARGO = I005U.NU_CARGO
                                            INNER JOIN sisgem.C001T_ROL_USUARIO C001U ON C001U.CO_USUARIO = I004W.CO_USUARIO 
                                            INNER JOIN sisgem.J035T_MENU_ROL J035U ON J035U.CO_ROL = C001U.CO_ROL 
                                            INNER JOIN sisgem.I024T_BLOQUE I024U ON I024U.CO_BLOQUE = J035U.CO_BLOQUE AND I024U.CO_BLOQUE='10'
                                            WHERE  I004U.CO_USUARIO ='".$coUsuario."'
                                            GROUP BY PUESTO_TRAB ORDER BY GRUPO_SOLUCIONADOR

                                            ) AS TESTS where GRUPO_SOLUCIONADOR ='".$group."' and GRUPO_PLAN  in (".$Plam_post.")");
       return $this->rs;                       
    }    
    /// devuelve un grupo de mantenedores partir de un codigo de usuario y un grupo solucionador
    // $coUsuario = codigo de usuario
    // $group = grupo solucionador
    public function Select_main($coUsuario,$group)
    {
       
        
      $this->rs = $this->MySQL->ExecuteSQL("SELECT distinct  INDICADOR
                                            FROM (
                                            SELECT CONCAT(USUHIJ.CO_CARGO_PADRE,USUHIJ.CO_CARGO) as GRUPO_SOLUCIONADOR,C003.CO_GRUPO_PLAN AS GRUPO_PLAN,C003.CO_PUESTO_TRAB AS PUESTO_TRAB,I004R2.NB_INDICADOR AS INDICADOR
                                            FROM sisgem.I004T_USUARIO I004
                                            INNER JOIN sisgem.I005T_CARGO I005 ON I004.NU_CARGO = I005.NU_CARGO
                                            INNER JOIN  (
                                            SELECT I005G.CO_CARGO_PADRE As CO_CARGO_PADRE,I005G.CO_CARGO As CO_CARGO,I005G.NU_CARGO
                                            FROM sisgem.I005T_CARGO I005G  
                                            INNER JOIN sisgem.I004T_USUARIO I004R ON I004R.NU_CARGO = I005G.NU_CARGO
                                            INNER JOIN sisgem.C001T_ROL_USUARIO C001 ON C001.CO_USUARIO = sisgem.I004R.CO_USUARIO 
                                            INNER JOIN sisgem.J035T_MENU_ROL J035 ON J035.CO_ROL = sisgem.C001.CO_ROL 
                                            INNER JOIN sisgem.I024T_BLOQUE I024 ON I024.CO_BLOQUE = J035.CO_BLOQUE AND I024.CO_BLOQUE='10'
                                              )
                                            AS USUHIJ ON USUHIJ.CO_CARGO_PADRE = CONCAT(I005.CO_CARGO_PADRE,I005.CO_CARGO)
                                            INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003 ON C003.NU_CARGO = USUHIJ.NU_CARGO
                                            INNER JOIN sisgem.I004T_USUARIO I004R2 ON I004R2.NU_CARGO = USUHIJ.NU_CARGO
                                            WHERE  I004.CO_USUARIO ='".$coUsuario."'

                                            UNION

                                            SELECT CONCAT(I005U.CO_CARGO_PADRE,I005U.CO_CARGO) as GRUPO_SOLUCIONADOR,C003U.CO_GRUPO_PLAN AS GRUPO_PLAN, C003U.CO_PUESTO_TRAB AS PUESTO_TRAB,null AS INDICADOR
                                            FROM sisgem.I004T_USUARIO I004U
                                            INNER JOIN sisgem.C002T_CARGO_GRUPO_PLAN C002U ON I004U.NU_CARGO = C002U.NU_CARGO
                                            INNER JOIN sisgem.C003T_CARGO_PUESTO_TRAB C003U ON C003U.CO_GRUPO_PLAN = C002U.CO_GRUPO_PLAN
                                            INNER JOIN sisgem.I005T_CARGO I005U ON I005U.NU_CARGO = C003U.NU_CARGO
                                            INNER JOIN sisgem.I004T_USUARIO I004W ON I004W.NU_CARGO = I005U.NU_CARGO
                                            INNER JOIN sisgem.C001T_ROL_USUARIO C001U ON C001U.CO_USUARIO = I004W.CO_USUARIO 
                                            INNER JOIN sisgem.J035T_MENU_ROL J035U ON J035U.CO_ROL = C001U.CO_ROL 
                                            INNER JOIN sisgem.I024T_BLOQUE I024U ON I024U.CO_BLOQUE = J035U.CO_BLOQUE AND I024U.CO_BLOQUE='10'
                                            WHERE  I004U.CO_USUARIO ='".$coUsuario."'
                                            GROUP BY PUESTO_TRAB ORDER BY GRUPO_SOLUCIONADOR

                                            ) AS TESTS where GRUPO_SOLUCIONADOR ='".$group."'");
       return $this->rs;                       
    }

    //  devuelve un la cantida de actividaes planificadas,asignadas,ejecutadas y progradas con la cantidad de HH
    //  filtrando por un grupo de puestos de trabajo, grupo planificadores y un rango de fecha 
    //$Plan : grupo planificador
    //$Tra  : puesto de trabajo
    //$D_start :fecha inicio
    //$D_end   :fecha fin

    public function Final_report($Plan,$Tra,$D_start,$D_end)
    {
       
        
      $this->rs = $this->MySQL->ExecuteSQL("select data_report.Fecha,data_report.G_Planificador,data_report.Grupo_trabajo, sum(Cantidad_planificada) as 'Planificada' ,sum(Cantidad_programacion) as 'Programada',sum(Cantidad_Asignada) as 'Asignada',sum(Cantidad_ejecutada) as 'Ejecutada', SUM(HHT) as 'HHT', SUM(HV) as 'HV'
                                            from(
                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Plan)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Plan),char),'-',CONVERT(YEAR(Plan),char)) as 'Fecha',G_Planificador,Grupo_trabajo,count(*) as 'Cantidad_planificada', NULL as 'Cantidad_programacion', null as 'Cantidad_Asignada',null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                  FROM 
                                                    (SELECT I037T.FE_PROXIMA_PLAN AS 'Plan',I037T.CO_GRP_PLAN 'G_Planificador',I037T.CO_EJEC_PLAN 'Grupo_trabajo'
                                                    FROM sisgem.I037T_PLAN I037T 
                                                    ) DATA_COUNT
                                                    where Plan and (Plan BETWEEN  '".$D_start."' AND '".$D_end."')
                                                 Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Plan)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Plan),char),'-',CONVERT(YEAR(Plan),char)),G_Planificador,Grupo_trabajo
                                                    

                                                UNION ALL

                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Progrmacion)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Progrmacion),char),'-',CONVERT(YEAR(Progrmacion),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_planificada',count(*) as 'Cantidad_programacion',null as 'Cantidad_Asignada', null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                FROM 
                                                  (SELECT I022T.CO_PLAN AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo'
                                                  FROM sisgem.I037T_PLAN I037T2 
                                                  JOIN sisgem.I022T_ACTIVIDAD I022T
                                                    ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                  JOIN sisgem.I036T_ORDEN I036T 
                                                    ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                   ) DATA_COUNT
                                                   where Progrmacion and (Progrmacion BETWEEN  '".$D_start."' AND '".$D_end."')
                                                  Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Progrmacion)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Progrmacion),char),'-',CONVERT(YEAR(Progrmacion),char)),G_Planificador,Grupo_trabajo
                                                   
                                                UNION ALL

                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Asignado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Asignado),char),'-',CONVERT(YEAR(Asignado),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_planificada',null as 'Cantidad_programacion',count(*) as 'Cantidad_Asignada', null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                FROM 
                                                  (SELECT I022T.CO_PLAN AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo'
                                                  FROM sisgem.I037T_PLAN I037T2 
                                                  JOIN sisgem.I022T_ACTIVIDAD I022T
                                                    ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                  JOIN sisgem.I036T_ORDEN I036T 
                                                    ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                  ) DATA_COUNT
                                                  where Asignado and (Asignado BETWEEN  '".$D_start."' AND '".$D_end."')
                                                Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Asignado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Asignado),char),'-',CONVERT(YEAR(Asignado),char)),G_Planificador,Grupo_trabajo
                                             
                                             UNION ALL

                                              SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Ejecutado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Ejecutado),char),'-',CONVERT(YEAR(Ejecutado),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_plan',null as 'Cantidad_programacion',null as 'Cantidad_Asignada',count(*) as 'Cantidad_ejecutada',truncate(SUM(TIME_TO_SEC(HHT)/3600),1) as 'HHT',truncate(SUM(TIME_TO_SEC(HV)/3600),1) as 'HV'
                                              FROM 
                                                (SELECT I022T.CO_PLAN AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo',I022T.HH_TOTAL_HORAS_HOMBRE AS 'HHT',I022T.HH_VIAJE as 'HV'
                                                FROM sisgem.I037T_PLAN I037T2 
                                                JOIN sisgem.I022T_ACTIVIDAD I022T
                                                  ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                JOIN sisgem.I036T_ORDEN I036T 
                                                  ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                ) DATA_COUNT
                                                where Ejecutado and ( Ejecutado BETWEEN  '".$D_start."' AND '".$D_end."')
                                              Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Ejecutado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Ejecutado),char),'-',CONVERT(YEAR(Ejecutado),char)),G_Planificador,Grupo_trabajo      
                                             ) data_report
                                              group by data_report.Fecha,data_report.G_Planificador,data_report.Grupo_trabajo
                                              having  Grupo_trabajo in (".$Tra.") and G_Planificador in (".$Plan.")
                                              order by CONVERT(SUBSTRING_INDEX( data_report.Fecha,'-', -1 ),UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX( data_report.Fecha,'-', -2 ),'-',1 ),UNSIGNED INTEGER),CONVERT(SUBSTRING_INDEX( data_report.Fecha,'-', 1 ),UNSIGNED INTEGER)");
        return $this->rs;                       
    }

    //  devuelve un la cantida de actividaes planificadas,asignadas,ejecutadas y progradas con la cantidad de HH
    //  filtrando por un grupo de puestos de trabajo, grupo planificadores , un rango de fecha 
    //  y una ubicacion tecnica
    //$Plan : grupo planificador
    //$Tra  : puesto de trabajo
    //$D_start :fecha inicio
    //$D_end   :fecha fin
    //$workP : ubicacion tecnica
    public function Final_report_workPlace($Plan,$Tra,$D_start,$D_end,$workP)
    {
       
        
      $this->rs = $this->MySQL->ExecuteSQL("select data_report.Fecha,data_report.G_Planificador,data_report.Grupo_trabajo, sum(Cantidad_planificada) as 'Planificada' ,sum(Cantidad_programacion) as 'Programada',sum(Cantidad_Asignada) as 'Asignada',sum(Cantidad_ejecutada) as 'Ejecutada', SUM(HHT) as 'HHT', SUM(HV) as 'HV'
                                            from(
                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Plan)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Plan),char),'-',CONVERT(YEAR(Plan),char)) as 'Fecha',G_Planificador,Grupo_trabajo,count(*) as 'Cantidad_planificada', NULL as 'Cantidad_programacion', null as 'Cantidad_Asignada',null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                  FROM 
                                                    (SELECT I037T.FE_PROXIMA_PLAN AS 'Plan',I037T.CO_GRP_PLAN 'G_Planificador',I037T.CO_EJEC_PLAN 'Grupo_trabajo'
                                                    FROM sisgem.I037T_PLAN I037T 
                                                    WHERE I037T.CO_UBIC_TEC  LIKE '".$workP."'
                                                    ) DATA_COUNT
                                                    where Plan and (Plan BETWEEN  '".$D_start."' AND '".$D_end."')
                                                 Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Plan)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Plan),char),'-',CONVERT(YEAR(Plan),char)),G_Planificador,Grupo_trabajo
                                                    

                                                UNION ALL

                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Progrmacion)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Progrmacion),char),'-',CONVERT(YEAR(Progrmacion),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_planificada',count(*) as 'Cantidad_programacion',null as 'Cantidad_Asignada', null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                FROM 
                                                  (SELECT I022T.CO_PLAN AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo'
                                                  FROM sisgem.I037T_PLAN I037T2 
                                                  JOIN sisgem.I022T_ACTIVIDAD I022T
                                                    ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                  JOIN sisgem.I036T_ORDEN I036T 
                                                    ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                  WHERE I037T2.CO_UBIC_TEC  LIKE '".$workP."'
                                                   ) DATA_COUNT
                                                   where Progrmacion and (Progrmacion BETWEEN  '".$D_start."' AND '".$D_end."')
                                                  Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Progrmacion)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Progrmacion),char),'-',CONVERT(YEAR(Progrmacion),char)),G_Planificador,Grupo_trabajo
                                                   
                                                UNION ALL

                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Asignado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Asignado),char),'-',CONVERT(YEAR(Asignado),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_planificada',null as 'Cantidad_programacion',count(*) as 'Cantidad_Asignada', null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                FROM 
                                                  (SELECT I022T.CO_PLAN AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo'
                                                  FROM sisgem.I037T_PLAN I037T2 
                                                  JOIN sisgem.I022T_ACTIVIDAD I022T
                                                    ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                  JOIN sisgem.I036T_ORDEN I036T 
                                                    ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                  WHERE I037T2.CO_UBIC_TEC  LIKE '".$workP."'
                                                  ) DATA_COUNT
                                                  where Asignado and (Asignado BETWEEN  '".$D_start."' AND '".$D_end."')
                                                Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Asignado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Asignado),char),'-',CONVERT(YEAR(Asignado),char)),G_Planificador,Grupo_trabajo
                                             
                                             UNION ALL

                                              SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Ejecutado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Ejecutado),char),'-',CONVERT(YEAR(Ejecutado),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_plan',null as 'Cantidad_programacion',null as 'Cantidad_Asignada',count(*) as 'Cantidad_ejecutada',truncate(SUM(TIME_TO_SEC(HHT)/3600),1) as 'HHT',truncate(SUM(TIME_TO_SEC(HV)/3600),1) as 'HV'
                                              FROM 
                                                (SELECT I022T.CO_PLAN AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo',I022T.HH_TOTAL_HORAS_HOMBRE AS 'HHT',I022T.HH_VIAJE as 'HV'
                                                FROM sisgem.I037T_PLAN I037T2 
                                                JOIN sisgem.I022T_ACTIVIDAD I022T
                                                  ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                JOIN sisgem.I036T_ORDEN I036T 
                                                  ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                WHERE I037T2.CO_UBIC_TEC  LIKE '".$workP."'
                                                ) DATA_COUNT
                                                where Ejecutado and ( Ejecutado BETWEEN  '".$D_start."' AND '".$D_end."')
                                              Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Ejecutado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Ejecutado),char),'-',CONVERT(YEAR(Ejecutado),char)),G_Planificador,Grupo_trabajo      
                                             ) data_report
                                              group by data_report.Fecha,data_report.G_Planificador,data_report.Grupo_trabajo
                                              having  Grupo_trabajo in (".$Tra.") and G_Planificador in (".$Plan.")
                                              order by CONVERT(SUBSTRING_INDEX( data_report.Fecha,'-', -1 ),UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX( data_report.Fecha,'-', -2 ),'-',1 ),UNSIGNED INTEGER),CONVERT(SUBSTRING_INDEX( data_report.Fecha,'-', 1 ),UNSIGNED INTEGER)");
        return $this->rs;                       
    }

    //  devuelve un la cantida de actividaes planificadas,asignadas,ejecutadas y progradas con la cantidad de HH
    //  filtrando por un grupo de puestos de trabajo, grupo planificadores , un rango de fecha 
    //  y un grupo de mantenedores
    //$Plan : grupo planificador
    //$Tra  : puesto de trabajo
    //$D_start :fecha inicio
    //$D_end   :fecha fin
    //$maintener : grupo de mantenedores
    
    public function Final_report_Maintener($Plan,$Tra,$D_start,$D_end,$maintener)
    {

       
      $this->rs = $this->MySQL->ExecuteSQL("select data_report.Fecha,data_report.G_Planificador,data_report.Grupo_trabajo, sum(Cantidad_planificada) as 'Planificada' ,sum(Cantidad_programacion) as 'Programada',sum(Cantidad_Asignada) as 'Asignada',sum(Cantidad_ejecutada) as 'Ejecutada', SUM(HHT) as 'HHT', SUM(HV) as 'HV'
                                            from(
                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Plan)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Plan),char),'-',CONVERT(YEAR(Plan),char)) as 'Fecha',G_Planificador,Grupo_trabajo,count(*) as 'Cantidad_planificada', NULL as 'Cantidad_programacion', null as 'Cantidad_Asignada',null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                  FROM 
                                                    ( SELECT Plan, G_Planificador,Grupo_trabajo
                                                        FROM (
                                                        
                                                          SELECT I022T.CO_PLAN 'co_ac',  I037T.FE_PROXIMA_PLAN AS 'Plan',I037T.CO_GRP_PLAN 'G_Planificador',I037T.CO_EJEC_PLAN 'Grupo_trabajo'
                                                          FROM sisgem.I037T_PLAN I037T 
                                                          JOIN sisgem.I022T_ACTIVIDAD I022T
                                                          ON I037T.CO_PLAN = I022T.CO_PLAN 
                                                          WHERE  I022T.CO_USUARIO IN 
                                                                      (
                                                                        SELECT DISTINCT I004T.CO_USUARIO AS  'co_usuario' 
                                                                        FROM sisgem.I004T_USUARIO I004T
                                                                        WHERE I004T.NB_INDICADOR IN (".$maintener.")
                                                                      )
                                                             ) GRO GROUP BY co_ac
                                                          
                                                       )DATA_COUNT
                                                    where Plan and (Plan BETWEEN  '".$D_start."' AND '".$D_end."')
                                                 Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Plan)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Plan),char),'-',CONVERT(YEAR(Plan),char)),G_Planificador,Grupo_trabajo
                                                    

                                                UNION ALL

                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Progrmacion)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Progrmacion),char),'-',CONVERT(YEAR(Progrmacion),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_planificada',count(*) as 'Cantidad_programacion',null as 'Cantidad_Asignada', null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                FROM 
                                                  (SELECT I022T.CO_USUARIO AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo'
                                                  FROM sisgem.I037T_PLAN I037T2 
                                                  JOIN sisgem.I022T_ACTIVIDAD I022T
                                                    ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                  JOIN sisgem.I036T_ORDEN I036T 
                                                    ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                   ) DATA_COUNT
                                                   where Progrmacion and (Progrmacion BETWEEN  '".$D_start."' AND '".$D_end."') AND Actividad IN(
                                                                                                                                                  SELECT DISTINCT I004T.CO_USUARIO AS  'co_usuario' 
                                                                                                                                                  FROM sisgem.I004T_USUARIO I004T
                                                                                                                                                  WHERE I004T.NB_INDICADOR IN (".$maintener.")
                                                                                                                                                )
                                                  Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Progrmacion)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Progrmacion),char),'-',CONVERT(YEAR(Progrmacion),char)),G_Planificador,Grupo_trabajo
                                                   
                                                UNION ALL

                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Asignado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Asignado),char),'-',CONVERT(YEAR(Asignado),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_planificada',null as 'Cantidad_programacion',count(*) as 'Cantidad_Asignada', null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                FROM 
                                                  (SELECT I022T.CO_USUARIO AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo'
                                                  FROM sisgem.I037T_PLAN I037T2 
                                                  JOIN sisgem.I022T_ACTIVIDAD I022T
                                                    ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                  JOIN sisgem.I036T_ORDEN I036T 
                                                    ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                  ) DATA_COUNT
                                                  where Asignado and (Asignado BETWEEN  '".$D_start."' AND '".$D_end."') AND Actividad IN  (
                                                                                                                                            SELECT DISTINCT I004T.CO_USUARIO AS  'co_usuario' 
                                                                                                                                            FROM sisgem.I004T_USUARIO I004T
                                                                                                                                            WHERE I004T.NB_INDICADOR IN (".$maintener.")
                                                                                                                                            )
                                                Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Asignado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Asignado),char),'-',CONVERT(YEAR(Asignado),char)),G_Planificador,Grupo_trabajo
                                             
                                             UNION ALL

                                              SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Ejecutado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Ejecutado),char),'-',CONVERT(YEAR(Ejecutado),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_plan',null as 'Cantidad_programacion',null as 'Cantidad_Asignada',count(*) as 'Cantidad_ejecutada',truncate(SUM(TIME_TO_SEC(HHT)/3600),1) as 'HHT',truncate(SUM(TIME_TO_SEC(HV)/3600),1) as 'HV'
                                              FROM 
                                                (SELECT I022T.CO_USUARIO AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo',I022T.HH_TOTAL_HORAS_HOMBRE AS 'HHT',I022T.HH_VIAJE as 'HV'
                                                FROM sisgem.I037T_PLAN I037T2 
                                                JOIN sisgem.I022T_ACTIVIDAD I022T
                                                  ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                JOIN sisgem.I036T_ORDEN I036T 
                                                  ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                ) DATA_COUNT
                                                where Ejecutado and ( Ejecutado BETWEEN  '".$D_start."' AND '".$D_end."') AND Actividad IN (
                                                                                                                                            SELECT DISTINCT I004T.CO_USUARIO AS  'co_usuario' 
                                                                                                                                            FROM sisgem.I004T_USUARIO I004T
                                                                                                                                            WHERE I004T.NB_INDICADOR IN (".$maintener.")
                                                                                                                                            )
                                              Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Ejecutado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Ejecutado),char),'-',CONVERT(YEAR(Ejecutado),char)),G_Planificador,Grupo_trabajo      
                                             ) data_report
                                              group by data_report.Fecha,data_report.G_Planificador,data_report.Grupo_trabajo
                                              having  Grupo_trabajo in (".$Tra.") and G_Planificador in (".$Plan.")
                                              order by CONVERT(SUBSTRING_INDEX( data_report.Fecha,'-', -1 ),UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX( data_report.Fecha,'-', -2 ),'-',1 ),UNSIGNED INTEGER),CONVERT(SUBSTRING_INDEX( data_report.Fecha,'-', 1 ),UNSIGNED INTEGER)");
        return $this->rs;                     
    }

    //  devuelve un la cantida de actividaes planificadas,asignadas,ejecutadas y progradas con la cantidad de HH
    //  filtrando por un grupo de puestos de trabajo, grupo planificadores , un rango de fecha 
    //  , un grupo de mantenedores y una ubicacion tecnica
    //$Plan : grupo planificador
    //$Tra  : puesto de trabajo
    //$D_start :fecha inicio
    //$D_end   :fecha fin
    //$maintener : grupo de mantenedores
    //$workP : ubicacion tecnica


    public function Final_report_Maintener_workPlace($Plan,$Tra,$D_start,$D_end,$maintener,$workP)
    {

       
      $this->rs = $this->MySQL->ExecuteSQL("select data_report.Fecha,data_report.G_Planificador,data_report.Grupo_trabajo, sum(Cantidad_planificada) as 'Planificada' ,sum(Cantidad_programacion) as 'Programada',sum(Cantidad_Asignada) as 'Asignada',sum(Cantidad_ejecutada) as 'Ejecutada', SUM(HHT) as 'HHT', SUM(HV) as 'HV'
                                            from(
                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Plan)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Plan),char),'-',CONVERT(YEAR(Plan),char)) as 'Fecha',G_Planificador,Grupo_trabajo,count(*) as 'Cantidad_planificada', NULL as 'Cantidad_programacion', null as 'Cantidad_Asignada',null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                  FROM 
                                                    ( SELECT Plan, G_Planificador,Grupo_trabajo
                                                        FROM (
                                                        
                                                          SELECT I022T.CO_PLAN 'co_ac',  I037T.FE_PROXIMA_PLAN AS 'Plan',I037T.CO_GRP_PLAN 'G_Planificador',I037T.CO_EJEC_PLAN 'Grupo_trabajo'
                                                          FROM sisgem.I037T_PLAN I037T 
                                                          JOIN sisgem.I022T_ACTIVIDAD I022T
                                                          ON I037T.CO_PLAN = I022T.CO_PLAN
                                                          WHERE I037T.CO_UBIC_TEC  LIKE '".$workP."' AND I022T.CO_USUARIO IN 
                                                                      (
                                                                        SELECT DISTINCT I004T.CO_USUARIO AS  'co_usuario' 
                                                                        FROM sisgem.I004T_USUARIO I004T
                                                                        WHERE I004T.NB_INDICADOR IN (".$maintener.")
                                                                      )
                                                             ) GRO GROUP BY co_ac
                                                          
                                                       )DATA_COUNT
                                                    where Plan and (Plan BETWEEN  '".$D_start."' AND '".$D_end."')
                                                 Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Plan)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Plan),char),'-',CONVERT(YEAR(Plan),char)),G_Planificador,Grupo_trabajo
                                                    

                                                UNION ALL

                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Progrmacion)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Progrmacion),char),'-',CONVERT(YEAR(Progrmacion),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_planificada',count(*) as 'Cantidad_programacion',null as 'Cantidad_Asignada', null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                FROM 
                                                  (SELECT I022T.CO_USUARIO AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo'
                                                  FROM sisgem.I037T_PLAN I037T2 
                                                  JOIN sisgem.I022T_ACTIVIDAD I022T
                                                    ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                  JOIN sisgem.I036T_ORDEN I036T 
                                                    ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                  WHERE I037T2.CO_UBIC_TEC  LIKE '".$workP."'
                                                   ) DATA_COUNT
                                                   where Progrmacion and (Progrmacion BETWEEN  '".$D_start."' AND '".$D_end."') AND Actividad IN(
                                                                                                                                                  SELECT DISTINCT I004T.CO_USUARIO AS  'co_usuario' 
                                                                                                                                                  FROM sisgem.I004T_USUARIO I004T
                                                                                                                                                  WHERE I004T.NB_INDICADOR IN (".$maintener.")
                                                                                                                                                )
                                                  Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Progrmacion)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Progrmacion),char),'-',CONVERT(YEAR(Progrmacion),char)),G_Planificador,Grupo_trabajo
                                                   
                                                UNION ALL

                                                SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Asignado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Asignado),char),'-',CONVERT(YEAR(Asignado),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_planificada',null as 'Cantidad_programacion',count(*) as 'Cantidad_Asignada', null as 'Cantidad_ejecutada', null as 'HHT', null as 'HV'
                                                FROM 
                                                  (SELECT I022T.CO_USUARIO AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo'
                                                  FROM sisgem.I037T_PLAN I037T2 
                                                  JOIN sisgem.I022T_ACTIVIDAD I022T
                                                    ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                  JOIN sisgem.I036T_ORDEN I036T 
                                                    ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                  WHERE I037T2.CO_UBIC_TEC  LIKE '".$workP."'
                                                  ) DATA_COUNT
                                                  where Asignado and (Asignado BETWEEN  '".$D_start."' AND '".$D_end."') AND Actividad IN  (
                                                                                                                                            SELECT DISTINCT I004T.CO_USUARIO AS  'co_usuario' 
                                                                                                                                            FROM sisgem.I004T_USUARIO I004T
                                                                                                                                            WHERE I004T.NB_INDICADOR IN (".$maintener.")
                                                                                                                                            )
                                                Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Asignado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Asignado),char),'-',CONVERT(YEAR(Asignado),char)),G_Planificador,Grupo_trabajo
                                             
                                             UNION ALL

                                              SELECT CONCAT ( CONVERT( (FLOOR((DayOfMonth(Ejecutado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Ejecutado),char),'-',CONVERT(YEAR(Ejecutado),char)) as 'Fecha',G_Planificador,Grupo_trabajo,NULL as 'Cantidad_plan',null as 'Cantidad_programacion',null as 'Cantidad_Asignada',count(*) as 'Cantidad_ejecutada',truncate(SUM(TIME_TO_SEC(HHT)/3600),1) as 'HHT',truncate(SUM(TIME_TO_SEC(HV)/3600),1) as 'HV'
                                              FROM 
                                                (SELECT I022T.CO_USUARIO AS 'Actividad',I036T.CO_ORDEN as 'Orden',I037T2.FE_PROXIMA_PLAN AS 'Plan',I036T.FE_ASIGNACION AS 'Progrmacion' ,I022T.FE_ASIGNACION AS 'Asignado',I022T.FE_FIN AS 'Ejecutado',I037T2.CO_GRP_PLAN AS 'G_Planificador',I037T2.CO_EJEC_PLAN 'Grupo_trabajo',I022T.HH_TOTAL_HORAS_HOMBRE AS 'HHT',I022T.HH_VIAJE as 'HV'
                                                FROM sisgem.I037T_PLAN I037T2 
                                                JOIN sisgem.I022T_ACTIVIDAD I022T
                                                  ON I037T2.CO_PLAN = I022T.CO_PLAN 
                                                JOIN sisgem.I036T_ORDEN I036T 
                                                  ON  I022T.CO_ORDEN = I036T.CO_ORDEN
                                                WHERE I037T2.CO_UBIC_TEC  LIKE '".$workP."'
                                                ) DATA_COUNT
                                                where Ejecutado and ( Ejecutado BETWEEN  '".$D_start."' AND '".$D_end."') AND Actividad IN (
                                                                                                                                            SELECT DISTINCT I004T.CO_USUARIO AS  'co_usuario' 
                                                                                                                                            FROM sisgem.I004T_USUARIO I004T
                                                                                                                                            WHERE I004T.NB_INDICADOR IN (".$maintener.")
                                                                                                                                            )
                                              Group by CONCAT ( CONVERT( (FLOOR((DayOfMonth(Ejecutado)-1)/7)+1 ), char) , '-' ,CONVERT(MONTH (Ejecutado),char),'-',CONVERT(YEAR(Ejecutado),char)),G_Planificador,Grupo_trabajo      
                                             ) data_report
                                              group by data_report.Fecha,data_report.G_Planificador,data_report.Grupo_trabajo
                                              having  Grupo_trabajo in (".$Tra.") and G_Planificador in (".$Plan.")
                                              order by CONVERT(SUBSTRING_INDEX( data_report.Fecha,'-', -1 ),UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX( data_report.Fecha,'-', -2 ),'-',1 ),UNSIGNED INTEGER),CONVERT(SUBSTRING_INDEX( data_report.Fecha,'-', 1 ),UNSIGNED INTEGER)");
        return $this->rs;                     
    }


}
?>
