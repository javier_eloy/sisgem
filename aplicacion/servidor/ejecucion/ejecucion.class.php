<?php

class ejecucion {

    private $MySQL = null;
    private $rs = null;

    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQL->ConnectMySQL();
    }

    public function __destruct() {
        $this->MySQL->CloseConnect();
    }

    //===============  METODOS PUBLICOS =================

    function SelCarteleraEjecucion($co_usuario, $fecha) {
        /* Selecciona la cartelera que fue asignado (TIPO_ASIGNACION) por:
         * 1 = Asignacion por Programacion
         * 2 = Asignacion Especifica
         * 4 = Asignacion por reasignacion
         */

        $this->rs = $this->MySQL->ExecuteSQL("SELECT CO_PLAN,VA_DETALLE,
                                                   VA_HOJARUTA,FE_ULTIMO,
                                                   NU_SEMANA,FE_PROXIMA_PLAN,
                                                   NU_SEMANA_PROXIMA,CO_ESTATUS,
                                                   VA_TIPO_PLAN, CO_ESTATUS_ORDEN,
                                                   CO_ORDEN,FE_ASIG_ORDEN,
                                                   NU_ORDEN,PC_AVANCE,
                                                   CO_ACTIVIDAD,
                                                   FE_ASIGNACION,
                                                   NU_PORCENTAJE_AVANCE, 
                                                   FE_INICIO, 
                                                   FE_FIN,
                                                   HH_TOTAL_HORAS_HOMBRE,
                                                   HH_VIAJE,
                                                   TX_OBSERVACION
                                         FROM V001_CARTELERA   
                                         WHERE CO_USUARIO={$co_usuario} 
                                              AND FE_PROXIMA_PLAN <= STR_TO_DATE('{$fecha->format("d/m/Y")}','%d/%m/%Y')
                                         ORDER BY NU_ORDEN, VA_DETALLE;");
        return $this->rs;
    }

    /***
     * 
     */

      function SelOrdenFormato($co_usuario, $orden_trabajo) {
        /* Selecciona la cartelera que fue asignado (TIPO_ASIGNACION) por:
         * 1 = Asignacion por Programacion
         * 2 = Asignacion Especifica
         * 4 = Asignacion por reasignacion
         */

        $this->rs = $this->MySQL->ExecuteSQL("SELECT CO_PLAN,VA_DETALLE,
                                                   VA_HOJARUTA,FE_ULTIMO,
                                                   NU_SEMANA,FE_PROXIMA_PLAN,
                                                   NU_SEMANA_PROXIMA,CO_ESTATUS,
                                                   VA_TIPO_PLAN, CO_ESTATUS_ORDEN,
                                                   CO_ORDEN,FE_ASIG_ORDEN,
                                                   NU_ORDEN,PC_AVANCE,
                                                   CO_UBIC_TEC, TX_HOJARUTA,
                                                   CO_ACTIVIDAD,
                                                   FE_ASIGNACION,
                                                   NU_PORCENTAJE_AVANCE,
                                                   FE_INICIO,
                                                   FE_FIN,
                                                   HH_TOTAL_HORAS_HOMBRE,
                                                   HH_VIAJE,
                                                   TX_OBSERVACION,
                                                   USU.NB_INDICADOR,
                                                   SUPER.NB_INDICADOR_SUPER
                                         FROM V001_CARTELERA As CART
                                          INNER JOIN I004T_USUARIO AS USU ON USU.CO_USUARIO = CART.CO_USUARIO
                                          INNER JOIN I005T_CARGO CGO ON CGO.NU_CARGO = USU.NU_CARGO
                                           LEFT JOIN (SELECT GROUP_CONCAT(USUP.NB_INDICADOR) As NB_INDICADOR_SUPER, CGOP.CO_CARGO
                                                     FROM I004T_USUARIO As USUP
                                                     INNER JOIN I005T_CARGO As CGOP ON CGOP.NU_CARGO = USUP.NU_CARGO
                                                     GROUP BY CGOP.CO_CARGO) As SUPER ON SUPER.CO_CARGO = CGO.CO_CARGO_PADRE
                                         WHERE CART.CO_USUARIO={$co_usuario}
                                           AND NU_ORDEN = {$orden_trabajo}");
        return $this->rs;
    }

    public function SelActividad($co_actividad) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT CO_PLAN,VA_DETALLE,
                                                   VA_HOJARUTA,FE_ULTIMO,
                                                   NU_SEMANA,FE_PROXIMA_PLAN,
                                                   NU_SEMANA_PROXIMA,CO_ESTATUS,
                                                   VA_TIPO_PLAN, CO_ESTATUS_ORDEN,
                                                   CO_ORDEN,FE_ASIG_ORDEN,
                                                   NU_ORDEN,PC_AVANCE,
                                                   CO_ACTIVIDAD,
                                                   FE_ASIGNACION,
                                                   NU_PORCENTAJE_AVANCE,
                                                   TX_OBSERVACION,
                                                   FE_INICIO, FE_FIN, HH_TOTAL_HORAS_HOMBRE, HH_VIAJE
                                         FROM V001_CARTELERA   
                                         WHERE CO_ACTIVIDAD = $co_actividad");
        return $this->rs;
    }

    public function SelProcedimiento($co_actividad, $co_usuario) {

        $this->rs = $this->MySQL->ExecuteSQL("SELECT PRP.CO_PLAN,PRP.NB_TEXTO,PRP.NU_NODO,CAST(PRP.NU_NODO AS UNSIGNED) AS UNU_NODO,PRP.NU_POSICION,PRP.VA_PROCEDIMIENTO,
                                                     PL.TX_HOJARUTA, 
                                            (SELECT  COUNT(EJ.CO_USUARIO) FROM I031T_EJECUCION As EJ 
                                                     WHERE EJ.CO_PLAN = PL.CO_PLAN 
                                                       AND EJ.VA_PROCEDIMIENTO = PRP.VA_PROCEDIMIENTO 
                                                       AND EJ.CO_ORDEN = ACT.CO_ORDEN
                                                       AND EJ.CO_USUARIO <> $co_usuario) As TOT_EJECUTADO ,
                                             ACT.CO_ACTIVIDAD,
                                             ACT.CO_ORDEN,
                                             ACT.CO_USUARIO As CO_USUARIO_ACTIVIDAD,                                            
                                             EJUS.FE_EJECUCION
                                             FROM I020T_PROCEDIMIENTO_PLAN PRP
                                             INNER JOIN I037T_PLAN PL ON PRP.CO_PLAN = PL.CO_PLAN
                                             INNER JOIN I022T_ACTIVIDAD ACT ON ACT.CO_PLAN = PL.CO_PLAN
                                             LEFT JOIN I031T_EJECUCION EJUS ON EJUS.VA_PROCEDIMIENTO = PRP.VA_PROCEDIMIENTO AND PRP.CO_PLAN = EJUS.CO_PLAN AND EJUS.CO_ACTIVIDAD = ACT.CO_ACTIVIDAD
                                             WHERE ACT.CO_ACTIVIDAD =  $co_actividad"
              . "                            ORDER BY UNU_NODO");

        return $this->rs;
    }

    function SelEjecutores($co_actividad, $co_usuario, $va_procedimiento) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT US.NB_INDICADOR,
                                             US.NB_NOMBRE,
                                             US.NU_CEDULA,
                                             EJ.FE_EJECUCION
                                        FROM I031T_EJECUCION EJ 
                                        INNER JOIN I004T_USUARIO US ON US.CO_USUARIO = EJ.CO_USUARIO  
                                        INNER JOIN I022T_ACTIVIDAD ACT  ON ACT.CO_ORDEN = EJ.CO_ORDEN
                                        WHERE ACT.CO_ACTIVIDAD = $co_actividad AND EJ.VA_PROCEDIMIENTO = '$va_procedimiento'
                                                AND EJ.CO_USUARIO <> $co_usuario");
        return $this->rs;
    }

    function ActualizaEjecucion($co_actividad, $co_usuario, $fe_inicio, $hh_total_horas_hombre, $hh_viaje, $fe_fin, $tx_notas, $conjunto_procedimientos, $arrayEjecucion) {

        $trans = true;

        $this->MySQL->BeginTrans();
        try {
            //* Elimina los procedimientos 
            $this->MySQL->ExecuteSQL("DELETE FROM I031T_EJECUCION 
                                      WHERE CO_ACTIVIDAD =$co_actividad");

            //* Inserta los nuevos procedimientos anexos
            for ($i = 0; $i < count($arrayEjecucion); $i++) {
                $this->MySQL->ExecuteSQL("INSERT INTO I031T_EJECUCION(VA_PROCEDIMIENTO,CO_ORDEN,CO_PLAN,CO_ACTIVIDAD,
                                                                  FE_EJECUCION,CO_USUARIO) 
                                          SELECT '{$arrayEjecucion[$i]['VA_PROCEDIMIENTO']}',ACT.CO_ORDEN, ACT.CO_PLAN, ACT.CO_ACTIVIDAD,
                                                 STR_TO_DATE('{$arrayEjecucion[$i]['FE_EJECUCION']}', '%d/%m/%Y'), $co_usuario 
                                          FROM  I022T_ACTIVIDAD As ACT
                                          WHERE ACT.CO_ACTIVIDAD = {$co_actividad}");
            }

            //* Actualiza los datos actividad
            $this->MySQL->ExecuteSQL("UPDATE I022T_ACTIVIDAD SET 
                                            FE_INICIO=STR_TO_DATE('{$fe_inicio}', '%d/%m/%Y'),
                                            FE_FIN=STR_TO_DATE('{$fe_fin}', '%d/%m/%Y'),
                                            HH_TOTAL_HORAS_HOMBRE=SEC_TO_TIME(TIME_TO_SEC(HH_TOTAL_HORAS_HOMBRE) + TIME_TO_SEC('{$hh_total_horas_hombre}')),
                                            HH_VIAJE=SEC_TO_TIME(TIME_TO_SEC(HH_VIAJE) + TIME_TO_SEC('{$hh_viaje}')),
                                            TX_OBSERVACION = '{$tx_notas}'
                                        WHERE CO_ACTIVIDAD = {$co_actividad}");

            //* Actualiza los procentajes de Ejecucion de la Actividad 
            $this->MySQL->ExecuteSQL("UPDATE C004T_PLAN_ORDEN As PRO
                                        INNER JOIN 
                                        (SELECT  PL.VA_HOJARUTA,PL.CO_PLAN,
                                        SUM((SELECT CASE COUNT(EJ.VA_PROCEDIMIENTO) WHEN 0 THEN 0 ELSE 1 END 
                                        FROM I031T_EJECUCION EJ
                                        WHERE EJ.CO_PLAN = PRL.CO_PLAN AND EJ.VA_PROCEDIMIENTO = PRL.VA_PROCEDIMIENTO)) /
                                        COUNT(PRL.VA_PROCEDIMIENTO) * 100.0 As PERCENT_ACTIV  
                                        FROM I020T_PROCEDIMIENTO_PLAN PRL  
                                        INNER JOIN I037T_PLAN PL ON PRL.CO_PLAN = PL.CO_PLAN
                                        WHERE PL.CO_PLAN = (SELECT CO_PLAN FROM I022T_ACTIVIDAD WHERE CO_ACTIVIDAD = {$co_actividad})
                                        GROUP BY PL.VA_HOJARUTA,PL.CO_PLAN) As UPD_PLAN ON PRO.CO_PLAN = UPD_PLAN.CO_PLAN
                                        SET PRO.PC_AVANCE = UPD_PLAN.PERCENT_ACTIV");
            //* Actualiza a los usuarios con estado de en ejecucion y asignado a orden
            $this->MySQL->ExecuteSQL("UPDATE I022T_ACTIVIDAD  As ACT
                                        INNER JOIN (SELECT  * FROM C004T_PLAN_ORDEN 
                                                    WHERE CO_PLAN = (SELECT CO_PLAN FROM I022T_ACTIVIDAD WHERE CO_ACTIVIDAD = {$co_actividad})) As PO
                                        ON PO.CO_PLAN = ACT.CO_PLAN 
                                        SET ACT.NU_PORCENTAJE_AVANCE = PO.PC_AVANCE
                                       WHERE CO_ESTATUS IN ('1','2')");
            //* Actualiza el porcentaje de la orden
            $this->MySQL->ExecuteSQL("UPDATE I036T_ORDEN As ORDEN 
                                       INNER JOIN 
                                       (SELECT  ORN.CO_ORDEN,
                                            SUM((SELECT CASE COUNT(EJ.VA_PROCEDIMIENTO) WHEN 0 THEN 0 ELSE 1 END
                                                 FROM I031T_EJECUCION EJ
                                                 WHERE EJ.CO_PLAN = PRL.CO_PLAN AND EJ.VA_PROCEDIMIENTO = PRL.VA_PROCEDIMIENTO))/
                                            COUNT(PRL.VA_PROCEDIMIENTO) * 100.0 As PERCENT_ORDEN
                                        FROM I020T_PROCEDIMIENTO_PLAN PRL  
                                          INNER JOIN I037T_PLAN PL ON PRL.CO_PLAN = PL.CO_PLAN
                                          INNER JOIN C004T_PLAN_ORDEN PLO ON PLO.CO_PLAN = PL.CO_PLAN
                                          INNER JOIN I036T_ORDEN ORN ON ORN.CO_ORDEN = PLO.CO_ORDEN
                                        WHERE ORN.CO_ORDEN = (SELECT CO_ORDEN FROM I022T_ACTIVIDAD WHERE CO_ACTIVIDAD = {$co_actividad})
                                        GROUP BY ORN.CO_ORDEN) As UPD_ORDEN ON ORDEN.CO_ORDEN = UPD_ORDEN.CO_ORDEN
                                        SET ORDEN.PC_AVANCE = UPD_ORDEN.PERCENT_ORDEN");
        } catch (ADODB_Exception $e) {
            $trans = false;
            $this->MySQL->RollbackTrans();
        } catch (Exception $e) {
            $trans = false;
            $this->MySQL->RollbackTrans();
        }

        $this->MySQL->CommitTrans();
        return $trans;
    }

    public function SelecOrdenesActivas($co_usuario, $fecha) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT CO_PLAN,VA_DETALLE,
                                                   VA_HOJARUTA,FE_ULTIMO,
                                                   NU_SEMANA,FE_PROXIMA_PLAN,
                                                   NU_SEMANA_PROXIMA,CO_ESTATUS,
                                                   VA_TIPO_PLAN, CO_ESTATUS_ORDEN,
                                                   CO_ORDEN,FE_ASIG_ORDEN,
                                                   NU_ORDEN,PC_AVANCE_ORDEN, PC_AVANCE_ACTIVIDAD,
                                                   TX_HOJARUTA
                                         FROM V002_ORDENES
                                         WHERE CO_GRP_PLAN IN (SELECT CO_GRUPO_PLAN
                                                                FROM C002T_CARGO_GRUPO_PLAN GP
                                                                 INNER JOIN I004T_USUARIO US ON GP.NU_CARGO = US.NU_CARGO
                                                                 WHERE CO_USUARIO={$co_usuario})
                                                     AND FE_ASIG_ORDEN <= STR_TO_DATE('{$fecha->format('d/m/Y')}','%d/%m/%Y');");
        return $this->rs;
    }

    public function SelecOrdenesR($fechai, $fechaf, $estatus) {
        if($estatus != '-1' && $fechai != '' && $fechaf != ''){
        $this->rs = $this->MySQL->ExecuteSQL("SELECT  PL.CO_PLAN AS CO_PLAN,  PL.VA_DETALLE AS VA_DETALLE,  
                                                      PL.VA_HOJARUTA AS VA_HOJARUTA,  PL.FE_ULTIMO AS FE_ULTIMO,  
                                                      PL.NU_SEMANA AS NU_SEMANA,  PL.FE_PROXIMA_PLAN AS FE_PROXIMA_PLAN,  
                                                      PL.NU_SEMANA_PROXIMA AS NU_SEMANA_PROXIMA,  PL.CO_ESTATUS AS CO_ESTATUS,  
                                                      PL.VA_TIPO_PLAN AS VA_TIPO_PLAN,  PL.CO_ESTATUS_ORDEN AS CO_ESTATUS_ORDEN,  
                                                      PL.TX_HOJARUTA AS TX_HOJARUTA,  PL.CO_GRP_PLAN AS CO_GRP_PLAN,  
                                                      PR.PC_AVANCE AS PC_AVANCE_ACTIVIDAD,  PR.CO_ORDEN AS CO_ORDEN,  
                                                      PR.FE_ASIG_ORDEN AS FE_ASIG_ORDEN,  ORN.NU_ORDEN AS NU_ORDEN,  
                                                      COALESCE(ORN.PC_AVANCE,0.00) AS PC_AVANCE_ORDEN,  ORN.FE_ASIGNACION AS FE_ASIGNACION,
                                                      ORN.CO_ESTATUS_ORDEN AS ESTATUS_ORDEN, ORN.TX_ESTATUS, EO.NB_CO_ESTATUS_ORDEN
                                                FROM (((I037T_PLAN PL  JOIN C004T_PLAN_ORDEN PR  ON ((PL.CO_PLAN = PR.CO_PLAN)))  
                                                     JOIN I036T_ORDEN ORN  ON ((ORN.CO_ORDEN = PR.CO_ORDEN)))
                                                     JOIN I025T_ESTATUS_ORDEN EO ON ((EO.CO_ESTATUS_ORDEN = ORN.CO_ESTATUS_ORDEN)))
                                                WHERE ORN.TX_ESTATUS = '{$estatus}'
                                                     AND PR.FE_ASIG_ORDEN >= STR_TO_DATE('{$fechai->format('d/m/Y')}','%d/%m/%Y')
                                                     AND PR.FE_ASIG_ORDEN <= STR_TO_DATE('{$fechaf->format('d/m/Y')}','%d/%m/%Y');");
        }
        else{
            if($estatus != '-1'){
                $this->rs = $this->MySQL->ExecuteSQL("SELECT  PL.CO_PLAN AS CO_PLAN,  PL.VA_DETALLE AS VA_DETALLE,  
                                                      PL.VA_HOJARUTA AS VA_HOJARUTA,  PL.FE_ULTIMO AS FE_ULTIMO,  
                                                      PL.NU_SEMANA AS NU_SEMANA,  PL.FE_PROXIMA_PLAN AS FE_PROXIMA_PLAN,  
                                                      PL.NU_SEMANA_PROXIMA AS NU_SEMANA_PROXIMA,  PL.CO_ESTATUS AS CO_ESTATUS,  
                                                      PL.VA_TIPO_PLAN AS VA_TIPO_PLAN,  PL.CO_ESTATUS_ORDEN AS CO_ESTATUS_ORDEN,  
                                                      PL.TX_HOJARUTA AS TX_HOJARUTA,  PL.CO_GRP_PLAN AS CO_GRP_PLAN,  
                                                      PR.PC_AVANCE AS PC_AVANCE_ACTIVIDAD,  PR.CO_ORDEN AS CO_ORDEN,  
                                                      PR.FE_ASIG_ORDEN AS FE_ASIG_ORDEN,  ORN.NU_ORDEN AS NU_ORDEN,  
                                                      COALESCE(ORN.PC_AVANCE,0.00) AS PC_AVANCE_ORDEN,  ORN.FE_ASIGNACION AS FE_ASIGNACION,
                                                      ORN.CO_ESTATUS_ORDEN AS ESTATUS_ORDEN, ORN.TX_ESTATUS, EO.NB_CO_ESTATUS_ORDEN
                                                FROM (((I037T_PLAN PL  JOIN C004T_PLAN_ORDEN PR  ON ((PL.CO_PLAN = PR.CO_PLAN)))  
                                                     JOIN I036T_ORDEN ORN  ON ((ORN.CO_ORDEN = PR.CO_ORDEN)))
                                                     JOIN I025T_ESTATUS_ORDEN EO ON ((EO.CO_ESTATUS_ORDEN = ORN.CO_ESTATUS_ORDEN)))
                                                WHERE ORN.TX_ESTATUS = '{$estatus}';");
            }
            else{
                if($fechai != '' && $fechaf != ''){
                   $this->rs = $this->MySQL->ExecuteSQL("SELECT  PL.CO_PLAN AS CO_PLAN,  PL.VA_DETALLE AS VA_DETALLE,  
                                                      PL.VA_HOJARUTA AS VA_HOJARUTA,  PL.FE_ULTIMO AS FE_ULTIMO,  
                                                      PL.NU_SEMANA AS NU_SEMANA,  PL.FE_PROXIMA_PLAN AS FE_PROXIMA_PLAN,  
                                                      PL.NU_SEMANA_PROXIMA AS NU_SEMANA_PROXIMA,  PL.CO_ESTATUS AS CO_ESTATUS,  
                                                      PL.VA_TIPO_PLAN AS VA_TIPO_PLAN,  PL.CO_ESTATUS_ORDEN AS CO_ESTATUS_ORDEN,  
                                                      PL.TX_HOJARUTA AS TX_HOJARUTA,  PL.CO_GRP_PLAN AS CO_GRP_PLAN,  
                                                      PR.PC_AVANCE AS PC_AVANCE_ACTIVIDAD,  PR.CO_ORDEN AS CO_ORDEN,  
                                                      PR.FE_ASIG_ORDEN AS FE_ASIG_ORDEN,  ORN.NU_ORDEN AS NU_ORDEN,  
                                                      COALESCE(ORN.PC_AVANCE,0.00) AS PC_AVANCE_ORDEN,  ORN.FE_ASIGNACION AS FE_ASIGNACION,
                                                      ORN.CO_ESTATUS_ORDEN AS ESTATUS_ORDEN, ORN.TX_ESTATUS, EO.NB_CO_ESTATUS_ORDEN
                                                FROM (((I037T_PLAN PL  JOIN C004T_PLAN_ORDEN PR  ON ((PL.CO_PLAN = PR.CO_PLAN)))  
                                                     JOIN I036T_ORDEN ORN  ON ((ORN.CO_ORDEN = PR.CO_ORDEN)))
                                                     JOIN I025T_ESTATUS_ORDEN EO ON ((EO.CO_ESTATUS_ORDEN = ORN.CO_ESTATUS_ORDEN)))
                                                WHERE PR.FE_ASIG_ORDEN >= STR_TO_DATE('{$fechai->format('d/m/Y')}','%d/%m/%Y')
                                                     AND PR.FE_ASIG_ORDEN <= STR_TO_DATE('{$fechaf->format('d/m/Y')}','%d/%m/%Y');"); 
                }
                else{
                    $this->rs = $this->MySQL->ExecuteSQL("SELECT  PL.CO_PLAN AS CO_PLAN,  PL.VA_DETALLE AS VA_DETALLE,  
                                                      PL.VA_HOJARUTA AS VA_HOJARUTA,  PL.FE_ULTIMO AS FE_ULTIMO,  
                                                      PL.NU_SEMANA AS NU_SEMANA,  PL.FE_PROXIMA_PLAN AS FE_PROXIMA_PLAN,  
                                                      PL.NU_SEMANA_PROXIMA AS NU_SEMANA_PROXIMA,  PL.CO_ESTATUS AS CO_ESTATUS,  
                                                      PL.VA_TIPO_PLAN AS VA_TIPO_PLAN,  PL.CO_ESTATUS_ORDEN AS CO_ESTATUS_ORDEN,  
                                                      PL.TX_HOJARUTA AS TX_HOJARUTA,  PL.CO_GRP_PLAN AS CO_GRP_PLAN,  
                                                      PR.PC_AVANCE AS PC_AVANCE_ACTIVIDAD,  PR.CO_ORDEN AS CO_ORDEN,  
                                                      PR.FE_ASIG_ORDEN AS FE_ASIG_ORDEN,  ORN.NU_ORDEN AS NU_ORDEN,  
                                                      COALESCE(ORN.PC_AVANCE,0.00) AS PC_AVANCE_ORDEN,  ORN.FE_ASIGNACION AS FE_ASIGNACION,
                                                      ORN.CO_ESTATUS_ORDEN AS ESTATUS_ORDEN, ORN.TX_ESTATUS, EO.NB_CO_ESTATUS_ORDEN
                                                FROM (((I037T_PLAN PL  JOIN C004T_PLAN_ORDEN PR  ON ((PL.CO_PLAN = PR.CO_PLAN)))  
                                                     JOIN I036T_ORDEN ORN  ON ((ORN.CO_ORDEN = PR.CO_ORDEN)))
                                                     JOIN I025T_ESTATUS_ORDEN EO ON ((EO.CO_ESTATUS_ORDEN = ORN.CO_ESTATUS_ORDEN)));");
                }
            }
        }
        return $this->rs;
    }
    
    /*
     * Obtiene los datos de una orden
     */

    function SelecDatosOrden($co_orden) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT CO_PLAN,VA_DETALLE,
                                                   VA_HOJARUTA,FE_ULTIMO,
                                                   NU_SEMANA,FE_PROXIMA_PLAN,
                                                   NU_SEMANA_PROXIMA,CO_ESTATUS,
                                                   VA_TIPO_PLAN, CO_ESTATUS_ORDEN,
                                                   CO_ORDEN,FE_ASIG_ORDEN, FE_ASIGNACION,
                                                   NU_ORDEN,PC_AVANCE_ORDEN, PC_AVANCE_ACTIVIDAD,
                                                   TX_HOJARUTA
                                         FROM V002_ORDENES 
                                         WHERE CO_ORDEN={$co_orden}");
        return $this->rs;
    }
    
    /*
     * Obtiene los motivos de cierre/aplazamiento/omision de actividades
     */

    function SelectMotivoCierre($_co_tipo_cierre) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT J041T.NU_MOTIVO_CIERRE,
                                                     J041T.TX_DESCRIPCION
                                             FROM J041T_MOTIVO_CIERRE J041T
                                             WHERE J041T.CO_TIPO = ".$_co_tipo_cierre."
                                             AND J041T.IN_ACTIVO = TRUE");
        return $this->rs;
    }
    

    /**
     * Cierra una actividad
     */
    public function CerrarActividad($co_actividad, $motivo_Cierre) {
        $bres = (boolean) $this->MySQL->ExecuteSQL("UPDATE I022T_ACTIVIDAD SET CO_ESTATUS=3,
                                                                               NU_MOTIVO_CIERRE={$motivo_Cierre} 
                                                    WHERE CO_ACTIVIDAD={$co_actividad}");
                                                    
        return $bres;
    }

    /*
     * Usado en cerrar Orden para preparar el arreglo que almacena los procedimientos
     */

    private function ConvertirArregloProc($arrayProcedimiento) {
        $arrayProcPlan = array();
        //---- Procedimientos
        for ($i = 0, $j = 0; $i < count($arrayProcedimiento); $i++, $j++) {
            $arrayProcPlan[$j]["VA_PROCEDIMIENTO"] = $arrayProcedimiento[$i]['PLNNR'] . "-" . $arrayProcedimiento[$i]['PLNAL'] . "-" . $arrayProcedimiento[$i]['PLNKN'];
            $arrayProcPlan[$j]["NB_TEXTO"] = $arrayProcedimiento[$i]['LTXA1'];
            $arrayProcPlan[$j]["NU_POSICION"] = $i + 1;
            $arrayProcPlan[$j]["NU_NODO"] = $arrayProcedimiento[$i]['PLNKN'];
            $arrayProcPlan[$j]["ANZZL"] = $arrayProcedimiento[$i]['ANZZL'];
        }
        return $arrayProcPlan;
    }

    /*
     * Cierra la orden y actualiza las notas
     */

    public function CerrarOrden($co_orden, $tx_notas) {
       
        $trans = true;
        $this->MySQL->BeginTrans();
        
        try {
            /* Cierra la orden */
            $bres = (boolean) $this->MySQL->ExecuteSQL("UPDATE I036T_ORDEN 
                                                      SET TX_ESTATUS = 3,
                                                          TX_MOTIVO='{$tx_notas}'
                                                     WHERE CO_ORDEN={$co_orden}");
                                                     
            if (!$bres)  throw new Exception("No se puede actualizar el estado de la orden");
            

            /* Actualiza las fechas de cierre */
            $this->MySQL->ExecuteSQL("UPDATE I022T_ACTIVIDAD ACT
                                        SET ACT.FE_FIN = (SELECT MAX(EJ.FE_EJECUCION) FROM I031T_EJECUCION EJ WHERE EJ.CO_ACTIVIDAD=ACT.CO_ACTIVIDAD)                                         
                                     WHERE ACT.FE_FIN='00-00-0000' AND ACT.FE_INICIO != '00-00-0000'
                                       AND ACT.CO_ORDEN={$co_orden}");

            /* Actualiza las actividades a cerradas */
            $this->MySQL->ExecuteSQL("UPDATE I022T_ACTIVIDAD SET CO_ESTATUS = 3 
                                     WHERE CO_ORDEN={$co_orden}");
            /* Actualiza el plan a cerrado */
            $this->MySQL->ExecuteSQL("UPDATE I037T_PLAN SET CO_ESTATUS = 3 
                                     WHERE CO_PLAN IN 
                                         (SELECT CO_PLAN FROM C004T_PLAN_ORDEN WHERE CO_ORDEN = {$co_orden})");

            /* ====================== RECALCULAR NUEVO PLAN CICLICO ==================== */
            $lstNivelPlanCiclo = "'" . str_replace(",", "','", NIVEL_PLAN_CICLO) . "'";
            $rsPlanes = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                      PL.VA_HOJARUTA,
                                                      PL.VA_TIPO_PLAN,
                                                      PL.VA_DETALLE,
                                                      PL.FE_PROXIMA_PLAN,
                                                      PL.NU_SEMANA_PROXIMA
                                               FROM I037T_PLAN PL
                                                  INNER JOIN C004T_PLAN_ORDEN POR ON POR.CO_PLAN = PL.CO_PLAN
                                               WHERE LEFT(PL.TX_HOJARUTA,3) IN ({$lstNivelPlanCiclo})
                                                 AND POR.CO_ORDEN = {$co_orden}");
                                                 
              $objInventario = new inventarioSAP();
              $objPlan = new plan();
              
            while (!$rsPlanes->EOF) {
                                        
                //* Obtiene el codigo del objeto tecnico
                $ObjetoTecnico = $rsPlanes->fields['VA_DETALLE'];

                //* Obtiene la Hoja de Ruta y otros datos del SGE
                list($coGrupoHojaRuta,$coNivelHojaRuta) = explode("-",$rsPlanes->fields['VA_HOJARUTA']);
                if ($rsPlanes->fields['VA_TIPO_PLAN'] == 'E') {
                    $arrayDatos = $objInventario->SelecDetalleEqpSAP($ObjetoTecnico);
                    $arrayHojaRuta = $objInventario->SelectHojaRutaEquipo($ObjetoTecnico,$coNivelHojaRuta);
                } else {
                    $arrayDatos = $objInventario->SelecUbicacionTecnica($ObjetoTecnico);
                    $arrayHojaRuta = $objInventario->SelectHojaRutaInstalacion($ObjetoTecnico,$coNivelHojaRuta);
                }
                
                //* Asigna el nombre de la Hoja de Ruta
                $txHojaRuta = (isset($arrayHojaRuta)) ? $txHojaRuta = $arrayHojaRuta[1]['KTEXT'] : "";     
                
                //* Prepara el arreglo para replicarse del SGE en la BD
                $arrayProcPlan = $this->ConvertirArregloProc($arrayHojaRuta);
                
                //* Obtiene los dias de Frecuencia para calcular
                $dias = $objPlan->getDiasFrecMtto($ObjetoTecnico, $coGrupoHojaRuta, $coNivelHojaRuta, $arrayHojaRuta);
                                         
                //* Se verifica si se ejecut� alguna actividad planificada asociada a la orden
                $fechaEjecPlan = null;
                $rsPlanEjecutado =  $this->MySQL->ExecuteSQL("SELECT MAX(I031.FE_EJECUCION) AS FE_EJECUCION
                                                              FROM   I022T_ACTIVIDAD I022,
                                                                     I031T_EJECUCION I031
                                                              WHERE  I031.CO_ORDEN     	= I022.CO_ORDEN
                                                              AND    I031.CO_ACTIVIDAD 	= I022.CO_ACTIVIDAD 
                                                              AND    I022.CO_PLAN 	= {$rsPlanes->fields['CO_PLAN']}"); 
                                                              
                if(!$rsPlanEjecutado->EOF){
                    ($rsPlanEjecutado->fields['FE_EJECUCION'] != null) ? $fechaEjecPlan = $rsPlanEjecutado->fields['FE_EJECUCION'] : null;
                    $rsPlanEjecutado->MoveNext();
                }
                                
                if($fechaEjecPlan){
                    //Calcula la fecha de mantenimiento tomando en cuenta la �ltima ejecuci�n de actividad (avance diferente a 0%)
                    $fecha_mtto = new DateTime($fechaEjecPlan);
                    $ns_mtto =  $fecha_mtto->format("W-Y");
                } else {
                    //* Ubica la ultima fecha de mantenimiento en el plan y se asigna a la anterior
                    $fecha_mtto = new DateTime($rsPlanes->fields['FE_PROXIMA_PLAN']);
                    $ns_mtto = $rsPlanes->fields['NU_SEMANA_PROXIMA'];
                }
              
                //* Se calcula el proximo mantenimiento considerando los dias
                $fecha_proximo = clone $fecha_mtto;
                $fecha_proximo->add(new DateInterval("P" . $dias . "D"));
                $ns_proximo = $fecha_proximo->format("W-Y");

                //*----- Ejecuta la insersion ------
                if ($this->MySQL->ExecuteSQL("INSERT INTO I037T_PLAN(VA_DETALLE,
                                                                     VA_HOJARUTA,
                                                                     TX_HOJARUTA,
                                                                     CO_TIPO_ASIGNACION,
                                                                     FE_ULTIMO,
                                                                     NU_SEMANA,
                                                                     FE_PROXIMA_PLAN,
                                                                     NU_SEMANA_PROXIMA,
                                                                     CO_ESTATUS,
                                                                     VA_TIPO_PLAN,
                                                                     CO_UBIC_TEC,
                                                                     CO_GRP_PLAN,
                                                                     CO_EJEC_PLAN,
                                                                     CO_ESTATUS_ORDEN) 
                                                            VALUES ('{$ObjetoTecnico}',
                                                                    '{$rsPlanes->fields['VA_HOJARUTA']}',
                                                                    '{$txHojaRuta}',    
                                                                    '1',
                                                                    '{$fecha_mtto->format("Y-m-d")}',
                                                                    '{$ns_mtto}',
                                                                    '{$fecha_proximo->format("Y-m-d")}',
                                                                    '{$ns_proximo}',
                                                                    '0',
                                                                    '{$rsPlanes->fields['VA_TIPO_PLAN']}',
                                                                    '{$arrayDatos[1]['TPLNR']}',
                                                                    '{$arrayDatos[1]['INGRP']}',
                                                                    '{$arrayDatos[1]['GEWRK']}',
                                                                    '0')") === false)
                    throw new Exception("No se puede crear el plan para los planes por programacion y ciclico");
                                             
                //*------ Obtiene el ID del ultimo plan insertado ----
                $last_co_plan= $this->MySQL->LastInsertId("I037T_PLAN");
                
                //*-------- Replica los procedimientos de SGE -------
                $countArray = count($arrayProcPlan);
                for ($k = 0; $k < $countArray; $k++) {
                    $bres = (boolean) $this->MySQL->ExecuteSQL("INSERT INTO I020T_PROCEDIMIENTO_PLAN(CO_PLAN,
                                                                                                     VA_PROCEDIMIENTO,
                                                                                                     NB_TEXTO,
                                                                                                     NU_POSICION,
                                                                                                     NU_NODO,
                                                                                                     CAPACIDAD)
                                                                                            VALUES ({$last_co_plan},
                                                                                                   '{$arrayProcPlan[$k]['VA_PROCEDIMIENTO']}',
                                                                                                   '{$arrayProcPlan[$k]['NB_TEXTO']}',
                                                                                                   '{$arrayProcPlan[$k]['NU_POSICION']}',
                                                                                                   '{$arrayProcPlan[$k]['NU_NODO']}',
                                                                                                   '{$arrayProcPlan[$k]['ANZZL']}');");
                                                                                                   
                    if (!$bres) throw new Exception("No se puede continuar cargando procedimientos");
                }
                $rsPlanes->MoveNext();
            }

        //*************************************************************************
        } catch (ADODB_Exception $e) {
            $trans = false;
            $this->MySQL->RollbackTrans();
        } catch (Exception $e) {
            $trans = false;
            $this->MySQL->RollbackTrans();
        }

        $this->MySQL->CommitTrans();
        return $trans;
    }
    
    function SelFormatoActividad($co_actividad) {
       $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN AS CO_PLAN,
                                                PL.VA_DETALLE AS VA_DETALLE,
                                                PL.VA_HOJARUTA AS VA_HOJARUTA,
                                                PL.FE_ULTIMO AS FE_ULTIMO,
                                                PL.NU_SEMANA AS NU_SEMANA,
                                                PL.FE_PROXIMA_PLAN AS FE_PROXIMA_PLAN,
                                                PL.NU_SEMANA_PROXIMA AS NU_SEMANA_PROXIMA,
                                                PL.CO_ESTATUS AS CO_ESTATUS,
                                                PL.VA_TIPO_PLAN AS VA_TIPO_PLAN,
                                                PL.CO_ESTATUS_ORDEN AS CO_ESTATUS_ORDEN,
                                                PL.TX_HOJARUTA AS TX_HOJARUTA,
                                                PL.CO_UBIC_TEC AS CO_UBIC_TEC,
                                                PR.CO_ORDEN AS CO_ORDEN,
                                                PR.FE_ASIG_ORDEN AS FE_ASIG_ORDEN,
                                                ORN.NU_ORDEN AS NU_ORDEN,
                                                COALESCE(ORN.PC_AVANCE,0.00) AS PC_AVANCE,
                                                ACT.CO_USUARIO AS CO_USUARIO,
                                                ACT.CO_ACTIVIDAD AS CO_ACTIVIDAD,
                                                ACT.FE_ASIGNACION AS FE_ASIGNACION,
                                                ACT.NU_PORCENTAJE_AVANCE AS NU_PORCENTAJE_AVANCE,
                                                ACT.FE_INICIO AS FE_INICIO,
                                                ACT.FE_FIN AS FE_FIN,
                                                ACT.HH_TOTAL_HORAS_HOMBRE AS HH_TOTAL_HORAS_HOMBRE,
                                                ACT.HH_VIAJE AS HH_VIAJE,
                                                ACT.TX_OBSERVACION AS TX_OBSERVACION, 
                                                ACT.NU_MOTIVO_CIERRE AS NU_MOTIVO_CIERRE
                                            FROM (((I037T_PLAN PL JOIN C004T_PLAN_ORDEN PR ON((PL.CO_PLAN = PR.CO_PLAN))) 
                                                JOIN I036T_ORDEN ORN ON((ORN.CO_ORDEN = PR.CO_ORDEN))) 
                                                JOIN I022T_ACTIVIDAD ACT ON(((ACT.CO_ORDEN = ORN.CO_ORDEN) 
                                                    AND (ACT.CO_PLAN = PR.CO_PLAN)))) 
                                            WHERE (ACT.CO_ACTIVIDAD =".$co_actividad.")");
        return $this->rs;
    }
    
    function SelMantSuperv($co_usuario) {
       $this->rs = $this->MySQL->ExecuteSQL("SELECT USU.NB_INDICADOR,SUPER.NB_INDICADOR_SUPER
                                               FROM I004T_USUARIO USU INNER JOIN I005T_CARGO CGO ON CGO.NU_CARGO = USU.NU_CARGO
                                                 LEFT JOIN (SELECT GROUP_CONCAT(USUP.NB_INDICADOR) AS NB_INDICADOR_SUPER, CGOP.CO_CARGO
                                                              FROM I004T_USUARIO AS USUP
                                                              INNER JOIN I005T_CARGO AS CGOP ON CGOP.NU_CARGO = USUP.NU_CARGO
                                                              GROUP BY CGOP.CO_CARGO) AS SUPER ON SUPER.CO_CARGO = CGO.CO_CARGO_PADRE
                                               WHERE USU.CO_USUARIO = {$co_usuario}");
        return $this->rs;
    }
    
    function SelMotivoCierre($nu_motivo) {
       $this->rs = $this->MySQL->ExecuteSQL("SELECT TX_DESCRIPCION
                                               FROM J041T_MOTIVO_CIERRE
                                               WHERE NU_MOTIVO_CIERRE = {$nu_motivo}");
        return $this->rs;
    }
    
    function SelEjecucion($nu_procedimiento, $co_actividad) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT FE_EJECUCION
                                               FROM I031T_EJECUCION
                                               WHERE VA_PROCEDIMIENTO = '{$nu_procedimiento}'
                                                 AND CO_ACTIVIDAD = {$co_actividad}");
        return $this->rs;
    }
  
} // FIN ejecucion.class

?>
