<?php

class Cambioclave {
    private $MySQL = null;   

    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQL->ConnectMySQL();
    }

    public function __destruct() {
        $this->MySQL->CloseConnect();
    }

    //===============  METODOS PUBLICOS =================   
    public function ActClaveAdmin($co_usuario,$clave){
       $bres=(boolean) $this->MySQL->ExecuteSQL("UPDATE I004T_USUARIO SET TX_CLAVE = '{$clave}' WHERE CO_USUARIO={$co_usuario}");       
       return $bres;                
    }  
     
}

?>
