<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of carga_masiva
 *
 * @author portilloga
 */
//require_once('../aplicacion/librerias/ClassesPHPExcel/PHPExcel.php');
//require_once('../aplicacion/librerias/ClassesPHPExcel/PHPExcel/Reader/Excel5.php');
class CargaMasiva {

    private $MySQL = null;
    private $rs = null;
    private $esTmpGrpPto = false;

    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQL->ConnectMySQL();
    }

    public function __destruct() {
        $this->MySQL->CloseConnect();
    }

    //===============  METODOS PUBLICOS =================

    public function query($sql) {
        $this->rs = $this->MySQL->ExecuteSQL($sql);        
    }


}

?>
