<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of plan
 *
 * @author portilloga
 */
class plan {

    private $MySQL = null;
    private $rs = null;
    private $coUltimoPlan = 0;
    private $coUltimaOrden = 0;
    private $nuUltimaOrden = 0;
    private $tipoMtto = null;
    private $valorbuscado = "";

    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQL->ConnectMySQL();
    }

    public function __destruct() {
        $this->MySQL->CloseConnect();
    }

    /*
     * 
     */

    private function setCoUltimoPlan() {
        $sql = "SELECT MAX(CO_PLAN) as CO_ULTIMO_PLAN
               FROM I037T_PLAN";
        $tmp = $this->MySQL->ExecuteSQL($sql);
        $this->coUltimoPlan = $tmp->fields['CO_ULTIMO_PLAN'];
    }

    /*
     * 
     */

    private function setCoNuUltimaOrden() {
        $sql = "SELECT max(CO_ORDEN) as MAX_CO_ORDEN
               FROM I036T_ORDEN";

        $tmp = $this->MySQL->ExecuteSQL($sql);

        $this->coUltimaOrden = $tmp->fields['MAX_CO_ORDEN'];
        $this->nuUltimaOrden = (int) $tmp->fields['MAX_CO_ORDEN'] + 1000000000;
    }

    /*
     *  Ejecuta una transaccion
     */

    private function ejecutaTransaccion($sentencia) {
        $f = true;
        $this->MySQL->BeginTrans();
        try {
            $this->MySQL->ExecuteSQL($sentencia);
        } catch (ADODB_Exception $e) {
            $f = false;
        }//return false; }
        catch (Exception $e) {
            $this->MySQL->RollbackTrans();
            $f = false;
//            return false;
        }
        $this->MySQL->CommitTrans();
        return $f;
    }

    //===============  METODOS PUBLICOS =================
    /*
     * Obtiene el codigo del Ultimo Plan Insertado
     */
    public function getCoUltimoPlan() {
        return $this->coUltimoPlan;
    }

    /*
     * Asigna el codigo del Ultimo Plan Insertado
     */

    public function getCoUltimaOrden() {
        return $this->coUltimaOrden;
    }

    /*
     *  Excluye las hojas de rutas asignadas a un plan
     */

    public function ExcluirHojasRutas($co_detalle, $resFilter) {
        //* Obtiene los planes activoas para verificar si estan asignados
        $rsHojasAsignadas = $this->MySQL->ExecuteSQL("SELECT VA_HOJARUTA FROM I037T_PLAN 
                                                      WHERE VA_DETALLE ='{$co_detalle}' 
                                                         AND CO_ESTATUS NOT IN (3)");
        //----- Excluye las hojas de rutas asignadas ------
        $resResult = array_values($resFilter);
        while (!$rsHojasAsignadas->EOF) {
            /* Elimina el asignado */
            foreach ($resResult as $key => $value) {
                if (($rsHojasAsignadas->fields['VA_HOJARUTA'] == sprintf("%s-%s", $value['PLNNR'], $value['PLNAL']))) {
                    array_splice($resResult, $key, 1, array());
                    break;
                }
            }
            $rsHojasAsignadas->MoveNext();
        }
        //----- Revisa Excluir aquellas que no usa el sistema
        foreach ($resResult as $key => $value) {
            if (PalabraExisteInicio($value['KTEXT'], explode(",", TIPO_NIVEL_PLAN)) === false)
                array_splice($resResult, $key, 1, array());
        }


        return $resResult;
    }

    /*
     * Obtiene las Hojas de Rutas de un Equipo
     */

    public function getHojaRutaEquipo($coEquipo, $objEquipo) {
        $j = 0;
        $res = array();
        $arrDatosHojaRuta = $objEquipo->SelectHojaRutaEquipo($coEquipo);

        for ($i = 1; $i <= count($arrDatosHojaRuta); $i++) {
            if (!$res || $arrDatosHojaRuta[$i]['PLNAL'] != $res[$j]['PLNAL']) {
                $j = $j + 1;                
                $res[$j]['PLNAL']     = $arrDatosHojaRuta[$i]['PLNAL']; //CODIGO HOJAS DE RUTA
                $res[$j]['PLNNR']     = $arrDatosHojaRuta[$i]['PLNNR']; //CODIGO GRUPO HOJAS DE RUTA
                $res[$j]['ANZZL']     = $this->getNumPersonHojaRuta($arrDatosHojaRuta, $arrDatosHojaRuta[$i]['PLNAL']); //CANTIDAD MINIMA DE PERSONAS
                $res[$j]['MAX_ANZZL'] = $this->getSumPersonHojaRuta($arrDatosHojaRuta, $arrDatosHojaRuta[$i]['PLNAL']); //CANTIDAD MAXIMA DE PERSONAS
                $res[$j]['KTEXT']     = $arrDatosHojaRuta[$i]['KTEXT']; //TIPO DE MANTENIMIENTO
            }
        }

        return $res;
    }

    /*
     * Obtiene las Hojas de Ruta de la Instalacion
     */

    public function getHojaRutaInstalacion($coInstalacion, $objInstalacion) {
        $j = 0;
        $res = array();
        $tmp = array();
        $arrDatosHojaRuta = $objInstalacion->SelectHojaRutaInstalacion($coInstalacion);

        for ($i = 1; $i <= count($arrDatosHojaRuta); $i++) {
            if (!$res || $arrDatosHojaRuta[$i]['PLNAL'] != $res[$j]['PLNAL']) {
                $j = $j + 1;
                $res[$j]['PLNAL']     = $arrDatosHojaRuta[$i]['PLNAL']; //CODIGO HOJAS DE RUTA
                $res[$j]['PLNNR']     = $arrDatosHojaRuta[$i]['PLNNR']; //CODIGO GRUPO HOJAS DE RUTA
                $res[$j]['ANZZL']     = $this->getNumPersonHojaRuta($arrDatosHojaRuta, $arrDatosHojaRuta[$i]['PLNAL']); //CANTIDAD MINIMA DE PERSONAS
                $res[$j]['MAX_ANZZL'] = $this->getSumPersonHojaRuta($arrDatosHojaRuta, $arrDatosHojaRuta[$i]['PLNAL']); //CANTIDAD MAXIMA DE PERSONAS
                $res[$j]['KTEXT']     = $arrDatosHojaRuta[$i]['KTEXT']; //TIPO DE MANTENIMIENTO
            }
        }
        return $res;
    }

    /*
     * Obtiene el Texto del Arreglo de Hoja de Ruta
     */

    public function getKtextHojaRuta($coGrupoHojaRuta, $coHojaRuta, $arrDatosHojaRuta) {
        for ($i = 1; $i <= count($arrDatosHojaRuta); $i++)
            if ($arrDatosHojaRuta[$i]['PLNNR'] == $coGrupoHojaRuta && $arrDatosHojaRuta[$i]['PLNAL'] == $coHojaRuta) {
                $res = $arrDatosHojaRuta[$i]['KTEXT'];
                return $res;
            }
    }

    /*
     * class
     */

    public function getDetalletHojaRuta($coGrupoHojaRuta, $coHojaRuta, $arrDatosHojaRuta) {
        $res = array();
        for ($i = 1; $i <= count($arrDatosHojaRuta); $i++)
            if ($arrDatosHojaRuta[$i]['PLNNR'] == $coGrupoHojaRuta && $arrDatosHojaRuta[$i]['PLNAL'] == $coHojaRuta) {
                $res['PLNAL'] = $arrDatosHojaRuta[$i]['PLNAL']; //CODIGO HOJAS DE RUTA
                $res['PLNNR'] = $arrDatosHojaRuta[$i]['PLNNR']; //CODIGO GRUPO HOJAS DE RUTA
                $res['ANZZL'] = $this->getNumPersonHojaRuta($arrDatosHojaRuta, $arrDatosHojaRuta[$i]['PLNAL']); //CANTIDAD DE PERSONAS
                $res['KTEXT'] = $arrDatosHojaRuta[$i]['KTEXT']; //TIPO DE MANTENIMIENTO 
                return $res;
            }
    }

    /*
     * Obtiene el Numero mayor de Personas para una Hoja de Ruta especifica
     */

    public function getNumPersonHojaRuta($arrDatosHojaRuta, $coHojaRuta) {
        $mayor = 0;
        for ($i = 1; $i < count($arrDatosHojaRuta); $i++)
            if ($arrDatosHojaRuta[$i]['ANZZL'] >= $mayor && $arrDatosHojaRuta[$i]['PLNAL'] == $coHojaRuta)
                $mayor = $arrDatosHojaRuta[$i]['ANZZL'];
        return $mayor;
    }
    
    /*
     * Obtiene la sumatoria  de Personas para una Hoja de Ruta especifica
     */

    public function getSumPersonHojaRuta($arrDatosHojaRuta, $coHojaRuta) {
        $suma = 0;
        for ($i = 1; $i < count($arrDatosHojaRuta); $i++)
            if ($arrDatosHojaRuta[$i]['PLNAL'] == $coHojaRuta)
                $suma = $suma + $arrDatosHojaRuta[$i]['ANZZL'];
        return $suma;
    }
    

    /*
     * Obtener la lista personas que tiene por debajo de un cargo
     */

    public function getPersonCargo($co_cargo, $co_usuario, $pto_plan) {
        $pto_plan = stripslashes($pto_plan);
        $this->rs = $this->MySQL->ExecuteSQL("SELECT  US.CO_USUARIO, 
                                                      US.NB_INDICADOR,
                                                      US.NB_NOMBRE As NB_USUARIO, 
                                                      US.TX_DIRECCION, 
                                                      CR.NB_CARGO, 
                                                      CR.CO_CARGO_PADRE 
                                            FROM I005T_CARGO CR 
                                            INNER JOIN I004T_USUARIO US ON US.NU_CARGO = CR.NU_CARGO 
                                            WHERE CR.NU_CARGO IN (SELECT PTO_T.NU_CARGO 
                                                                  FROM C003T_CARGO_PUESTO_TRAB PTO_T 
                                                                  WHERE CO_PUESTO_TRAB IN  ($pto_plan)) 
                                            AND CR.CO_CARGO_PADRE LIKE '{$co_cargo}%'
                                             OR CONCAT(CR.CO_CARGO_PADRE,CR.CO_CARGO) = '{$co_cargo}'
                                            AND US.IN_DISPONIBLE=0 
                                            AND COALESCE(US.IN_ADMIN,0) = 0
                                            ORDER BY US.NB_NOMBRE");
        return $this->rs;
    }

    /*
     * Obtiene la frecuencia de Mantenimiento para un Equipo o Instalación
     */

    public function getDiasFrecMtto($coEquipo, $coGrupoHojaRuta, $coHojaRuta, $arrDatosHojaRuta) {
        $FrecuenciaMaximaDias = 0;
        for ($i = 1; $i <= count($arrDatosHojaRuta); $i++) {
            $valor = $arrDatosHojaRuta[$i]['KZYK1'];
            if ($arrDatosHojaRuta[$i]['PLNNR'] == $coGrupoHojaRuta && $arrDatosHojaRuta[$i]['PLNAL'] == $coHojaRuta) {
                $Frecuencia = preg_replace("/[^0-9]/", "", $valor);
                $unidadFrecuencia = preg_replace("/[0-9]/", "", $valor);
                switch ($unidadFrecuencia) {
                    case 'D':
                        $FrecuenciaActual = (int) $Frecuencia;
                       break;
                    case 'S':
                        $FrecuenciaActual = (int) $Frecuencia * 7;
                        break;
                    case 'M':
                        $FrecuenciaActual = (int) $Frecuencia * 30;
                        break;
                    case 'A':
                        $FrecuenciaActual = (int) $Frecuencia * 365;
                        break;
                    default:
                        $FrecuenciaActual = (int) 0;
                        break;
                }
                if ($FrecuenciaActual > $FrecuenciaMaximaDias)
                    $FrecuenciaMaximaDias = $FrecuenciaActual;
            }
        }
        return $FrecuenciaMaximaDias;
    }

    /*
     * OBtiene el Tipo de Mantenimiento de la estructura de cadena pasada
     */

    public function getTipoMantenimiento($descHojaRuta) {
        return substr($descHojaRuta, 0, 3);
    }
    
     /*
     * Obtiene los tipos de plan asociados a un objeto tecnico dado
     */

    public function getTipoMttoPlanObjTecnico($coObjTecnico) {
        
        $this->rs = $this->MySQL->ExecuteSQL("SELECT I037T.TX_HOJARUTA
                                              FROM I037T_PLAN I037T
                                              WHERE I037T.VA_DETALLE =".$coObjTecnico);
        return substr($descHojaRuta, 0, 3);
    }

    /*
     * Obtiene el total de Historicos para un plan
     */

    public function getTotalHistorico($co_plan) {
        $res = $this->MySQL->ExecuteRow("SELECT COUNT(XMTO.VA_DETALLE) AS TOT_HISTORICO 
                                  FROM X032T_HISTORICO_MTTO XMTO 
                                 INNER JOIN I037T_PLAN As PL
                                  ON PL.VA_HOJARUTA = XMTO.VA_HOJARUTA AND PL.VA_DETALLE = XMTO.VA_DETALLE
                                  WHERE PL.CO_PLAN = {$co_plan}");
        return $res['TOT_HISTORICO'];
    }

    /*
     * Inserta un historico
     */

    public function InsertPlanHistorico($co_detalle, $co_nivel, $fe_ultimo, $nu_semana) {
        $hist = "INSERT INTO X032T_HISTORICO_MTTO(VA_DETALLE,
                                                VA_HOJARUTA,
                                                FE_HISTORICO_MTTO,
                                                NU_SEMANA) 
                                     VALUES ('" . $co_detalle . "',
                                             '" . $co_nivel . "',
                                             '" . $fe_ultimo . "',
                                            '" . $nu_semana . "')";
        return $this->MySQL->ExecuteSQL($hist);
    }

    /*
     * Inseta un Plan
     */

    public function InsertPlan($co_detalle,         $co_nivel,                $tx_nivel, 
                               $co_tipo_asignacion, $fe_ultimo,               $nu_semana, 
                               $fe_proxima_plan,    $nu_semana_proxima,       $co_estatus, 
                               $tipo_plan,          $co_ubic_tec,             $co_grupo_plan, 
                               $co_ejec_plan,       $co_estatus_orden,        $in_cdc,
                               $in_tercero,         $in_person_propio,        $asig_especifica,    
                               $estatusOrdenActividades, $cantPersonal,       $coPersonal,         
                               $arrayProcPlan) {
        $f = false;
        $this->MySQL->BeginTrans();
        try {

            if ($fe_ultimo != '')
                $this->InsertPlanHistorico($co_detalle, $co_nivel, $fe_ultimo, $nu_semana);
            $plan = "INSERT INTO I037T_PLAN( VA_DETALLE,
                                            VA_HOJARUTA,
                                            TX_HOJARUTA,
                                            CO_TIPO_ASIGNACION,
                                            FE_ULTIMO,
                                            NU_SEMANA,
                                            FE_PROXIMA_PLAN,
                                            NU_SEMANA_PROXIMA,
                                            CO_ESTATUS,
                                            VA_TIPO_PLAN,
                                            CO_UBIC_TEC,
                                            CO_GRP_PLAN,
                                            CO_EJEC_PLAN,
                                            CO_ESTATUS_ORDEN,
                                            IN_TERCERO,
                                            IN_ESFUER_PROPIO,
                                            IN_CDC) 
                                   VALUES ('{$co_detalle}',
                                           '{$co_nivel}',
                                           '{$tx_nivel}',    
                                           '{$co_tipo_asignacion}',
                                           '{$fe_ultimo}',
                                           '{$nu_semana}',
                                           '{$fe_proxima_plan}',
                                           '{$nu_semana_proxima}',
                                           '{$co_estatus}',
                                           '{$tipo_plan}',
                                           '{$co_ubic_tec}',
                                           '{$co_grupo_plan}',
                                           '{$co_ejec_plan}',
                                           '{$co_estatus_orden }',
                                           '{$in_tercero}',
                                           '{$in_person_propio}',
                                           '{$in_cdc}')";
            $bres = (boolean) $this->MySQL->ExecuteSQL($plan);
            $this->setCoUltimoPlan();

            if ($asig_especifica && $bres) {
                // SE INSERTA LA ORDEN DE TRABAJO:
                $this->InsertOrdenTrabajo($fe_proxima_plan, $estatusOrdenActividades, $cantPersonal);

                // SE INSERTAN LAS ACTIVIDADES
                $this->InsertActividades($this->getCoUltimaOrden(), $this->getCoUltimoPlan(), $fe_proxima_plan,$co_estatus, $cantPersonal, $coPersonal);
            }
            //* Replica los procedimientos de SGE
            $countArray = count($arrayProcPlan);
            for ($k = 0; $k < $countArray; $k++) {
                $bres = (boolean) $this->MySQL->ExecuteSQL("INSERT INTO I020T_PROCEDIMIENTO_PLAN (CO_PLAN,VA_PROCEDIMIENTO,NB_TEXTO,NU_POSICION,NU_NODO,CAPACIDAD)
                                                             VALUES ({$this->getCoUltimoPlan()},
                                                                      '{$arrayProcPlan[$k]['VA_PROCEDIMIENTO']}',
                                                                      '{$arrayProcPlan[$k]['NB_TEXTO']}',
                                                                      '{$arrayProcPlan[$k]['NU_POSICION']}',
                                                                      '{$arrayProcPlan[$k]['NU_NODO']}',
                                                                      '{$arrayProcPlan[$k]['ANZZL']}');");
                if (!$bres)
                    throw new Exception("No se puede continuar cargando procedimientos");
            }
            $f = true;
        } catch (ADODB_Exception $e) {
            $this->MySQL->RollbackTrans();
        } catch (Exception $e) {
            $this->MySQL->RollbackTrans();
        }
        $this->MySQL->CommitTrans();
        return $f;
    }

    /**
     * Obtiene la orden si existe para un plan
     */
    function getOrdenPlan($co_plan) {
        $row = $this->MySQL->ExecuteRow("SELECT CO_ORDEN 
                                        FROM C004T_PLAN_ORDEN 
                                        WHERE CO_PLAN = {$co_plan}");

        if ($row) return $row['CO_ORDEN'];
             else return null;
    }

    /*
     * Actualiza un plan
     */

    public function UpdatePlan($co_plan,               $co_tipo_asignacion, $fe_ultimo_mtto, 
                               $nu_semana_ultimo_mtto, $fe_proxima_plan,    $nu_semana_proxima, 
                               $co_estatus,            $co_detalle,         $co_nivel, 
                               $cantPersonal,          $coPersonal,         $in_tercero,
                               $in_esfuer_propio,      $in_cdc) {


        $f = true;
        $this->MySQL->BeginTrans();
        try {
            /*
             * **************** Actualiza el historico antes de actulizar el plan *********************
             */
            $hist = "UPDATE X032T_HISTORICO_MTTO XMTO 
                        INNER JOIN I037T_PLAN As PL
                            ON PL.FE_ULTIMO = XMTO.FE_HISTORICO_MTTO
                            AND PL.VA_HOJARUTA = XMTO.VA_HOJARUTA
                            AND PL.VA_DETALLE = XMTO.VA_DETALLE
                            AND PL.NU_SEMANA = XMTO.NU_SEMANA
                       SET  XMTO.FE_HISTORICO_MTTO = '$fe_ultimo_mtto',
                            XMTO.NU_SEMANA = '$nu_semana_ultimo_mtto'
                        WHERE PL.CO_PLAN = {$co_plan}";

            $this->MySQL->ExecuteSQL($hist);
            /* Solo correccion, si no tiene historico lo crea */
            if ($this->getTotalHistorico($co_plan) === 0 && $nu_semana_ultimo_mtto != '')
                $this->InsertPlanHistorico($co_detalle, $co_nivel, $fe_ultimo_mtto, $nu_semana_ultimo_mtto);
            /*             * ************************************************************************************** */

            //* Elimina las actividades primero para reinsertarla si es necesario
            $this->DeleteActividades($co_plan);

            //* Crea la orden si existe o la actualiza
            if ($co_tipo_asignacion == ASIGNACION_ESPECIFICA) {                
                $act_co_orden = $this->getOrdenPlan($co_plan);
                if (!empty($act_co_orden)) {
                    // Actualiza la fecha proxima para las actividades
                    $this->InsertActividades($act_co_orden, $co_plan, $fe_proxima_plan, $act_estatus, $cantPersonal, $coPersonal);
                } else {
                    // Crea Orden por cambio de tipo de Asignacion
                    $this->InsertOrdenTrabajo($fe_proxima_plan, $co_estatus, $cantPersonal);
                    // Inerta las activiades por cambio de tipo de aignacion
                    $this->InsertActividades($this->getCoUltimaOrden(), $co_plan, $fe_proxima_plan, $co_estatus, $cantPersonal, $coPersonal);
                }
            }

            $update = "UPDATE I037T_PLAN 
                            SET CO_TIPO_ASIGNACION  = $co_tipo_asignacion,
                                FE_ULTIMO = '$fe_ultimo_mtto',
                                NU_SEMANA  = '$nu_semana_ultimo_mtto',
                                FE_PROXIMA_PLAN  = '$fe_proxima_plan',
                                NU_SEMANA_PROXIMA = '$nu_semana_proxima',
                                CO_ESTATUS ='$co_estatus',
                                IN_TERCERO ='$in_tercero',
                                IN_ESFUER_PROPIO='$in_esfuer_propio',
                                IN_CDC='$in_cdc'
                         WHERE CO_PLAN= $co_plan";
            $this->MySQL->ExecuteSQL($update);
        } catch (ADODB_Exception $e) {
            echo $e->getMessage();
            $f = false;
        } catch (Exception $e) {
            $this->MySQL->RollbackTrans();
            $f = false;
        }
        $this->MySQL->CommitTrans();
        return $f;
    }

    /*
     * Elimina un Plan 
     */

    public function DeletePlan($co_plan) {
        $f = true;
        $this->MySQL->BeginTrans();
        try {
            $bres = (boolean) $this->MySQL->ExecuteSQL("DELETE FROM I020T_PROCEDIMIENTO_PLAN WHERE CO_PLAN = '{$co_plan}'");
            if ($bres) {
                $this->MySQL->ExecuteSQL("DELETE FROM I022T_ACTIVIDAD WHERE CO_PLAN='{$co_plan}'");
                $this->MySQL->ExecuteSQL("DELETE FROM C004T_PLAN_ORDEN WHERE CO_PLAN='{$co_plan}'");
                $this->MySQL->ExecuteSQL("DELETE FROM I037T_PLAN WHERE CO_PLAN='{$co_plan}'");
            } else
                throw new Exception("No se pudo eliminar los procedimientos");
        } catch (ADODB_Exception $e) {
            echo $e->getMessage();
            $f = false;
        } catch (Exception $e) {
            $this->MySQL->RollbackTrans();
            $f = false;
        }
        $this->MySQL->CommitTrans();
        return $f;
    }

    /*
     * Inserta una Orden de Trabajo directamente cuando es por Asignacion Específica 
     */

    public function InsertOrdenTrabajo($fe_asignacion, $co_status, $ca_actividades) {
        $insert = "INSERT INTO I036T_ORDEN(NU_ORDEN,
                                            CO_ESTATUS_ORDEN,
                                            CA_ACTIVIDADES,
                                            FE_ASIGNACION,
                                            TX_ESTATUS) 
                                    VALUES ('0',
                                            '0',
                                            '" . $ca_actividades . "',
                                            '" . $fe_asignacion . "',
                                            '" . $co_status . "')";
        $this->MySQL->ExecuteSQL($insert);

        $this->setCoNuUltimaOrden();
//                $update = "UPDATE I036T_ORDEN SET NU_ORDEN ='".$this->nuUltimaOrden."' 
//                           WHERE CO_ORDEN ='". $this->coUltimaOrden."'"; 
        $update = "UPDATE I036T_ORDEN SET NU_ORDEN=LAST_INSERT_ID(CO_ORDEN)+1000000000";
        $this->MySQL->ExecuteSQL($update);

        $co_plan = $this->getCoUltimoPlan();
        $co_orden = $this->getCoUltimaOrden();

        // SE INSERTA REGISTRO EN TABLA RELACIONAL
        $insert = "INSERT INTO C004T_PLAN_ORDEN(CO_PLAN,
                                                CO_ORDEN,
                                                FE_ASIG_ORDEN) 
                                        VALUES (" . $co_plan . ",
                                                " . $co_orden . ",
                                                now())";
        $this->MySQL->ExecuteSQL($insert);
    }

    /*
     *   Inserta Actividades cuando es por Asignación Específica
     */

    public function InsertActividades($co_orden, $co_plan, $fe_asignacion, $co_status, $cantPersonal, $coPersonal) {
        for ($i = 1; $i <= $cantPersonal; $i++) {
            $insert = "INSERT INTO I022T_ACTIVIDAD(CO_ORDEN,
                                   CO_PLAN,
                                   CO_TIPO_ASIGNACION,
                                   FE_ASIGNACION,
                                   CO_ESTATUS,
                                   CO_USUARIO) 
                           VALUES ('" . $co_orden . "',
                                   '" . $co_plan . "',
                                   '2',
                                   '" . $fe_asignacion . "',
                                   '" . $co_status . "',
                                   '" . $coPersonal[$i - 1] . "')";
            $this->MySQL->ExecuteSQL($insert);
        }
    }

    /*
     * Actualiza la Actividad: Fecha de Asignacion y Usuario
     */

    public function UpdateActividad($co_actividad, $fe_asignacion, $co_usuario) {


        $sql = "UPDATE I022T_ACTIVIDAD SET FE_ASIGNACION = '" . $fe_asignacion . "',
                                           CO_USUARIO = '" . $co_usuario . "'
                WHERE CO_ACTIVIDAD = '" . $co_actividad . "'";

        $this->MySQL->ExecuteSQL($sql);
    }

    /*
     *  Elimina Actividades
     */

    public function DeleteActividades($co_plan) {
        $del = " DELETE FROM I022T_ACTIVIDAD 
                WHERE CO_PLAN   =   '" . $co_plan . "'";

        $this->MySQL->ExecuteSQL($del);
    }

    /*
     *  Selecciona las Actividades
     */

    public function SelectActividades($co_plan) {
        $sql = " SELECT CO_ACTIVIDAD FROM I022T_ACTIVIDAD WHERE CO_PLAN='" . $co_plan . "'";

        $this->rs = $this->MySQL->ExecuteSQL($sql);

        return $this->rs;
    }

    /*
     * Obtiene el Resumen del plan
     */

    public function getSumarioPlan($co_plan) {

        $sql = "SELECT SUBSTRING(I037.VA_HOJARUTA, 1,8 ) AS CO_GRUPO_HOJARUTA, 
                       SUBSTRING(I037.VA_HOJARUTA, 10, 2) AS NU_HOJARUTA, 
                       DATE_FORMAT(I037.FE_PROXIMA_PLAN, '%d-%m-%Y') AS FE_PROXIMA_PLAN, 
                       I037.CO_TIPO_ASIGNACION, 
                       DATE_FORMAT(I037.FE_ULTIMO, '%d-%m-%Y') AS FE_ULTIMO,
                       IN_TERCERO,
                       IN_ESFUER_PROPIO,
                       IN_CDC
                FROM I037T_PLAN I037
                WHERE I037.CO_PLAN = '" . $co_plan . "'";

        $this->rs = $this->MySQL->ExecuteSQL($sql);
        return $this->rs;
    }

    /*
     * Obtiene el Detalle del Plan
     */

    public function getDetallePlan($co_plan) {

        $sql = "SELECT  SUBSTRING(I037.VA_HOJARUTA, 1,8 )  AS CO_GRUPO_HOJARUTA,
                        SUBSTRING(I037.VA_HOJARUTA, 10, 2) AS NU_HOJARUTA,
                        DATE_FORMAT(I037.FE_PROXIMA_PLAN, '%d-%m-%Y') AS FE_PROXIMA_PLAN,
                        I037.CO_TIPO_ASIGNACION, 
                        DATE_FORMAT(I037.FE_ULTIMO, '%d-%m-%Y') AS FE_ULTIMO, 
                        I009.TX_SIGLAS_CLASE,
                        I009.CO_TIPO_CLASE,
                        I022.CO_ACTIVIDAD,
                        I004.CO_USUARIO 
               FROM 	I037T_PLAN I037 
               LEFT JOIN I022T_ACTIVIDAD I022 ON (I037.CO_PLAN  = I022.CO_PLAN) 
               LEFT JOIN I009T_CLASE 	 I009 ON (I037.CO_CLASE = I009.CO_CLASE)
               LEFT JOIN I004T_USUARIO 	 I004 ON (I022.CO_USUARIO = I004.CO_USUARIO)
               WHERE I037.CO_PLAN = '" . $co_plan . "'";
//echo $sql;
        $this->rs = $this->MySQL->ExecuteSQL($sql);
        return $this->rs;
    }

    /*
     * Lista de Planes y anexa información si tiene ordenes activas, es decir, 
     * con:
     *  TX_ESTATUS != 3 (Orden Cerrada) 
     *  TX_ESTATUS != 4 (Orden Cerrada sin concluir)
     */

    public function getListaPlanActivo($co_detalle) {
        $sql = "SELECT   I037.CO_PLAN, 
                         SUBSTRING(I037.VA_HOJARUTA, 1,8 )  AS CO_GRUPO_HOJARUTA,
                         SUBSTRING(I037.VA_HOJARUTA, 10, 2) AS NU_HOJARUTA,
                         DATE_FORMAT(I037.FE_PROXIMA_PLAN, '%d-%m-%Y') AS FE_PROXIMA_PLAN,
                         I037.CO_TIPO_ASIGNACION, 
                         DATE_FORMAT(I037.FE_ULTIMO, '%d-%m-%Y') AS FE_ULTIMO, 
                         COUNT(I036.CO_ORDEN) As TOT_ORDEN_ACTIVAS,
                         COUNT(X032.VA_DETALLE) As TOT_HISTORICO,
                         COUNT(I031.CO_ACTIVIDAD) As TOT_EJECUCION
                FROM 	I037T_PLAN I037 
                LEFT JOIN I022T_ACTIVIDAD I022 ON (I037.CO_PLAN = I022.CO_PLAN) 
                LEFT JOIN I031T_EJECUCION I031 ON (I031.CO_PLAN = I022.CO_PLAN)
                LEFT JOIN I036T_ORDEN I036 ON (I036.CO_ORDEN = I022.CO_ORDEN AND I036.TX_ESTATUS NOT IN (3,4)) 
                LEFT JOIN X032T_HISTORICO_MTTO X032 ON (X032.VA_DETALLE = I037.VA_DETALLE AND X032.VA_HOJARUTA = I037.VA_HOJARUTA)                
                WHERE I037.VA_DETALLE = '" . $co_detalle . "' AND (I037.CO_ESTATUS != 3)
                GROUP BY  I037.CO_PLAN";

        $this->rs = $this->MySQL->ExecuteSQL($sql);
        return $this->rs;
    }

    /*
     * Obtiene las personas asignadas a un Plan
     */

    public function getPersonAsigPlan($coPlan) {
        $sql = "SELECT CO_USUARIO
                FROM I022T_ACTIVIDAD I022
                WHERE I022.CO_PLAN = '" . $coPlan . "'";

        return $this->MySQL->ExecuteSQL($sql);
    }

    /*
     * Obtiene la lista de registros de observacion para un plan
     * Parametros:
     *     $coPlan = Id del Plan
     */
    public function getRegistroObs($coPlan) {
      $sql="SELECT TX_REGISTRO,
                   FE_REGISTRO
            FROM I038T_PLAN_OBSERVACION
            WHERE CO_PLAN = {$coPlan}";
      return $this->MySQL->ExecuteSQL($sql);
    }
    /*
     * Inserta una nueva observacion
     * Parametros:
     *    $coPlan = Id del Plan
     *    $txRegistro = Texto del registro
     */
    public function insRegistroObs($coPlan,$txRegistro) {
      $sql="INSERT INTO I038T_PLAN_OBSERVACION(CO_PLAN,TX_REGISTRO)
                   VALUES($coPlan,'$txRegistro')";
       return $this->MySQL->ExecuteSQL($sql);
    }

}

?>
