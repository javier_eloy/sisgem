<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of planificacion
 *
 * @author portilloga
 */
class planificacion {

    private $MySQL = null;
    private $rs = null;
    private $numZonaGerencia = 0;

    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQL->ConnectMySQL();
    }

    public function __destruct() {
        $this->MySQL->CloseConnect();
    }

    //===============  METODOS PUBLICOS =================
    
    /*
     * * detEquipoCargo
     * Retorna el detalle de equipos asociados a un grupo de trabajo y/o puesto de trabajo responsable
     */

    public function detEquipoCargo($coCargo) {
         $sql = "
            SELECT C002R.CO_GRUPO_PLAN AS CO_GRUPO_PLAN, 
                    I005.CO_UBIC_TECNICA as CO_UBIC_TECNICA,
                    Null AS CO_PUESTO_TRAB
                    FROM C002T_CARGO_GRUPO_PLAN C002R
                            LEFT JOIN C003T_CARGO_PUESTO_TRAB C003R 
                                ON C002R.CO_GRUPO_PLAN = C003R.CO_GRUPO_PLAN AND C002R.NU_CARGO = '".$coCargo."'
                            INNER JOIN I005T_CARGO I005 
                                ON C002R.NU_CARGO = I005.NU_CARGO AND C002R.NU_CARGO = '".$coCargo."'
                    WHERE C002R.NU_CARGO = '".$coCargo."' GROUP BY CO_GRUPO_PLAN 

            UNION 

            SELECT C003.CO_GRUPO_PLAN AS CO_GRUPO_PLAN, 
                    I005R.CO_UBIC_TECNICA as CO_UBIC_TECNICA,
                    C003.CO_PUESTO_TRAB AS CO_PUESTO_TRAB
                    FROM C003T_CARGO_PUESTO_TRAB C003
                            INNER JOIN I005T_CARGO I005R ON C003.NU_CARGO = I005R.NU_CARGO 
                                AND C003.NU_CARGO = '".$coCargo."'
                    WHERE C003.NU_CARGO = '".$coCargo."' 
                    AND NOT EXISTS 
                            (SELECT C002.CO_GRUPO_PLAN 
                                    FROM C002T_CARGO_GRUPO_PLAN C002 
                                        WHERE C002.CO_GRUPO_PLAN = C003.CO_GRUPO_PLAN 
                                            AND C002.NU_CARGO = '".$coCargo."') 
                                        GROUP BY C003.CO_PUESTO_TRAB";
        $this->rs = $this->MySQL->ExecuteSQL($sql);
        $res=array();
        $invSap = new inventarioSAP();
        while (!$this->rs->EOF) {
            if ($this->rs->fields['CO_PUESTO_TRAB']==Null){
                $res = array_merge($invSap->SelectEquipoGrpPlanUbicTec($this->rs->fields['CO_GRUPO_PLAN'], $this->rs->fields['CO_UBIC_TECNICA'] . '%'),$res);
            }  else {
                $arrayPuesto = $invSap->SelectEquipoGrpPlanUbicTec($this->rs->fields['CO_GRUPO_PLAN'], $this->rs->fields['CO_UBIC_TECNICA'] . '%');
                for ($i = 1; $i <=count($arrayPuesto); $i++) {
                    if($arrayPuesto[$i]["GEWRK"] == $this->rs->fields['CO_PUESTO_TRAB']){
                        $res = array_merge(array($arrayPuesto[$i]),$res);
                    }
                }
            }
            $this->rs->MoveNext();
        }
            return $res;
    }

    /*
     * detInstalacionCargo: 
     *    Retorna el detalle de ubicación técnica asociado a grupo planificador y puesto de trabajo
     */

    public function detInstalacionCargo($coCargo) {
         $sql = "
            SELECT C002R.CO_GRUPO_PLAN AS CO_GRUPO_PLAN, 
                    I005.CO_UBIC_TECNICA as CO_UBIC_TECNICA,
                    Null AS CO_PUESTO_TRAB
                    FROM C002T_CARGO_GRUPO_PLAN C002R
                            LEFT JOIN C003T_CARGO_PUESTO_TRAB C003R 
                                ON C002R.CO_GRUPO_PLAN = C003R.CO_GRUPO_PLAN AND C002R.NU_CARGO = '".$coCargo."'
                            INNER JOIN I005T_CARGO I005 
                                ON C002R.NU_CARGO = I005.NU_CARGO AND C002R.NU_CARGO = '".$coCargo."'
                    WHERE C002R.NU_CARGO = '".$coCargo."' GROUP BY CO_GRUPO_PLAN 

            UNION 

            SELECT C003.CO_GRUPO_PLAN AS CO_GRUPO_PLAN, 
                    I005R.CO_UBIC_TECNICA as CO_UBIC_TECNICA,
                    C003.CO_PUESTO_TRAB AS CO_PUESTO_TRAB
                    FROM C003T_CARGO_PUESTO_TRAB C003
                            INNER JOIN I005T_CARGO I005R ON C003.NU_CARGO = I005R.NU_CARGO 
                                AND C003.NU_CARGO = '".$coCargo."'
                    WHERE C003.NU_CARGO = '".$coCargo."' 
                    AND NOT EXISTS 
                            (SELECT C002.CO_GRUPO_PLAN 
                                    FROM C002T_CARGO_GRUPO_PLAN C002 
                                        WHERE C002.CO_GRUPO_PLAN = C003.CO_GRUPO_PLAN 
                                            AND C002.NU_CARGO = '".$coCargo."') 
                                        GROUP BY C003.CO_PUESTO_TRAB";
        $this->rs = $this->MySQL->ExecuteSQL($sql);
        $UbicTec= $this->rs->GetArray();
        $invSap = new inventarioSAP();
        $arrayInvSap = $invSap->SelectEstacionUbicTecUbicTec($UbicTec[0]['CO_UBIC_TECNICA'] . '%');
        //var_dump($arrayInvSap); 
        $res=array();
        for ($j = 0; $j <count($UbicTec); $j++) { 
            for ($i = 1; $i <=count($arrayInvSap); $i++) {            
                if ($UbicTec[$j]['CO_PUESTO_TRAB']==Null){
                    if($arrayInvSap[$i]["INGRP"] == $UbicTec[$j]['CO_GRUPO_PLAN']
                            AND $arrayInvSap[$i]["GEWRK"]<>""){
                        $res = array_merge(array($arrayInvSap[$i]),$res);
                    }

                }  else {
                    if($arrayInvSap[$i]["GEWRK"] == $UbicTec[$j]['CO_PUESTO_TRAB']){
                        $res = array_merge(array($arrayInvSap[$i]),$res);
                    }
                }
            }
            $this->rs->MoveNext();
        }
        return $res;        
    }

    /*
     * 
     */

    public function detEquipoCargo2($coCargo) {
        $sql = "select C002.CO_GRUPO_PLAN as CO_GRUPO_PLAN
                from   C002T_CARGO_GRUPO_PLAN C002
                where  C002.NU_CARGO ='" . $coCargo . "'";

        echo $sql;
        $this->rs = $this->MySQL->ExecuteSQL($sql);

        $invSap = new inventarioSAP();

        return $invSap->SelectEquipoGrpPlanUbicTec($this->rs->fields['CO_GRUPO_PLAN']);
    }

    /*
     * detPlanObjTecnico
     * Retorna los tipos de planes asosciados a un objeto tecnico (instalacion/equipo) en caso de existir
     * registro alguno enla tabla I037T_PLAN
     */
    public function detPlanObjTecnico($vaDetalle){

        $res         ="&nbsp;";
        $primero     = true;
        $arrTipoPlan = explode(',',TIPO_NIVEL_PLAN);

        $sql = "SELECT I037.TX_HOJARUTA
                FROM I037T_PLAN I037
                WHERE I037.VA_DETALLE = '$vaDetalle'
                GROUP BY I037.TX_HOJARUTA";
        $this->rs = $this->MySQL->ExecuteSQL($sql);

        while (!$this->rs->EOF) {
            for($i = 0; $i < count($arrTipoPlan); $i++)
                if(substr($this->rs->fields['TX_HOJARUTA'],0,3) == $arrTipoPlan[$i])
                    if($primero) {
                        $res = $arrTipoPlan[$i];
                        $primero = false;
                    }
                    else{ 
                        $tmp = strpos($res,substr($this->rs->fields['TX_HOJARUTA'],0,3));
                        if($tmp === false) $res = $res.','.$arrTipoPlan[$i]; 
                    }  
            $this->rs->MoveNext();
        }
            return $res;        
    }
}

?>
