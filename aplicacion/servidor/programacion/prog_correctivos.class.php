<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of prog_correctivos
 *
 * @author hernandezjnz
 */
class prog_correctivos {
     private $MySQL = null;
     private $MySQLInv = null;
     private $rs    = null;
    
    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQLInv = new conexion();
        $this->MySQL->ConnectMySQL();
        $this->MySQLInv->ConnectInventario();
    }

    public function __destruct(){
        $this->MySQL->CloseConnect();
        $this->MySQLInv->CloseConnect();
        
    }
    //===============  METODOS PUBLICOS =================
    public function InsertProgCorrectivos($cod_planes_fallas,$cod_detalle,$fecha_actual_c,$cod_usuario){
       $rsEqFalla=$this->MySQL->ExecuteSQL("SELECT e.CO_TIPO_EQUIPO,
                                      n.NU_CANTIDAD_PERSONAS,
                                      Count(act.CO_ACTIVIDAD_FALLA ) AS CantidActiviAsigna        
                               FROM J038T_PLAN_FALLA p1
                               INNER JOIN I021T_NIVEL n ON (p1.CO_NIVEL = n.CO_NIVEL)
                               INNER JOIN I017T_EQUIPO e ON (n.CO_EQUIPO = e.CO_EQUIPO)
                               INNER JOIN I023T_ACTIVIDAD_FALLA act ON (p1.CO_PLAN_FALLA = act.CO_PLAN_FALLA)
                               WHERE p1.CO_PLAN_FALLA='$cod_planes_fallas' 
                               GROUP BY e.CO_TIPO_EQUIPO,n.NU_CANTIDAD_PERSONAS");
     try{ 
      if($this->MySQL->ExecuteSQL("INSERT INTO I023T_ACTIVIDAD_FALLA(CO_PLAN_FALLA,CO_DETALLE,
                                                       CO_TIPO_ASIGNACION ,FE_ASIGNACION,
                                                       CO_ESTATUS,CO_USUARIO) 
                                               VALUES ('$cod_planes_fallas','$cod_detalle',
                                                        '1',DATE_FORMAT('$fecha_actual_c ','%d-%m-%Y'),
                                                        '1','$cod_usuario')"))
      {
         $ultimo_id = $this->MySQL->getLastID();
         $orden = $ultimo_id + (1000000 * ((int) $rsEqFalla->fields['CO_TIPO_EQUIPO']) * 2); // SE MULTIPLICA POR 2 POR SER CORRECTIVOS
         $this->MySQL->ExecuteSQL("UPDATE I023T_ACTIVIDAD_FALLA SET CO_ORDEN='$orden' WHERE CO_ACTIVIDAD_FALLA='$ultimo_id'");
     
      }
     }catch(ADODB_Exception $e) {
          return false;
     }catch(Exception $e){
          $this->MySQL->RollbackTrans();
          return false;
    }
    if(!$this->MySQL->hasFailed()) $this->MySQL->CommitTrans();
    return true;
         
    }
    /*
     * 
     */
    public function SelectActivoMantenimiento($usuario_cod_zona, $usuario_cod_gerencia,$usuario_cod_departamento,
                                             $filterUnidad1,$filterUnidad2)
   {
        $this->rs=$this->MySQL->ExecuteSQL("SELECT  dTax5.CO_TAXONOMIA5,
                                            dLoc.CO_LOCALIDAD,
                                            e.CO_EQUIPO AS ing_IdEquipo, 
                                            e.TX_DESCRIPCION AS chg_DescriEquipo,
                                            n.*,
                                            c.NB_CLASE AS chg_NombreClase,
                                            d1.CO_DETALLE_EQUIPO AS ing_idDetallEquipo, 
                                            d1.BD_INV_CO_DETALLE_EQUIPO, 
                                            d1.NU_DETALLE_EQUIPO AS ing_CodigoEquipo,
                                            d2.CO_LOCALIDAD AS ing_locali1, 
                                            d2.NU_DETALLE_ESTACION AS ing_CodigoEstaci, 
                                            d2.TI_TIEMPO_VIAJE AS chg_tiempoViaje1,
                                            p1.CO_PLAN_FALLA AS ing_IdPlanesCorrec, 
                                            d3.CO_LOCALIDAD AS ing_locali2, 
                                            d3.TI_TIEMPO_VIAJE AS chg_tiempoViaje2
                                    FROM C011T_DPTO_TAXONOMIA dTax5
                                    INNER JOIN I026T_DPTO_LOCALIDAD dLoc ON (dTax5.CO_DEPARTAMENTO = dLoc.CO_DEPARTAMENTO  
                                                                             AND dTax5.CO_ZONA=dLoc.CO_ZONA $filterUnidad2 )
                                    INNER JOIN I017T_EQUIPO e ON (dTax5.CO_TAXONOMIA5 = e.CO_TAXONOMIA5 
                                                                  AND dLoc.CO_LOCALIDAD = e.CO_LOCALIDAD AND e.CO_GERENCIA = '$usuario_cod_gerencia'  
                                                                  AND e.CO_ZONA= '$usuario_cod_zona')
                                    INNER JOIN I021T_NIVEL n ON (e.CO_EQUIPO = n.CO_EQUIPO)
                                    INNER JOIN I009T_CLASE c ON (n.CO_CLASE = c.CO_CLASE)
                                    INNER JOIN J038T_PLAN_FALLA p1 ON (n.CO_NIVEL = p1.CO_NIVEL AND NU_ASIGNACION='1')
                                    LEFT OUTER JOIN J018T_DETALLE_EQUIPO d1 ON (e.CO_EQUIPO = d1.CO_EQUIPO AND d1.CO_ESTADO='1')
                                    LEFT OUTER JOIN J016T_DETALLE_ESTACION d2 ON (e.CO_EQUIPO = d2.CO_EQUIPO)
                                    LEFT OUTER JOIN J016T_DETALLE_ESTACION d3 ON (d1.CO_LOCALIDAD = d3.CO_LOCALIDAD)
                                    WHERE dTax5.CO_ZONA ='$usuario_cod_zona' 
                                    AND   dTax5.CO_DEPARTAMENTO ='$usuario_cod_departamento' $filterUnidad1 
                                    AND p1.CO_PLAN_FALLA NOT IN (SELECT CASE
                                                                 WHEN 
                                                                    (SELECT COUNT(act2.CO_ACTIVIDAD_FALLA) 
                                                                     FROM I023T_ACTIVIDAD_FALLA act2 
                                                                     WHERE act2.CO_PLAN_FALLA = p1.CO_PLAN_FALLA 
                                                                     AND act2.CO_DETALLE = d1.CO_DETALLE_EQUIPO) = (n.NU_CANTIDAD_PERSONAS)
                                                                THEN act2.CO_PLAN_FALLA 
                                                                ELSE '' 
                                                                END AS Conteo
                                    FROM I023T_ACTIVIDAD_FALLA act2 
                                    WHERE act2.CO_PLAN_FALLA = p1.CO_PLAN_FALLA 
                                    AND act2.CO_DETALLE= d1.CO_DETALLE_EQUIPO)");
        return $this->rs;
    }
    
    public function SelecDetalleEquipoInv($ing_localidad_1,$ing_localidad_2,$BD_INV_CO_DETALLE_EQUIPO){
       $this->rs=$this->MySQLInv->ExecuteSQL(" SELECT t027_2.descripcion, 
                                               t009.codigo AS ing_CodigoEquipo, 
                                               t009.n_serial, 
                                               t005.nombre
                                        FROM t027_localidad t027
                                        INNER JOIN t027_localidad t027_2 ON (t027.codigo = t027_2.codigo AND t027_2.estado = 'A' 
                                                                             AND (t027_2.codigo = '$ing_localidad_1' 
                                                                             OR t027_2.codigo = '$ing_localidad_2'))
                                        LEFT OUTER JOIN t009_detalle_equipo t009 ON (t009.codigo='$BD_INV_CO_DETALLE_EQUIPO' 
                                                                                     AND t009.estado='A')
                                        LEFT OUTER JOIN t005_equipo t005 ON ( t009.cod_equipo = t005.codigo
                                                                              AND t005.estado = 'A')");
       return $this->rs;
       
    }
    
   public function SelecActividadesAsignadas($ing_IdPlanesCorrec,$ing_idDetallEquipo){
        $this->rs=$this->MySQL->ExecuteSQL("SELECT CO_USUARIO 
                                           FROM I023T_ACTIVIDAD_FALLA
                                           WHERE CO_PLAN_FALLA = '$ing_IdPlanesCorrec' 
                                            AND CO_DETALLE = '$ing_idDetallEquipo' 
                                            ORDER BY CO_ACTIVIDAD_FALLA DESC");
        return $this->rs;
   }
     /*
     * Obtiene la lista de procedimientos a un nivel especificado
     */
    public function SelecProcedimiento($co_nivel){
        $this->rs = $this->MySQL->ExecuteCacheSQL("SELECT tx_descripcion FROM I020T_PROCEDIMIENTO 
                                             WHERE co_nivel='$co_nivel' 
                                             ORDER BY nu_posicion_nivel");
        return $this->rs;
    }
    /*
     * Obtiene los usuarios disponibles según el plan
     */
    public function SelecUsuariosDisponibles($usuario_co_departamento, $filterUnidad, $condWhere){
        $this->rs = $this->MySQL->ExecuteSQL("SELECT CO_USUARIO,NB_NOMBRE
                                             FROM I004T_USUARIO
                                             WHERE CO_DEPARTAMENTO='$usuario_cod_departamento' $filterUnidad  
                                                   AND IN_DISPONIBLE='0' $condWhere");
        return $this->rs;
        
    }
}

?>
