<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of programacion
 *
 * @author hernandezjnz
 */
class programacion {

    private $MySQL = null;
    private $rs = null;

    //================= METODOS PRIVADOS ================
    public function __construct() {
        $this->MySQL = new conexion();
        $this->MySQL->ConnectMySQL();
    }

    public function __destruct() {
        $this->MySQL->CloseConnect();
    }

    //===============  METODOS PUBLICOS =================
/**
 * Devuelva la planificación con una lista de estados, puede ser asignado o por asignar
 * La programacion puede haber realizado a por Programacion o Asignación Especifica
 */
    public function SelecActivoMantenimiento($co_estatus, $semanas, $ubic_tec, $no_aplazada,$grp_plan, $co_tipo_asignacion) {
     
        $where_aplazada = "";
        if ($no_aplazada)$where_aplazada = "AND COALESCE(PL.CO_ESTATUS_ORDEN,0) = 0";

        $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                    PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    PL.CO_ESTATUS_ORDEN,
                                                    PL.CO_UBIC_TEC,
                                                    PL.CO_EJEC_PLAN
                                            FROM I037T_PLAN As PL
                                             WHERE PL.CO_TIPO_ASIGNACION IN ($co_tipo_asignacion)  
                                               AND PL.CO_ESTATUS IN ($co_estatus) 
                                               AND PL.NU_SEMANA_PROXIMA IN($semanas) 
                                               AND PL.CO_GRP_PLAN IN ($grp_plan)    
                                               $where_aplazada
                                               AND PL.CO_UBIC_TEC LIKE '{$ubic_tec}%' ;");
        return $this->rs;
    }
/*
 * Devuelve los Mantenimientos Programados
 */
    public function SelecActivoMantenimientoProgramados($co_estatus, $ubic_tec, $grp_plan, $co_tipo_asignacion) {
     

        $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                    PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    CO_ESTATUS_ORDEN
                                            FROM I037T_PLAN As PL
                                             WHERE PL.CO_TIPO_ASIGNACION IN ($co_tipo_asignacion)  
                                               AND PL.CO_ESTATUS IN ($co_estatus) 
                                               AND PL.CO_GRP_PLAN IN ($grp_plan)    
                                               AND COALESCE(PL.CO_ESTATUS_ORDEN,0) = 0
                                               AND PL.CO_UBIC_TEC LIKE '{$ubic_tec}%' ;");
        return $this->rs;
    }
    /*
     * Muestra el mantenimiento Aplazado
     */

    public function SelecActivoMantenimientoAplazado($ubic_tec,$grp_plan) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                    PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    CO_ESTATUS_ORDEN
                                            FROM I037T_PLAN As PL
                                             WHERE PL.CO_ESTATUS_ORDEN > 0   
                                               AND PL.CO_UBIC_TEC LIKE '$ubic_tec%' 
                                               AND PL.CO_GRP_PLAN IN ($grp_plan)
                                               AND PL.CO_ESTATUS IN (0,1,2);");


        return $this->rs;
    }

    /*
     * Obtiene los planes pendientes con fecha del plan menor a la actual
     */

    public function SelecActivoMantenimientoPendiente($fecha_tope, $ubic_tec,$grp_plan) {
        $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                    PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    PL.CO_UBIC_TEC,
                                                    PL.CO_EJEC_PLAN
                                             FROM I037T_PLAN As PL
                                             WHERE PL.CO_TIPO_ASIGNACION = 1  AND PL.CO_ESTATUS = '0' 
                                               AND COALESCE(PL.CO_ESTATUS_ORDEN,0) = 0
                                               AND PL.FE_PROXIMA_PLAN < STR_TO_DATE('$fecha_tope','%d/%m/%Y')
                                                AND PL.CO_GRP_PLAN IN ($grp_plan)   
                                                AND PL.CO_UBIC_TEC LIKE '$ubic_tec%';");              
        return $this->rs;
    }

    /**
     * SelectActivosUbicacion
     * Lista los datos asociados a los activos (equipos/instalaciones) pertenecientes a un grupo planificador y plan específico, siendo que
     * la fecha de planificación sea menor o igual a la fecha tope suministrada
     */
    
    public function SelectActivosUbicacion($co_plan, $fecha_tope,$ubic_tec,$grp_plan,$pto_plan,$show_only_ubic) {

       //(SELECT CO_UBIC_TEC FROM I037T_PLAN WHERE CO_PLAN = $co_plan)

       $cond_ubic_tec=($show_only_ubic == "false") ? $cond_ubic_tec = "LIKE '$ubic_tec%'": $cond_ubic_tec= " = '$ubic_tec'";

       $this->rs = $this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN,
                                                    PL.VA_DETALLE,
                                                    PL.VA_HOJARUTA,
                                                    PL.FE_ULTIMO,
                                                    PL.NU_SEMANA,
                                                    PL.FE_PROXIMA_PLAN,
                                                    PL.NU_SEMANA_PROXIMA,
                                                    PL.CO_ESTATUS,
                                                    PL.VA_TIPO_PLAN,
                                                    PL.IN_TERCERO
                                             FROM I037T_PLAN As PL
                                             WHERE PL.CO_TIPO_ASIGNACION = 1  AND PL.CO_ESTATUS = '0' 
                                               AND COALESCE(PL.CO_ESTATUS_ORDEN,0) = 0
                                               AND PL.FE_PROXIMA_PLAN <= STR_TO_DATE('$fecha_tope','%d/%m/%Y')
                                                AND PL.CO_UBIC_TEC $cond_ubic_tec
                                                AND PL.CO_GRP_PLAN IN ($grp_plan)
                                                AND PL.CO_EJEC_PLAN IN ($pto_plan)");
        return $this->rs;
    }
    /*
     * Selecciona datos de los objetos
     */
    public function SelecObjetos($l_plan) {
        
        $this->rs=$this->MySQL->ExecuteSQL("SELECT PL.CO_PLAN, PL.VA_DETALLE, PL.VA_HOJARUTA, PL.VA_TIPO_PLAN 
                                            FROM I037T_PLAN As PL
                                            WHERE PL.CO_PLAN IN ($l_plan)");
        return $this->rs;
    }

    /**
     * InsOrdenTrabajoDocumento
     *  Inserta un nuevo documento de Orden de Trabajo y actualiza los estados del plan, adicionalmente ingresa la relación actividad
     */

    public function InsOrdenTrabajoDocumento($fe_asignacion, $ca_actividades, 
                                             $s_co_planes,   $s_co_doc_sge) {

        $trans = true;

        $this->MySQL->BeginTrans();
        try {
            $this->MySQL->ExecuteSQL("INSERT INTO I036T_ORDEN(NU_ORDEN,CO_ESTATUS_ORDEN,CA_ACTIVIDADES,FE_ASIGNACION) 
                                      VALUES (0,1,$ca_actividades,STR_TO_DATE('$fe_asignacion', '%d/%m/%Y'));");
            $bres = (boolean) $this->MySQL->ExecuteSQL("UPDATE I036T_ORDEN SET NU_ORDEN=LAST_INSERT_ID(CO_ORDEN)+1000000000;");
            $nu_orden = $this->MySQL->LastInsertId("I036T_ORDEN");
            if ($bres) {
                $bres = (boolean) $this->MySQL->ExecuteSQL("INSERT INTO C004T_PLAN_ORDEN(CO_PLAN, CO_ORDEN,FE_ASIG_ORDEN)
                                                                      SELECT  CO_PLAN,$nu_orden, STR_TO_DATE('$fe_asignacion', '%d/%m/%Y')
                                                                      FROM I037T_PLAN 
                                                                      WHERE CO_PLAN IN ($s_co_planes);");            
                //**** Se cambia a estado 2 = Asignado por espera de usuarios
                $bres = $this->MySQL->ExecuteSQL("UPDATE I037T_PLAN SET CO_ESTATUS = '2' 
                                                  WHERE CO_PLAN IN ($s_co_planes)");
        
                /**** Se inserta en la tabla C005T_PLAN_TERCERO la relacion entre una actividad planificada y el
                 **** numero de controato/orden de trabajo/solpe
                 **** validada con el SGE en caso de existir***/
                 if($s_co_doc_sge != ""){
                     $arrCoDocSge = explode(',',$s_co_doc_sge);
                     $arrCoPlanes = explode(',',$s_co_planes);

                     for($i=0; $i < count($arrCoDocSge); $i++){
                       if($arrCoDocSGE[$i] != "") {
                         $coDocSGE = $arrCoDocSge[$i];
                         $coPlanInsert= $arrCoPlanes[$i] ;
                         $bres = (boolean) $this->MySQL->ExecuteSQL("INSERT INTO C005T_PLAN_TERCERO(CO_PLAN,CO_DOC_SGE) 
                                                                     values($coPlanInsert,$coDocSGE)");

                       }
                    }
                 }
                                                      
            } else
                throw new Exception("No se pudo insertar la orden");
            
          
        } catch (ADODB_Exception $e) {
 echo($e->getMessage());
            $trans = false;
            $this->MySQL->RollbackTrans();
        } catch (Exception $e) {
 echo($e->getMessage());
             $trans = false;
            $this->MySQL->RollbackTrans();
        }

        $this->MySQL->CommitTrans();
        return $trans;
    }

    /*
     * Devuelve la lista de Estados de Orden
     */

    public function SelecListaEstadoOrden() {
        $this->rs = $this->MySQL->ExecuteCacheSQL("SELECT CO_ESTATUS_ORDEN,NB_CO_ESTATUS_ORDEN 
                                                 FROM I025T_ESTATUS_ORDEN ");
        return $this->rs;
    }

    /*
     *  Actualiza el estado de un plan
     */
    public function ActualizarEstadoPlan($co_plan, $co_estado_plan) {
        $rsl = $this->MySQL->ExecuteSQL("UPDATE I037T_PLAN 
                                          SET CO_ESTATUS_ORDEN=$co_estado_plan
                                       WHERE CO_PLAN= $co_plan");
        return $rsl;
    }
    
    public function converUnidadHoras($cantTiempo,$coUnidadTiempo){
        switch($coUnidadTiempo){
            case 'S':  //segundo
                break;
            case 'MIN'://minuto
                break;
            case 'STD'://hora   
                break;
            case 'TAG'://dia
                break;
            case 'WCH'://semana
                break;
            case 'MON'://mes
                break;
            case 'JHR'://año
                break;

        }
    }
    
    /**
     * selectHHRequeridas
     * Metodo que calcula la cantidad de horas/hombre minimas y maximas requeridas para la ejecución 
     * de una actividad especifica, segun la hoja de ruta asociada a la instalación o equipo
     * 
     * parametros:
     * $objTecnico: Objeto de la clase InventarioSAP, se aprovecha el que esté instanciado al momento de la 
     * llamada de esta función,
     * $co_grupo_hoja_ruta: codigo del grupo de hoja de ruta asociada a la instalacion/equipo
     * $va_tipo_plan: indicador tipo que puede ser:
     * I = Instalación
     * E = Equipo
     *
     */
    public function getHHRequeridas($objTecnico,$co_grupo_hoja_ruta, $va_tipo_plan){
        $res              = array();
        $mayorCantTiempo  = 0; 
        $sumaTiempo       = 0; 
        switch($va_tipo_plan){
            case 'I':
                
                $arrDatosHojaRuta = $objTecnico->SelectHojaRutaInstalacion($co_grupo_hoja_ruta);
                break;
            
            case 'E':
                $arrDatosHojaRuta = $objTecnico->SelectHojaRutaEquipo($co_grupo_hoja_ruta);                                  
                break;
        }
        for ($i = 1; $i <= count($arrDatosHojaRuta); $i++){
            if ($arrDatosHojaRuta[$i]['DAUNO'] >= $mayorCantTiempo)
                $mayorCantTiempo   = $arrDatosHojaRuta[$i]['DAUNO'];
            $sumaTiempo        = $sumaTiempo + $arrDatosHojaRuta[$i]['DAUNO'];                     
        } 
               
        
        $res[1] =  $mayorCantTiempo;
        $res[2] =  $sumaTiempo;
        return $res;
    }
    
    /**
     * selectPersonRequeridas
     * Metodo que obtiene la cantidad de perosnas minimas y maximas requeridas para la ejecución 
     * de una actividad especifica, segun la hoja de ruta asociada a la instalación o equipo
     * 
     * parametros:
     * $objTecnico: Objeto de la clase InventarioSAP, se aprovecha el que esté instanciado al momento de la 
     * llamada de esta función,
     * $co_grupo_hoja_ruta: codigo del grupo de hoja de ruta asociada a la instalacion/equipo
     * $va_tipo_plan: indicador tipo que puede ser:
     * I = Instalación
     * E = Equipo
     *
     */
    public function getPersonRequeridas($objTecnico,$co_ubic_tecnica,$co_grupo_hoja_ruta, $va_tipo_plan){
        $res              = array();
        $mayorCantPersonas  = 0; 
        $sumaPersonas       = 0; 
        switch($va_tipo_plan){
            case 'I':
                
                $arrDatosHojaRuta = $objTecnico->SelectHojaRutaInstalacion($co_ubic_tecnica);
                break;
            
            case 'E':
                $arrDatosHojaRuta = $objTecnico->SelectHojaRutaEquipo($co_ubic_tecnica);                                  
                break;
        }
        for ($i = 1; $i <= count($arrDatosHojaRuta); $i++){
            if ($arrDatosHojaRuta[$i]['ANZZL'] >= $mayorCantPersonas && $arrDatosHojaRuta[$i]['PLNNR'] == $co_grupo_hoja_ruta)
                $mayorCantPersonas   = $arrDatosHojaRuta[$i]['ANZZL'];
            $sumaPersonas        = $sumaPersonas + $arrDatosHojaRuta[$i]['ANZZL'];                     
        } 
               
        
        $res[1] =  $mayorCantPersonas;
        $res[2] =  $sumaPersonas;
        return $res;
    }
    
    /**
     * validaDocSGE
     * Metodo que obtiene los datos asociados a un número de documento SAP dado.
     * 
     * parametros:
     * $objTecnico: Objeto de la clase InventarioSAP, se aprovecha el que esté instanciado al momento de la 
     * llamada de esta función,
     * $coDocSAP: codigo del documentos SAP a validar, puede ser un número de contrato, una orden de trabajo o una SOLPE     
   
     *
     */
    public function validaDocSGE($coDocSGE){//,$coTipoMsge){
   
        $res    =  $coDocSGE;
        $valido = true;
        $objSge = new inventarioSAP();
        $rsDatosDocSGE = $objSge->SelectDetalleDocSAP($coDocSGE);
       
        if(isset($rsDatosDocSGE)){
            switch($rsDatosDocSGE['V_VALID']){
                case '0':
                    $res = "$res. No asignado";
                    $valido = false;
                    break;
                case '1':
                    $res =  "$res. Contrato";
                    break;
                case '2':
                    $res =  "$res. Pedido";
                    break;
                case '3':
                    $res =  "$res. Solped";
                    break;
                case '4':
                    $res = "$res. No es Contrato/Pedido/Solped";
                    $valido = false;
                    break;
            }            
        
            if($valido)
                switch($rsDatosDocSGE['V_VIGEN']){
                    case '0':
                        $res = "$res No Vigente";
                        break;
                    case '1':
                        $res = "$res Vigente";
                        break;       
                }
        }
        else
            $res = "No se pudo validar el número contra el SGE";
        return $res;
    }
    
}

?>
