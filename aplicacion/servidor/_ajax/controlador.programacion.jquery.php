<?php
define("TEXTO_DESACTIVADO","disabled=true");
define("TEXTO_DESACTIVADO_OCULTO","disabled=true style='display:none'");
/*
 * Controlador para devolución de datos en forma de tablas para programacion
 */
require_once('../../../aplicacion/configuracion/aut_lib.inc.php');


$JSONResponse = new Services_JSON;
switch ($_REQUEST['fnc']) {
   //--- Para programar en las siguientes 2 semanas
   case "programacion":
      $ubic_tec = $_POST['ubic_tec'];
      $grp_plan = $_POST['grp_plan'];
      $pto_plan = $_POST['pto_plan'];

      $Date = new DateTime();
      $semana_actual = $Date->format("W-Y");
      $Date->add(new DateInterval('P8D'));
      $semana_proxima = $Date->format("W-Y");
      $semanas = "'$semana_actual','$semana_proxima'";
      $objProgramacion = new programacion();
      $objInventario = new inventarioSAP();
      $objComun = new comun();

      //====  Carga los procedimientos o operacion
      //====  Carga los estados de ordenes
      $rsEstadoOrden = $objProgramacion->SelecListaEstadoOrden();
      $scombo = "<SELECT name='idmotivo' id='idmotivo_#conteo#' onChange='cargaEspera(this.id)' class=': texto' style='width:160px;'>";
      //$scombo.="<OPTION value=''>Seleccione una opci&oacute;n...</option>";
      $scombo.="<optgroup label=\"Genera Orden de Trabajo\">";
      $scombo.="<OPTION value='0'>EJECUTAR LABOR</option>";
      $scombo.="</optgroup >";
      $scombo.="<optgroup label=\"En Espera\">";

      while (!$rsEstadoOrden->EOF) {
         $scombo.= "<OPTION value='" . $rsEstadoOrden->fields['CO_ESTATUS_ORDEN'] . "'>" . $rsEstadoOrden->fields['NB_CO_ESTATUS_ORDEN'] . "</option>";
         $rsEstadoOrden->MoveNext();
      }
      $scombo.="</optgroup >";
      $scombo.="</SELECT>";

      //===== Carga la Tabla
      echo "<table id='listaSaldos' class='tablaprogramacion'>
             <thead><tr>
            <th scope='col'>C&oacute;digo / Caracter&iacute;sticas</th>
            <th scope='col'>Lugar</th>
            <th scope='col'>Actividad<br /><font face='Arial' size='0.2em'>(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></th>
            <th scope='col'>Situaci&oacute;n Actual</th>
            <th scope='col'>Fecha de Plan</th>
            <th scope='col'>Opcion</th>
            </tr></thead>
            <tbody>";
      $conteo = 0;
      $rsProgramacion = $objProgramacion->SelecActivoMantenimiento('0', $semanas, $ubic_tec, true, $grp_plan, ASIGNACION_PROGRAMACION);
      while (!$rsProgramacion->EOF) {
         $conteo++;
         echo "<tr>";
         list($grupohr, $operacionhr) = explode("-", $rsProgramacion->fields['VA_HOJARUTA']);
         if ($rsProgramacion->fields['VA_TIPO_PLAN'] == "E") {
            $arrayDatos = $objInventario->SelecDetalleEqpSAP($rsProgramacion->fields['VA_DETALLE']);
            $Desc = $arrayDatos[1]['EQKTU'];
            $sCodigo = $arrayDatos[1]['TPLNR'];
            $arrayDatos = $objInventario->SelecUbicacionTecnica($sCodigo);
            $sUbic = $arrayDatos[1]['TXT_TPLMA'];
         } else {
            $arrayDatos = $objInventario->SelecUbicacionTecnica($rsProgramacion->fields['VA_DETALLE']);
            $sUbic = $arrayDatos[1]['TXT_TPLMA'];
            $Desc = $arrayDatos[1]['PLTXU'];
         }
         //* Obtiene la ubicacion tecnica del plan
         $ubic_tec_prg = $rsProgramacion->fields['CO_UBIC_TEC'];

         //* Codigo de Equipo o Estacion y descripcion
         echo "<td nowrap>(" . ltrim($rsProgramacion->fields['VA_DETALLE'], "0") . ")<br>" . $Desc . "</td>";

         //* Lugar
         echo "<td>", utf8_encode($sUbic), "</td>";

         //* Actividad
         $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsProgramacion->fields['CO_PLAN']);
         if (!$rsProcedimiento->EOF) {
            $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
            $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
            $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
            $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong>
                          <ul>";
            while (!$rsProcedimiento->EOF) {

               $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
               $rsProcedimiento->MoveNext();
            }
            $sProc.="</ul> \"></div>";
            $sProc.= substr($textoActividad, 4) . "</td>";
         }

         echo $sProc;

         //* Combo Box
         echo "<td>" . str_replace("#conteo#", $conteo, $scombo) . "</td>";

         //* Fecha Plan
         echo "<td>" . date_format(new DateTime($rsProgramacion->fields['FE_PROXIMA_PLAN']), "d/m/Y") . "</td>";

         //* Opcion
         $sLink = "<div id=\"entorno_$conteo\">";
         $sLink.="<a id=\"programar_$conteo\"  href=\"#\" onclick=\"OpenShadow('programacion/orden_trabajo.php?co_plan={$rsProgramacion->fields['CO_PLAN']}&co_plan_pto={$rsProgramacion->fields['CO_EJEC_PLAN']}&grp_plan=" . urlencode($grp_plan) . "&pto_plan=" . urlencode($pto_plan) . "&ubic_tec=" . urlencode($ubic_tec_prg) . "');\">Programar</a>";
         $sLink.="<input id=\"detener_$conteo\" class=\"boton\" value=\"Actualizar\" type=\"button\" style=\"display: none;\" onclick=\"return ActualizarEstado(this," . $rsProgramacion->fields['CO_PLAN'] . ",idmotivo_$conteo.value);\"/>";
         $sLink.="</div>";
         echo "<td>" . $sLink . "</td>";
         echo "</tr>";
         $rsProgramacion->MoveNext();
      }

      echo "</tbody></table>";


      break;
   //  *****************  Pendiente para programa, revisa la fecha anterior a esta
   case "programacion_pendiente":
      $ubic_tec = $_POST['ubic_tec'];
      $grp_plan = $_POST['grp_plan'];
      $pto_plan = $_POST['pto_plan'];

      $Date = new DateTime();
      $dia = (int) $Date->format("N"); // Obtiene el dia de la semana
      if ($dia == 1)
         $resto = 1;
      else
         $resto = 7 - $dia;
      $Date->sub(new DateInterval("P" . $resto . "D"));

      $objInventario = new inventarioSAP();
      $objProgramacion = new programacion();
      $objComun = new comun();

      $rsProgramacion = $objProgramacion->SelecActivoMantenimientoPendiente($Date->format("d/m/Y"), $ubic_tec, $grp_plan);
      //====  Carga los estados de ordenes
      $rsEstadoOrden = $objProgramacion->SelecListaEstadoOrden();

      $scombo = "<SELECT name='idmotivo' id='idmotivo_#conteo#' onChange='cargaEspera(this.id)' class=': texto' style='width:160px;'>";
      $scombo.="<optgroup label=\"Genera Orden de Trabajo\">";
      $scombo.="<OPTION value='0'>EJECUTAR LABOR</option>";
      $scombo.="</optgroup >";
      $scombo.="<optgroup label=\"En Espera\">";
      while (!$rsEstadoOrden->EOF) {
         $scombo.= "<OPTION value='" . $rsEstadoOrden->fields['CO_ESTATUS_ORDEN'] . "'>" . $rsEstadoOrden->fields['NB_CO_ESTATUS_ORDEN'] . "</option>";
         $rsEstadoOrden->MoveNext();
      }
      $scombo.="</optgroup>";
      $scombo.="</SELECT>";
      //==== Carga la tabla
      echo "<table id='listaSaldos' class='tablaprogramacion'>
             <thead>
             <tr>
            <th scope='col'>C&oacute;digo / Caracter&iacute;sticas</th>
            <th scope='col'>Lugar</th>
            <th scope='col'>Actividad<br /><font face='Arial' size='0.2em'>(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></th>
            <th scope='col'>Situaci&oacute;n Actual</th>
            <th scope='col'>Fecha de Plan</th>
            <th scope='col'>Opcion</th>
            </tr>
            </thead>
            <tbody>";
      $conteo = 0;
      while (!$rsProgramacion->EOF) {
         $conteo++;
         echo "<tr>";
         list($grupohr, $operacionhr) = explode("-", $rsProgramacion->fields['VA_HOJARUTA']);
         if ($rsProgramacion->fields['VA_TIPO_PLAN'] == "E") {
            $arrayDatos = $objInventario->SelecDetalleEqpSAP($rsProgramacion->fields['VA_DETALLE']);
            $Desc = $arrayDatos[1]['EQKTU'];
            $sCodigo = $arrayDatos[1]['TPLNR'];
            $arrayDatos = $objInventario->SelecUbicacionTecnica($sCodigo);
            $sUbic = $arrayDatos[1]['TXT_TPLMA'];
         } else {
            $arrayDatos = $objInventario->SelecUbicacionTecnica($rsProgramacion->fields['VA_DETALLE']);
            $sUbic = $arrayDatos[1]['TXT_TPLMA'];
            $Desc = $arrayDatos[1]['PLTXU'];
         }
         //* Obtiene la ubicacion tecnica del plan
         $ubic_tec_prg = $rsProgramacion->fields['CO_UBIC_TEC'];

         //* Codigo de Equipo o Estacion y descripcion
         echo "<td nowrap>(" . ltrim($rsProgramacion->fields['VA_DETALLE'], "0") . ")<br>" . $Desc . "</td>";
         //* Lugar
         echo "<td>", utf8_encode($sUbic), "</td>";

         //* Actividad
         $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsProgramacion->fields['CO_PLAN']);
         if (!$rsProcedimiento->EOF) {
            $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
            $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
            $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
            $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong>
                          <ul>";
            while (!$rsProcedimiento->EOF) {

               $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
               $rsProcedimiento->MoveNext();
            }
            $sProc.="</ul> \"></div>";
            $sProc.= substr($textoActividad, 4) . "</td>";
         }
         echo $sProc;

         //* Combo Box
         echo "<td>" . str_replace("#conteo#", $conteo, $scombo) . "</td>";

         //* Fecha Plan
         echo "<td>" . date_format(new DateTime($rsProgramacion->fields['FE_PROXIMA_PLAN']), "d/m/Y") . "</td>";
         //* Opcion
         $sLink = "<div id=\"entorno_$conteo\">";
         $sLink.="<a id=\"programar_$conteo\" href=\"#\" onclick=\"OpenShadow('programacion/orden_trabajo.php?co_plan={$rsProgramacion->fields['CO_PLAN']}&co_plan_pto={$rsProgramacion->fields['CO_EJEC_PLAN']}&grp_plan=" . urlencode($grp_plan) ."&pto_plan=" . urlencode($pto_plan)."&ubic_tec=" . urlencode($ubic_tec_prg) . "');\">Programar</a>";
         $sLink.="<input id=\"detener_$conteo\" class=\"boton\" value=\"Actualizar\" type=\"button\" style=\"display: none;\"  onclick=\"return ActualizarEstado(this," . $rsProgramacion->fields['CO_PLAN'] . ",$('idmotivo_$conteo').value);\"/>";
         $sLink.="</div>";

         echo "<td>" . $sLink . "</td>";
         echo "</tr>";
         $rsProgramacion->MoveNext();
      }

      echo "</tbody></table>";

      break;

   // ***************  Actividades Programadas
   case "actividad_programada":
      $ubic_tec = $_POST['ubic_tec'];
      $grp_plan = $_POST['grp_plan'];

      $Date = new DateTime();
      $semana_actual = $Date->format("W-Y");
      $Date->add(new DateInterval('P8D'));
      $semana_proxima = $Date->format("W-Y");
      $semanas = "'$semana_actual','$semana_proxima'";
      $objProgramacion = new programacion();
      $objInventario = new inventarioSAP();
      $objComun = new comun();

      //====  Carga los procedimientos o operacion
      //====  Carga los estados de ordenes
      $rsEstadoOrden = $objProgramacion->SelecListaEstadoOrden();
      $scombo = "<SELECT name='idmotivo' id='idmotivo_#conteo#' onChange='cargaEspera(this.id)' class=': texto' style='width:160px;'>";
      $scombo.="<OPTION value=''>Seleccione una opci&oacute;n...</option>";
      while (!$rsEstadoOrden->EOF) {
         $scombo.= "<OPTION value='" . $rsEstadoOrden->fields['CO_ESTATUS_ORDEN'] . "'>" . $rsEstadoOrden->fields['NB_CO_ESTATUS_ORDEN'] . "</option>";
         $rsEstadoOrden->MoveNext();
      }
      $scombo.="</SELECT>";

      //===== Carga la Tabla
      echo "<table id='listaSaldos' class='tablaprogramacion'>
             <thead><tr>
            <th scope='col'>C&oacute;digo / Caracter&iacute;sticas</th>
            <th scope='col'>Lugar</th>
            <th scope='col'>Actividad<br /><font face='Arial' size='0.2em'>(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></th>
            <th scope='col'>Situaci&oacute;n Actual</th>
            <th scope='col'>Fecha de Plan</th>
            <th scope='col'>Opcion</th>
            </tr></thead>
            <tbody>";
      $conteo = 0;
      $rsProgramacion = $objProgramacion->SelecActivoMantenimientoProgramados('1,2', $ubic_tec, $grp_plan, ASIGNACION_ESPECIFICA . ',' . ASIGNACION_PROGRAMACION);
      while (!$rsProgramacion->EOF) {
         $conteo++;
         echo "<tr>";
         list($grupohr, $operacionhr) = explode("-", $rsProgramacion->fields['VA_HOJARUTA']);
         if ($rsProgramacion->fields['VA_TIPO_PLAN'] == "E") {
            $arrayDatos = $objInventario->SelecDetalleEqpSAP($rsProgramacion->fields['VA_DETALLE']);
            $Desc = $arrayDatos[1]['EQKTU'];
            $sCodigo = $arrayDatos[1]['TPLNR'];
            $arrayDatos = $objInventario->SelecUbicacionTecnica($sCodigo);
            $sUbic = $arrayDatos[1]['TXT_TPLMA'];
         } else {
            $arrayDatos = $objInventario->SelecUbicacionTecnica($rsProgramacion->fields['VA_DETALLE']);
            $sUbic = $arrayDatos[1]['TXT_TPLMA'];
            $Desc = $arrayDatos[1]['PLTXU'];
         }

         //* Codigo de Equipo o Estacion y descripcion
         echo "<td nowrap>(" . ltrim($rsProgramacion->fields['VA_DETALLE'], "0") . ")<br>" . $Desc . "</td>";
         //* Lugar
         echo "<td>", utf8_encode($sUbic), "</td>";

         //* Actividad
//            $countProc = count($arrayProcedimiento);
//            if ($countProc > 0) {
//                $sProc = "<td class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\"  onmouseout=\"return onMouseLeaveProg(this);\" >";
//                $textoActividad = $arrayProcedimiento[1]['KTEXT'];
//                $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $arrayProcedimiento[1]['PLNAL'] . ":" . $textoActividad . "</strong>
//                          <ul>";
//                for ($i = 1; $i < $countProc; $i++) {
//                    if ($operacionhr == $arrayProcedimiento[$i]['PLNAL'])
//                        $sProc.="<li>-" . $arrayProcedimiento[$i]['LTXA1'] . "</li>";
//                }
//                $sProc.="</ul> \"></div>";
//                $sProc.= substr($textoActividad, 4) . "</td>";
//            } else
//                $sProc = "<td class=\"text\"></td>";

         $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsProgramacion->fields['CO_PLAN']);
         if (!$rsProcedimiento->EOF) {
            $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
            $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
            $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
            $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong>
                          <ul>";
            while (!$rsProcedimiento->EOF) {

               $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
               $rsProcedimiento->MoveNext();
            }
            $sProc.="</ul> \"></div>";
            $sProc.= substr($textoActividad, 4) . "</td>";
         }
         echo $sProc;

         //* Combo Box
         if ($rsProgramacion->fields['CO_ESTATUS'] == 1)
            echo "<td>(Actividad Asignada)</td>";
         else
            echo "<td>" . str_replace("#conteo#", $conteo, $scombo) . "</td>";

         //* Fecha Plan
         echo "<td>" . date_format(new DateTime($rsProgramacion->fields['FE_PROXIMA_PLAN']), "d/m/Y") . "</td>";
         //* Opcion
         $sLink = "<div id=\"entorno_$conteo\">";
         $sLink.="<input id=\"detener_$conteo\" class=\"boton\" value=\"Actualizar\" type=\"button\" style=\"display: none;\"  onclick=\"return ActualizarEstado(this," . $rsProgramacion->fields['CO_PLAN'] . ",$('idmotivo_$conteo').value);\"/>";
         $sLink.="</div>";

         echo "<td>" . $sLink . "</td>";
         echo "</tr>";
         $rsProgramacion->MoveNext();
      }

      echo "</tbody></table>";

      break;
   //******************* Actividad Aplazada  o En espera
   case "actividad_aplazada":
      $ubic_tec = $_POST['ubic_tec'];
      $grp_plan = $_POST['grp_plan'];

      $objProgramacion = new programacion();
      $objInventario = new inventarioSAP();
      $objComun = new comun();
      //====  Carga los procedimientos o operacion
      //====  Carga los estados de ordenes
      $rsEstadoOrden = $objProgramacion->SelecListaEstadoOrden();
      $scombo = "<SELECT name='idmotivo' id='idmotivo_#conteo#' onChange='cargaEspera(this.id)' class=': texto' style='width:160px;'>";
      $scombo.="<OPTION value='0'>>>CONTINUAR<<</option>";
      while (!$rsEstadoOrden->EOF) {
         $scombo.= "<OPTION value='" . $rsEstadoOrden->fields['CO_ESTATUS_ORDEN'] . "'>" . $rsEstadoOrden->fields['NB_CO_ESTATUS_ORDEN'] . "</option>";
         $rsEstadoOrden->MoveNext();
      }
      $scombo.="</SELECT>";

      //===== Carga la Tabla
      echo "<table id='listaSaldos' class='tablaprogramacion'>
             <thead><tr>
            <th scope='col'>C&oacute;digo / Caracter&iacute;sticas</th>
            <th scope='col'>Lugar</th>
            <th scope='col'>Actividad<br /><font face='Arial' size='0.2em'>(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></th>
            <th scope='col'>Situaci&oacute;n Actual</th>
            <th scope='col'>Fecha de Plan</th>
            <th scope='col'>Opcion</th>
            </tr></thead>
            <tbody>";
      $conteo = 0;
      $rsProgramacion = $objProgramacion->SelecActivoMantenimientoAplazado($ubic_tec, $grp_plan);
      while (!$rsProgramacion->EOF) {
         $conteo++;
         echo "<tr>";
         list($grupohr, $operacionhr) = explode("-", $rsProgramacion->fields['VA_HOJARUTA']);
         if ($rsProgramacion->fields['VA_TIPO_PLAN'] == "E") {
            $arrayDatos = $objInventario->SelecDetalleEqpSAP($rsProgramacion->fields['VA_DETALLE']);
            $Desc = $arrayDatos[1]['EQKTU'];
            $sCodigo = $arrayDatos[1]['TPLNR'];
            $arrayDatos = $objInventario->SelecUbicacionTecnica($sCodigo);
            $sUbic = $arrayDatos[1]['TXT_TPLMA'];
         } else {
            $arrayDatos = $objInventario->SelecUbicacionTecnica($rsProgramacion->fields['VA_DETALLE']);
            $sUbic = $arrayDatos[1]['TXT_TPLMA'];
            $Desc = $arrayDatos[1]['PLTXU'];
         }

         //* Codigo de Equipo o Estacion y descripcion
         echo "<td nowrap>(" . ltrim($rsProgramacion->fields['VA_DETALLE'], "0") . ")<br>" . $Desc . "</td>";
         //* Lugar
         echo "<td>" . utf8_encode($sUbic) . "</td>";

         //* Actividad

         $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsProgramacion->fields['CO_PLAN']);
         if (!$rsProcedimiento->EOF) {
            $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
            $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
            $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
            $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong>
                          <ul>";
            while (!$rsProcedimiento->EOF) {

               $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
               $rsProcedimiento->MoveNext();
            }
            $sProc.="</ul> \"></div>";
            $sProc.= substr($textoActividad, 4) . "</td>";
         }
         echo $sProc;

         //* Combo Box
         $sfind = "<OPTION value='" . $rsProgramacion->fields['CO_ESTATUS_ORDEN'] . "'>"; // Valor a buscar
         $sfindselected = "<OPTION value='" . $rsProgramacion->fields['CO_ESTATUS_ORDEN'] . "' selected>"; // Valor a reemplazar para seleccionarlo
         echo "<td>" . str_replace($sfind, $sfindselected, str_replace("#conteo#", $conteo, $scombo)) . "</td>";

         //* Fecha Plan
         echo "<td>" . date_format(new DateTime($rsProgramacion->fields['FE_PROXIMA_PLAN']), "d/m/Y") . "</td>";
         //* Opcion
         $sLink = "<div id=\"entorno_$conteo\">";
         $sLink.="<input id=\"continuar_$conteo\" class=\"boton\" value=\"Reactivar\" type=\"button\" style=\"display: none;\" onclick=\"return ActualizarEstado(this," . $rsProgramacion->fields['CO_PLAN'] . ",0);\"/>";
         $sLink.="<input id=\"detener_$conteo\" class=\"boton\" value=\"Actualizar\" type=\"button\"  onclick=\"return ActualizarEstado(this," . $rsProgramacion->fields['CO_PLAN'] . ",jQuery('#idmotivo_$conteo').val());\"/>";
         $sLink.="</div>";

         echo "<td>" . $sLink . "</td>";
         echo "</tr>";

         $rsProgramacion->MoveNext();
      }

      echo "</tbody></table>";

      break;


//---- Actualiza un plan a aplazada
   case "actualizar_plan":
      $objProgramacion = new programacion();
      $co_plan = $_POST['co_plan'];
      $co_estado_plan = $_POST['co_estado_plan'];
      if ($objProgramacion->ActualizarEstadoPlan($co_plan, $co_estado_plan))
         echo "1";
      else
         echo "0";
      break;

//---- Muestra mensaje asincrono de la validacion del formato del documento SGE
   case "msge_valida_formato_doc_sge":

      echo " <span style=\"color:#f00;font-size:x-small;\"><b>***" . utf8_encode($_GET['msge']) . "***</b></span>";
      break;

//---- Valida si un número introducido por pantalla es un número de contrato, orden de trabajo o solpe en SAP
   case "valida_doc_sge":

      $objProgramacion = new programacion();
      $resDatosDocSge = $objProgramacion->validaDocSGE($_GET['co_doc_sge']);
      echo " <span style=\"color:#f00;font-size:x-small;\"><b>$resDatosDocSge</b></span>";
      break;

//*--- Para cargar los datos de la orden
   case "ordactivos_ubic":
      $objProgramacion = new programacion();
      $objInventario = new inventarioSAP();

      $co_plan = $_POST['co_plan'];
      $fecha_prm = $_POST['fecha_tope'];
      $ubic_tec = $_POST['ubic_tec'];
      $grp_plan = $_POST['grp_plan'];
      $pto_plan = $_POST['pto_plan'];
      $show_only = $_POST['showAll'];
      if(empty($show_only)) $show_only=false;

      //---- Asigna la fecha tope para mostrar en la orden y corrige si está no es el último dia de la semana
      if (!empty($fecha_prm)) {
         list($dia, $mes, $ano) = explode("/", $fecha_prm);
         $fecha_tope = new DateTime();
         $fecha_tope->setDate($ano, $mes, $dia);
      } else {
         $fecha_tope = new DateTime;
         $fecha_tope->add(new DateInterval("P28D"));
         $dia_semana = (int) $fecha_tope->format("N");         
         if ($dia_semana < 7)
            $fecha_tope->add(new DateInterval("P" . (7 - $dia_semana) . "D")); 
      }
      echo '<input type="hidden" name="fecha_tope" id="fecha_tope" value="'.$fecha_tope->format("d/m/Y").'">';
      //-----------------------
      $rsProgramacion = $objProgramacion->SelectActivosUbicacion($co_plan, $fecha_tope->format("d/m/Y"), $ubic_tec, $grp_plan,$pto_plan,$show_only);
      $cantPersonas = 0;
      $cantPersonasMin = 0;
      $cantPersonasTotal = 0;
      $conteo = 1;
      $today = time();

      while (!$rsProgramacion->EOF) {

         list($grupohr, $operacionhr) = explode("-", $rsProgramacion->fields['VA_HOJARUTA']);

         if ($rsProgramacion->fields['VA_TIPO_PLAN'] == "E") {
            $arrayDatos = $objInventario->SelecDetalleEqpSAP($rsProgramacion->fields['VA_DETALLE']);
            $Desc = $arrayDatos[1]['EQKTU'];
            $sCodigo = $arrayDatos[1]['TPLNR'];
         } else {
            $arrayDatos = $objInventario->SelecUbicacionTecnica($rsProgramacion->fields['VA_DETALLE']);
            $Desc = $arrayDatos[1]['PLTXU'];
         }
         $dbdate = strtotime($rsProgramacion->fields['FE_PROXIMA_PLAN']);
         if ($dbdate > $today)
            echo "<tr>";
         else
            echo "<tr title='Actividad pendiente' class=\"selected\">";


         //* Número de actividad asociada a la orden
         echo "<td>" . $conteo  . "</td>";
         //* Codigo de Equipo o Estacion y descripcion
         echo "<td>" . ltrim($rsProgramacion->fields['VA_DETALLE'], "0") . "/" . $Desc . "</td>";

         //* Actividad
         $objComun = new comun();
         $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsProgramacion->fields['CO_PLAN']);
         if (!$rsProcedimiento->EOF) {
            $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
            $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
            $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
            $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong><ul>";
            while (!$rsProcedimiento->EOF) {
               //* Busca las capacidades mix.  y max. requeridas para la ejecución
               $cantPersonas = (int) $rsProcedimiento->fields['CAPACIDAD'];
               if ($cantPersonas > $cantPersonasMin)
                  $cantPersonasMin = $cantPersonas;
               $cantPersonasTotal+=$cantPersonas;

               $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
               $rsProcedimiento->MoveNext();
            }
            $sProc.="</ul> \"></div>";
            $sProc.= substr($textoActividad, 4) . "</td>";
         }
         echo $sProc;

         //* Horas/Hombre mínimas requeridas
         echo "<td align = center>" . $cantPersonasMin . "</td>";
         //* Horas/Hombre maximas requeridas
         echo "<td align = center>" . $cantPersonasTotal . "</td>";
         //* Fecha Plan
         list($ano, $mes, $dia) = explode("-", $rsProgramacion->fields['FE_PROXIMA_PLAN']);
         $fecha_muestra = new DateTime();
         $fecha_muestra->setDate($ano, $mes, $dia);
         echo "<td align = center>" . $fecha_muestra->format("d/m/Y") . "</td>";

         // Coloca Desactivado si tiene plan a tercero, en caso contrario lo oculta
         if ($rsProgramacion->fields['IN_TERCERO'])$docSgeStatus = TEXTO_DESACTIVADO;
                                              else $docSgeStatus = TEXTO_DESACTIVADO_OCULTO;

         //* Opcion
         if ($rsProgramacion->fields['CO_PLAN'] == $co_plan) {
            $esChecked = "checked disabled=true";
            $sfechaMinima = $rsProgramacion->fields['FE_PROXIMA_PLAN'];
            if($docSgeStatus == TEXTO_DESACTIVADO)  $docSgeStatus = ""; //Deja activo el Documento pq está seleccionado
         } else {
            $esChecked = "";
         }
            
         echo "<td><input type=\"checkbox\" name=\"ListaPlanes[]\" value=\"" , $rsProgramacion->fields['CO_PLAN'], "\" $esChecked  ", (($rsProgramacion->fields['IN_TERCERO']) ? "onclick=\"javascript:activaCampo($conteo)\"" : "&nbsp;" ), " onBlur=javascript:jq(\"#validaDocSgeDisp\").hide();></td>";
         echo "<td><input type=\"textbox\" name=\"DocSGE[]\" maxlength=\"10\" size=\"10\" onkeyup=\"if(getEnter(event) == true) validaFormatDocSGE(this)\" onBlur=\"jq('#validaDocSgeDisp').hide();\" $docSgeStatus ></td>";
         echo "</tr>";

         $rsProgramacion->MoveNext();
         $conteo++;
      }
      break;
}
?>
