<?php

require_once('../../../aplicacion/configuracion/aut_lib.inc.php');

$hoy = new Datetime("now");

$JSONResponse = new Services_JSON;
switch ($_REQUEST['fnc']) {
    case "buscar_usuario" :
        $tx_busqueda=$_POST['tx_busqueda'];
        $co_cargo= $_POST['co_cargo'];

        $objGestionPersonal = new gestion_personal();
        $rsUsuario=$objGestionPersonal->SelecConsultaPersonal($tx_busqueda,$co_cargo);
        
        if(!$rsUsuario->EOF) {
            echo "<hr><span class='etiqueta'>Trabajador: ",
                 $rsUsuario->fields['NB_NOMBRE'],
                 " (",$rsUsuario->fields['NB_INDICADOR'],") ",
                 "<br>Cargo: ",$rsUsuario->fields['NB_CARGO'],
                 "<br>Estado:",($rsUsuario->fields['IN_DISPONIBLE']!=1)? "<b>DISPONIBLE</b>":"<font style=\"color:red\">NO DISPONIBLE</font></span>";	
            echo "<br><br><input type='button' id='ausencia' class='boton_min' size='200' value='Registrar Ausencia' onclick=\"OpenShadow('gestion_personal/ausencia.php?co_usuario={$rsUsuario->fields['CO_USUARIO']}&co_cargo={$co_cargo}',341,720);\">";
            
        } else
            echo  "<font face='Arial' size='2' color='red'>".stripslashes("Su busqueda no arroj&oacute; resultados para <strong>$tx_busqueda</strong>")."</font>";
        break;
    case "grafico_personal":
        $MesNombre=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $objGestionPersonal = new gestion_personal();
        $rsUsuarioG=$objGestionPersonal->SelecGrafPersonalVaca(date("Y"));
        
        $array=array();        
        while (!$rsUsuarioG->EOF) {
            $array[]=array($MesNombre[$rsUsuarioG->fields['MesReporte']],(int)$rsUsuarioG->fields['TotalReporte'],(int)$rsUsuarioG->fields['TotalReporte']);            
            $rsUsuarioG->MoveNext();
        }
        
        echo $JSONResponse->encode($array);
        break;
        
    case "buscar_usuario_reemplazo" :
        $tx_busqueda=$_POST['tx_busqueda'];
        $co_cargo= $_POST['co_cargo'];

        $objGestionPersonal = new gestion_personal();
        $rsUsuario=$objGestionPersonal->SelecConsultaPersonal($tx_busqueda,$co_cargo);
        
        $array=null;
        if(!$rsUsuario->EOF) {
            $array["CO_USUARIO"] = $rsUsuario->fields['CO_USUARIO'];
            $array["NB_NOMBRE"] = $rsUsuario->fields['NB_NOMBRE'];
            $array["IN_DISPONIBLE"] = $rsUsuario->fields['IN_DISPONIBLE'];
            $array["NB_INDICADOR"] = $rsUsuario->fields['NB_INDICADOR'];
            $array["NB_CARGO"] = $rsUsuario->fields['NB_CARGO'];

            $listaDisp="";
            $rsDisp=$objGestionPersonal->SelecNoDisponible($rsUsuario->fields['CO_USUARIO']);
            if(!$rsDisp->EOF) {
               $listaDisp="<h2 style='color:black'>Ausencias Registradas</h2><ul>";
            while(!$rsDisp->EOF) {
               $sPeriodo=sprintf("<li>%s al %s </li>",date_format(new DateTime($rsDisp->fields['FE_INICIO']),"d/m/Y"),date_format(new DateTime($rsDisp->fields['FE_FIN']),"d/m/Y"));
               $listaDisp.=$sPeriodo;
               $rsDisp->MoveNext();
             }
             $listaDisp.="</ul>";
            }
            $array["NO_DISPONIBLE"] = $listaDisp;
        } 
            
        echo $JSONResponse->encode($array);        

        break;
        
}
?>
