<?php

require_once('../../../aplicacion/configuracion/aut_lib.inc.php');

$JSONResponse = new Services_JSON;
switch ($_REQUEST['fnc']) {
    case "UsuarioLDAP":
        $objComun = new seguridad();
        $username = $_REQUEST['username'];

        $arrayResult = $objComun->selDatosLDAP($username);
        echo $JSONResponse->encode($arrayResult);
        break;
    case "UsuarioRegistrado":
        $rsValidaUser = new registro_personal();
        if (intval($rsValidaUser->usuarioExiste($_POST['indicador'])) > 0)
            echo "El indicador " . $_POST['indicador'] . "  se encuentra registrado...";
        else echo "";
        break;
}
?>
