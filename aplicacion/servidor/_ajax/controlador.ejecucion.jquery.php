<?php

/*
 * Controlador para devolución de datos en forma de tablas para programacion
 */
require_once('../../../aplicacion/configuracion/aut_lib.inc.php');

$hoy = new Datetime("now");

$JSONResponse = new Services_JSON;
switch ($_REQUEST['fnc']) {
    //***** Muestra la cartelera
    case "cartelera":

        $co_usuario = $_POST['co_usuario'];
        $fecha = DateTime::createFromFormat("d/m/Y",$_POST['fecha']);

        echo '<table class="tablaprogramacion">
             <thead>
            <tr>
            <th scope="col"></th>
            <th scope="col">&nbsp;</th>
            <th scope="col">No de Orden</th>            
            <th scope="col" width="60%">Lugar</th>            
            <th scope="col">Fecha de <br>Asignaci&oacute;n</th>
            <th scope="col">% Total de<br>Ejecucion</th>
            <th scope="col">Opcion</th>
            </tr>
             </thead>';

        $objEjecucion = new ejecucion();
        $objInventario = new inventarioSAP();
        $objComun = new comun();
        //====  Carga los procedimientos o operacion
        //====  Carga los estados de ordenes

        $conteo = 0;
        $conteoActividad = 0;
        $rsEjecucion = $objEjecucion->SelCarteleraEjecucion($co_usuario,$fecha);
        $nroOrdenActual = "";
        while (!$rsEjecucion->EOF) {
            $conteo++;
            $conteoActividad++;
            //*** Agrupa la orden
            $objComun->SelectUbicDesc($objInventario,
                                          $rsEjecucion->fields['VA_HOJARUTA'],$rsEjecucion->fields['VA_DETALLE'],$rsEjecucion->fields['VA_TIPO_PLAN'],
                                          $Desc,$sUbic);
            if ($rsEjecucion->fields['NU_ORDEN'] != $nroOrdenActual) {
                $conteoActividad = 1;
                $fechaAsig = new DateTime($rsEjecucion->fields['FE_ASIGNACION']);
                //********** Cabecera de la Orden ********************
                $param=encryptLink("orden_trabajo=".$rsEjecucion->fields['NU_ORDEN']."&co_usuario=".$co_usuario);

                echo "<tbody id='orden'>", "<tr>";
                echo "<td>(<a class=\"ClassMas\" href='#'>+</a>)</td>",
                "<td>&nbsp;</td>",
                "<td>", $rsEjecucion->fields['NU_ORDEN'], "</td>",
                "<td>", utf8_encode($sUbic), "</td>",
                "<td align=\"center\" title='", utf8_encode(strftime("%A, %d de %B", $fechaAsig->getTimestamp())), "'>",
                date_format($fechaAsig, "d/m/Y"),
                "</td>",
                "<td align=\"center\">", $rsEjecucion->fields['PC_AVANCE'], "%</td>",
                "<td align=\"center\"><a class='printButton' target='_blank' href='reportes/formato_procedimiento.php?p={$param}'></a></td>",
                "</tr>";
                echo "</tbody>";
                echo "<tbody id=\"detalle\">";

                $nroOrdenActual = $rsEjecucion->fields['NU_ORDEN'];

                //****** Titulo del Subdetalle **********************
                echo "<tr class='inamovibletitulo'>",
                "<td class='inamovibleblanco'></td>",
                "<td>No. Actividad</td>",
                "<td>Fecha Plan</td>",
                "<td>C&oacute;digo / Caracter&iacute;sticas</td>",
                "<td align=\"center\">Actividad<br><font face='Arial' size='0.2em'>(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></td>",
                "<td nowrap>% Ejecutado</td>",
                "<td></td>",
                "</tr>";
            }
            //**** Fin agrupa orden

           echo "<tr class='inamovible'>";
            //* Blanco
            echo "<td class='inamovibleblanco'></td>";
            echo "<td>",$rsEjecucion->fields['NU_ORDEN']."-".$conteoActividad, "</td>";
            $plan_date = new DateTime($rsEjecucion->fields['FE_PROXIMA_PLAN']);
            if ($plan_date < $fecha)
                echo "<td>", "<font color=\"red\">", date_format($plan_date, "d/m/Y"), "</font>", "</td>";
            else
                echo "<td>", date_format($plan_date, "d/m/Y"), "</td>";

            //* Codigo de Equipo o Estacion y descripcion
            echo "<td nowrap>(", ltrim( $rsEjecucion->fields['VA_DETALLE'],"0"), ")<BR>", $Desc, "</td>";
            //* Actividad
            $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsEjecucion->fields['CO_PLAN']);
            if (!$rsProcedimiento->EOF) {
                $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
                $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
                $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
                $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong>
                          <ul>";
                while (!$rsProcedimiento->EOF) {

                    $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
                    $rsProcedimiento->MoveNext();
                }
                $sProc.="</ul> \"></div>";
                $sProc.= substr($textoActividad, 4) . "</td>";
            }
            echo $sProc;

            //* Porcentaje de Avance de la hoja de ruta
            echo "<td align=\"center\">", $rsEjecucion->fields['NU_PORCENTAJE_AVANCE'], "%</td>";
            //* Enlace
            $sLink = "<div id=\"entorno_$conteo\">";
            $sLink.="<input id=\"ejecutar_$conteo\"  type=\"button\"  class=\"boton\" onclick=\"OpenShadow('ejecucion/ejecutar.php?co_actividad={$rsEjecucion->fields['CO_ACTIVIDAD']}&co_usuario={$co_usuario}',551);\" value=\"Ejecutar\">";
            $sLink.="</div>";
            echo "<td>", $sLink, "</td>";
            echo "</tr>";
            $rsEjecucion->MoveNext();
        }

        echo "</tbody>";
        echo "</table>";
        break;

    //************************************************************************************     
    case "ordenes" :

        $co_usuario = $_POST['co_usuario'];
        $fecha = DateTime::createFromFormat("d/m/Y",$_POST['fecha']);
        echo '<table class="tablaprogramacion">
             <thead>
            <tr>
            <th scope="col"></th>
            <th scope="col">&nbsp;</th>
            <th scope="col">No de Orden</th>            
            <th scope="col" width="60%">Lugar</th>            
            <th scope="col" nowrap>Fecha de Orden</th>
            <th scope="col">% Total de<br>Ejecucion</th>
            <th scope="col">Opcion</th>
            </tr>
             </thead>';

        $objEjecucion = new ejecucion();
        $objInventario = new inventarioSAP();
        $objComun = new comun();
        //====  Carga los procedimientos o operacion
        //====  Carga los estados de ordenes

        $conteo = 0;
        $conteoActividad = 0;
        $rsEjecucion = $objEjecucion->SelecOrdenesActivas($co_usuario, $fecha);
        $nroOrdenActual = "";
        while (!$rsEjecucion->EOF) {
            $conteo++;
            $conteoActividad++;
            //*** Agrupa la orden
             $objComun->SelectUbicDesc($objInventario,
                                       $rsEjecucion->fields['VA_HOJARUTA'],$rsEjecucion->fields['VA_DETALLE'],$rsEjecucion->fields['VA_TIPO_PLAN'],
                                       $Desc,$sUbic);
            if ($rsEjecucion->fields['NU_ORDEN'] != $nroOrdenActual) {
                $conteoActividad = 1;
                $fechaAsig = new DateTime($rsEjecucion->fields['FE_ASIG_ORDEN']);

                $sLink = "<div id=\"entorno_$conteo\">";
                $sLink.="<input id=\"cerrar_$conteo\"  type=\"button\"  class=\"boton\" onclick=\"OpenShadow('ejecucion/cerrar.php?co_orden=" . $rsEjecucion->fields['CO_ORDEN'] . "');\" value=\"Cerrar\">";
                $sLink.="</div>";

                //********** Cabecera de la Orden ********************
                echo "<tbody id='orden'>", "<tr>";
                echo "<td>(<a class=\"ClassMas\" href='#'>+</a>)</td>",
                "<td>&nbsp;</td>",
                "<td>", $rsEjecucion->fields['NU_ORDEN'], "</td>",
                "<td>", utf8_encode($sUbic), "</td>",
                "<td align=\"center\" title='", utf8_encode(strftime("%A, %d de %B", $fechaAsig->getTimestamp())), "'>",
                date_format($fechaAsig, "d/m/Y"),
                "</td>",
                "<td align=\"center\">", $rsEjecucion->fields['PC_AVANCE_ORDEN'], "%</td>",
                "<td>", $sLink, "</td>",
                "</tr>";
                echo "</tbody>";
                echo "<tbody id=\"detalle\">";

                $nroOrdenActual = $rsEjecucion->fields['NU_ORDEN'];

                //****** Titulo del Subdetalle **********************
                echo "<tr class='inamovibletitulo'>",
                "<td class='inamovibleblanco'></td>",
                "<td>No. Actividad</td>",
                "<td>Fecha Plan</td>",
                "<td>C&oacute;digo / Caracter&iacute;sticas</td>",
                "<td align=\"center\">Actividad</td>",
                "<td nowrap>% Ejecutado</td>",
                "<td align=\"center\">Estado</td>",
                "</tr>";
            }
            //**** Fin agrupa orden

            echo "<tr class='inamovible'>";
            //* Blanco
            echo "<td class='inamovibleblanco'></td>";
            echo "<td>", $rsEjecucion->fields['NU_ORDEN']."-".$conteoActividad, "</td>";
            $plan_date = new DateTime($rsEjecucion->fields['FE_PROXIMA_PLAN']);
            if ($plan_date < $fecha)
                echo "<td>", "<font color=\"red\">", date_format($plan_date, "d/m/Y"), "</font>", "</td>";
            else
                echo "<td>", date_format($plan_date, "d/m/Y"), "</td>";

            //* Codigo de Equipo o Estacion y descripcion
            echo "<td nowrap>(", $rsEjecucion->fields['VA_DETALLE'], ")<BR>", $Desc, "</td>";

            //* Actividad
            echo "<td nowrap>", $rsEjecucion->fields['TX_HOJARUTA'], "</td>";

            //* Porcentaje de Avance de la hoja de ruta
            echo "<td align=\"center\">", $rsEjecucion->fields['PC_AVANCE_ACTIVIDAD'], "%</td>";
            if ($rsEjecucion->fields['CO_ESTATUS']=='3'){
                $Estado = "Cerrada";
            }else{
                $Estado = "En ejecucion";
            };
            //* Estado de la actividad
            echo "<td align=\"center\">", $Estado, "</td>";
            echo "</tr>";
            $rsEjecucion->MoveNext();
        }

        echo "</tbody>";
        echo "</table>";
        break;
//************************************************************************************     
    case "ordenesR" :

        $estatus = $_POST['estatus'];
        $fechai = DateTime::createFromFormat("d/m/Y",$_POST['fechai']);
        $fechaf = DateTime::createFromFormat("d/m/Y",$_POST['fechaf']);
        echo '<table class="tablaprogramacion">
             <thead>
            <tr>
            <th scope="col"></th>
            <th scope="col">&nbsp;</th>
            <th scope="col">No de Orden</th>            
            <th scope="col" width="60%">Lugar</th>            
            <th scope="col" nowrap>Fecha de Orden</th>
            <th scope="col">% Total de<br>Ejecucion</th>
            <th scope="col">Estatus</th>
            </tr>
             </thead>';

        $objEjecucion = new ejecucion();
        $objInventario = new inventarioSAP();
        $objComun = new comun();
        //====  Carga los procedimientos o operacion
        //====  Carga los estados de ordenes

        $conteo = 0;
        $conteoActividad = 0;
        $rsEjecucion = $objEjecucion->SelecOrdenesR($fechai, $fechaf, $estatus);
        $nroOrdenActual = "";
        while (!$rsEjecucion->EOF) {
            $conteo++;
            $conteoActividad++;
            //*** Agrupa la orden
             $objComun->SelectUbicDesc($objInventario,
                                       $rsEjecucion->fields['VA_HOJARUTA'],$rsEjecucion->fields['VA_DETALLE'],$rsEjecucion->fields['VA_TIPO_PLAN'],
                                       $Desc,$sUbic);
            if ($rsEjecucion->fields['NU_ORDEN'] != $nroOrdenActual) {
                $conteoActividad = 1;
                $fechaAsig = new DateTime($rsEjecucion->fields['FE_ASIG_ORDEN']);
                if($rsEjecucion->fields['ESTATUS_ORDEN'] == 1){
                    if($rsEjecucion->fields['TX_ESTATUS'] == '0')
                        $estatusT = 'Creada sin asignacion'; 
                    if($rsEjecucion->fields['TX_ESTATUS'] == '1')
                        $estatusT = 'En ejecucion';
                    if($rsEjecucion->fields['TX_ESTATUS'] == '2')
                        $estatusT = 'Asignacion de personal';
                    if($rsEjecucion->fields['TX_ESTATUS'] == '3')
                        $estatusT = 'Cerrada';
                }
                else{
                  $estatusT = $rsEjecucion->fields['NB_CO_ESTATUS_ORDEN'];  
                }                
                
                //********** Cabecera de la Orden ********************
                echo "<tbody id='orden'>", "<tr>";
                echo "<td>(<a class=\"ClassMas\" href='#'>+</a>)</td>",
                "<td>&nbsp;</td>",
                "<td>", $rsEjecucion->fields['NU_ORDEN'], "</td>",
                "<td>", utf8_encode($sUbic), "</td>",
                "<td align=\"center\" title='", utf8_encode(strftime("%A, %d de %B", $fechaAsig->getTimestamp())), "'>",
                date_format($fechaAsig, "d/m/Y"),
                "</td>",
                "<td align=\"center\">", $rsEjecucion->fields['PC_AVANCE_ORDEN'], "%</td>",
                "<td>", $estatusT, "</td>",
                "</tr>";
                echo "</tbody>";
                echo "<tbody id=\"detalle\">";

                $nroOrdenActual = $rsEjecucion->fields['NU_ORDEN'];

                //****** Titulo del Subdetalle **********************
                echo "<tr class='inamovibletitulo'>",
                "<td class='inamovibleblanco'></td>",
                "<td>No. Actividad</td>",
                "<td>Fecha Plan</td>",
                "<td>C&oacute;digo / Caracter&iacute;sticas</td>",
                "<td align=\"center\">Actividad</td>",
                "<td nowrap>% Ejecutado</td>",
                "<td></td>",
                "</tr>";
            }
            //**** Fin agrupa orden

            echo "<tr class='inamovible'>";
            //* Blanco
            echo "<td class='inamovibleblanco'></td>";
            echo "<td>", $rsEjecucion->fields['NU_ORDEN']."-".$conteoActividad, "</td>";
            $plan_date = new DateTime($rsEjecucion->fields['FE_PROXIMA_PLAN']);
            if ($plan_date < $fecha)
                echo "<td>", "<font color=\"red\">", date_format($plan_date, "d/m/Y"), "</font>", "</td>";
            else
                echo "<td>", date_format($plan_date, "d/m/Y"), "</td>";

            //* Codigo de Equipo o Estacion y descripcion
            echo "<td nowrap>(", $rsEjecucion->fields['VA_DETALLE'], ")<BR>", $Desc, "</td>";

            //* Actividad
            echo "<td nowrap>", $rsEjecucion->fields['TX_HOJARUTA'], "</td>";

            //* Porcentaje de Avance de la hoja de ruta
            echo "<td align=\"center\">", $rsEjecucion->fields['PC_AVANCE_ACTIVIDAD'], "%</td>";
            //* Enlace
            echo "<td> </td>";
            echo "</tr>";
            $rsEjecucion->MoveNext();
        }

        echo "</tbody>";
        echo "</table>";
        break;
        
    default:
        echo "No existe funcion";
        break;
}
?>
