<?php
/**
 * Codigo para mostrar listado de seleccion multiple, utilizado para asociar
 * el personal a los planes con asignación específica
 */
require_once('../../../aplicacion/configuracion/aut_lib.inc.php');

$plan                  = new plan();
$checked = '';
$co_usuario=$_GET['co_usuario'];
$id_plan=$_GET['id_plan'];
$co_cargo_completo=$_GET['co_cargo_completo'];
$pto_plan =$_GET['pto_plan'];

$rsPersonalDisponible  = $plan->getPersonCargo($co_cargo_completo,$co_usuario,$pto_plan);
if($id_plan != 0) $rsPersonalAsignado = $plan->getPersonAsigPlan($id_plan);

$nuMinPersonas = (int)$_GET["nu_personas"];
$nuMaxPersonas = (int)$_GET["nu_max_personas"];
if($nuMinPersonas == $nuMaxPersonas) 
    echo '<div class="etiqueta"> Cantidad de Personas Requeridas: '.$_GET["nu_personas"].'</div>';
else
    echo '<div class="etiqueta"> Personas Requeridas: Min. '.$_GET["nu_personas"].' Max. '.$_GET["nu_max_personas"].'</div>';

    
while(!$rsPersonalDisponible->EOF)    
{
    if($_GET['id_plan'] != 0)
    {
        $rsPersonalAsignado->MoveFirst();
        while(!$rsPersonalAsignado->EOF) 
        {   
            if($rsPersonalDisponible->fields['CO_USUARIO'] == $rsPersonalAsignado->fields['CO_USUARIO'])
            {
                $checked = 'checked';
                break;
            }
            else
                $checked = '';
            $rsPersonalAsignado->MoveNext();
        }
    }
    echo "<div class=\"checklist\">"; 
    echo '<input name="chkperson[]" type="checkbox" value= "', $rsPersonalDisponible->fields['CO_USUARIO'] ,'" ',$checked.'>' , $rsPersonalDisponible->fields['NB_USUARIO']," (", $rsPersonalDisponible->fields['NB_INDICADOR'],")";
    echo "</div>";
    $rsPersonalDisponible->MoveNext(); 
}
?>
