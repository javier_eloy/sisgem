<?php

/*
 * Controlador para devolución de datos en forma de tablas
 */
require_once('../../../aplicacion/configuracion/aut_lib.inc.php');

//$JSONResponse = new Services_JSON;
//*-------- Funciones
if(isset($_REQUEST['fnc']))
switch ($_REQUEST['fnc']) {

   case "cargar_equipos_consulta":
        $objPlanificar  = new planificacion(); 
        $objCache = new CacheAPC();
        $sessionid = $_POST['sessionid'];
        $sessionid_apc = $sessionid.".listaequipos";
        $currentpage = $_POST['currentpage'];
        $stringtofind = $_POST['find'];
        $resetcache = $_POST['resetcache'];

        $objPagina = $objCache->getData($sessionid_apc);
        if (!isset($objPagina) || $resetcache==1) {
            $arrayTodosEquipo = $objPlanificar->detEquipoCargo($_SESSION['usuario_cod_cargos']);
            $objPagina = new PaginationArray($arrayTodosEquipo);
            $objCache->setData($sessionid_apc, $objPagina);
        }
        
        if($stringtofind != "") 
            $objPagina->find($stringtofind,array("EQUNR","EQKTU","TPLNR","PLTXU","HERST","SERGE"));        
        if(!$objPagina->setCurrentpage($currentpage)) 
            $objPagina->setCurrentpage(1);
        
        $arrayEquipo = $objPagina->getDatapage();
        echo "<table class=\"tabla\"><thead>
                <tr><th scope=\"col\">C&oacute;digo</th>
                    <th scope=\"col\">Equipo / Marca / Serial</th>
                    <th scope=\"col\">Ubicaci&oacute;n T&eacute;cnica</th>
                    <th scope=\"col\">Estado</th>
                    <th scope=\"col\">Opci&oacute;n</th>
                </tr>
            </thead><tbody>";
        echo "<tr><td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";     
        echo $objPagina->getLinks("MostrarTablaConsultaEquipos","'TablaDatos','$sessionid','$stringtofind',0");
        echo "</td></tr>";

        for ($i = 0; $i < $objPagina->getMaxPerPage(); $i++) {
            $sUbic="";
            $sDescrip=$arrayEquipo[$i]["EQKTU"]."<br><b>Marca:</b> ";
            if(isset($arrayEquipo[$i]["HERST"])) $sDescrip.= $arrayEquipo[$i]["HERST"]; 
                                            else $sDescrip.="N/D";
            $sDescrip.="<br><b>Serial:</b> ";                                              
            if($arrayEquipo[$i]["TIDNR"]!="") $sDescrip.=$arrayEquipo[$i]["TIDNR"];
                                         else $sDescrip.="N/D";
            echo "<tr>
                  <td>" , ltrim($arrayEquipo[$i]["EQUNR"],"0") , "</td>
                  <td nowrap>" , utf8_encode($sDescrip), "  </td>                       
                  <td nowrap> (" , utf8_encode($arrayEquipo[$i]["TPLNR"]) , ") <br>" , utf8_encode($arrayEquipo[$i]["PLTXU"]),"<br>",$sUbic ,"</td>
                  <td align='center'>" , $arrayEquipo[$i]["STTXT"] , "</td>
                  <td align='center'>
                     <a href=\"#\" onclick=\"javascript:OpenShadow('reportes/historico/detalle_historico.php?objeto_tecnico=".$arrayEquipo[$i]["EQUNR"]."',341,920)\">Consultar</a><br />                         
                  </td>
                  </tr>";
        }
         echo "</tbody></table><table class=\"tabla\">
               <tfoot>
               <tr><td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";     
        echo $objPagina->getLinks("MostrarTablaConsultaEquipos","'TablaDatos','$sessionid','$stringtofind'");
        echo "</td></tr>
              </tfoot>
             </table>";
        break;     
   

    case "cargar_estaciones_consulta":
        $objPlanificar  = new planificacion(); 
        $objCache = new CacheAPC();
        $sessionid = $_POST['sessionid'];
        $sessionid_apc=$sessionid.".listaestacion";
        $currentpage = $_POST['currentpage'];
        $stringtofind = $_POST['find'];
        $resetcache = $_POST['resetcache'];

        $objPagina = $objCache->getData($sessionid_apc);
        if (!isset($objPagina)|| $resetcache==1) {
            $arrayTodosEstacion= $objPlanificar->detInstalacionCargo($_SESSION['usuario_cod_cargos']);
            $objPagina = new PaginationArray($arrayTodosEstacion);
            $objCache->setData($sessionid_apc, $objPagina);
        }
        if($stringtofind != "") 
            $objPagina->find($stringtofind,array("TPLNR","PLTXU","INNAM"));        
        if(!$objPagina->setCurrentpage($currentpage)) 
            $objPagina->setCurrentpage(1);
        
        $arrayEquipo = $objPagina->getDatapage();
        echo "<table class=\"tabla\"><thead>
                <tr><th scope=\"col\">Ubicaci&oacute;n T&eacute;cnica</th>
                    <th scope=\"col\">Estaci&oacute;n o instalaci&oacute;n</th>
                    <th scope=\"col\">Localidad</th>
                    <th scope=\"col\">Opci&oacute;n</th>
                </tr>
            </thead><tbody>";
        
        echo "<tr><td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";     
        echo $objPagina->getLinks("MostrarTablaConsultaEstacion","'TablaDatos','$sessionid','$stringtofind',0");
        echo "</td></tr>";  
        
        for ($i = 0; $i < $objPagina->getMaxPerPage(); $i++) {
            echo "<tr><td>" . $arrayEquipo[$i]["TPLNR"] . "</td>
                      <td>" . utf8_encode($arrayEquipo[$i]["PLTXU"]). "</td>                       
                      <td>" . utf8_encode($arrayEquipo[$i]["INNAM"]) . "</td>               
                      <td align='center'>
                            <a href=\"#\" onclick=\"javascript:OpenShadow('reportes/historico/detalle_historico.php?objeto_tecnico=".$arrayEquipo[$i]["TPLNR"]."',341,920)\">Consultar</a><br />  
                       </td>
                   </tr>";
        }
         echo "</tbody></table><table class=\"tabla\">
               <tfoot>
               <tr>
                <td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";     
        echo $objPagina->getLinks("MostrarTablaConsultaEstacion","'TablaDatos','$sessionid','$stringtofind'");
        echo "</td></tr>
              </tfoot>
             </table>";
        break;  
        
     case "cargar_personal_consulta":
        $objReporte  = new reporte(); 
        $objCache = new CacheAPC();
        $sessionid = $_POST['sessionid'];
        $sessionid_apc=$sessionid.".listaestacion";
        $currentpage = $_POST['currentpage'];
        $stringtofind = $_POST['find'];
        $resetcache = $_POST['resetcache'];

        $objPagina = $objCache->getData($sessionid_apc);
        if (!isset($objPagina)|| $resetcache==1) {
            $arrayPersonal= $objReporte->SelecPersonCargo($_SESSION['usuario_co_cargo_completo']);
            $objPagina = new PaginationArray($arrayPersonal);
            $objCache->setData($sessionid_apc, $objPagina);
        }
        if($stringtofind != "") 
//            $objPagina->find($stringtofind,array("TPLNR","PLTXT","INNAM"));        
        if(!$objPagina->setCurrentpage($currentpage)) 
            $objPagina->setCurrentpage(1);
        
        $arrayDatosPersonal = $objPagina->getDatapage();
        echo "<table class=\"tabla\"><thead>
                <tr><th scope=\"col\">Nombre</th>
                    <th scope=\"col\">C&eacute;dula</th>
                    <th scope=\"col\">Indicador</th>                    
                    <th scope=\"col\">Cargo</th>
                    <th scope=\"col\">Opci&oacute;n</th>
                </tr>
            </thead><tbody>";
        
        echo "<tr><td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";     
        echo $objPagina->getLinks("MostrarTablaConsultaPersonal","'TablaDatos','$sessionid','$stringtofind',0");
        echo "</td></tr>";  
        
        for ($i = 0; $i < $objPagina->getMaxPerPage(); $i++) {
            echo "<tr><td>" . $arrayDatosPersonal[$i]["NB_USUARIO"] . "</td>
                      <td>" . $arrayDatosPersonal[$i]["NU_CEDULA"] . "</td>
                      <td>" . utf8_encode($arrayDatosPersonal[$i]["NB_INDICADOR"]). "</td>                       
                      <td>" . utf8_encode($arrayDatosPersonal[$i]["NB_CARGO"]) . "</td>               
                      <td align='center'>
                            <a href=\"#\" onclick=\"javascript:OpenShadow('reportes/historico_personal/detalle_historico_personal.php?co_usuario=".$arrayDatosPersonal[$i]["CO_USUARIO"]."',341,920)\">Consultar</a><br />  
                       </td>
                   </tr>";
        }
         echo "</tbody></table><table class=\"tabla\">
               <tfoot>
               <tr>
                <td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";     
        echo $objPagina->getLinks("MostrarTablaConsultaPersonal","'TablaDatos','$sessionid','$stringtofind'");
        echo "</td></tr>
              </tfoot>
             </table>";
        break; 
        
    default:
        echo "ERROR:Codigo de función no se encuentra";
        break;
}

 
?>
