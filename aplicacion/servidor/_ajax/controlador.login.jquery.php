<?php
include("../../../aplicacion/configuracion/aut_lib_min.inc.php");


$hoy = new Datetime("now");

switch ($_REQUEST['fnc']) {
   case "buscar_rol" :
      if (isset($_POST['usr']) && isset($_POST['pwd'])) {
         $password = base64_decode($_POST['pwd']);
         $user = base64_decode($_POST['usr']);

         $usuario_seguridad = new seguridad();
         $rtnUser = $usuario_seguridad->checkUser($user, $password);
         if ($rtnUser == OK_USUARIO) {


            $rsRoles=$usuario_seguridad->listarRol($user);
            $soption="";
            while(!$rsRoles->EOF) {
               $leftTitle=$rsRoles->fields['TX_DESCRIPCION'];
               $nbTitle=$rsRoles->fields['NB_ROL'];
               if (strlen($leftTitle) > 60) $leftTitle=substr($rsRoles->fields['TX_DESCRIPCION'],0,60)." ... ";
               $soption.=" <option value=\"{$rsRoles->fields['CO_ROL']}\" title=\"{$rsRoles->fields['TX_DESCRIPCION']}\">{$nbTitle}</option> ";
               $rsRoles->MoveNext();
            }
            echo "<hr>
            <div class='etiqueta'>Seleccione Rol:</div>
            <div>
              <select id=\"lstrol\"  name=\"lstrol\" style=\"width:210px\"> $soption </select>
              <div class=\"ayuda\">Carga las opciones del men&uacute;</div>
            </div>
            <br>
            <div>
                 <input name=\"rol\" id=\"rol\" type=\"submit\" class=\"boton\" value=\"Continuar\" onclick=\"validar();\">
           </div>";
         } else
            echo $rtnUser;
      } else
         echo ERR_USUARIOVACIO;
      break;

   case "autenticar":
      if (isset($_POST['usr']) && isset($_POST['pwd']) && isset($_POST['rol'])) {
         $password = base64_decode($_POST['pwd']);
         $user = base64_decode($_POST['usr']);
         $rol = base64_decode($_POST['rol']);
         $forzar = base64_decode($_POST['fz']);

         $usuario_seguridad = new seguridad();
         $rtnUser = $usuario_seguridad->checkUser($user, $password);

         if ($rtnUser == OK_USUARIO) {
            $usuario_datos = $usuario_seguridad->getDatosUsuario($user,$rol);

            session_cache_limiter('nocache,private');
            session_name(USUARIOS_SESION);
            session_start();
            /**=======  Manejo de multiples sesiones (Temporal)**/
            $sesiontrabajo=new WorkSession($user);
            if(!is_null($sesiontrabajo->connectSession()) && $forzar!= "1") {
               if($sesiontrabajo->isExpirated()){
                   $sesiontrabajo->saveNewSession (session_id(), SESION_EXPIRACION);
               }else if(!$sesiontrabajo->compareSessionId(session_id())) {
                  echo "mauth";
                  exit;
               } else {
                  $sesiontrabajo->setNextTimeExpired(SESION_EXPIRACION);
                  $sesiontrabajo->updateSession();
               }
            } else
              $sesiontrabajo->saveNewSession (session_id(), SESION_EXPIRACION);


            $_SESSION['usuario_rol'] = $rol;
            $_SESSION['usuario_id'] = $usuario_datos->fields['CO_USUARIO'];
            $_SESSION['usuario_login'] = $usuario_datos->fields['NB_INDICADOR'];
            $_SESSION['usuario_nombre'] = $usuario_datos->fields['NB_NOMBRE'];
            
// DEFINIMOS LOS CoDIGOS DE LA GERENCIA A LA QUE PERTENECE 
            $_SESSION['usuario_cod_cargos'] = $usuario_datos->fields['NU_CARGO'];
            $_SESSION['usuario_co_cargo_completo'] = $usuario_datos->fields['CO_CARGO'];
            $_SESSION['usuario_co_cargo_completo_plan'] = $usuario_datos->fields['CO_CARGO_PLANIFICA'];
            if (!isset($_SESSION['usuario_co_cargo_completo_plan']))
               $_SESSION['usuario_co_cargo_completo_plan'] = $_SESSION['usuario_co_cargo_completo'];

// DEFINICI�N DE DISPONIBILIDAD Y SI ES PLANIFICADOR O NO
            $_SESSION['usuario_disponible'] = $usuario_datos->fields['IN_DISPONIBLE'];
            $_SESSION['usuario_planificador'] = $usuario_datos->fields['IN_PLANIFICADOR'];
            $_SESSION['usuario_administrador'] = $usuario_datos->fields['IN_ADMIN'];
            $_SESSION['usuario_ubic_administrador'] = $usuario_datos->fields['CO_UBIC_TECNICA_ADMIN'];

            $_SESSION['usuario_ubic_tecnica'] = $usuario_datos->fields['CO_UBIC_TECNICA'];
            $_SESSION['usuario_grp_plan'] = "\'" . str_replace(",", "\',\'", $usuario_datos->fields['GRP_PLAN']) . "\'";
            $_SESSION['usuario_pto_plan'] = "\'" . str_replace(",", "\',\'", $usuario_datos->fields['PTO_PLAN']) . "\'";

            session_write_close();
            $usuario_seguridad->traza($_SESSION['usuario_login'], 'false', 'Ingreso Valido a SISGEM', 1, '');

            echo "auth";
         } else
            echo $rtnUser;
      } else
         echo ERR_DESCONOCIDO;

      break;
}
?>
