<?php

/*
 * Controlador para devolución de datos en forma de tablas para programacion
 */
require_once('../../../aplicacion/configuracion/aut_lib.inc.php');

$hoy = new Datetime("now");

$JSONResponse = new Services_JSON;
switch ($_REQUEST['fnc']) {
   //--- Para asignar  en las siguientes 2 semanas
   case "asignacion":
      $ubic_tec = $_POST['ubic_tec'];
      $grp_plan = $_POST['grp_plan'];
      $pto_plan = $_POST['pto_plan'];
      $co_cargo = $_POST['co_cargo'];

      $semana_actual = $hoy->format("W-Y");
      $hoySuma = clone $hoy;
      $hoySuma->add(new DateInterval('P8D'));
      $semana_proxima = $hoySuma->format("W-Y");
      $semanas = "'$semana_actual','$semana_proxima'";

      $objAsignacion = new asignacion_activ();
      $objInventario = new inventarioSAP();
      $objComun = new comun();

      //====  Carga los procedimientos u operacion
      //====  Carga los estados de ordenes
      $rsEstadoOrden = $objComun->SelecListaEstadoOrden();
      $scombo = "<SELECT name='idmotivo' id='idmotivo_#conteo#' onChange='cargaEspera(this.id)' class=': texto' style='width:160px;'>";
      //$scombo.="<OPTION value=''>Seleccione una opci&oacute;n...</option>";
      $scombo.="<optgroup label=\"Genera Orden de Trabajo\">";
      $scombo.="<OPTION value='0'>EJECUTAR LABOR</option>";
      $scombo.="</optgroup >";
      $scombo.="<optgroup label=\"En Espera\">";

      while (!$rsEstadoOrden->EOF) {
         $scombo.= "<OPTION value='" . $rsEstadoOrden->fields['CO_ESTATUS_ORDEN'] . "'>" . $rsEstadoOrden->fields['NB_CO_ESTATUS_ORDEN'] . "</option>";
         $rsEstadoOrden->MoveNext();
      }
      $scombo.="</optgroup >";
      $scombo.="</SELECT>";

      //===== Carga la Tabla
      echo "<table class='tablaprogramacion'>
             <thead><tr>
             <th scope='col'></th>
             <th scope='col'>&nbsp;</th>
             <th scope='col'>No de Orden</th>
             <th scope='col'>Fecha de <br> Programacion</th>
             <th scope='col' width='70%'>Lugar</th>            
             <th scope='col'>Opcion</th>
            </tr></thead>";

      $conteo = 0;
      $conteoActividad = 0;
      $rsAsignacionAct = $objAsignacion->SelecActividadAsignada('2', $semanas, $ubic_tec, false, $pto_plan);

      $nroOrdenActual = "";
      while (!$rsAsignacionAct->EOF) {
         $conteo++;
         $conteoActividad++;
         //*** Agrupa la orden
         $objComun->SelectUbicDesc($objInventario, $rsAsignacionAct->fields['VA_HOJARUTA'], $rsAsignacionAct->fields['VA_DETALLE'], $rsAsignacionAct->fields['VA_TIPO_PLAN'], $Desc, $sUbic);
         if ($rsAsignacionAct->fields['NU_ORDEN'] != $nroOrdenActual) {
             
            $conteoActividad = 1; 
            
            $fechaProgram = new DateTime($rsAsignacionAct->fields['FE_ASIG_ORDEN']);
            $sLink = "<div id=\"entorno_$conteo\">";
            $sLink.="<input id=\"asignar_$conteo\" type=\"button\" class=\"boton\"   onclick=\"OpenShadow('asignacion_activ/orden_asignacion.php?co_orden=" . $rsAsignacionAct->fields['CO_ORDEN'] . "&pto_plan=" . urlencode($pto_plan) . "&co_cargo={$co_cargo}');\" value=\"Asignar\">";
            $sLink.="</div>";

            echo "<tbody id='orden'>", "<tr>";
            echo "<td>(<a class=\"ClassMas\" href='#'>+</a>)</td>",
            "<td>&nbsp;</td>",
            "<td>{$rsAsignacionAct->fields['NU_ORDEN']}</td>",
            "<td align=\"center\" title='", utf8_encode(strftime("%A, %d de %B", $fechaProgram->getTimestamp())), "'>",
            date_format(new DateTime($rsAsignacionAct->fields['FE_ASIG_ORDEN']), "d/m/Y"),
            "</td>",
            "<td>", utf8_encode($sUbic), "</td>",
            "<td>", $sLink, "</td>",
            "</tr>";
            echo "</tbody>";
            echo "<tbody id=\"detalle\">";
            $nroOrdenActual = $rsAsignacionAct->fields['NU_ORDEN'];
            //****** Titulo del Subdetalle
            echo "<tr class='inamovibletitulo'>",
            "<td class='inamovibleblanco'></td>",
            "<td>No. Actividad</td>",
            "<td>Fecha Plan</td>",
            "<td>C&oacute;digo / Caracter&iacute;sticas</td>",
            "<td align=\"center\">Actividad<br><font face='Arial' size='0.2em'>(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></td>",
            "<td> </td>",
            "</tr>";
         }
         //**** Fin agrupa orden



         echo "<tr class='inamovible'>";
         //* Blanco
         echo "<td class='inamovibleblanco'></td>";
         echo "<td>",$rsAsignacionAct->fields['NU_ORDEN']."-".$conteoActividad,"</td>";
         $plan_date = new DateTime($rsAsignacionAct->fields['FE_PROXIMA_PLAN']);         
         if ($plan_date < $hoy)
            echo "<td>", "<font color=\"red\">", date_format($plan_date, "d/m/Y"), "</font>", "</td>";
         else
            echo "<td>", date_format($plan_date, "d/m/Y"), "</td>";

         //* Codigo de Equipo o Estacion y descripcion
         echo "<td nowrap>(",(($rsAsignacionAct->fields['VA_TIPO_PLAN'] === 'I')
                              ? $rsAsignacionAct->fields['VA_DETALLE']:ltrim($rsAsignacionAct->fields['VA_DETALLE'],'0')), ")<BR>", $Desc, "</td>";

         //* Actividad
         $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsAsignacionAct->fields['CO_PLAN']);
         if (!$rsProcedimiento->EOF) {
            $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
            $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
            $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
            $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong>
                          <ul>";
            while (!$rsProcedimiento->EOF) {

               $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
               $rsProcedimiento->MoveNext();
            }
            $sProc.="</ul> \"></div>";
            $sProc.= substr($textoActividad, 4) . "</td>";
         }

         echo $sProc;
         echo "<td> </td>";
         echo "</tr>";
         $rsAsignacionAct->MoveNext();
      }

      echo "</tbody></table>";


      break;
   //  *****************  Pendiente para programa, revisa la fecha anterior a esta
   case "asignacion_pendiente":
      $ubic_tec = $_POST['ubic_tec'];
      $grp_plan = $_POST['grp_plan'];
      $pto_plan = $_POST['pto_plan'];
      $co_cargo = $_POST['co_cargo'];
      $InicioSemana = $hoy;


      $objAsignacion = new asignacion_activ();
      $objInventario = new inventarioSAP();
      $objComun = new comun();
      //====  Carga los procedimientos o operacion
      //====  Carga los estados de ordenes
      $rsEstadoOrden = $objComun->SelecListaEstadoOrden();
      $scombo = "<SELECT name='idmotivo' id='idmotivo_#conteo#' onChange='cargaEspera(this.id)' class=': texto' style='width:160px;'>";
      //$scombo.="<OPTION value=''>Seleccione una opci&oacute;n...</option>";
      $scombo.="<optgroup label=\"Genera Orden de Trabajo\">";
      $scombo.="<OPTION value='0'>EJECUTAR LABOR</option>";
      $scombo.="</optgroup >";
      $scombo.="<optgroup label=\"En Espera\">";

      while (!$rsEstadoOrden->EOF) {
         $scombo.= "<OPTION value='" . $rsEstadoOrden->fields['CO_ESTATUS_ORDEN'] . "'>" . $rsEstadoOrden->fields['NB_CO_ESTATUS_ORDEN'] . "</option>";
         $rsEstadoOrden->MoveNext();
      }
      $scombo.="</optgroup >";
      $scombo.="</SELECT>";

      //===== Carga la Tabla
      echo "<table class='tablaprogramacion'>
             <thead><tr>
            <th scope='col'></th>
            <th scope='col'>&nbsp;</th>
            <th scope='col'>No de Orden</th>
            <th scope='col'>Fecha de <br> Programacion</th>
            <th scope='col' width='70%'>Lugar</th>            
            <th scope='col'>Opcion</th>
            </tr></thead>";
      $conteo = 0;
      $conteoActividad = 0;
      $rsAsignacionAct = $objAsignacion->SelecAsignacionPendiente($InicioSemana->format("d/m/Y"), $ubic_tec, $pto_plan);

      $nroOrdenActual = "";
      while (!$rsAsignacionAct->EOF) {
         $conteo++;
         $conteoActividad++;
         //*** Agrupa la orden
         $objComun->SelectUbicDesc($objInventario, $rsAsignacionAct->fields['VA_HOJARUTA'], $rsAsignacionAct->fields['VA_DETALLE'], $rsAsignacionAct->fields['VA_TIPO_PLAN'], $Desc, $sUbic);
         if ($rsAsignacionAct->fields['NU_ORDEN'] != $nroOrdenActual) {
             $conteoActividad = 1;

            $fechaProgram = new DateTime($rsAsignacionAct->fields['FE_ASIG_ORDEN']);
            $sLink = "<div id=\"entorno_$conteo\">";
            $sLink.="<input id=\"asignar_$conteo\"  type=\"button\"  class=\"boton\" onclick=\"OpenShadow('asignacion_activ/orden_asignacion.php?co_orden=" . $rsAsignacionAct->fields['CO_ORDEN'] . "&pto_plan=" . urlencode($pto_plan) . "&co_cargo={$co_cargo}');\" value=\"Asignar\">";
            $sLink.="</div>";

            echo "<tbody id='orden'>", "<tr>";
            echo "<td>(<a class=\"ClassMas\" href='#'>+</a>)</td>",
            "<td>&nbsp;</td>",
            "<td>{$rsAsignacionAct->fields['NU_ORDEN']}</td>",
            "<td align=\"center\" title='", utf8_encode(strftime("%A, %d de %B", $fechaProgram->getTimestamp())), "'>",
            date_format(new DateTime($rsAsignacionAct->fields['FE_ASIG_ORDEN']), "d/m/Y"),
            "</td>",
            "<td>", utf8_encode($sUbic), "</td>",
            "<td>", $sLink, "</td>",
            "</tr>";
            echo "</tbody>";
            echo "<tbody id=\"detalle\">";
            $nroOrdenActual = $rsAsignacionAct->fields['NU_ORDEN'];
            //***** Titulo del Subdetalle
            echo "<tr class='inamovibletitulo'>",
            "<td class='inamovibleblanco'></td>",
            "<td>No. Actividad</td>",
            "<td>Fecha Plan</td>",
            "<td>C&oacute;digo / Caracter&iacute;sticas</td>",
            "<td align=\"center\">Actividad<br><font face='Arial' size='0.2em'>(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></td>",
            "<td> </td>",
            "</tr>";
         }
         //**** Fin agrupa orden

         echo "<tr class='inamovible'>";
         //* Numero de Orden
         echo "<td class='inamovibleblanco'></td>";
         echo "<td>",$rsAsignacionAct->fields['NU_ORDEN']."-".$conteoActividad , "</td>";
         echo "<td>", date_format(new DateTime($rsAsignacionAct->fields['FE_PROXIMA_PLAN']), "d/m/Y"), "</td>";

         //* Codigo de Equipo o Estacion y descripcion
         echo "<td nowrap>(", (($rsAsignacionAct->fields['VA_TIPO_PLAN'] === 'I')
                              ? $rsAsignacionAct->fields['VA_DETALLE']:ltrim($rsAsignacionAct->fields['VA_DETALLE'],'0')), ")<br>", $Desc, "</td>";

         //* Actividad.
         $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsAsignacionAct->fields['CO_PLAN']);
         if (!$rsProcedimiento->EOF) {
            $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
            $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
            $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
            $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong>
                          <ul>";
            while (!$rsProcedimiento->EOF) {

               $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
               $rsProcedimiento->MoveNext();
            }
            $sProc.="</ul> \"></div>";
            $sProc.= substr($textoActividad, 4) . "</td>";
         }
         echo $sProc;
         echo "<td> </td>";
         echo "</tr>";
         $rsAsignacionAct->MoveNext();
      }

      echo "</tbody></table>";

      break;
   // ***************  Actividades Asignadas o Programadas
   case "asignacion_programada":
      $ubic_tec = $_POST['ubic_tec'];
      $grp_plan = $_POST['grp_plan'];
      $pto_plan = $_POST['pto_plan'];
      $co_cargo = $_POST['co_cargo'];

      $semana_actual = $hoy->format("W-Y");
      $hoySuma = clone $hoy;
      $hoySuma->add(new DateInterval('P8D'));
      $semana_proxima = $hoySuma->format("W-Y");
      $semanas = "'$semana_actual','$semana_proxima'";

      $objAsignacion = new asignacion_activ();
      $objInventario = new inventarioSAP();
      $objComun = new comun();
      //====  Carga los procedimientos o operacion
      //====  Carga los estados de ordenes
      $rsEstadoOrden = $objComun->SelecListaEstadoOrden();
      $scombo = "<SELECT name='idmotivo' id='idmotivo_#conteo#' onChange='cargaEspera(this.id)' class=': texto' style='width:160px;'>";
      //$scombo.="<OPTION value=''>Seleccione una opci&oacute;n...</option>";
      $scombo.="<optgroup label=\"Genera Orden de Trabajo\">";
      $scombo.="<OPTION value='0'>EJECUTAR LABOR</option>";
      $scombo.="</optgroup >";
      $scombo.="<optgroup label=\"En Espera\">";

      while (!$rsEstadoOrden->EOF) {
         $scombo.= "<OPTION value='" . $rsEstadoOrden->fields['CO_ESTATUS_ORDEN'] . "'>" . $rsEstadoOrden->fields['NB_CO_ESTATUS_ORDEN'] . "</option>";
         $rsEstadoOrden->MoveNext();
      }
      $scombo.="</optgroup >";
      $scombo.="</SELECT>";

      //===== Carga la Tabla
      echo "<table id='listaSaldos' class='tablaprogramacion'>
             <thead><tr>
            <th scope='col' style='width:100px;text-align:center;' >No de Orden</th>
            <th scope='col' style='text-align:center;' >Lugar</th>
            <th>&nbsp;</th>
            <th scope='col' style='width:100px;text-align:center;' >Fecha de<br>Orden de Trabajo</th>
            <th scope='col' style='width:100px;text-align:center;' >Opcion</th>
            </tr></thead>";
      $conteo = 0;
      $conteoActividad = 0;
      $rsAsignacionAct = $objAsignacion->SelecActividadesProgramadas($pto_plan, $ubic_tec);

      $nroOrdenActual = "";
      while (!$rsAsignacionAct->EOF) {
         $conteo++;
         $conteoActividad++;
         //*** Agrupa la orden
         $objComun->SelectUbicDesc($objInventario, $rsAsignacionAct->fields['VA_HOJARUTA'], $rsAsignacionAct->fields['VA_DETALLE'], $rsAsignacionAct->fields['VA_TIPO_PLAN'], $Desc, $sUbic);
         if ($rsAsignacionAct->fields['NU_ORDEN'] != $nroOrdenActual) {
             $conteoActividad = 1;

            $fechaProgram = new DateTime($rsAsignacionAct->fields['FE_ASIG_ORDEN']);

            echo "<tbody id='orden'>", "<tr style='border-left:4px solid lightgray;'>";
            echo "<td align=\"center\">{$rsAsignacionAct->fields['NU_ORDEN']}</td>", /*Numero de Orden*/
            "<td align=\"center\" nowrap>", utf8_encode($sUbic),"</td>", /*Lugar */
            "<td >&nbsp;</td>", /*Espacio dejado a proposito*/
            "<td align=\"center\" title='", utf8_encode(strftime("%A, %d de %B", $fechaProgram->getTimestamp())), "'>", /*Fecha de Programación*/
            date_format(new DateTime($rsAsignacionAct->fields['FE_ASIG_ORDEN']), "d/m/Y"), /*Fecha de Asgnacion de Orden*/
            "</td>",
            "<td align=\"center\"><img src=\"publico/imagenes/job_report.png\" width='20' height='20' class='enlaceblanco' onclick=\"OpenShadow('asignacion_activ/avance_activ.php?co_orden=" . $rsAsignacionAct->fields['CO_ORDEN']."',400,600);\" /></td>",
            "</tr>";
            echo "</tbody>";
            echo "<tbody id=\"ddetalle\">";
            $nroOrdenActual = $rsAsignacionAct->fields['NU_ORDEN'];

            //**** Titulo del Subdetalle
            echo "<tr class='inamovibletitulo'>",
            "<td align='center'>No. Actividad</td>",
            "<td align='center'>Actividad<br /><font face='Arial' size='0.2em'>(Pase el rat&oacute;n y ver&aacute; los procedimientos)</font></td>",
            "<td style='text-align:center;'>Fecha de Plan</td>",
            "<td style='text-align:center;'>C&oacute;digo/Caracteristica</td>",                  
            "<td align='center'></td>",
            "</tr>";
         }
         //**** Fin agrupa orden
         echo "<tr class='inamovible'>";

         //* Codigo de Actividad
         echo "<td nowrap align=\"center\"> &nbsp;",$rsAsignacionAct->fields['NU_ORDEN'],"-",$conteoActividad,"</td>";
         
         //* Actividad
         $rsProcedimiento = $objComun->SelecLocalProcedimientos($rsAsignacionAct->fields['CO_PLAN']);
         if (!$rsProcedimiento->EOF) {
            $sProc = "<td nowrap class=\"text\" onmouseover=\"return onMouseEnterProg(this,event);\" onmouseout=\"return onMouseLeaveProg(this);\" >";
            $textoActividad = $rsProcedimiento->fields['TX_HOJARUTA'];
            $codigoAct = explode("-", $rsProcedimiento->fields['VA_HOJARUTA']);
            $sProc.= "<div class=\"referencia\" style=\"display: none;\" content=\"<strong>NIVEL " . $codigoAct[1] . ":" . $textoActividad . "</strong>
                          <ul>";
            while (!$rsProcedimiento->EOF) {

               $sProc.="<li>-" . $rsProcedimiento->fields['NB_TEXTO'] . "</li>";
               $rsProcedimiento->MoveNext();
            }
            $sProc.="</ul> \"></div>";
            $sProc.= "Nivel ".$codigoAct[1].": ".substr($textoActividad, 4) . "</td>";
         }
         
         echo $sProc;
         //* Fecha Plan
         echo "<td nowrap align=\"center\">", date_format(new DateTime($rsAsignacionAct->fields['FE_PROXIMA_PLAN']), "d/m/Y"), "</td>";

         //* Codigo Caracteristica
         echo "<td nowrap>(", ltrim($rsAsignacionAct->fields['VA_DETALLE'], "0"), ")<br>", $Desc, "</td>";

         //* Opcion
         $p = encryptLink("nu_orden={$rsAsignacionAct->fields['NU_ORDEN']}&va_hojaruta={$rsAsignacionAct->fields['VA_HOJARUTA']}&n_hojaruta=$textoActividad&n_local=$Desc");
         
         $sLink = "<td align='center'><div id=\"entorno_$conteo\">";
         $sLink.= "<a href='cpanel.php?sistema=3001&p=$p'><img src=\"publico/imagenes/personal.png\" height=\"20\" width=\"20\" title=\"Editar Detalles del Personal\" alt=\"Editar Detalles del Personal\" /></a>";
         $sLink.="</div></td>";
         echo $sLink;

         echo "</tr>";

         $rsAsignacionAct->MoveNext();
      }

      echo "</tbody></table>";
      break;
}
?>
