<?php

/*
 * Controlador para devolución de datos en forma de tablas    
 */
require_once('../../../aplicacion/configuracion/aut_lib.inc.php');

$JSONResponse = new Services_JSON;
//*-------- Funciones
if (isset($_REQUEST['fnc'])) {
   switch ($_REQUEST['fnc']) {

      case "cargo_grp_pto": //Devuelve la lista de Equipos de Trabajo de un grupo planificador
         $objInventario = new inventarioSAP();
         $objConfigura = new configuracion();

         $co_centro_emp = $_REQUEST['co_centro_emp'];
         $nu_cargo = $_REQUEST['nu_cargo'];

         $arrayGrp = $objInventario->SelecGrupoPlanificador($co_centro_emp);
         if ($objConfigura->CrearTmpGrpPto($nu_cargo)) {
            $countArrayGrp = count($arrayGrp);
            for ($i = 1; $i <= $countArrayGrp; $i++) {
               $co_grp_plan = $arrayGrp[$i]["INGRP"];
               $co_grp_plan_desc = $arrayGrp[$i]["INNAM"];
               $objConfigura->InsTmpGrpPto($nu_cargo, $co_grp_plan, $co_grp_plan_desc, "", "", 0);
               $arrayPto = $objInventario->SelecPtoTrabajo($co_grp_plan, $co_centro_emp);
               $countArrayPto = count($arrayPto);
               for ($j = 1; $j <= $countArrayPto; $j++) {
                  $co_pto_trab = $arrayPto[$j]["ARBPL"];
                  $co_pto_trab_desc = utf8_encode($arrayPto[$j]["KTEXT"]);
                  if (!$objConfigura->InsTmpGrpPto($nu_cargo, $co_grp_plan, $co_grp_plan_desc, $co_pto_trab, $co_pto_trab_desc, ($j == $countArrayPto) ? 1 : 0)) {
                     echo "ERROR: No se puede insertar en la tabla temporal. No se puede continuar el proceso... ";
                     return;
                  }
               }
            }
         } else {
            echo "ERROR: No se puede crear tablas temporales";
            return;
         }
         $rsGrpPto = $objConfigura->GetTmpGtpPto($nu_cargo);
         $rsCargo = $objConfigura->SelecRegistroCargo($nu_cargo);

         $shtml = "<input type='hidden' name='nu_cargo' value='$nu_cargo'>";
         $shtml.="<div class=\"etiqueta\"><b>Grupos de Planificaci&oacute;n / Puestos de Trabajo</b></div>";
         $shtml.="<div class=\"etiqueta\">Cargo: " . $rsCargo->fields['NB_CARGO'] . " (" . $rsCargo->fields['TX_DESCRIPCION'] . ")</div>";
         $shtml.="<div class=\"checklistdiv\" >";
         while (!$rsGrpPto->EOF) {
            $escheck = ($rsGrpPto->fields['in_select'] == 1) ? "checked=\"checked\"" : "";
            if ($rsGrpPto->fields['co_pto_trab'] == "") {
               // Agrega el grupo planficador como padre de la lista
               $shtml.="<div class=\"checklist\"><input name=\"chkgrp[]\" type=\"checkbox\" value=\"" . $rsGrpPto->fields['co_grp_plan'] . "\" $escheck >" . $rsGrpPto->fields['co_grp_plan'] . " - " . utf8_encode($rsGrpPto->fields['co_grp_plan_desc']) . "</div>";
            } else {
               // Agrega los puestos de trabajo
               $classNode = ($rsGrpPto->fields['es_ultimo'] == 1) ? $classNode = "checklistchildlast" : "checklistchild";
               $shtml.="<div class=\"$classNode\"><input name=\"chkgrp[]\" type=\"checkbox\" title=\"{$rsGrpPto->fields['co_pto_trab']}\" value=\"" . $rsGrpPto->fields['co_grp_plan'] . "|" . $rsGrpPto->fields['co_pto_trab'] . "\" $escheck>" . $rsGrpPto->fields['co_pto_trab'] . " - " . utf8_encode($rsGrpPto->fields['co_pto_trab_desc']) . "</div>";
            }
            //<div class="checklistchildlast"><input name="chkgrp[]" type="checkbox" value="2"> Valor 2 </div>                        
            $rsGrpPto->MoveNext();
         }
         $shtml.="</div>";

         unset($objConfigura);
         echo $shtml;
         break;

      case "mostrar_cargo_grp_pto" :
         $objConfigura = new configuracion();
         $nu_cargo = $_GET['nu_cargo'];
         $rsCargo = $objConfigura->SelecRegistroCargo($nu_cargo);
         $rsCargoGrupoPto = $objConfigura->SelecCargoGrupoPuesto($nu_cargo);

         if (!$rsCargo->EOF) {

            $shtml = "<link type=\"text/css\" rel=\"stylesheet\" href=\"../../../publico/js/Archivos/estilos.css\">";

            $shtml.="<div class=\"etiqueta\">" . $rsCargo->fields['NB_CARGO'] . " (" . $rsCargo->fields['TX_DESCRIPCION'] . ")</div> <hr>";
            $ultimoGrupo = "";
            while (!$rsCargoGrupoPto->EOF) {
               if ($rsCargoGrupoPto->fields['CO_PUESTO_TRAB'] == "") {
                  $ultimoGrupo = $rsCargoGrupoPto->fields['CO_GRUPO_PLAN'];
                  $shtml.="<div class=\"checklist\">$ultimoGrupo</div>";
               } else {
                  $ptoTrabajo = $rsCargoGrupoPto->fields['CO_PUESTO_TRAB'];
                  //if (strrpos($ptoTrabajo, $ultimoGrupo))
                  if ($rsCargoGrupoPto->fields['CO_GRUPO_PLAN'] == $ultimoGrupo)
                     $shtml.="<div class=\"checklistchild\">$ptoTrabajo</div>";
                  else {
                     $codigo = explode("-", $ptoTrabajo);
                     if (count($codigo) >= 2)
                        $shtml.="<div class=\"checklistdisabled\">" . $codigo[1] . "</div>";
                     else
                        $shtml.="<div class=\"checklistdisabled\">(Sin Grupo Planificador)</div>";
                     $shtml.="<div class=\"checklistchild\">$ptoTrabajo</div>";
                  }
               }
               $rsCargoGrupoPto->MoveNext();
            }
            if ($rsCargoGrupoPto->CurrentRow() < 0)
               $shtml.="<div>No existen asociaciones</div>";
         } else
            $shtml = "No se pudo obtener los datos";

         echo $shtml;
         break;
      case "co_cargo_repetido": //Verifica si existe el co_cargo repetido
         $objConfigura = new configuracion();
         $nu_cargo = $_REQUEST['nu_cargo'];
         $co_cargo_estructura = $_REQUEST['co_cargo_estructura'];
         $brst = $objConfigura->EsCodigoRegistroCargo($co_cargo_estructura, $nu_cargo);
         echo ($brst == true) ? 1 : 0;
         break;

      case "co_estado_orden_repetido": //Verifica si existe el co_cargo repetido
         $objConfigura = new configuracion();
         $co_estado_orden = $_REQUEST['co_estado_orden'];
         $codigo = $_REQUEST['codigo'];
         $brst = $objConfigura->EsCodigoRegistroNomen($co_estado_orden, $codigo);
         echo ($brst == true) ? 1 : 0;
         break;

      case "co_clase_repetido":
         $objConfigura = new configuracion();
         $co_clase = $_REQUEST['co_clase'];
         $codigo = $_REQUEST['codigo'];
         $brst = $objConfigura->EsCodigoRegistroClase($co_clase, $codigo);
         echo ($brst == true) ? 1 : 0;
         break;

      case "co_motivo_cierre_repetido":
         $objConfigura = new configuracion();
         $co_motivo_cierre = $_REQUEST['co_motivo_cierre'];
         $brst = $objConfigura->EsCodigoRegistroMotivoCierre($co_motivo_cierre);
         echo ($brst == true) ? 1 : 0;
         break;

      case "nb_rol_repetido":
         $objConfigura = new configuracion();
         $nb_rol = $_REQUEST['nb_rol'];
         $co_rol = $_REQUEST['co_rol'];
         $brst = $objConfigura->EsNombreRolRepetido($nb_rol, $co_rol);
         echo ($brst == true) ? 1 : 0;

         break;
      case "datos_cargo" :   // Devuelve los datos de un cargo
         $objConfigura = new configuracion();
         $nu_cargo = $_REQUEST['nu_cargo'];
         $rs = $objConfigura->SelecRegistroCargo($nu_cargo);

         $rst = $rs->FetchRow();
         echo $JSONResponse->encode($rst);
         break;

      case "datos_nomen" :   // Devuelve los datos de una nomenclatura
         $objConfigura = new configuracion();
         $co_estado_cargo = $_REQUEST['co_estado_cargo'];
         $rs = $objConfigura->SelecRegistroNomenclatura($co_estado_cargo);

         $rst = $rs->FetchRow();
         echo $JSONResponse->encode($rst);
         break;

      case "datos_clase":
         $objConfigura = new configuracion();
         $co_clase = $_REQUEST['co_clase'];
         $rs = $objConfigura->SelecRegistroClase($co_clase);

         $rst = $rs->FetchRow();
         echo $JSONResponse->encode($rst);
         break;

      case "datos_motivo":   // Devuelve los datos de un motivo de cierre de actividad
         $objConfigura = new configuracion();
         $nu_motivo_cierre = $_REQUEST['nu_motivo_cierre'];
         $rs = $objConfigura->SelecRegistroMotivoCierre($nu_motivo_cierre);

         $rst = $rs->FetchRow();
         echo $JSONResponse->encode($rst);
         break;

      case "datos_rol":   // Devuelve los datos de los roles
         $objConfigura = new configuracion();
         $co_rol = $_REQUEST['co_rol'];
         $rs = $objConfigura->SelecRegistroRol($co_rol);

         $rst = $rs->FetchRow();
         echo $JSONResponse->encode($rst);
         break;

      case "datos_permisorol":
         $objConfigura = new configuracion();
         $co_rol = $_REQUEST['co_rol'];
         $in_admin = $_REQUEST['in_admin'];
         $rs = $objConfigura->SelecPermisoRol($co_rol, $in_admin);
         $rst = $rs->GetArray();
         echo $JSONResponse->encode($rst);
         break;

      case "elimina_rol":
         $objConfigura = new configuracion();
         $co_rol = $_REQUEST['co_rol'];
         $booRes = $objConfigura->DelRole($co_rol);
         echo ($booRes) ? "1" : "0";
         break;

       case "datos_permiso":
         $objConfigura = new configuracion();
         $in_admin = $_REQUEST['in_admin'];
         $rs = $objConfigura->SelecPermiso($in_admin);
         $rst = $rs->GetArray();
         echo $JSONResponse->encode($rst);
         break;

      case "cargar_equipos_apc":
         $objPlanificar = new planificacion();
         $objCache = new CacheAPC();
         $sessionid = $_POST['sessionid'];
         $sessionid_apc = $sessionid . ".listaequipos";
         $currentpage = $_POST['currentpage'];
         $stringtofind = $_POST['find'];
         $resetcache = $_POST['resetcache'];

         $objPagina = $objCache->getData($sessionid_apc);
         if (empty($objPagina) || $resetcache === 1) {
            $arrayTodosEquipo = $objPlanificar->detEquipoCargo($_SESSION['usuario_cod_cargos']);
            $objPagina = new PaginationArray($arrayTodosEquipo);
            $objCache->setData($sessionid_apc, $objPagina);
         }

         if ($stringtofind != "")
            $objPagina->find($stringtofind, array("EQUNR", "EQKTU", "TPLNR", "PLTXU", "HERST", "SERGE"));
         if (!$objPagina->setCurrentpage($currentpage))
            $objPagina->setCurrentpage(1);

         $arrayEquipo = $objPagina->getDatapage();
         echo "<table class=\"tabla\"><thead>
                <tr><th scope=\"col\">C&oacute;digo</th>
                    <th scope=\"col\">Equipo / Marca / Serial</th>
                    <th scope=\"col\">Ubicaci&oacute;n T&eacute;cnica</th>
                    <th scope=\"col\">Estado</th>
                    <th scope=\"col\">Plan</th>
                    <th scope=\"col\">Opci&oacute;n</th>                    
                </tr>
            </thead><tbody>";
         echo "<tr><td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";
         echo $objPagina->getLinks("MostrarTablaEquipos", "'TablaDatos','$sessionid','$stringtofind',0");
         echo "</td></tr>";

         for ($i = 0; $i < $objPagina->getMaxPerPage(); $i++) {
            $sUbic = "";
            $sDescrip = $arrayEquipo[$i]["EQKTU"] . "<br><b>Marca:</b> ";
            if (isset($arrayEquipo[$i]["HERST"]))
               $sDescrip.= $arrayEquipo[$i]["HERST"];
            else
               $sDescrip.="N/D";
            $sDescrip.="<br><b>Serial:</b> ";
            if ($arrayEquipo[$i]["TIDNR"] != "")
               $sDescrip.=$arrayEquipo[$i]["TIDNR"];
            else
               $sDescrip.="N/D";
            echo "<tr>
                  <td>", ltrim($arrayEquipo[$i]["EQUNR"], "0"), "</td>
                  <td nowrap>", utf8_encode($sDescrip), "  </td>                       
                  <td nowrap> (", utf8_encode($arrayEquipo[$i]["TPLNR"]), ") <br>", utf8_encode($arrayEquipo[$i]["PLTXU"]), "<br>", $sUbic, "</td>
                  <td align='center'>", $arrayEquipo[$i]["STTXT"], "</td>
                  <td align='center'>
                     ", $objPlanificar->detPlanObjTecnico($arrayEquipo[$i]["EQUNR"]), "
                  </td>
                  <td align='center'>
                     <a  href='#' onclick=\"OpenShadow('planificacion_mtto/plan.php?id={$arrayEquipo[$i]["EQUNR"]}&objeto_tecnico=2');\">Planificar</a>
                  </td>
                  </tr>";
         }


         echo "</tbody></table><table class=\"tabla\">
               <tfoot>
               <tr><td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";
         echo $objPagina->getLinks("MostrarTablaEquipos", "'TablaDatos','$sessionid','$stringtofind'");
         echo "</td></tr>
              </tfoot>
             </table>";
         break;

      case "cargar_estaciones_apc":
         $objPlanificar = new planificacion();
         $objCache = new CacheAPC();
         $sessionid = $_POST['sessionid'];
         $sessionid_apc = $sessionid . ".listaestacion";
         $currentpage = $_POST['currentpage'];
         $stringtofind = $_POST['find'];
         $resetcache = $_POST['resetcache'];

         $objPagina = $objCache->getData($sessionid_apc);
         if (empty($objPagina) || $resetcache === 1) {
            $arrayTodosEstacion = $objPlanificar->detInstalacionCargo($_SESSION['usuario_cod_cargos']);
            $objPagina = new PaginationArray($arrayTodosEstacion);
            $objCache->setData($sessionid_apc, $objPagina);
         }
         if ($stringtofind != "")
            $objPagina->find($stringtofind, array("TPLNR", "PLTXU", "INNAM"));
         if (!$objPagina->setCurrentpage($currentpage))
            $objPagina->setCurrentpage(1);

         $arrayEquipo = $objPagina->getDatapage();
         echo "<table class=\"tabla\"><thead>
                <tr><th scope=\"col\">Ubicaci&oacute;n T&eacute;cnica</th>
                    <th scope=\"col\">Estaci&oacute;n o instalaci&oacute;n</th>
                    <th scope=\"col\">Localidad</th>
                    <th scope=\"col\">Plan</th>
                    <th scope=\"col\">Opci&oacute;n</th>
                </tr>
            </thead><tbody>";

         echo "<tr><td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";
         echo $objPagina->getLinks("MostrarTablaEstacion", "'TablaDatos','$sessionid','$stringtofind',0");
         echo "</td></tr>";

         for ($i = 0; $i < $objPagina->getMaxPerPage(); $i++) {
            $tmp = $objPlanificar->detPlanObjTecnico($arrayEquipo[$i]["TPLNR"]);
            echo "<tr><td>" . $arrayEquipo[$i]["TPLNR"] . "</td>
                      <td>" . utf8_encode($arrayEquipo[$i]["PLTXU"]) . "</td>
                      <td>" . utf8_encode($arrayEquipo[$i]["INNAM"]) . "</td>
                      <td>" . $tmp . " </td>
                      <td align='center'>                            
                         <a  href='#' onclick=\"OpenShadow('planificacion_mtto/plan.php?id={$arrayEquipo[$i]["TPLNR"]}&objeto_tecnico=3');\">Planificar</a>
                      </td>
                   </tr>";
         }
         echo "</tbody></table><table class=\"tabla\">
               <tfoot>
               <tr>
                <td align=\"center\" colspan=\"6\" class=\"titulomodulo\">";
         echo $objPagina->getLinks("MostrarTablaEstacion", "'TablaDatos','$sessionid','$stringtofind'");
         echo "</td></tr>
              </tfoot>
             </table>";
         break;

      default:
         echo "ERROR:Codigo de función no se encuentra";
         break;
   }
}

//*======================= == COMBOS = ========================================
if (isset($_GET['combo']))
   switch ($_GET['combo']) {
      case "ubicacion_tecnica":
         $idopcion = $_GET['idopcion'];
         $idcombo = $_GET['idcombo'];
         $ubic_tec = $_GET['ubic_tec'];
         $objConfigure = new InventarioSAP();
         $ubic_tec = (strlen($ubic_tec) < 2) ? $ubic_tec = "__" : $ubic_tec.= "%";

         $array = $objConfigure->SelecUbicacionTecnica($ubic_tec);
         $shtml = "<select id='$idcombo' name='$idcombo' style='width:350px'>";
         $shtml.="<option value='0'>Seleccione... </option>";

         $esSel = "";
         for ($i = 1; $i <= count($array); $i++) {
            if (isset($idopcion) && ($array[$i]['TPLNR'] == $idopcion))
               $esSel = "selected";
            else
               $esSel = "";

            $shtml.="<option value='" . $array[$i]['TPLNR'] . "' $esSel >" . $array[$i]['TPLNR'] . " - " . utf8_encode($array[$i]['PLTXU']) . "</option>";
         }
         $shtml.="</select>";
         echo $shtml;

         break;
      case "cargos":
         $idopcion = $_GET['idopcion'];
         $idcombo = $_GET['idcombo'];
         $ubic_tec = $_GET['ubic_tec'];
         $objConfigure = new configuracion();
         $rs = $objConfigure->SelecListaCargo($ubic_tec);
         $shtml = "<select id='$idcombo' name='$idcombo' style='width:350px'>";
         $shtml.="<option value='0'>Seleccione... </option>";

         $esSel = "";
         while (!$rs->EOF) {
            if ($idopcion > 0 && ($rs->fields['NU_CARGO'] == $idopcion))
               $esSel = "selected";
            else
               $esSel = "";

            $shtml.="<option value='" . $rs->fields['NU_CARGO'] . "' $esSel >" . $rs->fields['Codigo'] . " - " . $rs->fields['NB_CARGO'] . "</option>";
            $rs->MoveNext();
         }
         $shtml.="</select>";
         echo $shtml;
         break;

      case "ubicacion_tecnica_cargo":
         $idopcion = $_GET['idopcion'];
         $idcombo = $_GET['idcombo'];
         $idcargo = $_GET['idcargo'];
         $objConfigura = new configuracion();
         $objInventario = new InventarioSAP();

         $rsCargo = $objConfigura->SelecRegistroCargo($idcargo);
         $ubic_tec = $rsCargo->fields['CO_UBIC_TECNICA'];

         $ubic_tec = (strlen($ubic_tec) < 2) ? $ubic_tec = "__" : $ubic_tec.= "%";

         $array = $objInventario->SelecUbicacionTecnica($ubic_tec);
         $shtml = "<select id='$idcombo' name='$idcombo' style='width:350px'>";
         $shtml.="<option value='0'>Seleccione... </option>";

         $esSel = "";
         for ($i = 1; $i <= count($array); $i++) {
            if (isset($idopcion) && ($array[$i]['TPLNR'] == $idopcion))
               $esSel = "selected";
            else
               $esSel = "";

            $shtml.="<option value='" . $array[$i]['TPLNR'] . "' $esSel >" . $array[$i]['TPLNR'] . " - " . utf8_encode($array[$i]['PLTXU']) . "</option>";
         }
         $shtml.="</select>";
         echo $shtml;


         break;
   }
?>
