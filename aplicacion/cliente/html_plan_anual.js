var url_tabla_PPAE="aplicacion/servidor/_ajax/controlador.grafico_PPAE.jquery.php"
var GRUPO_SOLUCIONADOR = [];
var acount;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////////////////////////Obtine la data de la base de datos///////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function data_DropList(load,data_div,code,list_Group){
  
  jq.ajax({
      url: url_tabla_PPAE,
      type: "POST",
      async:false,
      data: {
         fnc:"select_group",
         user_id:code
      },
      beforeSend:function(){

         jq(load).show();
         jq(data_div).hide();
      },
      success:function(data){
         
         if (data == -8) 
         {
            alert("error codigo usuario");
         }
         else
         {
          //console.log(data);
            var item = data.split("\n");
            GRUPO_SOLUCIONADOR.length = 0;
            for (i = 1; i < item.length; i++)
             {
              GRUPO_SOLUCIONADOR.push(item[i]);
            }

            

        }
   
         
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
        
      },
      complete:function(){
      
      if(GRUPO_SOLUCIONADOR[0])
      {
         
        insertDataDropList(list_Group,GRUPO_SOLUCIONADOR);
        
      }
        
        jq(load).hide();
        jq(data_div).show();

        
      }
   });
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////Insetar los datos en el DropList/////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function insertDataDropList(list,values)
{
 


  
   for (i = 0; i < values.length -1 ; i++)
    {
      jq(list).append('<option value='+values[i]+'>'+values[i]+'</option>');
    }
  
   
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////Extraemos las semanas de los meses///////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function weekCount(year, month_number) 
{

    var firstOfMonth = new Date(year, month_number-1, 1);
    var lastOfMonth = new Date(year, month_number, 0);

    var used = firstOfMonth.getDay() + lastOfMonth.getDate();

    return Math.ceil( used / 7);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////Crea las tabla a partir de la informacion sumunistrada//////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function lazy(year)
{
  acount =0;
  create_table(year,"#Enero",1,"Enero");
  create_table(year,"#Febrero",2,"Febrero");
  create_table(year,"#Marzo",3,"Marzo");
  create_table(year,"#Abril",4,"Abril");
  create_table(year,"#Mayo",5,"Mayo");
  create_table(year,"#Junio",6,"Junio");
  create_table(year,"#Julio",7,"Julio");
  create_table(year,"#Agosto",8,"Agosto");
  create_table(year,"#Septiempre",9,"Septiempre");
  create_table(year,"#Octubre",10,"Octubre");
  create_table(year,"#Noviembre",11,"Noviembre");
  create_table(year,"#Diciembre",12,"Diciembre");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// Funcion para imprimir todas las tablas/////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function create_table(year,month,number,name)
{
  var weeks = weekCount(year,number);

  var html ="<tr><td class='Month_title' colspan='"+(weeks+1)+"'>"+name+"</td></tr>";
      html+="<tr><td class='sem_title'>SEM</td>";
  
  for (var i = 1; i < weeks+1; i++) {

        acount++;

         html+="<td class='sem_title'>"+acount+"</td>";
    
  }
  
  html+="</tr>";
  html+="<tr><td>MTTO</td>";

  for (var i = 1; i < weeks+1; i++) {
   
   if(i ==2 || i ==3  )
    {
      html+="<td class='estilo_plan_true'>"+0+"</td>";
    }
    else
    {
         html+="<td>"+0+"</td>";
    } 

  }

  html+="</tr>";
  html+="<tr><td>H/H</td>";

  for (var i = 1; i < weeks+1; i++) {
    if(i ==2 || i ==3  )
    {
      html+="<td class='estilo_HH'>"+0+"</td>";
    }
    else
    {
         html+="<td>"+0+"</td>";
    } 
  }
  html+="</tr>";

  jq(month).html(html);
  
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////Funcio de inicio//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function initBindLocal(year)
{
  lazy(year);
  data_DropList("#loading","#Content",jq("#U1").val(),"#Grupo_solucionador");



}