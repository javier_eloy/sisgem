function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false;
	try
	{
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			// Creacion del objet AJAX para IE
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(E)
		{
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') xmlhttp=new XMLHttpRequest();
		}
	}
	return xmlhttp; 
}

// Declaro los selects que componen el documento HTML. Su atributo ID debe figurar aqui.
function cargaPerson(idSelectOrigen)
{
    // Obtengo la cantidad de personas y organización a la que pertenece el personal
    var CantidPerson=document.getElementById("IdCantiPerson").value;
    var Organizacion=document.getElementById("IdOrgani").value;
    // Variable del ciclo for
    var y;
    var z;
               
    // Se crea el vector de los Select      
    var listadoSelects=new Array();
    for(y=0; y <= (CantidPerson-1); y = y+1)
    {
        z = y+1;
        listadoSelects[y]="IdUsuari_"+z;
    }
    
    
    function buscarEnArray(array, dato)
    {
    	// Retorna el indice de la posicion donde se encuentra el elemento en el array o null si no se encuentra
    	var x=0;
    	while(array[x])
    	{
    		if(array[x]==dato) return x;
    		x++;
    	}
    	return null;
    }

    
   	// Obtengo la posicion que ocupa el select que debe ser cargado en el array declarado mas arriba
    var posicionSelectDestino=buscarEnArray(listadoSelects, idSelectOrigen)+1;
    // Obtengo el select que el usuario modifico
    var selectOrigen=document.getElementById(idSelectOrigen);
    // Obtengo la opcion que el usuario selecciono
    var opcionSeleccionada=selectOrigen.options[selectOrigen.selectedIndex].value;
   	// Si el usuario eligio la opcion "Elige", no voy al servidor y pongo los selects siguientes en estado "Selecciona opcion..."
        
    var v;
    var w;
    var valorAnterior="";
    var valorConcatenado;
    
    // for para recorrer todo los id de los select
    for(v=0; v <= (CantidPerson-1); v = v+1)
    {
        // Se seleccionan los valores
        var selectUsuari=document.getElementById(listadoSelects[v]);
        var opcionSeleccUsuari=selectUsuari.options[selectUsuari.selectedIndex].value;
   	   // Se concatena lo que se va a enviar al archivo PHP:
   	   w=v+1;
   	   valorConcatenado="&usuario"+w+"="+opcionSeleccUsuari+valorAnterior;
       valorAnterior=valorConcatenado;
    }

    	if(opcionSeleccionada==0)
    	{
    		var x=posicionSelectDestino, selectActual=null;
    		// Busco todos los selects siguientes al que inicio el evento onChange y les cambio el estado y deshabilito
    		while(listadoSelects[x])
    		{
    			selectActual=document.getElementById(listadoSelects[x]);
    			selectActual.length=0;
    			
    			var nuevaOpcion=document.createElement("option"); nuevaOpcion.value=0; nuevaOpcion.innerHTML="Selecciona Opci&oacute;n...";
    			selectActual.appendChild(nuevaOpcion);	selectActual.disabled=true;
    			x++;
    		}
    	}
    	// Compruebo que el select modificado no sea el ultimo de la cadena
    	else if(idSelectOrigen!=listadoSelects[listadoSelects.length-1])
    	{
    		// Obtengo el elemento del select que debo cargar
    		var idSelectDestino=listadoSelects[posicionSelectDestino];
    		var selectDestino=document.getElementById(idSelectDestino);
    		// Creo el nuevo objeto AJAX y envio al servidor el ID del select a cargar y la opcion seleccionada del select origen
    		var ajax=nuevoAjax();
    		ajax.open("GET", "aplicacion/servidor/personal_personal.php?selectx="+idSelectDestino+"&opcion="+opcionSeleccionada+"&cantid="+CantidPerson+"&organi="+Organizacion+valorAnterior, true);
    		ajax.onreadystatechange=function() 
    		{ 
    			if (ajax.readyState==1)
    			{
    				// Mientras carga elimino la opcion "Selecciona Opcion..." y pongo una que dice "Cargando..."
    				selectDestino.length=0;
    				var nuevaOpcion=document.createElement("option"); nuevaOpcion.value=0; nuevaOpcion.innerHTML="Cargando...";
    				selectDestino.appendChild(nuevaOpcion); selectDestino.disabled=true;	
    			}
    			if (ajax.readyState==4)
    			{
    				selectDestino.parentNode.innerHTML=ajax.responseText;
    			} 
    		}
    		ajax.send(null);
    	}
}