/* 
 *  Funciones JQuery para Programacion.
 *  
 */
var url_tabla_asignacion_act="aplicacion/servidor/_ajax/controlador.asignacion.jquery.php";
var currentRequest = null;

function SelecAsignacion(div,loading,ubic_tec,grp_plan,pto_plan,co_cargo){
    var pars = 'fnc=asignacion&ubic_tec='+ubic_tec+'&grp_plan='+grp_plan+'&pto_plan='+pto_plan+'&co_cargo='+co_cargo;
    if(currentRequest instanceof Ajax.Request) currentRequest.abort();
    currentRequest=new Ajax.Updater(div,
        url_tabla_asignacion_act,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) { 
                $(loading).innerHTML="";  
            },
            onComplete:function(request) {
               initBindLocal();
            }
        }
        );
    return true;     
}

function SelecAsignacionPendiente(div,loading,ubic_tec,grp_plan,pto_plan,co_cargo){    
    var pars = 'fnc=asignacion_pendiente&ubic_tec='+ubic_tec+'&grp_plan='+grp_plan+'&pto_plan='+pto_plan+'&co_cargo='+co_cargo;
    if(currentRequest instanceof Ajax.Request) currentRequest.transport.abort();
    currentRequest=new Ajax.Updater(div,
        url_tabla_asignacion_act,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) {
                $(loading).innerHTML="";

            },
            onComplete:function(request) {
               initBindLocal();
            }
        }
        );

    return true;
}     


function SelecActividadAsignada(div,loading,ubic_tec,grp_plan,pto_plan,co_cargo) {
    var pars = 'fnc=asignacion_programada&ubic_tec='+ubic_tec+'&grp_plan='+grp_plan+'&pto_plan='+pto_plan+'&co_cargo='+co_cargo;
    if(currentRequest instanceof Ajax.Request) currentRequest.transport.abort();
    currentRequest=new Ajax.Updater(div,
        url_tabla_asignacion_act,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) {
                $(loading).innerHTML="";
            },
            onComplete:function(request) {
               initBindLocal();
            }
        }
        ); 
    return true;
}
