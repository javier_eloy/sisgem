// Para validar si el usuario existe


// Para verificar si las claves coinciden       
jQuery(function($){
        //variables
        
        var pass1 = $('#pass1');
        var pass2 = $('#pass2');
        // Variable que se crea para ubicar el mensaje antes del formulario
        var coincide = $('#coincide');
        var confirmacion = "";
        var negacion = "No coinciden las contrasenas";
        var vacio = "Las contrasenas no pueden estar vacias";
        //creo el elemento span
        var span = $('<font face="Arial" size="2" color="red"><span></span></font>').insertBefore(coincide);
        //oculto por defecto el elemento span
        span.hide();
        //funci�n que comprueba las dos contrase�as
        function coincidePassword(){
            var valor1 = pass1.val();
            var valor2 = pass2.val();
            //muestro el span
            span.hide();            
            span.fadeIn(1000);
            //condiciones dentro de la funci�n
            if(valor1 != valor2){
                span.text(negacion);
                //Permite que el boton siguiente permanezca bloqueado
                $('input[type="submit"]').attr('disabled','disabled');
            }
            if(valor1.length==0 || valor1==""){
                span.text(vacio);
                //Permite que el boton siguiente permanezca bloqueado
                $('input[type="submit"]').attr('disabled','disabled');
            }
            if(valor1.length!=0 && valor1==valor2){
                span.text(confirmacion);
                //Deshabilita el bloqueo del boton
                $('input[type="submit"]').removeAttr('disabled');
            }
        }
        //ejecuto la funci�n al soltar la tecla
        pass2.keyup(function(){
            coincidePassword();
        });
});