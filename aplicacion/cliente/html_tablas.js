/* 
 * Función que devuelve datos en formato de arreglos
 * 
 */


var url_tabla="aplicacion/servidor/_ajax/controlador.configuracion.jquery.php";

function CargarCargo(nucargo,frm)
{
   var pars = 'fnc=datos_cargo&nu_cargo='+nucargo;
   new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onSuccess:function(request){
            var JsonRec = eval( '(' + request.responseText + ')');
            if(JsonRec){
               frm._nu_cargo.value=JsonRec['NU_CARGO'];
               frm._nu_cargo_padre.value=(JsonRec['NU_CARGO_PADRE']) ? JsonRec['NU_CARGO_PADRE']: '0';
               SelectUbicacionesTecnicas('div_ubic_tecnica','_co_ubic_tecnica',JsonRec['CO_UBIC_TECNICA'],JsonRec['CO_UBIC_TECNICA_PADRE']);
               frm._nb_cargo.value=JsonRec['NB_CARGO'];
               frm._co_cargo.value=JsonRec['CO_CARGO'];
               frm._codigo_original.value=JsonRec['Codigo'];
               frm._co_ubic_tecnica_original.value=JsonRec['CO_UBIC_TECNICA'];
               frm._tx_descripcion.value=JsonRec['TX_DESCRIPCION'];
               frm._in_requerido.checked=(JsonRec['IN_REQUERIDO']=="1") ? true : false;
               $('grupobtn').innerHTML='<input class="boton"  id="accion" name="accion" value="Actualizar" type="submit" >'+
            '<input class="boton"  value="Cancelar" type="button" onclick="location.href=\'cpanel.php?sistema=6\';" >';
            }
         }
      }
      );
   return true;
}

function CargarNomen(co_estado_cargo,frm)
{
   var pars = 'fnc=datos_nomen&co_estado_cargo='+co_estado_cargo;
   new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onSuccess:function(request){
            var JsonRec = eval( '(' + request.responseText + ')');
            if(JsonRec){
                    
               frm.codigo.value=JsonRec['VA_ESTATUS_ORDEN'];
               frm.nombre.value=JsonRec['NB_CO_ESTATUS_ORDEN'];
               frm.descripcion.value=JsonRec['TX_DESCRIPCION'];
               frm.co_estado_orden.value=JsonRec['CO_ESTATUS_ORDEN'];
               $('grupobtn').innerHTML='<input class="boton"  id="accion" name="accion" value="Actualizar" type="submit" >'+
            '<input class="boton"  value="Cancelar" type="button" onclick="location.href=\'cpanel.php?sistema=127\';" >';
            }
         }
      }
      );
   return true;
}

function CargarClase(co_clase,frm)
{
   var pars = 'fnc=datos_clase&co_clase='+co_clase;
   new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onSuccess:function(request){
            var JsonRec = eval( '(' + request.responseText + ')');
            if(JsonRec){
                    
               frm.codigo.value=JsonRec['TX_SIGLAS_CLASE'];
               frm.nombre.value=JsonRec['NB_CLASE'];
               frm.descripcion.value=JsonRec['TX_DESCRIPCION'];
               frm.co_estado_clase.value=JsonRec['CO_CLASE'];
               $('grupobtn').innerHTML='<input class="boton"  id="accion" name="accion" value="Actualizar" type="submit" >'+
            '<input class="boton"  value="Cancelar" type="button" onclick="location.href=\'cpanel.php?sistema=125\';" >';
            }
         }
      }
      );
   return true;
}

function EliminaRol(co_rol) {
   var jq = jQuery.noConflict();
   var pars = 'fnc=elimina_rol&co_rol='+co_rol;

   var rq=new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onComplete:null
      });
   if(rq.transport.responseText=='1') return true;
   else return false;

}

function CargarRol(co_rol,frm,divdatos,divpermiso,inputrol,accion,in_admin) {
   var jq = jQuery.noConflict();
   CargarDatosRol(co_rol,frm);
   CargarPermisoRol(co_rol,divpermiso,in_admin);
   jq(inputrol).val(co_rol);
   jq(accion).html('<input class="boton"  id="accion" name="accion" value="Actualizar" type="submit" > &nbsp;'+
      '<input class="boton"  value="Cancelar" type="button" onclick="Cancelar();" >');

   jq(divdatos).show();
   jq(divpermiso).show();

}
/**
 * Usado en CagarRol
 * */
function CargarDatosRol(co_rol,frm) {
   var pars = 'fnc=datos_rol&co_rol='+co_rol;
   new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onSuccess:function(request){
            var JsonRec = eval( '(' + request.responseText + ')');
            if(JsonRec){
               var jq=jQuery.noConflict();
               jq(frm).find("#nombre").val(JsonRec['NB_ROL']);
               jq(frm).find("#descripcion").val(JsonRec['TX_DESCRIPCION']);
               jq(frm).find("#co_rol").val(co_rol);
               
               var objchk=jq(frm).find("#admin")
               if(JsonRec['IN_ADMIN'] == 1) {
                  objchk.attr("checked",true);
                  jq(frm).find("#co_ubic_tecnica").val(JsonRec['CO_UBIC_TECNICA_ADMIN']);
               } else {                   
                  objchk.attr("checked",false);
                  jq(frm).find("#co_ubic_tecnica").val("");
               }
               
            }
         }
      });
   return true;
}
/**
 * Usado en CargarRol
 * */
function CargarPermisoRol(co_rol,divroles,in_admin) {
   var pars = 'fnc=datos_permisorol&co_rol='+co_rol+'&in_admin='+in_admin;
   new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onSuccess:function(request){
            var jq = jQuery.noConflict();

            var JsonRec = eval( '(' + request.responseText + ')');
            
            if(JsonRec){
               /* Crea la tabla dinamicament con los datos enviados */
               jq(divroles).empty();
               jq(divroles).append("<table></table>");
               var countArray = JsonRec.length;
               var tabla = jq(divroles).children();
               for(var fila=0;fila< countArray;fila++) {
                  var sfila="<tr> <td>"+JsonRec[fila]["NB_BLOQUE"]+"</td><td>"+JsonRec[fila]["TX_DESCRIPCION"]+"</td>";
                  if(JsonRec[fila]["MARCADO"] === "0")
                     sfila=sfila+"<td><input name=\"lstPermiso["+fila+"]\" value=\""+JsonRec[fila]["CO_BLOQUE"]+"\"  type=\"checkbox\"></td> </tr>";
                  else sfila=sfila+"<td><input name=\"lstPermiso["+fila+"]\" value=\""+JsonRec[fila]["CO_BLOQUE"]+"\"  type=\"checkbox\" checked></td> </tr>";
                  tabla.append(sfila);
               }
                    
            }
         }
      });
   return true;

}


function CargarNuevoRol(divroles,in_admin){
   var pars = 'fnc=datos_permiso&in_admin='+in_admin;
   var jq = jQuery.noConflict();
    
   new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:true,
         onLoading:function(request) {
            jq("divroles").innerHTML="Cargando..."
            },
         onSuccess:function(request){
            var JsonRec = eval( '(' + request.responseText + ')');

            if(JsonRec){
               /* Crea la tabla dinamicament con los datos enviados */
               jq(divroles).empty();
               jq(divroles).append("<table></table>");
               var countArray = JsonRec.length;
               var tabla = jq(divroles).children();
               for(var fila=0;fila< countArray;fila++) {
                  var sfila="<tr> <td>"+JsonRec[fila]["NB_BLOQUE"]+"</td><td>"+JsonRec[fila]["TX_DESCRIPCION"]+"</td>";
                  sfila=sfila+"<td><input name=\"lstPermiso["+fila+"]\" value=\""+JsonRec[fila]["CO_BLOQUE"]+"\"  type=\"checkbox\"></td> </tr>";
                  tabla.append(sfila);
               }

            }
         }
      });
   return true;

}



function CargarMotivo(nu_motivo_cierre,frm)
{
   var pars = 'fnc=datos_motivo&nu_motivo_cierre='+nu_motivo_cierre;
   new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onSuccess:function(request){
            var JsonRec = eval( '(' + request.responseText + ')');
            if(JsonRec){
                    
               frm.codigo.value=JsonRec['CO_MOTIVO_CIERRE'];
               frm.descripcion.value=JsonRec['TX_DESCRIPCION'];
               frm.motivoCierre.value=JsonRec['CO_TIPO'];
               if(JsonRec['IN_ACTIVO']==true)
                  frm.activo.checked=true;
               else
                  frm.activo.checked=false;
               $('grupobtn').innerHTML='<input class="boton"  id="accion" name="accion" value="Actualizar" type="submit" >'+
            '<input class="boton"  value="Cancelar" type="button" onclick="location.href=\'cpanel.php?sistema=129\';" >';
            }
         }
      }
      );
   return true;
}

function SelecGrupoPto(btn,div,co_centro_emp,nu_cargo){
   var pars = 'fnc=cargo_grp_pto&co_centro_emp='+co_centro_emp+'&nu_cargo='+nu_cargo;
   new Ajax.Updater(div,
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:true,
         onLoading:function(request) {
            $(btn).style.display="none";
            $(div).innerHTML="Cargando...";
         },
         onSuccess:function(request) {
            $(btn).style.display="block";
         }
      }
      );
   return true;
}

function MostrarTablaEquipos(page,div,sessionid,find,resetcache){
   var pars = 'fnc=cargar_equipos_apc&sessionid='+sessionid+'&currentpage='+page+'&find='+find+'&resetcache='+resetcache;
   new Ajax.Updater(div,
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:true,
         onLoading:function(request) {
            $("loading").innerHTML="Cargando..."
         },
         onSuccess:function(request) {
            $("loading").innerHTML="";
         },
         onComplete:function(request) {
            ajustarPaneles();
         }
      }
      );
   return true;
    
}


function MostrarTablaEstacion(page,div,sessionid,find,resetcache){    
   var pars = 'fnc=cargar_estaciones_apc&sessionid='+sessionid+'&currentpage='+page+'&find='+find+'&resetcache='+resetcache;
    
   new Ajax.Updater(div,
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:true,
         onLoading:function(request) {
            $("loading").innerHTML="Cargando..."
         },
         onSuccess:function(request) {
            $("loading").innerHTML="";
         },
         onComplete:function(request) {
            ajustarPaneles();
         }
      }
      );
   return true;
    
}

function MostarGrupoPto(co_centro_emp,nu_cargo)
{
   var w=window.open(url_tabla+'?fnc=mostrar_cargo_grp_pto&co_centro_emp='+co_centro_emp+'&nu_cargo='+nu_cargo,'mostrarcargos','left=100px,top=100px,status=0,toolbar=0,width=300px,height=400px');
   w.focus();
}

function EsCodigoRepetidoCargo(co_cargo_estructura,nu_cargo)
{
   var pars = 'fnc=co_cargo_repetido&nu_cargo='+nu_cargo+'&co_cargo_estructura='+co_cargo_estructura;
   var rq=new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onComplete:null
      }
      );
   if(rq.transport.responseText=='1') return true;
   else return false;
}

function EsCodigoRepetidoRol(nb_rol,co_rol)
{
   var pars = 'fnc=nb_rol_repetido&nb_rol='+nb_rol+'&co_rol='+co_rol;
   var rq=new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onComplete:null
      }
      );
   if(rq.transport.responseText=='1') return true;
   else return false;
}


function EsCodigoRepetidoNomen(scodigo,co_estado_orden)
{
   var pars = 'fnc=co_estado_orden_repetido&co_estado_orden='+co_estado_orden+'&codigo='+scodigo;
   var rq=new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onComplete:null
      }
      );
   if(rq.transport.responseText=='1') return true;
   else return false;
}

function EsCodigoRepetidoClase(scodigo,co_clase)
{
   var pars = 'fnc=co_clase_repetido&co_clase='+co_clase+'&codigo='+scodigo;
   var rq=new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onComplete:null
      }
      );
   if(rq.transport.responseText=='1') return true;
   else return false;
}

function EsCodigoRepetidoMotivoCierre(co_motivo_cierre)
{
   var pars = 'fnc=co_motivo_cierre_repetido&co_motivo_cierre='+co_motivo_cierre;
   var rq=new Ajax.Request(
      url_tabla,
      {
         method: 'post',
         parameters: pars,
         asynchronous:false,
         onComplete:null
      }
      );
   if(rq.transport.responseText=='1') return true;
   else return false;
}
