/* 
 *  Funciones JQuery para Programacion 2.1.
 *  
 */
var url_tabla_programacion="aplicacion/servidor/_ajax/controlador.programacion.jquery.php";

function SelecProgramacion(div,loading,objtitle,ubic_tec,grp_plan,pto_plan){
    var pars = 'fnc=programacion&ubic_tec='+ubic_tec+'&grp_plan='+grp_plan+'&pto_plan='+pto_plan;
    new Ajax.Updater(div,
        url_tabla_programacion,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,     
            evalScripts: true, 
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) { 
                $(loading).innerHTML="";
                $(objtitle).show();
            }
        }
        ); 
    return true;     
}

function SelecProgramacionPendiente(div,loading,ubic_tec,grp_plan,pto_plan){
    var pars = 'fnc=programacion_pendiente&ubic_tec='+ubic_tec+'&grp_plan='+grp_plan+'&pto_plan='+pto_plan;
    new Ajax.Updater(div,
        url_tabla_programacion,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) {
                $(loading).innerHTML="";
            }
        }
        ); 
    return true;
}     


function SelecActividadProgramada(div,loading,ubic_tec,grp_plan) {
    var pars = 'fnc=actividad_programada&ubic_tec='+ubic_tec+'&grp_plan='+grp_plan;
    new Ajax.Updater(div,
        url_tabla_programacion,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) {
                $(loading).innerHTML="";
            }
        }
        ); 
    return true;
}

function SelecActividadAplazada(div,loading,ubic_tec,grp_plan) {

    var pars = 'fnc=actividad_aplazada&ubic_tec='+ubic_tec+'&grp_plan='+grp_plan;
    new Ajax.Updater(div,
        url_tabla_programacion,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) {
                $(loading).innerHTML="";
            }
        }
        ); 
    return true;
}


function ActualizarAplazada(co_plan,co_estado_plan) {
    var pars = 'fnc=actualizar_plan&co_plan='+co_plan+'&co_estado_plan='+co_estado_plan;
    var rq=new Ajax.Request(
        url_tabla_programacion,
        {
            method: 'post',
            parameters: pars,
            asynchronous:false, 
            onSuccess:function(request) {}
        }
        );     
      if(rq.transport.responseText=='1') return true;
                                   else return false;
}


function  SelecOrdActivosUbicacion(div,co_plan, fecha_tope, ubic_tec, grp_plan,pto_plan, showAll){
  var pars = 'fnc=ordactivos_ubic&co_plan='+co_plan+'&fecha_tope='+fecha_tope+'&ubic_tec='+ubic_tec+'&grp_plan='+grp_plan+'&pto_plan='+pto_plan+'&showAll='+showAll;
  var jq=jQuery.noConflict();

   new Ajax.Updater(div,
        "../"+url_tabla_programacion,
        {
            method: 'post',
            parameters: pars,
            asynchronous:false,
            cache:false,
            onLoading:function() {
                jq('#'+div).html("Cargando...");
                
            },
            onComplete:function(request){
               jq("#datepicker").datepicker('setDate',jq('#fecha_tope').val());
               jq("#btnUpdate").attr("disabled", true);
            }
        }
        );
    return true;
   
}