/* 
 * Función que devuelve datos en formato de arreglos
 * 
 */


var url_tabla="aplicacion/servidor/_ajax/controlador.reportes.jquery.php";

function MostrarTablaConsultaEquipos(page,div,sessionid,find,resetcache){
    var pars = 'fnc=cargar_equipos_consulta&sessionid='+sessionid+'&currentpage='+page+'&find='+find+'&resetcache='+resetcache;
    new Ajax.Updater(div,
       url_tabla,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) { $("loading").innerHTML="Cargando..."},
            onSuccess:function(request) { $("loading").innerHTML="";},
            onComplete:function(request) { ajustarPaneles();}
        }
       ); 
    return true;     
    
}

function MostrarTablaConsultaEstacion(page,div,sessionid,find,resetcache){
    var pars = 'fnc=cargar_estaciones_consulta&sessionid='+sessionid+'&currentpage='+page+'&find='+find+'&resetcache='+resetcache;
    
    new Ajax.Updater(div,
       url_tabla,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) { $("loading").innerHTML="Cargando..."},
            onSuccess:function(request) { $("loading").innerHTML="";},
           onComplete:function(request) { ajustarPaneles();}
        }
       ); 
    return true;     
    
}

function MostrarTablaConsultaPersonal(page,div,sessionid,find,resetcache){
    var pars = 'fnc=cargar_personal_consulta&sessionid='+sessionid+'&currentpage='+page+'&find='+find+'&resetcache='+resetcache;
    
    new Ajax.Updater(div,
       url_tabla,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) { $("loading").innerHTML="Cargando..."},
            onSuccess:function(request) { $("loading").innerHTML="";},
           onComplete:function(request) { ajustarPaneles();}
        }
       ); 
    return true;     
    
}
