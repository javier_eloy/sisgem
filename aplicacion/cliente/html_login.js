/*
 *  Funciones JQuery para login.
 *
 */
var url_tabla_login="aplicacion/servidor/_ajax/controlador.login.jquery.php";

function SelecRole(div,load,user,pawd,array_msg){
   var resp=false;
   $.ajax({
      url: url_tabla_login,
      type: "POST",
      async:false,
      data: {
         fnc:"buscar_rol",
         usr:user,
         pwd:pawd
      },
      beforeSend:function(){
         $(load).show();
      },
      success:function(shtml){
         if($.isNumeric(shtml)){
            var valInt=parseInt(shtml);
            if (valInt >= 0) {
               Sexy.alert(array_msg[valInt]);
            } else
               Sexy.error("Error desconocido");
            resp=false;
            return;
         } else
            $(div).html(shtml);
         resp=true;
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
         resp=false;
      },
      complete:function(){
         $(load).hide();
      }
   });
   return resp;

}

function Autenticar(load,usr,pawd,role,forz,array_msg){
   var resp=false;
   $.ajax({
      url: url_tabla_login,
      type: "POST",
      async:false,
      data: {
         fnc:"autenticar",
         usr:usr,
         pwd:pawd,
         rol:role,
         fz:forz
      },
      beforeSend:function(){
         $(load).show();
      }
      ,
      success:function(shtml){         
         if($.isNumeric(shtml)){
            var valInt=parseInt(shtml);
            if (valInt >= 0) {
               Sexy.alert(array_msg[valInt]);
            } else
               Sexy.error("Error desconocido");
            resp=-1;
            return;
         } else
            switch(shtml) {
               case "mauth": /* Multiple sesion */
                  resp=1;
                  break;
               case "auth":
                  resp=0;
                  break;
               default:
                  resp=-1;
                  break;
            }
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
         resp=-1;
      },
      complete:function(){
         $(load).hide();
      }
   });

   return resp;
   
}