// Cuando el mouse se pone encima de un elemento con el class=text
function onMouseEnterProg(obj,event){
    var jq=jQuery.noConflict();
    var referencia = jq(obj, 'div').find('.referencia');
    var contenido = referencia.attr("content");
    var notaflotante= jq("#flotante").attr("content");
        
    if (contenido!=notaflotante && contenido != ''){
        jq("#flotante").html(contenido);
        jq("#flotante").css({
            left:event.pageX+5, 
            top:event.pageY+5,
            display:"block",
            position:"absolute",
            'z-index':"1000"
        });
        
    }
            
            
};
    
// Cuando el mouse sale del elemento con el class=text
function onMouseLeaveProg(obj){
    // Escondemos el div flotante
    var jq=jQuery.noConflict();
    jq("#flotante").hide();
};
