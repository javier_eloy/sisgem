/* 
 * Función que especifica funciones generales para cargar
 * Combos
 */

var url_combos="aplicacion/servidor/_ajax/controlador.configuracion.jquery.php";
var lastCargo;

function SelectUbicacionesTecnicas(div,idselect,idoption,ubic_tec,onchange)
{
      var pars = 'combo=ubicacion_tecnica&idcombo='+idselect+'&idopcion='+idoption+'&ubic_tec='+ubic_tec;
       new Ajax.Updater(div,
        url_combos,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){},
            onComplete:function(request){ $(idselect).setAttribute("onChange",onchange );  }
        }); 
      return true;
}

function SelectUbicacionesTecnicasCargo(div,idselect,idoption,objcargo,onchange)
{
     if(idoption!='0') {
         
         if (!confirm('El cambio de cargos cargará las ubicaciones técnicas relacionadas al cargo superior. Asimismo, eliminará las ubicaciones tecnicas asignadas a los cargos supervisados¿Desea continuar?'))
            {
              objcargo.value = lastCargo;
              return false;
             }
     }      
      var pars = 'combo=ubicacion_tecnica_cargo&idcombo='+idselect+'&idopcion='+idoption+'&idcargo='+objcargo.value;
       new Ajax.Updater(div,
        url_combos,
        {
            method: 'get',
            parameters: pars,
            asynchronous:true,
            evalScripts:true,
            onLoading:function(request){ $(div).innerHTML="Cargando...";},
            onComplete:function(request){ $(idselect).setAttribute("onChange",onchange ); }
        });
        
        lastCargo=objcargo.value;
        return true;
}

function SelectCargos(div,idselect,idoption,ubic_tec,onchange)
{
      var pars = 'combo=cargos&idcombo='+idselect+'&idopcion='+idoption+'&ubic_tec='+ubic_tec;
       new Ajax.Updater(div,
        url_combos,
        {
            method: 'get',
            parameters: pars,
            asynchronous:false,
            evalScripts:true,
            onLoading:function(request){},
            onComplete:function(request){ $(idselect).setAttribute("onChange",onchange );}
        });         
  return true;
}
