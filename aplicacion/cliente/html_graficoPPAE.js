/*
 *  Funciones JQuery para grafico PPAE.
 *
 */



var url_tabla_PPAE="aplicacion/servidor/_ajax/controlador.grafico_PPAE.jquery.php"
// vectores  donde se guarda la informacion de los select list
var GRUPO_SOLUCIONADOR = [];
var GRUPO_PLAN         = [];
var PUESTO_TRAB        = [];
var INDICADOR          = [];
var data_report        = [];
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//variables para grafico
// semanas
var V_planificado      = []; 
var V_asignado         = [];
var V_ejecutado        = [];
var V_programado       = [];
var V_x                = [];
//meses
var Month_planificado      = []; 
var Month_asignado         = [];
var Month_ejecutado        = [];
var Month_programado       = [];
var Month_x                = [];




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//      modifica los valores de los vectores dependiendo de la fecha especificada                                  ///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function check_date(date,V_d,V_pla,V_pro,V_asig,V_ejec,pla,pro,asig,ejec)
{
  
  flag = false;
  if(V_d.length != 0)
  {
    jq.each(V_d,function(key,val){
  
      if(val[1] === date)
      {
        V_pla[key][1]+=pla;
        V_asig[key][1]+=asig;
        V_ejec[key][1]+=ejec;
        V_pro[key][1]+=pro;
        flag = true;
      }
  
    });
    if(!flag)
    {
      V_d.push([V_d.length,date]);
      V_pla.push([V_pla.length,(pla+V_pla[V_pla.length-1][1])]);
      V_pro.push([V_pro.length,(pro+V_pro[V_pro.length-1][1])]);
      V_asig.push([V_asig.length,(asig+V_asig[V_asig.length-1][1])]);
      V_ejec.push([V_ejec.length,(ejec+V_ejec[V_ejec.length-1][1])]);
    }
  
   
  }
  else
  {

    V_d.push([V_d.length,date]);
    V_pla.push([V_pla.length,pla]);
    V_pro.push([V_pro.length,pro]);
    V_asig.push([V_asig.length,asig]);
    V_ejec.push([V_ejec.length,ejec]);
  
    
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////                        inicializa los vectores por la data suministrada                                     ///
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Orderdata(data,I)
{

 temp_planificado      = data[3]=== null || data[3]=== ""   ?0:data[3]; 
 temp_programado       = data[4]=== null || data[4]=== ""   ?0:data[4];
 temp_asignado         = data[5]=== null || data[5]=== ""   ?0:data[5];
 temp_ejecutado        = data[6]=== null || data[6]=== ""   ?0:data[6];

 
 temp_planificado      =  parseInt(temp_planificado);
 temp_programado       =  parseInt(temp_programado);
 temp_asignado         =  parseInt(temp_asignado);
 temp_ejecutado        =  parseInt(temp_ejecutado);



  
  check_date(data[0],V_x,V_planificado,V_programado,V_asignado,V_ejecutado,temp_planificado,temp_programado,temp_asignado,temp_ejecutado);

  temp = data[0].split("-");
  c_dt = temp[1]+"-"+temp[2];
  
  check_date(c_dt,Month_x,Month_planificado,Month_programado,Month_asignado,Month_ejecutado,temp_planificado,temp_programado,temp_asignado,temp_ejecutado);


}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////                  renicia los valores del select list                               ////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function clean(list)
{
  if(list != "#Puesto_trabajador")
  {
    jq("#Grupo_planificador")
    .find('option')
    .remove()
    .end();
    
   

    jq("#Puesto_trabajador")
    .find('option')
    .remove()
    .end();
    

    jq("#Mantenedor")
    .find('option')
    .remove()
    .end();
    
    jq("#Grupo_planificador").multipleSelect( 'refresh' );
    jq("#Mantenedor").multipleSelect( 'refresh' );
   

  }
  else
  {
    jq("#Puesto_trabajador")
    .find('option')
    .remove()
    .end();
    
  }


   jq("#Puesto_trabajador").multipleSelect( 'refresh' );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////      Insierta los valores en el select list especificado             /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function insertDataDropList(list,values)
{
 
 if("#Mantenedor" != list )
  clean(list);

  
   for (i = 0; i < values.length -1 ; i++)
    {
      jq(list).append('<option value='+values[i]+'>'+values[i]+'</option>');
    }
  
   
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////Insierta los valores de los mantenedores dependiendo del grupo solucionador/////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Select_maintainer(list_Group,group,code){
  
 
   
  jq.ajax({
      url: url_tabla_PPAE,
      type: "POST",
      async:false,
      data: {
         fnc:"Select_maintainer",
         user_id:code,
         group:group
      },
      beforeSend:function(){

         
        
      },
      success:function(data){
         
         if (data == -8) 
         {
            alert("error codigo usuario");
         }
         else
         {
            var item = data.split("\n");
            INDICADOR.length = 0;
            if(item.length > 3)
            {
             jq( "#test_s" ).show( 2000 ); 
              
              for (i = 1; i < item.length; i++)
              {
                INDICADOR.push(item[i]);
              }
            }
            else
            {
              jq( "#test_s" ).hide( "fast");
              
            }

            
        }
       
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
        
      },
      complete:function(){
      
      if(INDICADOR.length > 0)
      {
         
        insertDataDropList(list_Group,INDICADOR);
   
      }
        jq(list_Group).multipleSelect( 'refresh' );
        jq(list_Group).multipleSelect( 'checkAll' );
       
        

      }
   });
  

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////Busca Informacion de los grupos solucionadores                //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function data_selectList(load,data_div,code,list_Group){
  
  
   
  jq.ajax({
      url: url_tabla_PPAE,
      type: "POST",
      async:false,
      data: {
         fnc:"select_group",
         user_id:code
      },
      beforeSend:function(){

         jq(load).show();
         jq(data_div).hide();
      },
      success:function(data){
         
         if (data == -8) 
         {
            alert("error codigo usuario");
         }
         else
         {
            var item = data.split("\n");
            GRUPO_SOLUCIONADOR.length = 0;
            for (i = 1; i < item.length; i++)
             {
              GRUPO_SOLUCIONADOR.push(item[i]);
            }

            

        }
   
         
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
        
      },
      complete:function(){
      
      if(GRUPO_SOLUCIONADOR[0])
      {
         
        insertDataDropList(list_Group,GRUPO_SOLUCIONADOR);
        
      }
        
        jq(load).hide();
        jq(data_div).show();

        
      }
   });
  

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////Busca Informacion de los grupos planificadores                 //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Plan_group(load,list_Group,group,code){
  
 
   
  jq.ajax({
      url: url_tabla_PPAE,
      type: "POST",
      async:false,
      data: {
         fnc:"Select_plam",
         user_id:code,
         group:group
      },
      beforeSend:function(){

         jq(load).show();
        
      },
      success:function(data){
         
         if (data == -8) 
         {
            alert("error codigo usuario");
         }
         else
         {
            var item = data.split("\n");
            GRUPO_PLAN.length = 0;
            for (i = 1; i < item.length; i++)
            {
              GRUPO_PLAN.push(item[i]);
            }

           

        }
      
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
        
      },
      complete:function(){
      
      if(GRUPO_PLAN[0])
      {
         
        insertDataDropList(list_Group,GRUPO_PLAN);
   
      }
        
        jq(load).hide();
        Select_maintainer("#Mantenedor",group,code);
        jq(list_Group).multipleSelect( 'refresh' );
        jq(list_Group).multipleSelect( 'checkAll' );
      }
   });
  

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////Muestra la informacion necesaria para generar el reporte        //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function show_Report(load,G_plani,Pus_tra,start,end,G_sol)
{
  

        

  jq.ajax({
      url: url_tabla_PPAE,
      type: "POST",
      async:false,
      data: {
         fnc:"Show_report",
         Plan:G_plani,
         Tra:Pus_tra,
         D_start:start,
         D_end:end
      },
      beforeSend:function(){

         jq(load).show();
        
      },
      success:function(data){
        

    
         
        var item = data.split("\n");
          data_report .length = 0;
          for (i = 1; i < item.length; i++)
          {
            data_report.push(item[i].split(","));
          }


         Grid(data_report);
           
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
        
      },
      complete:function(){
        
        jq(load).hide();
        jq("#report_title").html("<p>Grupo Solucionador: "+G_sol+"</p>");
      
      }
    });
  
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////Muestra la informacion necesaria para generar el reporte     con el puesto de trabajo/////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function show_Report_Place(load,G_plani,Pus_tra,start,end,G_sol,workP)
{
  

        

  jq.ajax({
      url: url_tabla_PPAE,
      type: "POST",
      async:false,
      data: {
         fnc:"Show_report_place",
         Plan:G_plani,
         Tra:Pus_tra,
         D_start:start,
         D_end:end,
         Wplace:workP
      },
      beforeSend:function(){

         jq(load).show();
        
      },
      success:function(data){
        

     
         
        var item = data.split("\n");
          data_report .length = 0;
          for (i = 1; i < item.length; i++)
          {
            data_report.push(item[i].split(","));
          }


         Grid(data_report);
           
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
        
      },
      complete:function(){
        
        jq(load).hide();
        jq("#report_title").html("<p>Grupo Solucionador: "+G_sol+"</p>");
      
      }
    });
  
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////Muestra la informacion necesaria para generar el reporte con el mantenedor////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function show_Report_Maintener(load,G_plani,Pus_tra,start,end,G_sol,main)
{
  

        

  jq.ajax({
      url: url_tabla_PPAE,
      type: "POST",
      async:false,
      data: {
         fnc:"Show_report_maintener",
         Plan:G_plani,
         Tra:Pus_tra,
         D_start:start,
         D_end:end,
         maintener:main
      },
      beforeSend:function(){

         jq(load).show();
        
      },
      success:function(data){
        

     
         
        var item = data.split("\n");
          data_report .length = 0;
          for (i = 1; i < item.length; i++)
          {
            data_report.push(item[i].split(","));
          }

          
         Grid(data_report);
           
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
        
      },
      complete:function(){
        
        jq(load).hide();
        jq("#report_title").html("<p>Grupo Solucionador: "+G_sol+"</p>");
      
      }
    });
  
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////Muestra la informacion necesaria para generar el reporte con el mantenedor y el puesto de trabajo/////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function show_Report_Maintener_W(load,G_plani,Pus_tra,start,end,G_sol,main,W_place)
{
  

  jq.ajax({
      url: url_tabla_PPAE,
      type: "POST",
      async:false,
      data: {
         fnc:"Show_report_maintener_workPlace",
         Plan:G_plani,
         Tra:Pus_tra,
         D_start:start,
         D_end:end,
         maintener:main,
         WorkPlace:W_place
      },
      beforeSend:function(){

         jq(load).show();
        
      },
      success:function(data){
        

        
         
        var item = data.split("\n");
          data_report .length = 0;
          for (i = 1; i < item.length; i++)
          {
            data_report.push(item[i].split(","));
          }

        
         Grid(data_report);
           
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
        
      },
      complete:function(){
        
        jq(load).hide();
        jq("#report_title").html("<p>Grupo Solucionador: "+G_sol+"</p>");
      
      }
    });
  
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////        trae y verifica la informacion para la construccion del reporte                                ////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function invoke_Report()
{
  



  var G_plani = "";
  var Pus_tra = "";
  var mant    = "";


    jq.each(jq("#Grupo_planificador").multipleSelect("getSelects"),function(index,value)
      {
        G_plani+="'"+this+"',"
      });

    jq.each(jq("#Puesto_trabajador").multipleSelect("getSelects"),function(index,value)
      {
        Pus_tra+="'"+this+"',"
      });


    jq.each(jq("#Mantenedor").multipleSelect("getSelects"),function(index,value)
      {
        mant+="'"+this+"',"
      });


    var ubi_tec = jq("#Ubicacion_tec").val();


    if( jq('#datepicker').datepicker('getDate') === null  || jq('#datepicker2').datepicker('getDate') === null  || G_plani.length === 0 || Pus_tra.length === 0 || (jq("#test_s").is(':visible') && mant.length === 0 ) )
    {
      alert('debe seleccionar todos los valores');
    }
    else
    {

      if( Number( jq('#datepicker').datepicker('getDate') ) <= Number( jq('#datepicker2').datepicker('getDate')) )
      {
        var start = jq('#datepicker').datepicker('getDate').getFullYear()+"-"+(jq('#datepicker').datepicker('getDate').getMonth()+1)+"-"+jq('#datepicker').datepicker('getDate').getDate();
        var end   = jq('#datepicker2').datepicker('getDate').getFullYear()+"-"+(jq('#datepicker2').datepicker('getDate').getMonth()+1)+"-"+jq('#datepicker2').datepicker('getDate').getDate();
        G_plani=G_plani.substring(0, G_plani.length-1);
        Pus_tra=Pus_tra.substring(0, Pus_tra.length-1);
        mant=mant.substring(0, mant.length-1);


        if(jq("#test_s").is(':visible'))
          {
            if(jq("#Ubicacion_tec").val())
            {

               if(!jq('#rest_ubi').is(':checked') )
                workp="%"+jq("#Ubicacion_tec").val()+"%";       
               else
                workp=jq("#Ubicacion_tec").val();

              show_Report_Maintener_W("#loading",G_plani,Pus_tra,start,end,jq("#Grupo_solucionador").val(),mant,workp);
              

            }
            else
            {
             show_Report_Maintener("#loading",G_plani,Pus_tra,start,end,jq("#Grupo_solucionador").val(),mant,workp); 
            }
            
            
          }
        else
        {
          var workp="";

          if(jq("#Ubicacion_tec").val())
          {

             if(!jq('#rest_ubi').is(':checked') )
              workp="%"+jq("#Ubicacion_tec").val()+"%";       
             else
              workp=jq("#Ubicacion_tec").val();


            show_Report_Place("#loading",G_plani,Pus_tra,start,end,jq("#Grupo_solucionador").val(),workp);

          }
          else
          {
            show_Report("#loading",G_plani,Pus_tra,start,end,jq("#Grupo_solucionador").val());
          }
         
         

        }  
      }
      else
      {
        alert("la fecha de fin no puede ser menor a la fecha de inicio");
      }

      
    }
      

      
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////            Trae la informacion de los puestos de Puestos trabjos                       /////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function Plan_group_work(load,list_Group,group,plam,code){
  
  
   
  jq.ajax({
      url: url_tabla_PPAE,
      type: "POST",
      async:false,
      data: {
         fnc:"Select_work",
         user_id:code,
         group:group,
         plam:plam
      },
      beforeSend:function(){

         jq(load).show();
        
      },
      success:function(data){
         
         if (data == -8) 
         {
            alert("error codigo usuario");
         }
         else
         {
            var item = data.split("\n");
            PUESTO_TRAB.length = 0;
            for (i = 1; i < item.length; i++)
            {
             PUESTO_TRAB.push(item[i]);
            }
           

        }
        
      },
      error:function(){
         Sexy.alert("Error de Comunicación:"+textStatus);
        
      },
      complete:function(){
      
      if(PUESTO_TRAB[0])
      {
        insertDataDropList(list_Group,PUESTO_TRAB);
      }
        
        jq(load).hide();
        
        jq(list_Group).multipleSelect( 'refresh' );
        jq(list_Group).multipleSelect( 'checkAll' );
      }
   });
  

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////      Crea el objeto Htdmlx apartir de la informacion suministrada                                ///////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Grid(data)
{
 
   V_planificado      = []; 
   V_asignado         = [];
   V_ejecutado        = [];
   V_programado       = [];
   V_x                = []; 


  Month_planificado      = []; 
  Month_asignado         = [];
  Month_ejecutado        = [];
  Month_programado       = [];
  Month_x                = [];


   

   myGrid = new dhtmlXGridObject('gridbox');
   myGrid.setImagePath("publico/dhtmlx/imgs/");
   myGrid.setHeader("Fecha,#cspan,#cspan,Grupo Planificador,Puesto de trabajo,Cantidad,#cspan,#cspan,#cspan,separador,Horas Hombre,#cspan,#cspan");
   myGrid.attachHeader(["Semana","Mes","Año","#rspan","#rspan","Planificado","Programado","Asignado","Ejecutado","#rspan","Planificado","Real","Viaje"]);
   myGrid.setInitWidths("100,100,100,100,100,100,100,100,100,0,100,100,100");
   myGrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center,center");
   myGrid.setColumnColor("white,#f2f2f2,white,#f2f2f2,white,#f2f2f2,white,#f2f2f2,white,black,#f2f2f2,white,#f2f2f2");
   myGrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str,str");
   myGrid.setEditable(false);
  
   myGrid.init();

   var i =0;
   for( i =0;i<data.length-1;i++)
   {
    Orderdata(data[i],i+1);

    var date = data[i][0].split("-");
    myGrid.addRow(i+1,""+date[0]+","+date[1]+","+date[2]+","+data[i][1]+","+data[i][2]+","+data[i][3]+","+data[i][4]+","+data[i][5]+","+data[i][6]+",-,Planificado,"+data[i][7]+","+data[i][8]+""); 
    
   }
   

   window.localStorage.setItem("V_planificado", JSON.stringify(V_planificado));
   window.localStorage.setItem("V_asignado", JSON.stringify(V_asignado));
   window.localStorage.setItem("V_ejecutado", JSON.stringify(V_ejecutado));
   window.localStorage.setItem("V_programado", JSON.stringify(V_programado));
   window.localStorage.setItem("V_x", JSON.stringify(V_x));   
        
   window.localStorage.setItem("Month_planificado", JSON.stringify(Month_planificado));
   window.localStorage.setItem("Month_programado", JSON.stringify(Month_programado));
   window.localStorage.setItem("Month_asignado", JSON.stringify(Month_asignado));
   window.localStorage.setItem("Month_ejecutado", JSON.stringify(Month_ejecutado));
   window.localStorage.setItem("Month_x", JSON.stringify(Month_x));

   
     

    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////                             crea los multiselect list apartir del un select list              //////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 function transform(lists)
 {
    jq(lists).multipleSelect({
         allSelected: '(Todos)',
         selectAllText: 'Seleccionar Todos',
         countSelected: '# de % seleccionados',
         multipleWidth: '100%',
         width: '70%',
         onCheckAll: function() {
            jq("#btnUpdate").removeAttr("disabled");
         },
         onUncheckAll: function() {
            jq("#btnUpdate").removeAttr("disabled");
         },
         onClick: function() {
            jq("#btnUpdate").removeAttr("disabled");
         }});  
 }



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    function initBindLocal(){
        jq("tbody#detalle").hide();
        jq( "#test_s" ).hide( "fast");
        jq("tbody#orden .ClassMas").click(function(event){
            var desplegable = jq(this).parents('#orden').get(0).next('#detalle')/*jq(this).next()*/;
            desplegable.toggle();
            event.preventDefault();
        });
        jq("#datepicker").datepicker({ 
            maxDate:"+1d",
            showWeek:true,
           weekHeader:"Sem.",
            showOn: "both",
            buttonImage: "publico/imagenes/calendar-icon.png",
            buttonImageOnly: true,
            defaultDate: new Date(2012,1,1,0,0,0,0)
           });
        jq("#datepicker2").datepicker({ 
            maxDate:"+1d",
            showWeek:true,
           weekHeader:"Sem.",
            showOn: "both",
            buttonImage: "publico/imagenes/calendar-icon.png",
            buttonImageOnly: true
           });

          
        jq( "#Grupo_solucionador" ).change(function()
         {
          if (jq("#Grupo_solucionador").val() != 0)
          {

            
            Plan_group('#loading','#Grupo_planificador',jq("#Grupo_solucionador").val(),jq('#U1').val());
          }
          else
          {
            jq( "#test_s" ).hide( "fast");
            clean("#Grupo_solucionador");

          }
         });
        

        jq( "#Grupo_planificador").change(function()
         {

          
          if (jq("#Grupo_planificador").multipleSelect("getSelects").length  != 0)
          {


            var G_plani = "";
            jq.each(jq("#Grupo_planificador").multipleSelect("getSelects"),function(index,value)
              {
                G_plani+="'"+this+"',"
              });

              G_plani=G_plani.substring(0, G_plani.length-1);

            Plan_group_work('#loading','#Puesto_trabajador',jq("#Grupo_solucionador").val(),G_plani,jq('#U1').val());

          }
          else
          {
            
            clean("#Puesto_trabajador");
            jq("#Puesto_trabajador").multipleSelect( 'refresh' );
          }
         });
        
        transform("#Puesto_trabajador");
        transform("#Grupo_planificador");
        transform("#Mantenedor");


        

        jq("#impr")
                .mouseover(function()
                { 
                  jq(this).attr("src", "publico/imagenes/print_over.png");
                  
                })

                .mouseout(function() 
                {
                  
                  jq(this).attr("src", "publico/imagenes/print.png");
                  
                });

      jq("#cmdbuscar").click(function(){invoke_Report()});


      jq("#chart").click(function()
        {});




    }
