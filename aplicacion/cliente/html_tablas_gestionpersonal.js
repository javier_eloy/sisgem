/* 
 *  Funciones JQuery para Programacion.
 *  
 */
var url_tabla_gestionpersonal="aplicacion/servidor/_ajax/controlador.gestionpersonal.jquery.php";

function SelecUsuario(div,loading,busqueda,co_cargo){
  
    var pars = 'fnc=buscar_usuario&tx_busqueda='+busqueda+'&co_cargo='+co_cargo;
    new Ajax.Updater(div,
        url_tabla_gestionpersonal,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) {
                $(div).innerHTML="";                
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) { 
                $(loading).innerHTML="";  
            },
            onComplete:function(request) {
                initBindLocal();
            }
        }
        ); 
    return true;     
}

//*---

function SelecUsuarioReemplazo(loading,busqueda,co_cargo){

    var pars = 'fnc=buscar_usuario_reemplazo&tx_busqueda='+busqueda+'&co_cargo='+co_cargo;
    new Ajax.Request("../" + url_tabla_gestionpersonal,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,
            onLoading:function(request) {
                $(loading).innerHTML="Buscando...";
            },
            onSuccess:function(request) {
                $(loading).innerHTML="";
                var JsonRec=eval("("+request.responseText+")");
                if(JsonRec){
                    ActualizarCampos(JsonRec);
                } else {
                    LimpiarCampos();
                    alert("No se enconró el trabajador o no tiene acceso al cargo de la persona a buscar");
                }
            }
        }
        );
    return true;
}

//*---

function SelecUsuarioVaca(){
    var pars="fnc=grafico_personal";
    var rq=new Ajax.Request(
        url_tabla_gestionpersonal,
        {
            method: 'post',
            parameters: pars,
            asynchronous:false, 
            onSuccess:function(request) {
                var data=eval("("+request.responseText+")");               
                  jQuery.jqplot('chart_vacacion',[data],{
                        title: "Relación de trabajadores No disponible durante el año en curso",
                   seriesDefaults: {
                       renderer:jQuery.jqplot.BarRenderer,
                       pointLabels: {show: true, location:'s', ypadding:3}
                   }, 
                   axesDefaults: {
                        tickRenderer: jQuery.jqplot.CanvasAxisTickRenderer ,
                        tickOptions: {
                            angle: -30,
                            fontSize: '10pt'
                        }
                    },
                    axes: {
                        xaxis: {
                            renderer: jQuery.jqplot.CategoryAxisRenderer
                        },
                        yaxis: {
                               pad: 1.6,
                               tickInterval:1,
                               tickOptions: {formatString: '%d'},
                               padMin: 0
                        }
                    }
                  }); 
                
            }
        }
        );                 

}