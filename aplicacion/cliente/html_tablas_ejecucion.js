/* 
 *  Funciones JQuery para Ejecucion.
 *  
 */
var url_tabla_ejecucion="aplicacion/servidor/_ajax/controlador.ejecucion.jquery.php";

function SelecCartelera(div,loading,fecha,co_usuario){
    var pars = 'fnc=cartelera&fecha='+fecha+'&co_usuario='+co_usuario;
    new Ajax.Updater(div,
        url_tabla_ejecucion,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,     
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) { 
                $(loading).innerHTML="";  
            },
            onComplete:function(request) {
                   initBindLocal();
                  ajustarPaneles();
            }
        }
        ); 
    return true;     
}

function SelecOrdenes(div,loading,fecha,co_usuario){
    var pars = 'fnc=ordenes&co_usuario='+co_usuario+'&fecha='+fecha;
    new Ajax.Updater(div,
        url_tabla_ejecucion,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,     
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) { 
                $(loading).innerHTML="";  
            },
            onComplete:function(request) {
                  initBindLocal();
                  ajustarPaneles();
            }
        }
        ); 
    return true;     
}

function SelecOrdenesR(div,loading,fechai, fechaf,estatus){
    var pars = 'fnc=ordenesR&estatus='+estatus+'&fechai='+fechai+'&fechaf='+fechaf;
    new Ajax.Updater(div,
        url_tabla_ejecucion,
        {
            method: 'post',
            parameters: pars,
            asynchronous:true,     
            onLoading:function(request) {
                $(loading).innerHTML="Cargando...";
            },
            onSuccess:function(request) { 
                $(loading).innerHTML="";  
            },
            onComplete:function(request) {
                  initBindLocal();
                  ajustarPaneles();
            }
        }
        ); 
    return true;     
}

