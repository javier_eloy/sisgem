<?php

/**
 * Description of session
 *
 * @author hernandezjnz
 */
class websession {

   function __construct() {
      session_name(USUARIOS_SESION);
      if(session_id() == "") session_start();
   }

   function __destruct() {
      session_write_close();
   }

   function getData($key) {
      return $_SESSION[$key];
   }

   function setData($key, $value) {
      $_SESSION[$key] = $value;
   }

}
