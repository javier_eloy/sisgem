<?php

class CacheAPC {

   var $iTtl = CACHE_EXPIRACION; // Time To Live
   var $bEnabled = false; // APC enabled

    // constructor
   function __construct() {
      $this->bEnabled = extension_loaded('apc');
   }

   // get data from memory
   function getData($sKey) {

      $vStore = apc_fetch($sKey);
      if ($vStore !== false)
         $vData = unserialize($vStore);
      else
         $vData = false;
      
      return $vData;
   }

   // save data to memory
   function setData($sKey, $vData) {
      $vStore = serialize($vData);
      return apc_store($sKey, $vStore, (int) $this->iTtl);
   }

   // delete data from memory
   function delData($sKey) {
      $bRes = false;
      apc_fetch($sKey, $bRes);
      return ($bRes) ? apc_delete($sKey) : true;
   }

}

?>