<?php

/**
 * Description of WorkSession
 *
 * @author hernandezjnz
 */
class datasession {

   public $id; /* Id de la sesion */
   public $timesession; /* Tiempo maximo de la sesion */
   public $maxsession; /* Maximo tiempo definido de la sesion */

}

class WorkSession {

   private $path;
   private $fdata;

   function __construct($user) {
      $this->path = sys_get_temp_dir() . DIRECTORY_SEPARATOR . base64_encode($user) . ".wsgm";
      $this->fdata = new datasession();
   }

   /*
    * Almacena una nueva sesion
    */

   public function saveNewSession($session_id, $maxtime) {
      $this->fdata->id = $session_id;
      $this->fdata->timesession = time() + $maxtime;
      $this->fdata->maxsession = $maxtime;

      $res = file_put_contents($this->path, base64_encode(serialize($this->fdata)));
      return ($res !== false);
   }

   /*
    * Debe usarse luego de llamar a connectSession();
    */

   public function updateSession() {
      if(empty($this->fdata)) return false;
      
      $res = file_put_contents($this->path, base64_encode(serialize($this->fdata)));
      return ($res !== false);

   }

   /*
    * Conecta y Carga los datos de la sesion
    * Devuelve nulo si no se conecta
    */
   function connectSession() {
      $ret = null;
      if (file_exists($this->path)) {
         $texto = file_get_contents($this->path);
         if ($texto !== false) {
            $this->fdata = unserialize(base64_decode($texto));
            return $this->fdata;
         }
      }
      return null;
   }

   function setNextTimeExpired($secondslive) {
      $this->fdata->timesession = time() + $secondslive;
      $this->fdata->maxsession = $secondslive;
   }

   function removeSession() {
      if (file_exists($this->path)) {
         return unlink($this->path);
      } else
         return false;
   }

   function getTimesession() {
      return $this->fdata->timesession;
   }

   function getMaxsession() {
      return $this->fdata->maxsession;
   }

   function getSessionid() {
      return $this->fdata->id;
   }

   function isExpirated() {
      if (time() > $this->fdata->timesession) {
         return true;
      } else
         return false;
   }

/***
 * Devuelve Verdadero si son iguales y Falso en caso contrario
 */
   function compareSessionId($session_id) {
      if ($this->fdata->id == $session_id)
         return true;
      else
         return false;
   }

}

?>
