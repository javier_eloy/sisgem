<?php

class PaginationArray {

    var $array_resource;   // Array Object
    var $array_search;     // Array In Search
    var $page = 1;         // Current Page
    var $perPage = 10;     // Items on each page, defaulted to 10
    var $showFirstAndLast = false; // if you would like the first and last page options.
    var $linksPerPage = 15; // How many link return 
    var $startrecord = 1;         // Start record for current page
    var $length = 0;        // Length of data
    var $pages = 1;         // How many pages it has
    var $find = "";
    var $isfind = 0;

 
    // --- Constructor    
    function __construct($array, $linksPerPage = 15, $perPage = 15) {
        // Assign the items per page variable
        if (!empty($perPage))
            $this->perPage = $perPage;

        if (!empty($linksPerPage))
            $this->linksPerPage = $linksPerPage;
        $this->find="";
        $this->isfind=0;
        $this->array_resource = $array;
        $this->calculatePages(count($array));
    }
   // --- Calculate pages, startrecord 
   private function calculatePages($lengtharray) {
       // Take the length of the array
        $this->length = $lengtharray;

        // Get the number of pages
        $this->pages = ceil($this->length / $this->perPage);

        // Calculate the starting point
        $this->startrecord = ceil(($this->page - 1) * $this->perPage);

   } 
   
   // ---- Return how many records 
   public function getCount(){
       return $this->length;
   }
   // --- Return how many per page
   public function getPerPage() {
       return $this->perPage;
   }
   
   public function getMaxPerPage() {
       if($this->startrecord+$this->perPage < $this->length)
             return $this->perPage;
       else  return $this->length - $this->startrecord;
   }
   
    // --- Set current Page
    public function setCurrentpage($page) {
        // Assign the page variable
        if ($page <= $this->pages) {
            $this->page = $page; // using the get metho
            $this->startrecord = ceil(($this->page - 1) * $this->perPage);
            return true;
        } else
            return false;
    }

    // --- Return elements from page
    public function getDatapage() {
        // Return the part of the array we have requested
        if ($this->isfind==1)
            return array_slice($this->array_search, $this->startrecord, $this->perPage);
        else 
            return array_slice($this->array_resource, $this->startrecord, $this->perPage);
    }
    
    // --- Encapsulate find function
    public function find($find,$fields) {
        if(trim($find) == "") $this->unsetfind(); 
                         else  $this->setfind($find,$fields);        
    }
         
    public function cancelfind(){
        $this->unsetfind();
    }
    
   // --- Set find string
    private function setfind($find,$fields) {
        $this->find=  utf8_decode($find."");
        $this->array_search=array();
        
        $count=count($this->array_resource);
        for($i=0;$i<$count;$i++){
            $srch=$this->array_resource[$i];
            $count_srch=count($srch);
            
            foreach($srch as $key=>$values){
                if(in_array($key,$fields))
                  if(stristr($values,$this->find)!== FALSE) {
                        $this->array_search=array_merge(array($this->array_resource[$i]),$this->array_search);
                        break;
                }
            }
        }
        $this->isfind=1;
        $this->calculatePages(count($this->array_search));
    }
    
    //--- Unset find
    private function unsetfind() {
        $this->array_search = array();
        $this->isfind=0;
        $this->find="";
        $this->calculatePages(count($this->array_resource));
    }
    // ---- Obtiene la lista de links 
    public function getLinks($funcname = "pagination", $paramsarray=null) 
    {
        // Initiate the links array
        $plinks = array();
        $links = array();
        $slinks = array();

        if($paramsarray) $params=",".$paramsarray;
                   else  $params="";
        // If we have more then one pages
        if (($this->pages) > 1) {
            //------- Assign the 'previous page' link into the array if we are not on the first page
            if ($this->page != 1) {
                if ($this->showFirstAndLast) {
                    $plinks[] = ' <a href="#" onclick="' . $funcname . '(1'.$params.'); return true;">&laquo;&laquo; Primero</a> ';
                }
                $plinks[] = ' <a href="#" onclick="' . $funcname . '(' . ($this->page - 1).$params.'); return true;">&laquo; Anterior</a> ';
            }


            //----- Assign all the page numbers & links to the array

            for ($j = 1; $j < ($this->pages + 1); $j++) {
                if ($this->page == $j) {
                    $links[] = ' <span class="selected_page">' . $j . '</span> '; // If we are on the same page as the current item
                } else {
                    $links[] = ' <a href="#" onclick="' . $funcname . '(' . $j .$params.');return true;">' . $j . '</a> '; // add the link to the array
                }
            }

            //------ Assign the 'next page' if we are not on the last page
            if ($this->page < $this->pages) {
                $slinks[] = ' <a href="#" onclick="'.$funcname . '(' . ($this->page + 1).$params.');return true;"> Siguiente &raquo; </a> ';
                if ($this->showFirstAndLast) {
                    $slinks[] = ' <a href="#" onclick="' . $funcname . '(' . ($this->pages).$params.');return true;"> Ultimo &raquo;&raquo; </a> ';
                }
            }
            // Push the array into a string using any some glue
            return implode($plinks) . implode($links) . implode($slinks);
        } 
        return "";
    }
}

?>