<html>
   <script type="text/javascript" src="../publico/js/jquery-1.7.2.js"></script>
   <script type="text/javascript" src="../aplicacion/cliente/sesion.js"></script><!--*-->
   <script type="text/javascript" src="../publico/js/Archivos/funciones.js"></script><!--*-->
   <script type="text/javascript" src="../publico/js/jquery-ui.js"></script>
   <script type="text/javascript" src="../publico/js/jquery.ui-1.10.3.datepicker-es.js"></script>
   <link   rel="stylesheet"   type="text/css"  href="../publico/js/Archivos/tools.css">
   <link   rel="stylesheet"   type="text/css"  href="../publico/js/Archivos/estilos.css">
   <link   rel="stylesheet"       type="text/css" href="../publico/estilos/jquery-ui-1.10.3.custom.min.css" >
   <link   rel="stylesheet"       type="text/css" href="../publico/js/Archivos/styleestancia.css" ><!--*-->
   <script type="text/javascript" src="../publico/js/prototype.js"></script>
   <script type="text/javascript" src="../aplicacion/cliente/personal_personal.js"></script>

   <link type="text/css"  href="../publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
   <script type="text/javascript" src="../publico/js/shadowbox/shadowbox_1.js"></script>
   <script type="text/javascript" src="../publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
   <script type="text/javascript">
      Shadowbox.init({
         language: "es",
         modal: true,
         players: ['html', 'php'],
         overlayColor: "#000",
         overlayOpacity: "0.5"});
   </script>
   <body  bgcolor="#ffffff" >
      <?php
      require_once("../aplicacion/configuracion/aut_lib.inc.php");

      $RecordUsuario["usuario_id"] = $_SESSION['usuario_id'];
      $RecordUsuario["usuario_nombre"] = $_SESSION['usuario_nombre'];
      $RecordUsuario["usuario_co_cargo_completo"] = $_SESSION['usuario_co_cargo_completo'];
      $RecordUsuario["usuario_co_cargo_completo_plan"] = $_SESSION['usuario_co_cargo_completo_plan'];
      $RecordUsuario["usuario_ubic_tecnica"] = $_SESSION['usuario_ubic_tecnica'];
      $RecordUsuario["usuario_grp_plan"] = $_SESSION['usuario_grp_plan'];
      $RecordUsuario["usuario_pto_plan"] = $_SESSION['usuario_pto_plan'];
      $RecordUsuario["usuario_cod_cargos"] = $_SESSION['usuario_cod_cargos'];
      $RecordUsuario["usuario_disponible"] = $_SESSION['usuario_disponible'];
      $RecordUsuario["usuario_planificador"] = $_SESSION['usuario_planificador'];
      $RecordUsuario["usuario_administrador"] = $_SESSION['usuario_administrador'];
      $RecordUsuario["usuario_ubic_administrador"] = $_SESSION['usuario_ubic_administrador'];

      $desactivarGuardar = false;
      $msg = null;
      $localerror = null;
      $plan = new plan();
      $TipoObjTec = array(2 => 'E', 3 => 'I');
      $ObjetoTecnicoSAP = new inventarioSAP();

      /*       * **************************************************************************************************************** */
      /*       * *************************** ADECUACION DE VARIABLES FECHA SEGUN TIPO DE MANTENIMIENTO ************************** */
      /*       * **************************************************************************************************************** */
      if (isset($_POST['hojaRuta']) && isset($_POST['fecha'])) {
         list($coGrupoHojaRuta, $coNivelHojaRuta, $descHojaRuta, $cantPersonal) = explode("@", $_POST['hojaRuta']);
         list($dia, $mes, $ano) = explode("/", $_POST['fecha']);

         //COMO REGLA DE NEGOCIO SE INSERTA EL CODIGO GRUPO HOJA DE RUTA Y EL NUMERO, SEPARADO POR UN '-', PARA IDENTIFIAR LA HOJADE RUA EN LA BD
         $coHojaRuta = $coGrupoHojaRuta . '-' . $coNivelHojaRuta;

         switch ($_POST['tipoIngresoFecha']) {
            case "1" : /* Ingresa el Ultimo Mantenimiento */
               if ($TipoObjTec[$_GET["objeto_tecnico"]] == 'E')
                  $arrayHojaRuta = $ObjetoTecnicoSAP->SelectHojaRutaEquipo($_GET['id']);
               else
                  $arrayHojaRuta = $ObjetoTecnicoSAP->SelectHojaRutaInstalacion($_GET['id']);

               $intervDiasMtto = $plan->getDiasFrecMtto($_GET['id'], $coGrupoHojaRuta, $coNivelHojaRuta, $arrayHojaRuta);
               if ($intervDiasMtto <= 0) {
                  $localerror = ERROR_SGE_SININTERVALO_PLAN;
                  break;
               }

               $fecha_mtto = new DateTime();
               $fecha_mtto->setDate($ano, $mes, $dia);
               $ns_mtto = $fecha_mtto->format('W-Y');
               $fecha_proximo = clone $fecha_mtto;
               $fecha_proximo->add(new DateInterval('P' . $intervDiasMtto . 'D'));
               $ns_proximo = $fecha_proximo->format('W-Y');

               break;
            case "2" : /* Ingreso el Proximo Mantenimiento */
               $fecha_mtto = null;
               $ns_mtto = "";

               $fecha_proximo = new DateTime();
               $fecha_proximo->setDate($ano, $mes, $dia);
               $ns_proximo = $fecha_proximo->format('W-Y');
               break;
         }

         switch ($_POST['tipo_asignacion']) {
            case 2: //ASIGNACION_ESPECIFICA:
               $asignaEspecifica = true;
               $estatusOrdenActividades = 2;/** Estado de Creado con Asignacion de Personal para Orden y Actividades* */
               $estatus = 2;
               break;
            case 1: //ASIGNACION_PROGRAMACION:
               $asignaEspecifica = false;
               $estatusOrdenActividades = 0;/** Estado creado sin Asignacion de Personal para Orden y Actividades* */
               $estatus = 0;
               break;
         }
      }
      /*       * ***************************************************************************************************************** */
      /*       * ************************************************** INSERTAR ***************************************************** */
      /*       * ***************************************************************************************************************** */
      if (isset($_GET['insert']) && $_GET['insert'] == "true") {
         if (!isset($localerror)) {
            // Obtiene los objetos para replicar los procedimientos del SGE (SAP)
            $va_detalle = $_GET['id'];
            $lstPersonas = (isset($_POST['chkperson'])) ? $_POST['chkperson'] : null;
            $arrayProcPlan = array();
            $j = 0;
            list($grupohr, $operacionhr) = explode("-", $coHojaRuta);
            if ($TipoObjTec[$_GET["objeto_tecnico"]] == 'E')
               $arrayProcedimiento = $ObjetoTecnicoSAP->SelectHojaRutaEquipo($va_detalle, $operacionhr);
            else
               $arrayProcedimiento = $ObjetoTecnicoSAP->SelectHojaRutaInstalacion($va_detalle, $operacionhr);

            //---- Texto de la Hoja de Ruta
            if (isset($arrayProcedimiento))
               $txHojaRuta = $arrayProcedimiento[0]['KTEXT'];
            //---- Procedimientos
            for ($i = 0; $i < count($arrayProcedimiento); $i++, $j++) {
               $arrayProcPlan[$j]["VA_PROCEDIMIENTO"] = $arrayProcedimiento[$i]['PLNNR'] . "-" . $arrayProcedimiento[$i]['PLNAL'] . "-" . $arrayProcedimiento[$i]['PLNKN'];
               $arrayProcPlan[$j]["NB_TEXTO"] = $arrayProcedimiento[$i]['LTXA1'];
               $arrayProcPlan[$j]["NU_POSICION"] = $i + 1;
               $arrayProcPlan[$j]["NU_NODO"] = $arrayProcedimiento[$i]['PLNKN'];
               $arrayProcPlan[$j]["ANZZL"] = $arrayProcedimiento[$i]['ANZZL'];
            }

            // REGISTRO DE LA ACTIVIDAD ASIGNADA DE HABERSE ESCOGIDO ASIGNACION ESPECIFICA
            if ($plan->InsertPlan($va_detalle, $coHojaRuta, $txHojaRuta, $_POST['tipo_asignacion'],
             /* Ultimo Mantenimiento */ ((isset($fecha_mtto)) ? $fecha_mtto->format("Y-m-d") : ""), $ns_mtto,
             /* Proximo Mantenimiento */ $fecha_proximo->format("Y-m-d"), $ns_proximo, $estatus, $TipoObjTec[$_GET["objeto_tecnico"]], 
//                                         $_POST['ubicTecnica'], $_POST['grupoPlan'], $_POST['grupoEjec'], '0', 
                                         $_POST['ubicTecnica'], $_POST['grupoPlan'], $_POST['grupoEjec'], $estatus, 
                                         $_POST['_cdc_requerido'], $_POST['_contr_requerido'], $_POST['_prop_requerido'], 
                                         $asignaEspecifica, $estatusOrdenActividades, $cantPersonal, $lstPersonas, $arrayProcPlan))
               $msg = MSG_DATOS_REGISTRADOS;
            else
               $msg = MSG_ERROR_TRANSACCION;
         } else
            $msg = sprintf("ERROR: No se puede actualizar. DETALLE:%s", $localerror);
      }
      /*       * *********************************************************************************************************** */
      /*       * ************************************************* EDITAR ************************************************** */
      /*       * *********************************************************************************************************** */

      if (isset($_GET['update_2']) && $_GET['update_2'] == "true") {
         if (!isset($localerror)) {
            if ($plan->UpdatePlan($_GET['id_plan'], $_POST['tipo_asignacion'], ((isset($fecha_mtto)) ? $fecha_mtto->format("Y-m-d") : ""), $ns_mtto, /* Ultimo Mantenimiento */ $fecha_proximo->format("Y-m-d"), $ns_proximo, /* Proximo Mantenimiento */ $estatus, $_GET['id'], $coHojaRuta, $cantPersonal, $_POST['chkperson'], $_POST['_contr_requerido'], $_POST['_prop_requerido'], $_POST['_cdc_requerido']))
               $msg = MSG_DATOS_EDITADOS;
            else
               $msg = MSG_ERROR_TRANSACCION;
         } else
            $msg = sprintf("ERROR: No se puede actualizar. DETALLE:%s", $localerror);
      }
      /*       * *********************************************************************************************************** */
      /*       * *********************************************** ELIMINAR ************************************************** */
      /*       * *********************************************************************************************************** */

      if (isset($_GET['delete']) && $_GET['delete'] == "true") {
         if ($plan->DeletePlan($_GET['id_plan']))
            $msg = MSG_DATOS_ELIMINADOS;
         else
            $msg = MSG_ERROR_TRANSACCION;
      }

      //*----------  MUESTRA EL ENCABEZADO --------------
      switch ($_GET["objeto_tecnico"]) {
         case 2:
            //OBTNER DATOS DEL EQUIPO Y SUS HOJAS DE RUTA ASOCIADAS
            $detalleObjetoTecnicoSAP = $ObjetoTecnicoSAP->SelecDetalleEqpSAP($_GET['id']);
            $arrHojaRuta = $plan->getHojaRutaEquipo($_GET['id'], $ObjetoTecnicoSAP);
            $arrHojaRutaCombo = $plan->ExcluirHojasRutas($_GET['id'], $arrHojaRuta);
            echo '
                <div class="titulomodulo">Planificaci&oacute;n Mantenimiento a Equipos</div>
                <div class="f10bold"><strong>Equipo: </strong> ' . strtoupper(utf8_encode($detalleObjetoTecnicoSAP[1]["EQKTU"])) . ' <strong>Marca: </strong> ' . strtoupper($detalleObjetoTecnicoSAP[1]["HERST"]) . ' <strong>Serial: </strong> ' . $detalleObjetoTecnicoSAP[1]["TIDNR"] . ' </div>
                <div class="f10bold"><strong>Tipo:  </strong> ' . strtoupper($detalleObjetoTecnicoSAP[1]["PLTXU"]) . '<strong> Ubicaci&oacute;n: </strong> ' . strtoupper($detalleObjetoTecnicoSAP[1]["TPLNR"]) . ' </div>';
            break;

         case 3:
            //OBTNER DATOS DE LA INSTALACION Y SUS HOJAS DE RUTA ASOCIADAS
            $detalleObjetoTecnicoSAP = $ObjetoTecnicoSAP->SelecUbicacionTecnica($_GET['id']);
            $arrHojaRuta = $plan->getHojaRutaInstalacion($_GET['id'], $ObjetoTecnicoSAP);
            $arrHojaRutaCombo = $plan->ExcluirHojasRutas($_GET['id'], $arrHojaRuta);
            echo '
                <div class="titulomodulo">Planificaci&oacute;n Mantenimiento a Instalaciones </div>        
                <div class="f10bold"><strong>Instalacion:</strong> ' . strtoupper(utf8_encode($detalleObjetoTecnicoSAP[1]["PLTXU"])) . ' </div>
                <div class="f10bold"><strong>Ubicaci&oacute;n T&eacute;nica:</strong> ' . strtoupper(utf8_encode($detalleObjetoTecnicoSAP[1]["TPLNR"])) . ' </div>';
            break;
      }
      if (empty($detalleObjetoTecnicoSAP[1]['INGRP']) || empty($detalleObjetoTecnicoSAP[1]['GEWRK'])) {
         $msg = "No se puede registrar planes al objeto t&eacute;cnico debido a que no tiene Grupo Planificador o Puesto de Trabajo, <br> dirigirse a los analista del SGE para verificar los datos.";
         $desactivarGuardar = true;
      }

      /*       * *************************************************************************************************************************************** */
      /*       * ***************************************************** FORMULARIO PARA INSERTAR ********************************************************** */
      /*       * *************************************************************************************************************************************** */
      if (!isset($_GET['update']) || $_GET['update'] != "true") {
         ?>
         <form method="POST" name="formulario" id="formulario" action="plan.php?insert=true&id=<?php echo $_GET['id']; ?>&objeto_tecnico=<?php echo $_GET["objeto_tecnico"]; ?>" >
         <?php
         //  Datos Escondidos necesarios para la insersion del plan
         echo '<input type = "hidden" name = "ubicTecnica" id = "ubicTecnica" value   = "' . $detalleObjetoTecnicoSAP[1]['TPLNR'] . '">';
         echo '<input type = "hidden" name = "grupoPlan"   id = "grupoPlan"   value   = "' . $detalleObjetoTecnicoSAP[1]['INGRP'] . '">';
         echo '<input type = "hidden" name = "grupoEjec"   id = "grupoEjec"   value   = "' . $detalleObjetoTecnicoSAP[1]['GEWRK'] . '">';
         $puestoTrabObjTecSAP = "'{$detalleObjetoTecnicoSAP[1]['GEWRK']}'";

         ?>
            <table class="tablaformulario">
               <tr>
                  <td><span class="etiqueta">Nivel de Mantenimiento:</span></td>
                  <td><span class="etiqueta">Tipo de Asignaci&oacute;n:</span></td>
                  <td><span class="etiqueta">
                        <input type="radio" name="tipoIngresoFecha" value="1" title="Calcular el Proximo Mantenimiento según la informaic&oacute;n de SGE">Ultimo Mantenimiento:<br>
                        <input type="radio" name="tipoIngresoFecha" value="2" title="Asigna la fecha del Próximo Mantenimeinto" checked>Proximo Mantenimiento:
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>
                     <select name="hojaRuta" class=":required texto" id="id_nivel" onchange="return validarTipoAsignacion(this.value);">
                        <option value = '0'>Seleccione una opci&oacute;n...</option>
   <?php
   for ($i = 0; $i < count($arrHojaRutaCombo); $i++) {
      echo "<option value='" . $arrHojaRutaCombo[$i]['PLNNR'] . "@" . $arrHojaRutaCombo[$i]['PLNAL'] . "@" . $arrHojaRutaCombo[$i]['KTEXT'] . "@" . $arrHojaRutaCombo[$i]['ANZZL'] . "@" . $arrHojaRutaCombo[$i]['MAX_ANZZL'] . "'>"
      . $arrHojaRutaCombo[$i]['KTEXT'] .
      "</option>";
   }
   ?>
                     </select>
                  </td>
                  <td>
                        <?php
                        echo '<input type="hidden" id="id_asignacion_valor" name="tipo_asignacion_valor" value="0">',
                        '<select name="tipo_asignacion" class=":required texto" id="id_asignacion" onchange="return cargaPersonal(\'' . $_SESSION['usuario_co_cargo_completo'] . '\',\'' . $RecordUsuario["usuario_id"] . '\',\'' . urlencode($puestoTrabObjTecSAP) . '\',this.value,0);">
                                        <option value = 0>Seleccione una opci&oacute;n...</option>
                                        <option value = 1>POR DEFINIR</option> 
                                        <option value = 2>DIRECTA</option> 
                                  </select>';
                        ?>
                  </td>
                  <td>
                     <input type="text" id="datepicker" class=":required texto"  name="fecha" readonly />
                  </td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">
                     <div id="personalDisp" ></div>
                  </td>
               </tr>
               <tr>
                  <td colspan="3">
                     <input  name="_cdc_requerido" id="_in_requerido" type="checkbox" value="1"><span class="etiqueta">Requiere Control de Cambio</span><br>
                  </td>
               </tr>
               <tr>
                  <td colspan="3">
                     <input  name="_contr_requerido" id="_in_requerido" type="checkbox" value="1"><span class="etiqueta">Requiere Contrato de Servicios u Obras</span><br>
                  </td>
               </tr>
               <tr>
                  <td colspan="3">
                     <input  name="_prop_requerido" id="_in_requerido" type="checkbox" value="1" checked="checked"><span class="etiqueta">Se ejecutar&aacute; con esfuerzo propio</span><br>
                  </td>
               </tr>

            </table>

            <div class="grupobotones">
               <input class="submit boton"  value="Guardar" type="button" onClick ="return validaCampos()" <?php if ($desactivarGuardar) echo "disabled='disabled'"; ?> >
               <input class="boton"  value="Limpiar" type="reset" >
            </div>
   <?php if (strlen($msg) > 0) echo " <span style=\"color:#f00;font-size:x-small;\">**$msg**</span>"; ?>
         </form>
   <?php
}
/* * *************************************************************************************************************************************** */
/* * **********************************************************  EDITAR  ********************************************************************** */
/* * ***************************************************************************************************************************************** */
if (isset($_GET['update']) && $_GET['update'] == "true") {
   
   $rsDatosPlan = $plan->getSumarioPlan($_GET['id_plan']);

   $nivel = $plan->getDetalletHojaRuta($rsDatosPlan->fields['CO_GRUPO_HOJARUTA'], $rsDatosPlan->fields['NU_HOJARUTA'], $arrHojaRuta);
   $detalleHojaRuta = $rsDatosPlan->fields['CO_GRUPO_HOJARUTA'] . "@" . $rsDatosPlan->fields['NU_HOJARUTA'] . "@" . $nivel['KTEXT'] . "@" . $nivel['ANZZL'] . "@" . $arrHojaRuta[1]['MAX_ANZZL'];
   ?>

         <form method="POST" name="formulario" id="formulario" action="plan.php?update_2=true&id=<?php echo $_GET['id']; ?>&id_plan=<?php echo $_GET['id_plan']; ?>&objeto_tecnico=<?php echo $_GET["objeto_tecnico"]; ?>">
         <?php
         //  Datos Escondidos necesarios para la edicion del plan
         echo '<input type = "hidden" name = "ubicTecnica" id = "ubicTecnica" value   = "' . $detalleObjetoTecnicoSAP[1]['TPLNR'] . '">';
         echo '<input type = "hidden" name = "grupoPlan"   id = "grupoPlan"   value   = "' . $detalleObjetoTecnicoSAP[1]['INGRP'] . '">';
         echo '<input type = "hidden" name = "grupoEjec"   id = "grupoEjec"   value   = "' . $detalleObjetoTecnicoSAP[1]['GEWRK'] . '">';
         echo '<input type = "hidden" name = "hojaRuta"    id = "id_nivel"    value   = "' . $detalleHojaRuta . '">';
         $puestoTrabObjTecSAP = "'{$detalleObjetoTecnicoSAP[1]['GEWRK']}'";
         ?>
            <table class="tablaformulario">
               <tr>
                  <td><span class="etiqueta">Nivel de Mantenimiento:</span></td>
                  <td><span class="etiqueta">Tipo de Asignaci&oacute;n:</span></td>
                  <td><span class="etiqueta">
                        <input type="radio" name="tipoIngresoFecha" value="1" title="Calcular el Proximo Mantenimiento según la informaic&oacute;n de SGE">Ultimo Mantenimiento:<br>
                        <input type="radio" name="tipoIngresoFecha" value="2" title="Asigna la fecha del Próximo Mantenimeinto" checked>Próximo Mantenimiento:
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>
   <?php echo '<input type="text" id="tx_nivel" value="' . $nivel['KTEXT'] . '" class="texto"  name="tx_nivel" readonly />'; ?>
                  </td>
                  <td>
   <?php
   echo '<input type="hidden" id="id_asignacion_valor" name="tipo_asignacion_valor" value="">',
   '<select name="tipo_asignacion" class=":required texto" id="id_asignacion" onchange="return cargaPersonal(\'', $_SESSION['usuario_co_cargo_completo'], '\',\'', $RecordUsuario["usuario_id"], '\',\'', urlencode($puestoTrabObjTecSAP), '\',this.value,\'', $_GET['id_plan'], '\');">',
   '<option value="1" ', (($rsDatosPlan->fields['CO_TIPO_ASIGNACION'] == ASIGNACION_PROGRAMACION) ? "selected" : ""), '>POR DEFINIR</option>',
   '<option value="2" ', (($rsDatosPlan->fields['CO_TIPO_ASIGNACION'] == ASIGNACION_ESPECIFICA) ? "selected" : ""), '>DIRECTA</option></select>';
   ?>
                  </td>
                  <td>
                     <?php echo '<input type="text" id="datepicker" class=":required texto" name="fecha" value="', date_format(new DateTime($rsDatosPlan->fields['FE_PROXIMA_PLAN']), "d/m/Y"), '" readonly />'; ?>
                  </td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">
                     <div id="personalDisp" ></div>
                  </td>
               </tr>
   <?php
   echo '
                    <tr>
                        <td colspan="3">
                            <input  name="_cdc_requerido" id="_in_requerido" type="checkbox" value="1" ', (($rsDatosPlan->fields['IN_CDC']) ? 'checked="checked"' : ""), '><span class="etiqueta">Requiere Control de Cambio</span><br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input  name="_contr_requerido" id="_in_requerido" type="checkbox" value="1"', (($rsDatosPlan->fields['IN_TERCERO']) ? 'checked="checked"' : ""), '><span class="etiqueta">Requiere Contrato de Servicios u Obras</span><br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input  name="_prop_requerido" id="_in_requerido" type="checkbox" value="1"', (($rsDatosPlan->fields['IN_ESFUER_PROPIO']) ? 'checked="checked"' : ""), ' ><span class="etiqueta">Se ejecutar&aacute; con esfuerzo propio</span><br>
                        </td>
                    </tr>';
   ?>
            </table>

            <div id="id_usuario"></div>
            <div class="grupobotones">
               <input class="submit boton"  value="Guardar" type="button" onClick ="return validaCampos()"  <?php if ($desactivarGuardar) echo "disabled='disabled'"; ?> >
               <input class="boton"  value="Cancelar" type="button" onClick="return cancelarCampos()" >
            </div>
         </form>
               <?php
            }
            /*             * ****************************************************** Registros Cargados ************************************************************* */
            /*             * *************************************************************************************************************************************** */
            /*             * *************************************************************************************************************************************** */
            /*             * *************************************************************************************************************************************** */
            ?>
      <div class="titulodivision">Registros Cargados:</div>
      <table id="listaSaldos" class="tabla">
         <thead>
            <tr>
               <th scope="col">Nivel de mantenimiento</th>
               <th scope="col">Tipo de asignaci&oacute;n</th>
               <th scope="col">Ultimo mtto</th>
               <th scope="col">Pr&oacute;ximo mtto</th>
               <th scope="col"></th>
            </tr>
         </thead>
         <tbody>
<?php
$rsPlanActivo = $plan->getListaPlanActivo($_GET['id']);
while (!$rsPlanActivo->EOF) {
   $textoHojaRuta = $plan->getKtextHojaRuta($rsPlanActivo->fields['CO_GRUPO_HOJARUTA'], $rsPlanActivo->fields['NU_HOJARUTA'], $arrHojaRuta);
   echo "<tr>",
   "<td>", $textoHojaRuta, "</td>",
   "<td>";

   switch ($rsPlanActivo->fields['CO_TIPO_ASIGNACION']) {
      case ASIGNACION_PROGRAMACION:
         echo 'POR DEFINIR';
         break;
      case ASIGNACION_ESPECIFICA:
         echo 'DIRECTA';
         break;
   }

   echo "</td><td align=\"center\">", (($rsPlanActivo->fields['FE_ULTIMO'] === '00-00-0000') ? "&nbsp;" : $rsPlanActivo->fields['FE_ULTIMO']),
   "</td><td align=\"center\">", $rsPlanActivo->fields["FE_PROXIMA_PLAN"], "</td> <td>";

   // href QUE PERMITE ENVIAR id DEL ELEMENTO SELECIONADO y update="true" PARA ENTRAR EL LA RUTINA DE EDICI�N -->

   if ($rsPlanActivo->fields['TOT_ORDEN_ACTIVAS'] > 0 && $rsPlanActivo->fields['CO_TIPO_ASIGNACION'] == 1) {
      echo "<span style=\"color:#f00;font-size:x-small;font-style:italic\">(Plan con Ordenes Activas)</span>";
      //            } elseif($rsPlanActivo->fields['TOT_HISTORICO'] > 1 && in_array(substr($textoHojaRuta,0,3),explode(",",NIVEL_PLAN_CICLO))) {
      //                    echo "<span style=\"color:#f00;font-size:x-small;font-style:italic\">(Plan ciclico con historicos)</span>";
   } elseif ($rsPlanActivo->fields['TOT_EJECUCION'] > 0 && $rsPlanActivo->fields['CO_TIPO_ASIGNACION'] == 2) {
      echo "<span style=\"color:#f00;font-size:x-small;font-style:italic\">(Plan con Ejecuc&oacute;n de Actividades)</span>";
   } else
      echo "<a href='plan.php?update=true&id=", $_GET["id"], "&id_plan=", $rsPlanActivo->fields["CO_PLAN"], "&objeto_tecnico=", $_GET["objeto_tecnico"], "'>Editar</a>",
      "/ <a href='plan.php?delete=true&id=", $_GET["id"], "&id_plan=", $rsPlanActivo->fields["CO_PLAN"], "&objeto_tecnico=", $_GET["objeto_tecnico"], "' onclick=\"return confirm('Esta seguro de eliminar el Plan');\">Eliminar</a>";

   echo "<br><span style=\"align:center;horizontal-align:center;\"><a class=\"enlaceblanco\" onClick=\"return OpenShadow('plan_registro.php?id_plan=", $rsPlanActivo->fields["CO_PLAN"], "');\">**Notas**</a></span>";
   echo "</td>",
   "</tr>";
   $rsPlanActivo->MoveNext();
}
?>
         </tbody>
      </table>
      <script type="text/javascript">
         var jq = jQuery.noConflict();
         jq(function() {
            jq("#datepicker").datepicker();
         });
         jq(document).ready(function() {
            jq("#id_asignacion").change();
         });

         /**
          *FUNCION QUE GENERA UN AJAX PARA EL ENVIO DE DATOS PARA LA CARGA DE PERSONAL A ASIGNAR
          **/

         function validarTipoAsignacion(idNivel) {
            if (idNivel == 0) {
               jq("#id_asignacion").val(0);
               jq("#personalDisp").html("");
            } else
               jq("#id_asignacion").val(1);
            return true;
         }

         function cargaPersonal(cargoCompleto, usuario_id, pto_plan, tipoAsignacion, idPlan)
         {
            if (jq("id_nivel").value == 0 && tipoAsignacion != 0) {
               alert("Debe Seleccionar un Nivel de Mantenimiento");
               jq("#id_asignacion").val(0);
               return false;
            }
            if (tipoAsignacion == 2)
            {
               var tmp = jq("#id_nivel").val();
               var nuPersonas = tmp.split("@");
               var pars1 = 'co_cargo_completo=' + cargoCompleto + '&nu_personas=' + nuPersonas[3] + '&nu_max_personas=' + nuPersonas[4] + '&id_plan=' + idPlan + '&co_usuario=' + usuario_id + '&pto_plan=' + pto_plan;

               new Ajax.Updater("personalDisp", "../aplicacion/servidor/_ajax/controlador.planificacion.jquery.php",
                       {
                          method: 'get',
                          parameters: pars1,
                          asynchronous: false,
                          evalScripts: false,
                          onComplete: function(request) {
                             jq("#personalDisp").show();
                          }

                       });
            }
            else
            {
               jq("#personalDisp").innerHTML = "";
               jq("#personalDisp").hide();
            }
            return true;
         }

         function cancelarCampos() {
//                document.formulario.action="cpanel.php?sistema=2100&id=<?php echo $_GET['id']; ?>&objeto_tecnico=<?php echo $_GET["objeto_tecnico"]; ?>";
            document.formulario.action = "plan.php?id=<?php echo $_GET['id']; ?>&objeto_tecnico=<?php echo $_GET["objeto_tecnico"]; ?>";
            document.formulario.submit();

         }

         function validaCampos()
         {
            var seleccionado = 0;
            var chks = document.getElementsByName('chkperson[]');
            var fecha = jq("#datepicker").datepicker('getDate');
            var tmp = jq("#id_nivel").val();
            var nuPersonas = tmp.split("@");
            var ok = true;

            if ($("id_asignacion").value == 2)
            {
               for (i = 0; i < chks.length; i++)
               {
                  if (chks[i].checked)
                     seleccionado++;
               }

               if (seleccionado > nuPersonas[4])
               {
                  alert('El trabajo requiere un maximo de  ' + nuPersonas[4] + ' Personas, ha seleccionado  ' + seleccionado + ', rectifique y elimine el exceso');
                  ok = false;
               } else if (seleccionado < nuPersonas[3])
               {
                  alert('El trabajo requiere al menos ' + nuPersonas[3] + ' Personas, ha seleccionado  ' + seleccionado + ', por favor complete el faltante');
                  ok = false;
               }
            }
            if (fecha == null)
            {
               alert('El campo fecha es obligatorio');
               ok = false;
            } else if (tmp == 0)
            {
               alert('El campo Nivel de Mantenimiento es obligatorio');
               ok = false;
            } else if ($("id_asignacion").value == 0)
            {
               alert('El campo Tipo de Asignacion es obligatorio');
               ok = false;
            }

            if (ok)
               document.formulario.submit();
            else
               return false;
         }

         function OpenShadow(sPath, pHeight, pWidth) {
            if (typeof (pHeight) === 'undefined')
               pHeight = 401;
            if (typeof (pWidth) === 'undefined')
               pWidth = 551;

            Shadowbox.open({
               content: sPath,
               player: "iframe",
               height: pHeight,
               width: pWidth
            });
         }
      </script>
   </body>
</html>
