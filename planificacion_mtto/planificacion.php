<?php
$objeto_tecnico = (isset($_REQUEST["objeto_tecnico"])) ? $_REQUEST["objeto_tecnico"] : 0;
$sMenuLink=CodificarGet("sistema=2000");
?>
<script type="text/javascript" src="aplicacion/cliente/html_tablas.js"></script>
<link type="text/css"  href="publico/js/shadowbox/shadowbox_1.css" rel="stylesheet"/>
<script type="text/javascript" src="publico/js/shadowbox/shadowbox_1.js"></script>
<script type="text/javascript" src="publico/js/shadowbox/adapter/shadowbox-prototype.js"></script>
<script type="text/javascript"> 
   Shadowbox.init({
      language: "es",
      modal:true,      
      players:  ['img', 'html','php'],
      overlayColor: "#000",
      overlayOpacity: "0.5"});
</script>
<div class="titulosubmodulo">Planificaci&oacute;n del Mantenimiento</div>
<form method="POST" action="cpanel.php?k=<?php echo $sMenuLink?>" name="selecciona_consulta">
   <table >
      <tr>
         <td><span class="etiqueta">Objeto t&eacute;cnico a mantener</span></td>
      </tr>
      <tr>
         <td>
            <select name='objeto_tecnico' class=':required texto' onchange='submit();'>
               <!-- ESTE option IMPRIME EL VALOR A EDITAR -->

               <option value='0' <?php if ($objeto_tecnico == 0) echo "selected" ?> >Seleccione una opci&oacute;n...</option>
<!--                    <option value='1' <?php // if ($objeto_tecnico == 1) echo "selected"  ?> >APLICACI&Oacute;N</option>-->
               <option value='2' <?php if ($objeto_tecnico == 2) echo "selected" ?> >EQUIPO</option>
               <option value='3' <?php if ($objeto_tecnico == 3) echo "selected" ?> >INSTALACI&Oacute;N O SISTEMA</option>
            </select>
         </td>
      </tr>
   </table>
</form>

<?php
//************************** IMPRIME LA TABLA SI SON EQUIPOS
//if ($tipo_equipo > 0) {
if ($objeto_tecnico > 0) {
   ?>
   <div class="titulodivision">Registros Cargados:</div>
   <form method="POST" action="cpanel.php?k=<?php echo $sMenuLink?>" name="planificar" id="planificar">
      <input type="hidden" id="page" name="page" value="" />
      <table id="texto_informativo">
         <tr>
            <td>
               <input type="text" class="texto" name="search"   value="" id="search" />
               <!--<input class="boton"  id="cmdbuscar" name="cmdbuscar" value="Buscar" type="button" onclick="Buscar(<?php //echo ($tipo_equipo.",'".session_id()."'");  ?>,$('search').value);">-->
               <input class="boton"  id="cmdbuscar" name="cmdbuscar" value="Buscar" type="button" onclick="Buscar(<?php echo ($objeto_tecnico . ",'" . session_id() . "'"); ?>,$('search').value);">
               <input class="boton"  id="cmdLimpiar" name="cmdLimpiar" value="Limpiar" type="button" onclick="Buscar(<?php echo ($objeto_tecnico . ",'" . session_id() . "'"); ?>,'');$('search').value='';">
            </td>
            <td><div id="loading" class="etiqueta"> </div></td>
            <!-- IMAGEN QUE SOLO PERMITE  width=25% height=25% REFERENCIAR UNA CARGA PARA IMPRIMIR LA TABLA ANTERIOR -->
         </tr>
      </table>
   </form>
   <div id="TablaDatos" class="resultado_planificacion"> </div>

   <script type="text/javascript">
      var jq=jQuery.noConflict();
      jq('#search').keydown(function(e){
         if (e.keyCode == 13) {
            e.preventDefault();
            jq("#cmdbuscar").click();
         }
      });

      function Buscar(opcion,sesion,texto) {
         switch(opcion) {
            case 2: MostrarTablaEquipos(1,'TablaDatos',sesion,texto,1);
               break;
            case 3: MostrarTablaEstacion(1,'TablaDatos',sesion,texto,1);
               break;

         }
      }
      function OpenShadow(sPath,pHeight,pWidth) {
         if(typeof(pHeight)==='undefined') pHeight=501;
         if(typeof(pWidth)==='undefined') pWidth= 751;

         Shadowbox.open({
            content: sPath,
            player: "iframe",
            height: pHeight,
            width: pWidth
         });
      }
   <?php echo "Buscar($objeto_tecnico,'" . session_id() . "','');"; ?>
   </script>

   <?php }
?>
