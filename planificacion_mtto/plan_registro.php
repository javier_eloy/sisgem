<?php
include_once("../aplicacion/configuracion/aut_lib.inc.php");
$co_plan = $_GET['id_plan'];
$objPlanificar = new plan();


/* @var $_POST type */
if (isset($_POST["Guardar"])) {
   $tx_registro = filter_input(INPUT_POST,'tx_registro',FILTER_SANITIZE_STRING,FILTER_FLAG_ENCODE_HIGH+FILTER_FLAG_STRIP_LOW);
   if (!empty($tx_registro)) 
      $objPlanificar->insRegistroObs($co_plan, $tx_registro);
   else
      echo " <span style=\"color:#f00;font-size:x-small;\">**No se permite almacenar textos vacios**</span>";
}

$rsRegistroObs = $objPlanificar->getRegistroObs($co_plan);

?>
<!--Cuerpo-->
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/tools.css">
<link type="text/css" rel="stylesheet" href="../publico/js/Archivos/estilos.css">
<link  type="text/css" rel="stylesheet"  href="../publico/js/Archivos/styleestancia.css" >
<script type="text/javascript" src="../publico/js/jquery-1.7.2.js"></script>

<body  bgcolor="#E9E9E9" >
   <form method="POST" name="formulario" id="formulario" action="plan_registro.php?id_plan=<?php echo $co_plan;?>">
      <span class="f10bold">
      <strong>Observaciones para
      <?php
       $dt = new DateTime();
       $dt->setTimeZone(new DateTimeZone('America/Caracas'));
        echo str_replace(' De ', ' de ', ucwords(strftime("%A, %d de %B de %Y")));
        
      ?>
      </strong>
      </span>
      <textarea class="observacion" id="tx_registro" name="tx_registro" cols="100" rows="3" maxlength="300" required autofocus></textarea>
      <input class="boton" name="Guardar" value="Guardar" type="submit"/>
      <input class="boton" name="Cerrar" value="Cerrar" type="button" onclick="window.parent.Shadowbox.close();"/>
   </form>
   <hr>
   <H1>Historico:</H1>
   <?php
    while (!$rsRegistroObs->EOF){
       $dFeRegistro=new DateTime($rsRegistroObs->fields["FE_REGISTRO"]);
       echo "<p>-",str_pad($dFeRegistro->format(" l, j \d\e F \d\e Y h:i a "),110,"-"),"<br/>";
       echo wordwrap(htmlspecialchars_decode($rsRegistroObs->fields["TX_REGISTRO"]),90,"<br/>");
       echo "</p><br/>";
       
       $rsRegistroObs->MoveNext();
    }
   ?>

</body>